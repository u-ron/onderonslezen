<?php

class LoginAttempt extends \Base
{
	protected $id;
	protected $idMember;
	protected $time;
    protected $insertedBy;

	public function getId()
	{
		return $this->id;
	}

	public function getIdMember()
	{
		return $this->idMember;
	}

	public function getTime()
	{
		return $this->time;
	}

    public function getInsertedBy()
    {
        return $this->insertedBy;
    }


	public function setId($value)
	{
		if (is_numeric($value))
        {
            $this->id = $value;
            return TRUE;
        }
        else
		{
			return FALSE;
		}
	}

	public function setIdMember($value)
	{
        if (is_numeric($value))
        {
            $this->idMember = $value;
            return TRUE;
        }
        else
		{
			return FALSE;
		}
	}

	public function setTime($value)
	{
		if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->time=$value;
            return TRUE;
        }
	}

    public function setInsertedBy($value)
    {
       if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->insertedBy=$value;
            return TRUE;
        }
    }

    /*vergt het gebruik vd methode setIdMember en setInsertedBy*/
    public function insert()
	{
		$result = FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
		if($this->connect())
		{
			try
			{
                $preparedStatement = $this->pdo->prepare('CALL LoginAttemptInsert(@pId, :pIdMember, :pTime, :pInsertedBy)');
				$preparedStatement->bindParam(':pIdMember', $this->idMember, \PDO::PARAM_INT);
                $preparedStatement->bindParam(':pTime', $this->time, \PDO::PARAM_STR);
				$preparedStatement->bindParam(':pInsertedBy', $this->insertedBy, \PDO::PARAM_STR);
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					$this->setId($this->pdo->query('select @pId')->fetchColumn());
					$this->feedback = 'Rij met id <b> ' . $this->getId() . '</b> is geïnserted.';
					$result = TRUE;
				}
				else
				{
					$this->feedback = 'Fout in stored procedure LoginAttemptInsert(). Rij is niet geïnserted.';
					$sQLErrorInfo = $preparedStatement->errorInfo();
					$this->errorCode = $sQLErrorInfo[0];
					$this->errorCodeDriver = $sQLErrorInfo[1];
					$this->errorMessage = $sQLErrorInfo[2];
				}
			}
			catch (\PDOException $e)
			{
				$this->feedback = 'Rij is niet toegevoegd.';
				$this->errorCode = $sQLErrorInfo[0];
				$this->errorCodeDriver = $sQLErrorInfo[1];
				$this->errorMessage = $sQLErrorInfo[2];
			}
             $this->close();
		}
		return $result;
	}

    /*vergt het gebruik vd methode setTime en setIdMember*/
    /*retourneert 1dim array*/
    public function CountIdMemberByTime()
    {
		$result = FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
		if($this->connect())
		{
			try
			{
				$preparedStatement = $this->pdo->prepare("CALL LoginAttemptCountIdMemberByTime(:pIdMember, :pTime)");
                $preparedStatement->bindParam(':pIdMember', $this->idMember, \PDO::PARAM_STR);
				$preparedStatement->bindParam(':pTime', $this->time, \PDO::PARAM_STR);
				$preparedStatement->execute();
				$result = $preparedStatement->fetch();
				if ($result)
				{
					$this->feedback = "Rij(en) met tijd >= $this->time in tabel LoginAttempt gevonden.";
				}
				else
				{
					$this->feedback = "Tabel LoginAttempt is leeg.";
					$this->errorCode = $sQLErrorInfo[0];
					$this->errorCodeDriver = $sQLErrorInfo[1];
					$this->errorMessage = $sQLErrorInfo[2];
				}
			}
			catch (\PDOException $ex)
			{
				$this->feedback = 'Geen rijen is niet gevonden.';
				$this->errorMessage = $ex->getMessage();
				$this->errorCodeDriver = $ex->getCodeDriver();
				$this->errorCode = $ex->getCode();
			}
           $this->close();
		}
		return $result;
    }
}

?>



