<?php

class Sessie extends \Base
{
	protected $id;
	protected $sessionId;
	protected $lidId;
    protected $lastActivity;
    protected $modifiedBy;

	public function getId()
	{
		return $this->id;
	}

	public function getSessionId()
	{
		return $this->sessionId;
	}

	public function getLidId()
	{
		return $this->lidId;
	}

    public function getLastActivity()
	{
		return $this->lastActivity;
	}

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }


	public function setId($value)
	{
		if (is_numeric($value))
        {
            $this->id = $value;
            return TRUE;
        }
        else
		{
			return FALSE;
		}
	}

	public function setLidId($value)
	{
        if (is_numeric($value))
        {
            $this->lidId = $value;
            return TRUE;
        }
        else
		{
			return FALSE;
		}
	}

    public function setSessionId($value)
	{
        if (!empty($value))
        {
            $this->sessionId = $value;
            return TRUE;
        }
        else
		{
			return FALSE;
		}
	}

	public function setLastActivity($value)
	{
		if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->lastActivity=$value;
            return TRUE;
        }
	}

    public function setModifiedBy($value)
    {
       if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->modifiedBy=$value;
            return TRUE;
        }
    }

    /*vergt het gebruik van alle set methodes*/
    public function update()
	{
		$result = FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
		if($this->connect())
		{
			try
			{
                $preparedStatement = $this->pdo->prepare('CALL SessieUpdate(:pId, :pSessionId, :pLidId, :pLastActivity, :pModifiedBy)');
                $preparedStatement->bindParam(':pId', $this->id, \PDO::PARAM_INT);
                $preparedStatement->bindParam(':pSessionId', $this->sessionId, \PDO::PARAM_STR);
				$preparedStatement->bindParam(':pLidId', $this->lidId, \PDO::PARAM_INT);
                $preparedStatement->bindParam(':pLastActivity', $this->lastActivity, \PDO::PARAM_STR);
				$preparedStatement->bindParam(':pModifiedBy', $this->modifiedBy, \PDO::PARAM_STR);
				$preparedStatement->execute();
				$result = $preparedStatement->rowCount();
                if($result)
                {
                    $this->feedback =  "Sessie {$this->id} is gewijzigd.";
                    $result = TRUE;
                }
                else
                {
                    $this->feedback = "Sessie {$this->id} is niet gevonden en dus niet gewijzigd.";
                    $sQLErrorInfo = $preparedStatement->errorInfo();
                    $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                    $this->errorMessage = $sQLErrorInfo[2];
                }
			}
			catch (\PDOException $e)
			{
				$this->feedback = 'Rij is niet toegevoegd.';
				$this->errorCode = $sQLErrorInfo[0];
				$this->errorCodeDriver = $sQLErrorInfo[1];
				$this->errorMessage = $sQLErrorInfo[2];
			}
             $this->close();
		}
		return $result;
	}

    //retourneert FALSE bij mislukken en een 2dimens array bij slagen evenals een set van alle variabelen
    /*methode noodzaakt het gebruik vd de methode setId*/
    public function selectSessieById()
    {
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call sessieselectbyid(:pId)');
        $preparedStatement -> bindParam(':pId', $this->id, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();
        $this->rowCount = $preparedStatement->rowCount();
        //fetch the output
        if($result = $preparedStatement->fetchAll()) //Returns an array containing all of the result set rows 
        {
            $this->feedback = "{$preparedStatement->rowCount()} rij(en) met id = {$this->id} in de tabel Sessie gevonden.";
            $this->id = $result[0]['Id'];
            $this->lidId = $result[0]['LidId'];
            $this->sessionId = $result[0]['SessionId'];
            $this->lastActivity = $result[0]['LastActivity'];
            $this->modifiedBy = $result[0]['ModifiedBy'];
            $this->modifiedOn = $result[0]['ModifiedOn'];
        }
        else //retourneert lege array
        {
               $this->feedback = "Geen rijen met id = {$this->id} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
                $this->feedback = "Fout => rij is niet gevonden.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
                $this->rowCount = -1;
        }
        $this->close();
        return $result;
        }
         
    }

    static function checkSessionId()
    {
        $sessieObject = new Sessie();
        $sessieObject->setId(1);
        $sessieObject->selectSessieById();
        if($sessieObject->getSessionId() != NULL)
        {
            if(session_id() != $sessieObject->getSessionId())
            {
                header('Location: website_bezet.php');
                exit;
            }
        }
        
    }

    static function registerLastActivity()
    {
        $sessieObject = new Sessie();
        $sessieObject->setId(1);
        $sessieObject->setLidId($_SESSION['lidid']);//levert probleem bij afmelden en wildvreemde toegang
        $sessieObject->setSessionId(session_id());
        $sessieObject->setLastActivity(time());
        $sessieObject->update();
    }
}



?>



