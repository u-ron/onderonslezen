<?php
//nederlandse instellingen
setlocale(LC_TIME, ""); //onvermijdelijk nodig
setlocale(LC_TIME, "nl_NL");

function makeLocalTime($mysqlDate)
{
    if($mysqlDate != NULL)
    {
        $jaar = substr($mysqlDate, 0, 4);
        $maand = substr($mysqlDate, 5, 2);
        $dag = substr($mysqlDate, 8, 2);
        echo strftime("%d %b %Y", mktime(0, 0, 0, $maand, $dag, $jaar));
    }
    else
    {
        echo NULL;
    }
}
?>


