<?php
include('../../helpers/feedback.class.php');    
include('../../helpers/base.class.php');
include('../../webapp/model/uitgever.class.php'); 

session_start();

if(!isset($_SESSION['lidstatus']) || $_SESSION['lidstatus'] == 1)
{
   if($_SESSION['lidstatus'] == 1)
   {
       //sessionid wissen
       include('../help/sessie.class.php');
       $sessieObject1 = new Sessie();
       $sessieObject1->setId(1);
       $sessieObject1->setLidId($_SESSION['lidid']);
       $sessieObject1->setSessionId(NULL);
       $time = time();
       $sessieObject1->setLastActivity($time);
       $sessieObject1->setModifiedBy($_SESSION['username']);
       $sessieObject1->update();

       //gecachte bestanden wissen
       $files = glob('../view/cached/*');//array van bestanden in de cached folder
       foreach($files as $file)
       {
        if(is_file($file))
        {
            unlink($file);
        }    
       }
   }
   //alle sessie variabelen wissen
   session_destroy();
   header('Location: ../../../index.php');
}
else
{
    $lidStatus = $_SESSION['lidstatus']; 
    include('../help/sessie.class.php');
    Sessie::checkSessionId();
    Sessie::registerLastActivity();//heeft $_SESSION['lidid'] nodig
}


if(isset($_POST['uitgeverid']))
{
    $uitgeverObject = new Uitgever();
    $uitgeverId = $_POST['uitgeverid'];
    $uitgeverObject->setUitgeverId($uitgeverId);
    $uitgeverObject->selectUitgeverById();
}
else
{
    $uitgeverObject = new Uitgever();
    $uitgeverObject->setUitgeverId("");
    $uitgeverObject->setUitgever("");
    $uitgeverObject->setUitgeverInfo("");
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Uitgever formulier</title>
        <link rel="stylesheet" href="css/files.css" type="text/css">
        <link rel="stylesheet" href="css/formulier.css" type="text/css">
        <?php include ('../help/jquery.php');?>
        <script type="text/javascript">
            $(document).ready(function () {
                //1. menu
                $("#jMenu").jMenu(
                {
                    ulWidth: '220px',
                    effects: {
                        effectSpeedOpen: 300,
                        effectTypeClose: 'slide'
                    },
                    animatedText: true
                });

                //2. opslaan en cancel button voorzien van stijl
                $("button[type=submit]").button(
                {
                    icons: { primary: " ui-icon-disk" }
                });
                $("button[type=reset]").button(
                {
                    icons: { primary: " ui-icon-cancel" }
                });

            }); //einde ready event

            $(function () {
                $("#sluitinfo").click(function () {
                    $("#rodebalk").hide();
                });
            });
        </script>
    </head>
    <body>
        <div class="container">
        <div class="menuenwelkom">
        <?php include('../help/dashboard.php')?>
        <div class="pull-right">
            <div class="welcoming">Administrator</div>
        </div>
        </div>
        <div id="rodebalk" class="alert-info">
                <strong>&nbsp;<?php if(empty($_POST['uitgeverid'])){echo "Uitgever toevoegen";} else {echo "Uitgever wijzigen";}?></strong>
                <button id="sluitinfo" type="button" class="close">&times;</button>    
        </div>
        <p>
            <a href="uitgevers.php" class="buttonterug">&nbsp;Terug</a>
        </p>
        <form id="frmUitgever" method="POST" action="../control/uitgever.control.php" class="form-horizontal" enctype="multipart/form-data">
            <div class="control-group">
                 <label for="uitgever" class="control-label">NAAM:</label><div class="controls"><input id="uitgever" name="uitgever" type="text" autofocus="true" value="<?php echo $uitgeverObject->getUitgever();?>" required></div>
            </div>
            <div class="control-group">
                  <label for="infoUitgever" class="control-label">INFO:</label><div class="controls"><textarea id="infoUitgever" name="infoUitgever" rows="5" cols="40" placeholder="max 255 karakters" style="resize: none"><?php echo $uitgeverObject->getUitgeverInfo();?></textarea></div>
            </div>
            <div class="control-group">
                 <input id="idHidden" name="idHidden" value="<?php echo $uitgeverObject->getUitgeverId(); ?>">
            </div>         
            <div class="control-group">
                <div class="controls">
                <?php
                if(empty($_POST['uitgeverid']))
                {    
                ?>
                <button id="btnUitgeverSave" name="btnUitgeverSave" type="submit">&nbsp;Opslaan</button>
                <button id="btnUitgeverCancel" type="reset">&nbsp;Annuleren</button>
                <?php
                }
                else
                {
                ?>
                <button id="btnUitgeverUpdate" name="btnUitgeverUpdate" type="submit">&nbsp;Wijzigen</button>
                <?php
                }
                ?>
                </div>
            </div>          
         </form>     
         <div class="push"></div> 
        </div>
        <div id="footer" class="footer">vzw Onder Ons Lezen</div> 
    </body>
</html>

