<?php
include('../../helpers/feedback.class.php');    
include('../../helpers/base.class.php');
include('../model/lid.class.php'); 
include('../model/doc.class.php');   

//ajax ter ophaling van e-mail adres
if(isset($_GET['q']))
{
    $lidId = $_GET['q'];
    $lidObject = new Lid();
    $lidObject->setLidId($lidId);
    $id = $lidObject->getLidId();
    $lidObject->selectLidById();
    $mailLid = $lidObject->getEmail();
    echo $mailLid;//echo volstaat om terug te laten zenden naar de client pagina
} 

if(isset($_GET['verkoperid']))
{
    $verkoperId = $_GET['verkoperid'];
    $docObject = new Doc();
    $result = $docObject->selectKoopwaarVanVerkoper($verkoperId);//retourneert 2dim array
    $resultInJSON = json_encode($result);
    echo $resultInJSON;//retourneert igv 1dim array een resultaat zonder de omvattende rechte haken; aan AJAX
}

if(isset($_GET['verlenerid']))
{
    $verlenerId = $_GET['verlenerid'];
    $docObject = new Doc();
    $result = $docObject->selectLeenwaarVanVerlener($verlenerId);//retourneert 2dim array
    $resultInJSON = json_encode($result);
    echo $resultInJSON;//retourneert igv 1dim array een resultaat zonder de omvattende rechte haken; aan AJAX
}
?>


