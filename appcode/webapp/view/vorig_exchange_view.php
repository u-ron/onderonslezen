<?php
include('../../helpers/feedback.class.php');    
include('../../helpers/base.class.php');
include('../model/doc.class.php');
include('../model/transactie.class.php');
include('../model/transactiepartner.class.php');
include('../help/time.php');
include('../model/lid.class.php');

session_start();

if(!isset($_SESSION['lidstatus']))
{
   header('Location: ../../../index.php');
}
else
{
    if(isset($_POST['vexid']))
    {
        include('../help/sessie.class.php');
        Sessie::checkSessionId();
        Sessie::registerLastActivity();//heeft $_SESSION['lidid'] nodig
    }
    else
    {
       //sessionid wissen
       include('../help/sessie.class.php');
       $sessieObject1 = new Sessie();
       $sessieObject1->setId(1);
       $sessieObject1->setLidId($_SESSION['lidid']);
       $sessieObject1->setSessionId(NULL);
       $time = time();
       $sessieObject1->setLastActivity($time);
       $sessieObject1->setModifiedBy($_SESSION['username']);
       $sessieObject1->update();

       //gecachte bestanden wissen
       $files = glob('../view/cached/*');//array van bestanden in de cached folder
       foreach($files as $file)
       {
        if(is_file($file))
        {
            unlink($file);
        }    
       }

       //alle sessie variabelen wissen
       session_destroy();
       header('Location: ../../../index.php');
    }
}

//tbv welcoming
if(isset($_SESSION['lidid']))
{
    $lidObject = new Lid();
    $lidObject->setLidId($_SESSION['lidid']);
    $lid = $lidObject->selectLidById();
}


if(isset($_POST['vexid']))
{
    $taObject = new Transactie();
    $taId = $_POST['vexid'];
    $taObject->setTransactieId($taId);
    $taObject->selectTransactieById();
    //docs ophalen
    $docObject = new Doc();
    $exchangeId = $taId;
    $docsInExchangeLijst = $docObject->selectDocsInExchange($exchangeId);
}

function lenerOphalen($transactieId) {
    $lidObject = new Lid();
    $lener = $lidObject->selectLenerInExchange($transactieId);
    return $lener;
}

function verlenerOphalen($transactieId) {
    $lidObject = new Lid();
    $verlener = $lidObject->selectVerlenerInExchange($transactieId);
    return $verlener;
}

function statusOphalen($dsId)
{
    switch ($dsId)
    {
        case '6':
            echo 'Teruggekeerd'; break;
    }
}



//auteur ophalen
include('../model/auteur.class.php');
function auteurOphalen($auteurId)
{
    $auteurObject = new Auteur();
    if($auteurId != NULL)
    {
       $auteurObject->setAuteurId($auteurId);
       $auteur = $auteurObject->selectAuteurById();
       echo $auteur[0]['AuteurNaam'];     
    }
}

//uitgever ophalen
include('../model/uitgever.class.php');
function uitgeverOphalen($uitgeverId)
{
    $uitgeverObject = new Uitgever();
    if($uitgeverId != NULL)
    {
       $uitgeverObject->setUitgeverId($uitgeverId);
       $uitgever = $uitgeverObject->selectUitgeverById();
       echo $uitgever[0]['Uitgever'];     
    }
}

//doctype ophalen
include('../model/doctype.class.php');
function docTypeOphalen($docTypeId)
{
    $docTypeObject = new DocType(); 
    $docTypeObject->setDocTypeId($docTypeId);
    $docType = $docTypeObject->selectDocTypeById();     
    echo $docType[0]['DocType'];
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Vorig uitwisseling view</title>
        <link rel="stylesheet" href="css/files.css" type="text/css">
        <link rel="stylesheet" href="css/hangendexchangeformulier.css" type="text/css">
         <?php include ('../help/jquery.php');?>
        <script type="text/javascript">
            $(document).ready(function () {
                //1. hoofdmenu
                $("#jMenu").jMenu(
                {
                    ulWidth: '220px',
                    effects: {
                        effectSpeedOpen: 300,
                        effectTypeClose: 'slide'
                    },
                    animatedText: true
                });

                //2. kalender
                $("#dueDate").datepicker({inline:true });

            }); //einde ready event

            $(function () {
                $("#sluitinfo").click(function () {
                    $("#rodebalk").hide();
                });
            });
        </script>
    </head>
    <body>
        <div class="container">
        <div class="menuenwelkom">
        <?php include('../help/dashboard.php')?>
        <div class="pull-right">
             <div class="welcoming"><?php if ($_SESSION['lidstatus'] == 2) {echo "administrator";} elseif($_SESSION['lidstatus'] == 1) {echo $lid[0]['LidVoornaam']." ".$lid[0]['LidNaam'];}?></div>
        </div>
        </div>
        <div id="rodebalk" class="alert-info">
            <strong>&nbsp;Vorig uitwisseling view</strong>
            <button id="sluitinfo" type="button" class="close">&times;</button>    
        </div>
        <p>
            <a href="mijn_vorige_uitwisselingen.php" class="buttonterug">&nbsp;Terug</a>
        </p>
        <form id="frmVorigExchange" method="POST" action="../control/hangende_exchange.control.php" class="form-horizontal" enctype="multipart/form-data">
            <div class="control-group">
                <label for="nummer" class="control-label">uitwisseling nr:</label>
                <div class="controls"><input id="nummer" name="nummer" type="text" value="<?php echo $taObject->getTransactieId();?>" readonly="true"></div>
            </div>

            <div class="control-group">
                <label for="datum" class="control-label">DATUM:</label>
                <div class="controls"><input id="datum" name="datum" type="text" value="<?php makeLocalTime($taObject->getTADatum());?>" readonly="true"></div>
            </div>

            <div class="control-group">
                <label for="lener" class="control-label">Lener:</label>
                <div class="controls"><input id="lener" name="lener" type="text" value="<?php $lener = lenerOphalen($taObject->getTransactieId()); echo $lener['LidVoornaam']." ".$lener['LidNaam'];?>" readonly="true"></div>
            </div>

            <div class="control-group">
                <label for="verlener" class="control-label">Verlener:</label>
                <div class="controls"><input id="verlener" name="verlener" type="text" value="<?php $verlener = verlenerOphalen($taObject->getTransactieId()); echo $verlener['LidVoornaam']." ".$verlener['LidNaam'];?>" readonly="true"></div>
            </div>

            <div class="control-group">
                <label for="status" class="control-label">status:</label>
                <div class="controls"><input id="status" name="status" type="text" value="<?php statusOphalen($taObject->getExchangeStatusId());?>" readonly="true"></div>
            </div>

            <div class="control-group">
                <label for="duedate" class="control-label">Due date:</label>
                <div class="controls">
                <input id="dueDate" name="duedate" value="<?php makeLocalTime($taObject->getDueDate());?>" readonly="true">
                </div><!--einde controls-->
            </div><!--einde control group-->

            <div class="control-group">
                <label for="datumuit" class="control-label">Datum uit:</label>
                <div class="controls">
                <input id="datumuit" name="datumuit" value="<?php makeLocalTime($taObject->getDatumUit());?>" readonly="true">
                </div>
            </div>

            <div class="control-group">
                <label for="datumterug" class="control-label">Datum terug:</label>
                <div class="controls">
                <input id="datumterug" name="datumterug" value="<?php makeLocalTime($taObject->getDatumTerug());?>" readonly="true">
                </div>
            </div>

            <div id="divdocs" class="control-group">
                <label for="docs" class="control-label">documenten:</label>
                <div class="controls">
                <?php
                if(count($docsInExchangeLijst) != 0)
                {
                ?>
                <table id="docsInExchangeTabel">
                    <thead>
                        <tr>
                        <th>DOC NR.</th>
                        <th>TITEL</th>
                        <th>AUTEUR</th>
                        <th>UITGEVER</th>
                        <th>DOCUMENT TYPE</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    <?php
                    foreach ($docsInExchangeLijst as $die)
                    {
                    $i=$die['docid'];
                    ?>
                    <tr id="<?php echo "dieRij".$i ?>">
                        <td id="<?php echo "dieId".$i ?>" class="Id"><?php echo $die['docid'] ?></td>
                        <td id="<?php echo "dieTitel".$i ?>" class="Titel"><?php echo $die['titel'] ?></td>
                        <td id="<?php echo "dieAuteur".$i ?>" class="Auteur"><?php auteurOphalen($die['auteurid']); ?></td>
                        <td id="<?php echo "dieUitgever".$i ?>" class="Uitgever"><?php uitgeverOphalen($die['uitgeverid']); ?></td>
                        <td id="<?php echo "dieType".$i ?>" class="Type"><?php docTypeOphalen($die['doctypeid']); ?></td>
                    </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                    </table>
                    <?php
                    }
                    else
                    {
                    ?>
                    <div>De verlener heeft de documenten gewist</div>
                    <?php
                    }   
                    ?>
                </div>
            </div>
            <input id="idHidden" name="idhidden" type="hidden" value="<?php echo $taObject->getTransactieId(); ?>">
         </form> 
         <div class="push"></div>      
        </div>
        <div id="footer" class="footer">vzw Onder Ons Lezen</div>   
    </body>
</html>


