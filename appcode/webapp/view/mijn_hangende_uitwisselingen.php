<?php
include('../../helpers/feedback.class.php');    
include('../../helpers/base.class.php');
include('../model/transactie.class.php');
include('../model/transactiepartner.class.php');
include('../help/time.php');
include('../model/lid.class.php');

session_start();

if(!isset($_SESSION['lidstatus']))
{
   header('Location: ../../../index.php');
}
else
{
    $lidStatus = $_SESSION['lidstatus']; 
    include('../help/sessie.class.php');
    Sessie::checkSessionId();
    Sessie::registerLastActivity();//heeft $_SESSION['lidid'] nodig
}

//tbv welcoming
if(isset($_SESSION['lidid']))
{
    $lidObject = new Lid();
    $lidObject->setLidId($_SESSION['lidid']);
    $lid = $lidObject->selectLidById();
}

function lenerOphalen($transactieId) {
    $lidObject = new Lid();
    $lener = $lidObject->selectLenerInExchange($transactieId);
    echo $lener['LidVoornaam']." ".$lener['LidNaam'];
}

function verlenerOphalen($transactieId) {
    $lidObject = new Lid();
    $verlener = $lidObject->selectVerlenerInExchange($transactieId);
    echo $verlener['LidVoornaam']." ".$verlener['LidNaam'];
}

function statusOphalen($dsId)
{
    switch ($dsId)
    {
        case '1':
            echo 'Voorstel'; break;
        case '2':
            echo 'Gewijzigd voorstel'; break;
        case '3':
            echo 'Aanvaard'; break;
        case '4':
            echo 'Afgekeurd'; break;//kan niet voorkomen
        case '5':
            echo 'In uitwisseling'; break;
    }
}

$taObject = new Transactie();
$hangendeExchangesLijst = $taObject->selectMijnHangendeExchanges($_SESSION['lidid']);
//print_r($hangendeExchangesLijst);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Mijn hangende uitwisselingen</title>
        <link rel="stylesheet" href="css/files.css" type="text/css">
        <link rel="stylesheet" href="css/hangendeexchanges.css" type="text/css">
         <?php include ('../help/jquery.php');?>
        <script type="text/javascript">
            $(document).ready(function () {
                //1. menu
                $("#jMenu").jMenu(
                {
                    ulWidth: '220px',
                    effects: {
                        effectSpeedOpen: 300,
                        effectTypeClose: 'slide'
                    },
                    animatedText: true
                });

                //2. snel sorteren dankzij de expando sortKey
                var tabel = $("#hexTabel");
                $('th', tabel).each(function (columnIndex) {
                    if ($(this).is('.sorteer')) {
                        var col = this;
                        $(this).click(function () {
                            var rijen = tabel.find('tbody > tr');
                            /*vooraf opslaan van keyA en keyB in sortKey*/
                            $.each(rijen, function (index, rij) {

                                if ($(col).is('.alfabet')) {
                                    rij.sortKey = $(rij).children('td').eq(columnIndex).text().toUpperCase();
                                }

                                if ($(col).is('.getal')) {
                                    var waarde = $(rij).children('td').eq(columnIndex).text();
                                    rij.sortKey = parseFloat(waarde);
                                }
                            });

                            rijen.sort(function (a, b) {
                                if (a.sortKey < b.sortKey) return -1;
                                if (a.sortKey > b.sortKey) return 1;
                                return 0;
                            });

                            $.each(rijen, function (index, rij) {
                                tabel.children('tbody').append(rij);
                                rij.sortKey = null;
                            });

                        }); //einde click event
                    } //einde if sorteer
                }); //einde each

                //3. filteren
                $("#filter").change(function () {
                    var tekst = $(this).val();
                    $("tbody tr").hide();
                    $("tbody tr td:contains('" + tekst + "')").parent().show();
                })

                //4. knoppen in actie kolom
                $(".btnedit").button(
                    {
                        icons: { primary: "ui-icon-pencil" }
                    });

                //5.paginatie
                $("#aantalPaginas").change(function () {
                    var ps = $("select option:selected").text();
                    if (ps == "") {
                        $('#hexTabel').datatable('destroy');
                    }
                    else {
                        $('#hexTabel').datatable({
                            pageSize: ps,
                            pagingNumberOfPages: 5
                        });
                    };
                });

                //6.tooltips
                $('.btndelete').tooltip();
                $('.btnedit').tooltip();

            }); //einde ready event

            $(function () {
                $("#sluitinfo").click(function () {
                    $("#rodebalk").hide();
                });
            });

        </script>
    </head>
    <body>
        <div class="container">
        <div class="menuenwelkom">
        <?php include('../help/dashboard.php')?>
        <div class="pull-right">
             <div class="welcoming"><?php if ($lidStatus == 2) {echo "administrator";} elseif($lidStatus == 1) {echo $lid[0]['LidVoornaam']." ".$lid[0]['LidNaam'];}?></div>
        </div>
        </div>
            <div id="rodebalk" class="alert-info">
                <strong>&nbsp;Mijn hangende uitwisselingen: <?php echo count($hangendeExchangesLijst)?> rijen</strong>
                <button id="sluitinfo" type="button" class="close">&times;</button>    
            </div>
            <br />
            <?php
            if(count($hangendeExchangesLijst) != 0)
            {
            ?>
            <div class="row-fluid">
                <label id="paginatie">
                    <select size="1" id="aantalPaginas">
                        <option></option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                    </select>&nbsp;rijen per pagina
                </label>
                <label id="filtering">
                Zoek:&nbsp;<input type="text" id="filter">
                </label>
            </div>
            <table id="hexTabel">
                    <thead>
                        <tr>
                        <th class="sorteer getal Id">UITWISSELING NR</th>
                        <th class="Datum">TRANSACTIE DATUM</th>
                        <th class="sorteer alfabet Lener">LENER</th>
                        <th class="sorteer alfabet Verlener">VERLENER</th>
                        <th class="DueDate">DUE DATUM</th>
                        <th class="sorteer alfabet Status">STATUS</th>
                        <th class="Actie">ACTIE</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    <?php
                    foreach ($hangendeExchangesLijst as $hex)//$doc is ééndimensionale rij
                    {
                        $i=$hex['TransactieId'];
                    ?>
                    <tr id="<?php echo "hexRij".$i ?>">
                        <td id="<?php echo "hexId".$i ?>"><?php echo $hex['TransactieId'] ?></td>
                        <td id="<?php echo "hexDatum".$i ?>"><?php makeLocalTime($hex['TADatum']);?></td>
                        <td id="<?php echo "hexLener".$i ?>"><?php lenerOphalen($hex['TransactieId']); ?></td>
                        <td id="<?php echo "hexVerlener".$i ?>"><?php verlenerOphalen($hex['TransactieId']); ?></td>
                        <td id="<?php echo "hexDueDate".$i ?>"><?php makeLocalTime($hex['DueDate']); ?></td>
                        <td id="<?php echo "hexStatus".$i ?>"><?php echo statusOphalen($hex['ExchangeStatusId']); ?></td>
                        <td id="<?php echo "hexActie".$i?>"><form id="<?php echo "hexEdit".$i?>" method="post" action="hangend_uitwisseling_formulier.php"><input id="transactieid" name="transactieid" value="<?php echo $i;?>"><button id="<?php echo "hexBtnEdit".$i?>" type="submit" class="btnedit" title="edit"></button></form></td>
                    </tr>
                    <?php
                    }
                    ?>
                    </tbody>
             </table>
             <?php
             }
             ?>
            <div class="paging"></div>
            <div id="mailMessage"><?php if(isset ($_SESSION['mailmessagedeny'])) {echo $_SESSION['mailmessagedeny'];}; unset($_SESSION['mailmessagedeny']);?></div>
            <div class="push"></div>    
        </div>
        <div id="footer" class="footer">vzw Onder Ons Lezen</div>       
    </body>
</html>
