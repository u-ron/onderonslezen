<?php
include('../../helpers/feedback.class.php');    
include('../../helpers/base.class.php');
include('../model/woonplaats.class.php'); 

session_start();

if(!isset($_SESSION['lidstatus']) || $_SESSION['lidstatus'] == 1)
{
   if($_SESSION['lidstatus'] == 1)
   {
       //sessionid wissen
       include('../help/sessie.class.php');
       $sessieObject1 = new Sessie();
       $sessieObject1->setId(1);
       $sessieObject1->setLidId($_SESSION['lidid']);
       $sessieObject1->setSessionId(NULL);
       $time = time();
       $sessieObject1->setLastActivity($time);
       $sessieObject1->setModifiedBy($_SESSION['username']);
       $sessieObject1->update();

       //gecachte bestanden wissen
       $files = glob('../view/cached/*');//array van bestanden in de cached folder
       foreach($files as $file)
       {
        if(is_file($file))
        {
            unlink($file);
        }    
       }
   }
   //alle sessie variabelen wissen
   session_destroy();
   header('Location: ../../../index.php');
}
else
{
    $lidStatus = $_SESSION['lidstatus']; 
    include('../help/sessie.class.php');
    Sessie::checkSessionId();
    Sessie::registerLastActivity();//heeft $_SESSION['lidid'] nodig
}


if(isset($_POST['woonplaatsid']))
{
    $woonplaatsObject = new Woonplaats();
    $woonplaatsId = $_POST['woonplaatsid'];
    $woonplaatsObject->setWoonplaatsId($woonplaatsId);
    $woonplaatsObject->selectWoonplaatsById();
}
else{
    $woonplaatsObject = new Woonplaats();
    $woonplaatsObject->setWoonplaatsId("");
    $woonplaatsObject->setGemeente("");
    $woonplaatsObject->setPostcode("");
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Woonplaats formulier</title>
        <link rel="stylesheet" href="css/files.css" type="text/css">
        <link rel="stylesheet" href="css/formulier.css" type="text/css">
         <?php include ('../help/jquery.php');?>
        <script type="text/javascript">
            
            $(document).ready(function () {
                //1. menu
                $("#jMenu").jMenu(
                {
                    ulWidth: '220px',
                    effects: {
                        effectSpeedOpen: 300,
                        effectTypeClose: 'slide'
                    },
                    animatedText: true
                });

                //2. opslaan en cancel button voorzien van stijl
                $("button[type=submit]").button(
                {
                    icons: { primary: " ui-icon-disk" }
                });
                $("button[type=reset]").button(
                {
                    icons: { primary: " ui-icon-cancel" }
                });
                

            }); //einde ready event

            $(function () {
                $("#sluitinfo").click(function () {
                    $("#rodebalk").hide();
                });
            });
        </script>
    </head>
    <body>
        <div class="container">
        <div class="menuenwelkom">
        <?php include('../help/dashboard.php')?>
        <div class="pull-right">
            <div class="welcoming">Administrator</div>
        </div>
        </div>
        <div id="rodebalk" class="alert-info">
                <strong>&nbsp;<?php if(empty($_POST['woonplaatsid'])){echo "Woonplaats toevoegen";} else {echo "Woonplaats wijzigen";}?></strong>
                <button id="sluitinfo" type="button" class="close">&times;</button>    
        </div>
        <p>
            <a href="woonplaatsen.php" class="buttonterug">&nbsp;Terug</a>
        </p>
        <form id="frmWoonplaats" method="POST" action="../control/woonplaats.control.php" class="form-horizontal" enctype="multipart/form-data">
            <div class="control-group">
                 <label for="gemeente" class="control-label">GEMEENTE:</label><div class="controls"><input id="gemeente" name="gemeente" type="text" autofocus="true" value="<?php echo $woonplaatsObject->getGemeente();?>" required></div>
            </div>
            <div class="control-group">
                  <label for="postcode" class="control-label">POSTCODE:</label><div class="controls"><input id="postcode" name="postcode" value="<?php echo $woonplaatsObject->getPostcode();?>" required></div>
            </div>
            <div class="control-group">
                 <input id="idHidden" name="idHidden" value="<?php echo $woonplaatsObject->getWoonplaatsId(); ?>">
            </div>         
            <div class="control-group">
                <div class="controls">
                <?php
                if(empty($_POST['woonplaatsid']))
                {    
                ?>
                <button id="btnWoonplaatsSave" name="btnWoonplaatsSave" type="submit">&nbsp;Opslaan</button>
                <button id="btnWoonplaatsCancel" type="reset">&nbsp;Annuleren</button>
                <?php
                }
                else
                {
                ?>
                <button id="btnWoonplaatsUpdate" name="btnWoonplaatsUpdate" type="submit">&nbsp;Wijzigen</button>
                <?php
                }
                ?>
                </div>
            </div>          
         </form>     
         <div class="push"></div> 
        </div>
        <div id="footer" class="footer">vzw Onder Ons Lezen</div>    
    </body>
</html>

