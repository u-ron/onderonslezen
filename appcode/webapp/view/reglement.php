<?php
include('../../helpers/feedback.class.php');    
include('../../helpers/base.class.php');
include('../model/lid.class.php');

session_start();

if(isset($_SESSION['lidstatus']))
{
    include('../help/sessie.class.php');
    Sessie::checkSessionId();
    Sessie::registerLastActivity();//heeft $_SESSION['lidid'] nodig
}

//tbv welcoming
if(isset($_SESSION['lidid']))
{
    $lidObject = new Lid();
    $lidObject->setLidId($_SESSION['lidid']);
    $lid = $lidObject->selectLidById();
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Reglement</title>
        <link rel="stylesheet" href="css/files.css" type="text/css">
        <style>
            #tekst {
                list-style-type: disc;
            }
            
            #tekst li {
                margin: 0 10%;
                padding: 5px 0;
            }
        </style>
        <?php include ('../help/jquery.php');?>
        <script type="text/javascript">
            $(document).ready(function () {
                //1. menu
                $("#jMenu").jMenu(
                {
                    ulWidth: '220px',
                    effects: {
                        effectSpeedOpen: 300,
                        effectTypeClose: 'slide'
                    },
                    animatedText: true
                });

            }); //einde ready event

            $(function () {
                $("#sluitinfo").click(function () {
                    $("#rodebalk").hide();
                });
            });
        </script>
        
    </head>
    <body>
        <div class="container">
        <div class="menuenwelkom">
        <?php include('../help/dashboard.php')?>
        <div class="pull-right">
             <div class="welcoming">
              <?php if (isset($_SESSION['lidstatus']))
              {
                  if($_SESSION['lidstatus'] == 2) {echo "administrator";} elseif($_SESSION['lidstatus'] == 1) {echo $lid[0]['LidVoornaam']." ".$lid[0]['LidNaam'];} 
              }
              else {echo "bezoeker";}?></div>
        </div>
        </div>
        <div id="rodebalk" class="alert-info">
                <strong>&nbsp;Reglement</strong>
                <button id="sluitinfo" type="button" class="close">&times;</button>    
        </div>
        <div>
        <ul id="tekst">
        <li>Bezoekers van de website kunnen enkel grasduinen in de documenten.</li>
        <li>Men wordt lid van de community door zich in te schrijven. Na inschrijving dient een lid zich aan te melden op de website. Slechts één lid per keer kan gebruik maken van de website.</li>
        <li>Een lid van de community kan documenten registreren, dealen en uitwisselen.</li>
        <li>Bij ontbrekende informatie in de website zoals auteurs, uitgevers, woonplaatsen wordt een lid verzocht de administrator te contacteren. De administrator zorgt ervoor dat de benodigde gegevens in de website beschikbaar komen.</li>
        <li>Enkel de eigenaar van een document kan de data van het document toevoegen, wijzigen en wissen.</li>
        <li>Een document kan pas gewist worden als al zijn specifieke kenmerken en foto’s gewist zijn.</li>
        <li>Onder transactie van een document wordt deal of uitwisseling verstaan.</li>
        <li>Bij deal worden de volgende statussen onderscheiden: voorstel, gewijzigd voorstel, goedgekeurd, afgekeurd, betaald, verzonden, afgehaald.</li>
        <li>Bij uitwisseling worden de volgende statussen onderscheiden: voorstel, gewijzigd voorstel, goedgekeurd, afgekeurd, in uitwisseling, teruggebracht.</li>
        <li>Bij elke wijziging van de status van een transactie wordt automatisch een e-mail verzonden naar de andere partner.</li>
        <li>Eenmaal een transactie opgestart is, kunnen de documenten van de transactie niet gewijzigd of gewist worden. </li>
        <li>Documenten die tussen 2 leden in onderhandeling zijn, zijn niet meer voor anderen beschikbaar in de website.</li>
        <li>Bij een deal kunnen de documenten afgehaald worden bij de verkoper of verzonden worden op kosten van de koper. De website laat toe herhaaldelijk de transportkosten te wijzigen tot een akkoord wordt bereikt.</li>
        <li>Bij afkeuring van een verkoopsvoorstel worden de bijhorende documenten opnieuw beschikbaar in de website.</li>
        <li>Bij verkoop verdwijnen de bijhorende documenten uit de website. Het komt aan de nieuwe eigenaar toe om de documenten al dan niet opnieuw te registreren met het oog op verkoop en/of uitwisseling.</li>
        <li>Bij een kosteloze uitwisseling van documenten zijn er geen transportkosten. De website laat toe de due datum van de uitwisseling herhaaldelijk te wijzigen tot een akkoord wordt bereikt.</li>
        <li>Bij afkeuring van een uitwisselingsvoorstel worden de bijhorende documenten opnieuw beschikbaar in de website.</li>
        <li>Na afloop van een uitwisseling  worden de bijhorende documenten opnieuw beschikbaar in de website.</li>
        <li>Een lid kan steeds de data van voorbije transacties opvragen. Een lid kan zijn data van voorbije transacties wissen.</li>
        <li>De administrator beschikt over alle account data van de leden en kan in plaats van een lid handelen.</li>
        <li>De administrator kan het account van een lid verwijderen.</li>
        </ul>
        </div>
          <div class="push"></div>       
        </div>
        <div id="footer" class="footer">vzw Onder Ons Lezen</div>      
    </body>
</html>

