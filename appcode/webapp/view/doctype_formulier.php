<?php
include('../../helpers/feedback.class.php');    
include('../../helpers/base.class.php');
include('../model/doctype.class.php');

session_start();

if(!isset($_SESSION['lidstatus']) || $_SESSION['lidstatus'] == 1)
{
   if($_SESSION['lidstatus'] == 1)
   {
       //sessionid wissen
       include('../help/sessie.class.php');
       $sessieObject1 = new Sessie();
       $sessieObject1->setId(1);
       $sessieObject1->setLidId($_SESSION['lidid']);
       $sessieObject1->setSessionId(NULL);
       $time = time();
       $sessieObject1->setLastActivity($time);
       $sessieObject1->setModifiedBy($_SESSION['username']);
       $sessieObject1->update();

       //gecachte bestanden wissen
       $files = glob('../view/cached/*');//array van bestanden in de cached folder
       foreach($files as $file)
       {
        if(is_file($file))
        {
            unlink($file);
        }    
       }
   }
   //alle sessie variabelen wissen
   session_destroy();
   header('Location: ../../../index.php');
}
else
{
    $lidStatus = $_SESSION['lidstatus']; 
    include('../help/sessie.class.php');
    Sessie::checkSessionId();
    Sessie::registerLastActivity();//heeft $_SESSION['lidid'] nodig
}


if(isset($_POST['doctypeid']))
{
    $doctypeObject = new DocType();
    $doctypeId = $_POST['doctypeid'];
    $doctypeObject->setDocTypeId($doctypeId);
    $doctypeObject->selectDocTypeById();
}
else{
    $doctypeObject = new DocType();
    $doctypeObject->setDocTypeId("");
    $doctypeObject->setDocType("");
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Document type formulier</title>
        <link rel="stylesheet" href="css/files.css" type="text/css">
        <link rel="stylesheet" href="css/formulier.css" type="text/css">
        <?php include ('../help/jquery.php');?>
        <script type="text/javascript">
            $(document).ready(function () {
                //1. menu
                $("#jMenu").jMenu(
                {
                    ulWidth: '220px',
                    effects: {
                        effectSpeedOpen: 300,
                        effectTypeClose: 'slide'
                    },
                    animatedText: true
                });

                //2. opslaan en cancel button voorzien van stijl
                $("button[type=submit]").button(
                {
                    icons: { primary: " ui-icon-disk" }
                });
                $("button[type=reset]").button(
                {
                    icons: { primary: " ui-icon-cancel" }
                });

            }); //einde ready event

            $(function () {
                $("#sluitinfo").click(function () {
                    $("#rodebalk").hide();
                });
            });
        </script>
    </head>
    <body>
        <div class="container">
        <div class="menuenwelkom">
        <?php include('../help/dashboard.php')?>
        <div class="pull-right">
            <div class="welcoming">Administrator</div>
        </div>
        </div>
        <div id="rodebalk" class="alert-info">
                <strong>&nbsp;<?php if(empty($_POST['doctypeid'])){echo "Document type toevoegen";} else {echo "Document type wijzigen";}?></strong>
                <button id="sluitinfo" type="button" class="close">&times;</button>    
        </div>
        <p>
            <a href="documenttypes.php" class="buttonterug">&nbsp;Terug</a>
        </p>
        <form id="frmDocType" method="POST" action="../control/doctype.control.php" class="form-horizontal" enctype="multipart/form-data">
            <div class="control-group">
                 <label for="doctype" class="control-label">NAAM:</label><div class="controls"><input id="doctype" name="doctype" type="text" autofocus="true" value="<?php echo $doctypeObject->getDocType();?>" required></div>
            </div>
            
            <div class="control-group">
                 <input id="idHidden" name="idHidden" value="<?php echo $doctypeObject->getDocTypeId(); ?>">
            </div>         
            <div class="control-group">
                <div class="controls">
                <?php
                if(empty($_POST['doctypeid']))
                {    
                ?>
                <button id="btnDocTypeSave" name="btnDocTypeSave" type="submit">&nbsp;Opslaan</button>
                <button id="btnDocTypeCancel" type="reset">&nbsp;Annuleren</button>
                <?php
                }
                else
                {
                ?>
                <button id="btnDocTypeUpdate" name="btnDocTypeUpdate" type="submit">&nbsp;Wijzigen</button>
                <?php
                }
                ?>
                </div>
            </div>          
         </form>     
         <div class="push"></div> 
        </div>
        <div id="footer" class="footer">vzw Onder Ons Lezen</div>      
    </body>
</html>


