<?php
include('../../../appcode/helpers/feedback.class.php');    
include('../../helpers/base.class.php');
include('../model/doc.class.php');
include('../model/lid.class.php');

session_start();

if(!isset($_SESSION['lidstatus']))
{
   header('Location: ../../../index.php');
}
else
{
    $lidStatus = $_SESSION['lidstatus']; 
    include('../help/sessie.class.php');
    Sessie::checkSessionId();
    Sessie::registerLastActivity();//heeft $_SESSION['lidid'] nodig
}

//tbv welcoming
if(isset($_SESSION['lidid']))
{
    $lidObject = new Lid();
    $lidObject->setLidId($_SESSION['lidid']);
    $lid = $lidObject->selectLidById();
}


//documenten van ingelogd lid ophalen
$docObject = new Doc();
$mijnDocsLijst = $docObject->selectDocumentenPerLid($_SESSION['lidid']);//hier soms nog problemen

//tbv auteur van document ophalen
include('../model/auteur.class.php');
function auteurOphalen($auteurId)
{
    $auteurObject = new Auteur();
    $auteurObject->setAuteurId($auteurId);
    $auteurObject->selectAuteurById();
    $auteurNaam=$auteurObject->getAuteurNaam();
    echo $auteurNaam;//gaat niet met return
}

//documenttype ophalen
include('../model/doctype.class.php');
function docTypeOphalen($docTypeId)
{
    $docTypeObject = new DocType();
    $docTypeObject->setDocTypeId($docTypeId);
    $docTypeObject->selectDocTypeById();
    $docType = $docTypeObject->getDocType();
    echo $docType;
}

function transactieChecken($docId)
{
    $docObject1 = new Doc();
    $docObject1->setDocId($docId);
    $jaOfNee = $docObject1->isDocInTransaction();
    return $jaOfNee ;
}

isset($_SESSION['message']) ?  $message = $_SESSION['message'] : $message = "";
unset($_SESSION['message']);

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Documenten</title>
        <link rel="stylesheet" href="css/files.css" type="text/css">
        <link rel="stylesheet" href="css/mijndocumenten.css" type="text/css">
         <?php include ('../help/jquery.php');?>
        <script type="text/javascript">
            $(document).ready(function () {
                //1. menu
                $("#jMenu").jMenu(
                {
                    ulWidth: '220px',
                    effects: {
                        effectSpeedOpen: 300,
                        effectTypeClose: 'slide'
                    },
                    animatedText: true
                });

                //2. snel sorteren dankzij de expando sortKey
                var tabel = $("#mijnDocumentenTabel");
                $('th', tabel).each(function (columnIndex) {
                    if ($(this).is('.sorteer')) {
                        var col = this;
                        $(this).click(function () {
                            var rijen = tabel.find('tbody > tr');
                            /*vooraf opslaan van keyA en keyB in sortKey*/
                            $.each(rijen, function (index, rij) {

                                if ($(col).is('.alfabet')) {
                                    rij.sortKey = $(rij).children('td').eq(columnIndex).text().toUpperCase();
                                }

                                if ($(col).is('.getal')) {
                                    var waarde = $(rij).children('td').eq(columnIndex).text();
                                    rij.sortKey = parseFloat(waarde);
                                }
                            });

                            rijen.sort(function (a, b) {
                                if (a.sortKey < b.sortKey) return -1;
                                if (a.sortKey > b.sortKey) return 1;
                                return 0;
                            });

                            $.each(rijen, function (index, rij) {
                                tabel.children('tbody').append(rij);
                                rij.sortKey = null;
                            });

                        }); //einde click event
                    } //einde if sorteer


                }); //einde each

                //3. filteren
                $("#filter").change(function () {
                    var tekst = $(this).val();
                    $("tbody tr").hide();
                    $("tbody tr td:contains('" + tekst + "')").parent().show();
                })

                //4. knoppen in actie kolom
                $(".btndelete").button(
                    {
                        icons: { primary: "ui-icon-trash" }
                    });

                $(".btnedit").button(
                    {
                        icons: { primary: "ui-icon-pencil" }
                    });
                $(".btnTA").button(
                    {
                        icons: { primary: "ui-icon-notice" }
                    });

                //5. document verwijderen
                //$("table tbody tr td button").live("click", verwijderDoc);//gaat niet
                $("table").on("click", "button.btndelete", verwijderDocument);

                //6.paginatie
                $("#aantalPaginas").change(function () {
                    var ps = $("select option:selected").text();
                    if (ps == "") {
                        $('#mijnDocumentenTabel').datatable('destroy');
                    }
                    else {
                        $('#mijnDocumentenTabel').datatable({
                            pageSize: ps,
                            pagingNumberOfPages: 5
                        });
                    };
                });

                //7. dialog widget messages
                if ($("#message").text().trim().length != 0) {
                    $("#message").dialog({
                        buttons: {
                            "OK": function () { $(this).dialog("close"); }
                        }
                    }); //einde dialog
                }; //einde if

                //8. tooltip
                $('a[title="wis"]').tooltip();
                $('a[title="edit"]').tooltip();
                $('img[title="in transactie"]').tooltip();

            }); //einde ready event

            $(function () {
                $("#sluitinfo").click(function () {
                    $("#rodebalk").hide();
                });
            });

            function verwijderDocument() {
                var btnid = $(this).attr("id"); //attribuut lezen in jQuery
                var id = btnid.substring(12);
                //dialog widget bij verwijderen record
                $("#warningDeletion").dialog(
                {
                    buttons: [
                {
                    text: "Ja",
                    click: function () { window.location.href = '../control/document.control.php?documentid=' + id; }
                },
                {
                    text: "Nee",
                    click: function () { $(this).dialog("close"); }
                }]
                });
            } //einde verwijderAuteur


        </script>
    </head>
    <body>
        <div class="container">
        <div class="menuenwelkom">
        <?php include('../help/dashboard.php')?>
        <div class="pull-right">
             <div class="welcoming"><?php if ($lidStatus == 2) {echo "administrator";} elseif($lidStatus == 1) {echo $lid[0]['LidVoornaam']." ".$lid[0]['LidNaam'];} elseif($lidStatus == 0) {echo "bezoeker";}?></div>
        </div>
        </div>
        <div id="rodebalk" class="alert-info">
                <strong>&nbsp;Mijn documenten (beschikbaar en in transactie): <?php echo count($mijnDocsLijst)?> rijen</strong>
                <button id="sluitinfo" type="button" class="close">&times;</button>    
            </div>
            <p><a href="document_formulier.php" class="buttonadd">&nbsp;Document toevoegen</a>
            </p>
            <br />
            <?php
            if(count($mijnDocsLijst) != 0)
            {
            ?>
            <div class="row-fluid">
                <label id="paginatie">
                    <select size="1" id="aantalPaginas">
                        <option></option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                    </select>&nbsp;rijen per pagina
                </label>
                <label id="filtering">
                Zoek:&nbsp;<input type="text" id="filter">
                </label>
            </div>
            <table id="mijnDocumentenTabel">
                    <thead>
                        <tr>
                        <th class="sorteer getal Id">DOC NR.</th>
                        <th class="sorteer alfabet Titel">TITEL</th>
                        <th class="sorteer alfabet Auteur">AUTEUR</th>
                        <th class="sorteer alfabet Type">DOCUMENT TYPE</th>
                        <th class="sorteer alfabet Jaar">JAAR</th>
                        <th class="TeKoop">TE KOOP</th>
                        <th class="TeLeen">TE LEEN</th>
                        <th class="Actie">ACTIE</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    <?php
                    foreach ($mijnDocsLijst as $doc)//$doc is ééndimensionale rij
                    {
                        $i=$doc['DocId'];
                    ?>
                    <tr id="<?php echo "docRij".$i ?>">
                        <td id="<?php echo "docId".$i ?>"><?php echo $doc['DocId'] ?></td>
                        <td id="<?php echo "docTitel".$i ?>"><?php echo $doc['Titel'] ?></td>
                        <td id="<?php echo "docAuteur".$i ?>"><?php auteurOphalen($doc['AuteurId']); ?></td>
                        <td id="<?php echo "docType".$i ?>"><?php docTypeOphalen($doc['DocTypeId']); ?></td>
                        <td id="<?php echo "docJaar".$i ?>"><?php echo $doc['Jaar']; ?></td>
                        <td id="<?php echo "docTeKoop".$i?>"><?php if(isset($doc['TeKoop'])) { echo ($doc['TeKoop'] == 1) ? "Ja" : "Nee"; }; ?></td>
                        <td id="<?php echo "docTeLeen".$i?>"><?php if(isset($doc['TeLeen'])) { echo ($doc['TeLeen'] == 1) ? "Ja" : "Nee"; }; ?></td>
                        <td id="<?php echo "docActie".$i?>">
                            <a id="<?php echo "docLinkDelete".$i?>" title="wis"><button id="<?php echo "docBtnDelete".$i?>" type="button" class="btndelete"></button></a>
                            <a id="<?php echo "docLinkEdit".$i?>" href="<?php if(transactieChecken($i) != 1){echo "document_formulier.php?documentid=".$i;}else{echo "document_view.php?documentid=".$i;}?>" title="edit"><button id="<?php echo "docBtnEdit".$i?>" type="button" class="btnedit"></button></a>
                            <?php if(transactieChecken($i)) {?><img id="<?php echo "docBtnTA".$i?>" title="in transactie" alt="in transactie" src="../../../appcode/images/ExclamationMark.png"><?php }?>
                        </td>
                    </tr>
                    <?php
                    }
                    ?>
                    </tbody>
             </table>
             <div class="paging"></div>
            <?php
            }
            ?>
            <div id="warningDeletion">Bent u zeker om dit document te verwijderen?</div>
            <div id="message"><?php echo $message; ?></div>
        <div class="push"></div>    
        </div><!--einde container-->
        <div id="footer" class="footer">vzw Onder Ons Lezen</div>    
    </body>
</html>
 