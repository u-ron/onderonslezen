<?php
include('../../helpers/feedback.class.php');    
include('../../helpers/base.class.php');
include('../model/doc.class.php');
include('../model/transactie.class.php');
include('../model/transactiepartner.class.php');
include('../help/time.php');
include('../model/lid.class.php');

session_start();

if(!isset($_SESSION['lidstatus']))
{
   header('Location: ../../../index.php');
}
else
{
    if(isset($_POST['vdid']))
    {
        include('../help/sessie.class.php');
        Sessie::checkSessionId();
        Sessie::registerLastActivity();//heeft $_SESSION['lidid'] nodig
    }
    else
    {
       //sessionid wissen
       include('../help/sessie.class.php');
       $sessieObject1 = new Sessie();
       $sessieObject1->setId(1);
       $sessieObject1->setLidId($_SESSION['lidid']);
       $sessieObject1->setSessionId(NULL);
       $time = time();
       $sessieObject1->setLastActivity($time);
       $sessieObject1->setModifiedBy($_SESSION['username']);
       $sessieObject1->update();

       //gecachte bestanden wissen
       $files = glob('../view/cached/*');//array van bestanden in de cached folder
       foreach($files as $file)
       {
        if(is_file($file))
        {
            unlink($file);
        }    
       }

       //alle sessie variabelen wissen
       session_destroy();
       header('Location: ../../../index.php');
    }
}

//tbv welcoming
if(isset($_SESSION['lidid']))
{
    $lidObject = new Lid();
    $lidObject->setLidId($_SESSION['lidid']);
    $lid = $lidObject->selectLidById();
}

if(isset($_POST['vdid']))
{
    $taObject = new Transactie();
    $taId = $_POST['vdid'];
    $taObject->setTransactieId($taId);
    $taObject->selectTransactieById();
    //docs ophalen
    $docObject = new Doc();
    $dealId = $taId;
    $docsInDealLijst = $docObject->selectDocsinDeal($dealId);
}

function koperOphalen($transactieId) {
    $lidObject = new Lid();
    $koper = $lidObject->selectKoperInDeal($transactieId);
    return $koper;
}

function verkoperOphalen($transactieId) {
    $lidObject = new Lid();
    $verkoper = $lidObject->selectVerkoperInDeal($transactieId);
    return $verkoper;
}

function statusOphalen($dsId)
{
    switch ($dsId)
    {
        case '6':
            echo 'Afgehaald'; break;
        case '7':
            echo 'Verzonden'; break;
    }
}

//auteur ophalen
include('../model/auteur.class.php');
function auteurOphalen($auteurId)
{
    $auteurObject = new Auteur();
    if($auteurId != NULL)
    {
       $auteurObject->setAuteurId($auteurId);
       $auteur = $auteurObject->selectAuteurById();
       echo $auteur[0]['AuteurNaam'];     
    }
}

//doctype ophalen
include('../model/doctype.class.php');
function docTypeOphalen($docTypeId)
{
    $docTypeObject = new DocType(); 
    $docTypeObject->setDocTypeId($docTypeId);
    $docType = $docTypeObject->selectDocTypeById();     
    echo $docType[0]['DocType'];
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Vorig deal view</title>
        <link rel="stylesheet" href="css/files.css" type="text/css">
        <link rel="stylesheet" href="css/hangenddealformulier.css" type="text/css">
         <?php include ('../help/jquery.php');?>
        <script type="text/javascript">
            $(document).ready(function () {
                //1. hoofdmenu
                $("#jMenu").jMenu(
                {
                    ulWidth: '220px',
                    effects: {
                        effectSpeedOpen: 300,
                        effectTypeClose: 'slide'
                    },
                    animatedText: true
                });

            }); //einde ready event

            $(function () {
                $("#sluitinfo").click(function () {
                    $("#rodebalk").hide();
                });
            });
        </script>
    </head>
    <body>
        <div class="container">
        <div class="menuenwelkom">
        <?php include('../help/dashboard.php')?>
        <div class="pull-right">
             <div class="welcoming"><?php if ($_SESSION['lidstatus'] == 2) {echo "administrator";} elseif($_SESSION['lidstatus'] == 1) {echo $lid[0]['LidVoornaam']." ".$lid[0]['LidNaam'];}?></div>
        </div>
        </div>
        <div id="rodebalk" class="alert-info">
            <strong>&nbsp;Vorig deal view</strong>
            <button id="sluitinfo" type="button" class="close">&times;</button>    
        </div>
        <p>
            <a href="mijn_vorige_deals.php" class="buttonterug">&nbsp;Terug</a>
        </p>
        <form id="frmVorigDeal" method="POST" action="../control/vorig_deal.control.php" class="form-horizontal" enctype="multipart/form-data">
            <div class="control-group">
                <label for="nummer" class="control-label">deal NUMMER:</label>
                <div class="controls"><input id="nummer" name="nummer" type="text" value="<?php echo $taObject->getTransactieId();?>" readonly="true"></div>
            </div>

             <div class="control-group">
                <label for="datum" class="control-label">DATUM:</label>
                <div class="controls"><input id="datum" name="datum" type="text" value="<?php makeLocalTime($taObject->getTADatum());?>" readonly="true"></div>
            </div>

            <div class="control-group">
                <label for="koper" class="control-label">Koper:</label>
                <div class="controls"><input id="koper" name="koper" type="text" value="<?php $koper = koperOphalen($taObject->getTransactieId()); echo $koper['LidVoornaam']." ".$koper['LidNaam'];?>" readonly="true"></div>
            </div>

            <div class="control-group">
                <label for="verkoper" class="control-label">Verkoper:</label>
                <div class="controls"><input id="verkoper" name="verkoper" type="text" value="<?php $verkoper = verkoperOphalen($taObject->getTransactieId()); echo $verkoper['LidVoornaam']." ".$verkoper['LidNaam'];?>" readonly="true"></div>
            </div>

            <div class="control-group">
                <label for="status" class="control-label">status:</label>
                <div class="controls"><input id="status" name="status" type="text" value="<?php statusOphalen($taObject->getDealStatusId());?>" readonly="true"></div>
            </div>

            <div class="control-group">
                <label for="prijs" class="control-label">Prijs (€):</label>
                <div class="controls"><input id="prijs" name="prijs" type="text" value="<?php echo $taObject->getOrderBedrag();?>" readonly="true"></div>
            </div>

            <div class="control-group">
                <label for="tpkost" class="control-label">Transport kost (€):</label>
                <div class="controls">
                <input id="tpkost" name="tpkost" type="text" value="<?php echo $taObject->getTransportKost();?>" readonly="true">
                </div>
             </div>

            <div id="divdocs" class="control-group">
                <label for="docs" class="control-label">documenten:</label>
                <div class="controls">
                <?php
                if(count($docsInDealLijst) != 0)
                {
                ?>
                <table id="docsInDealTabel">
                    <thead>
                        <tr>
                        <th>NR.</th>
                        <th>TITEL</th>
                        <th>AUTEUR</th>
                        <th>DOCUMENT TYPE</th>
                        <th>PRIJS</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    <?php
                    foreach ($docsInDealLijst as $did)
                    {
                    $i=$did['docid'];
                    ?>
                    <tr id="<?php echo "didRij".$i ?>">
                        <td id="<?php echo "didId".$i ?>" class="Id"><?php echo $did['docid'] ?></td>
                        <td id="<?php echo "didTitel".$i ?>" class="Titel"><?php echo $did['titel'] ?></td>
                        <td id="<?php echo "didAuteur".$i ?>" class="Auteur"><?php auteurOphalen($did['auteurid']); ?></td>
                        <td id="<?php echo "didType".$i ?>" class="Type"><?php docTypeOphalen($did['doctypeid']); ?></td>
                        <td id="<?php echo "didPrijs".$i ?>" class="Prijs"><?php echo $did['prijs']; ?></td>
                    </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                    </table>
                    <?php
                    }
                    else
                    {
                    ?>
                    <div>De documenten van deze deal zijn gewist, want de verkoper is geen lid meer van de community.</div>
                    <?php
                    }   
                    ?>
                </div>
            </div>
            <input id="idHidden" name="idhidden" type="hidden" value="<?php echo $taObject->getTransactieId(); ?>">
         </form> 
          <div class="push"></div>       
        </div><!--einde container-->
        <div id="footer" class="footer">vzw Onder Ons Lezen</div>   
    </body>
</html>

