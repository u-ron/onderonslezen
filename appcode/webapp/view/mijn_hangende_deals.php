<?php
include('../../helpers/feedback.class.php');    
include('../../helpers/base.class.php');
include('../model/transactie.class.php');
include('../model/transactiepartner.class.php');
include('../help/time.php');
include('../model/lid.class.php');

session_start();

if(!isset($_SESSION['lidstatus']))
{
   header('Location: ../../../index.php');
}
else
{
    $lidStatus = $_SESSION['lidstatus']; 
    include('../help/sessie.class.php');
    Sessie::checkSessionId();
    Sessie::registerLastActivity();//heeft $_SESSION['lidid'] nodig
}

//tbv welcoming
if(isset($_SESSION['lidid']))
{
    $lidObject = new Lid();
    $lidObject->setLidId($_SESSION['lidid']);
    $lid = $lidObject->selectLidById();
}

function koperOphalen($transactieId) {
    $lidObject = new Lid();
    $koper = $lidObject->selectKoperInDeal($transactieId);
    echo $koper['LidVoornaam']." ".$koper['LidNaam'];    
}

function verkoperOphalen($transactieId) {
    $lidObject = new Lid();
    $verkoper = $lidObject->selectVerkoperInDeal($transactieId);
    echo $verkoper['LidVoornaam']." ".$verkoper['LidNaam'];
}

function statusOphalen($dsId)
{
    switch ($dsId)
    {
        case '1':
            echo 'Voorstel'; break;
        case '2':
            echo 'Gewijzigd voorstel'; break;
        case '3':
            echo 'Aanvaard'; break;
        case '4':
            echo 'Afgekeurd'; break;//kan niet voorkomen
        case '5':
            echo 'Betaald'; break;
    }
}

$taObject = new Transactie();
$hangendeDealsLijst = $taObject->selectMijnHangendeDeals($_SESSION['lidid']);

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Mijn hangende deals</title>
        <link rel="stylesheet" href="css/files.css" type="text/css">
        <link rel="stylesheet" href="css/hangendedeals.css" type="text/css">
         <?php include ('../help/jquery.php');?>
        <script type="text/javascript">
            $(document).ready(function () {
                //1. menu
                $("#jMenu").jMenu(
                {
                    ulWidth: '220px',
                    effects: {
                        effectSpeedOpen: 300,
                        effectTypeClose: 'slide'
                    },
                    animatedText: true
                });

                //2. snel sorteren dankzij de expando sortKey
                var tabel = $("#hdTabel");
                $('th', tabel).each(function (columnIndex) {
                    if ($(this).is('.sorteer')) {
                        var col = this;
                        $(this).click(function () {
                            var rijen = tabel.find('tbody > tr');
                            /*vooraf opslaan van keyA en keyB in sortKey*/
                            $.each(rijen, function (index, rij) {

                                if ($(col).is('.alfabet')) {
                                    rij.sortKey = $(rij).children('td').eq(columnIndex).text().toUpperCase();
                                }

                                if ($(col).is('.getal')) {
                                    var waarde = $(rij).children('td').eq(columnIndex).text();
                                    rij.sortKey = parseFloat(waarde);
                                }
                            });

                            rijen.sort(function (a, b) {
                                if (a.sortKey < b.sortKey) return -1;
                                if (a.sortKey > b.sortKey) return 1;
                                return 0;
                            });

                            $.each(rijen, function (index, rij) {
                                tabel.children('tbody').append(rij);
                                rij.sortKey = null;
                            });

                        }); //einde click event
                    } //einde if sorteer


                }); //einde each

                //3. filteren
                $("#filter").change(function () {
                    var tekst = $(this).val();
                    $("tbody tr").hide();
                    $("tbody tr td:contains('" + tekst + "')").parent().show();
                })

                //4. knoppen in actie kolom
                $(".btnedit").button(
                    {
                        icons: { primary: "ui-icon-pencil" }
                    });

                //6.paginatie
                $("#aantalPaginas").change(function () {
                    var ps = $("select option:selected").text();
                    if (ps == "") {
                        $('#hdTabel').datatable('destroy');
                    }
                    else {
                        $('#hdTabel').datatable({
                            pageSize: ps,
                            pagingNumberOfPages: 5
                        });
                    };
                });

                //7.tooltips
                $('.btndelete').tooltip();
                $('.btnedit').tooltip();

            }); //einde ready event

            $(function () {
                $("#sluitinfo").click(function () {
                    $("#rodebalk").hide();
                });
            });

        </script>
    </head>
    <body>
        <div class="container">
        <div class="menuenwelkom">
        <?php include('../help/dashboard.php')?>
        <div class="pull-right">
             <div class="welcoming"><?php if ($lidStatus == 2) {echo "administrator";} elseif($lidStatus == 1) {echo $lid[0]['LidVoornaam']." ".$lid[0]['LidNaam'];} ?></div>
        </div>
        </div>
            <div id="rodebalk" class="alert-info">
                <strong>&nbsp;Mijn hangende deals: <?php echo count($hangendeDealsLijst)?> rijen</strong>
                <button id="sluitinfo" type="button" class="close">&times;</button>    
            </div>
            <br />
            <?php
            if(count($hangendeDealsLijst) != 0)
            {
            ?>
            <div class="row-fluid">
                <label id="paginatie">
                    <select size="1" id="aantalPaginas">
                        <option></option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                    </select>&nbsp;rijen per pagina
                </label>
                <label id="filtering">
                Zoek:&nbsp;<input type="text" id="filter">
                </label>
            </div>
            <table id="hdTabel">
                    <thead>
                        <tr>
                        <th class="sorteer getal Id">DEAL NUMMER</th>
                        <th class="Datum">TRANSACTIE DATUM</th>
                        <th class="sorteer alfabet Koper">KOPER</th>
                        <th class="sorteer alfabet Verkoper">VERKOPER</th>
                        <th class="sorteer getal Bedrag">BEDRAG (€)</th>
                        <th class="sorteer alfabet Status">STATUS</th>
                        <th class="Actie">ACTIE</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    <?php
                    foreach ($hangendeDealsLijst as $hd)//$doc is ééndimensionale rij
                    {
                        $i=$hd['TransactieId'];
                    ?>
                    <tr id="<?php echo "hdRij".$i ?>">
                        <td id="<?php echo "hdId".$i ?>"><?php echo $hd['TransactieId'] ?></td>
                        <td id="<?php echo "hdDatum".$i ?>"><?php makeLocalTime($hd['TADatum']); ?></td>
                        <td id="<?php echo "hdKoper".$i ?>"><?php koperOphalen($hd['TransactieId']); ?></td>
                        <td id="<?php echo "hdVerkoper".$i ?>"><?php verkoperOphalen($hd['TransactieId']); ?></td>
                        <td id="<?php echo "hdBedrag".$i ?>"><?php echo $hd['OrderBedrag']; ?></td>
                        <td id="<?php echo "hdStatus".$i ?>"><?php echo statusOphalen($hd['DealStatusId']); ?></td>
                        <td id="<?php echo "hdActie".$i?>"><form id="<?php echo "hdEdit".$i?>" method="post" action="hangend_deal_formulier.php"><input id="transactieid" name="transactieid" value="<?php echo $i;?>"><button id="<?php echo "hdBtnEdit".$i?>" type="submit" class="btnedit" title="edit"></button></form></td>
                    </tr>
                    <?php
                    }
                    ?>
                    </tbody>
             </table>
             <?php
             }
             ?>
             <div class="paging"></div>
             <div id="mailMessage"><?php if(isset ($_SESSION['mailmessagedeny'])) {echo $_SESSION['mailmessagedeny'];}; unset($_SESSION['mailmessagedeny']);?></div>
             <div class="push"></div>   
        </div>
        <div id="footer" class="footer">vzw Onder Ons Lezen</div>    
    </body>
</html>
