<?php
include('../../helpers/feedback.class.php');    
include('../../helpers/base.class.php');
include('../model/transactie.class.php');
include('../model/transactiepartner.class.php');
include('../help/time.php');
include('../model/lid.class.php');

session_start();

if(!isset($_SESSION['lidstatus']))
{
   header('Location: ../../../index.php');
}
else
{
    $lidStatus = $_SESSION['lidstatus']; 
    include('../help/sessie.class.php');
    Sessie::checkSessionId();
    Sessie::registerLastActivity();//heeft $_SESSION['lidid'] nodig
}

//tbv welcoming
if(isset($_SESSION['lidid']))
{
    $lidObject = new Lid();
    $lidObject->setLidId($_SESSION['lidid']);
    $lid = $lidObject->selectLidById();
}

function koperOphalen($transactieId) {
    $lidObject = new Lid();
    $koper = $lidObject->selectKoperInDeal($transactieId);
    if($koper != NULL)
    {
        echo $koper['LidVoornaam']." ".$koper['LidNaam'];    
    }
    else
    {
        echo "Geen lid meer";
    }
}

function verkoperOphalen($transactieId) {
    $lidObject = new Lid();
    $verkoper = $lidObject->selectVerkoperInDeal($transactieId);
    if($verkoper != NULL)
    {
        echo $verkoper['LidVoornaam']." ".$verkoper['LidNaam'];
    }
    else
    {
        echo "Geen lid meer";
    }
}

function statusOphalen($dsId)
{
    switch ($dsId)
    {
        case '6':
            echo 'Afgehaald'; break;
        case '7':
            echo 'Verzonden'; break;
    }
}

$taObject = new Transactie();
$vorigeDealsLijst = $taObject->selectMijnVorigeDeals($_SESSION['lidid']);

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Mijn vorige deals</title>
        <link rel="stylesheet" href="css/files.css" type="text/css">
        <link rel="stylesheet" href="css/vorigedeals.css" type="text/css">
         <?php include ('../help/jquery.php');?>
        <script type="text/javascript">
            $(document).ready(function () {
                //1. menu
                $("#jMenu").jMenu(
                {
                    ulWidth: '220px',
                    effects: {
                        effectSpeedOpen: 300,
                        effectTypeClose: 'slide'
                    },
                    animatedText: true
                });

                //2. snel sorteren dankzij de expando sortKey
                var tabel = $("#vdTabel");
                $('th', tabel).each(function (columnIndex) {
                    if ($(this).is('.sorteer')) {
                        var col = this;
                        $(this).click(function () {
                            var rijen = tabel.find('tbody > tr');
                            /*vooraf opslaan van keyA en keyB in sortKey*/
                            $.each(rijen, function (index, rij) {

                                if ($(col).is('.alfabet')) {
                                    rij.sortKey = $(rij).children('td').eq(columnIndex).text().toUpperCase();
                                }

                                if ($(col).is('.getal')) {
                                    var waarde = $(rij).children('td').eq(columnIndex).text();
                                    rij.sortKey = parseFloat(waarde);
                                }
                            });

                            rijen.sort(function (a, b) {
                                if (a.sortKey < b.sortKey) return -1;
                                if (a.sortKey > b.sortKey) return 1;
                                return 0;
                            });

                            $.each(rijen, function (index, rij) {
                                tabel.children('tbody').append(rij);
                                rij.sortKey = null;
                            });

                        }); //einde click event
                    } //einde if sorteer


                }); //einde each

                //3. filteren
                $("#filter").change(function () {
                    var tekst = $(this).val();
                    $("tbody tr").hide();
                    $("tbody tr td:contains('" + tekst + "')").parent().show();
                })

                //4. knoppen in actie kolom
                $(".btndelete").button(
                    {
                        icons: { primary: "ui-icon-trash" }
                    });

                $(".btnedit").button(
                    {
                        icons: { primary: "ui-icon-pencil" }
                    });

                //5. deal verwijderen
                $("table").on("click", "button.btndelete", verwijderDeal);

                //6.paginatie
                $("#aantalPaginas").change(function () {
                    var ps = $("select option:selected").text();
                    if (ps == "") {
                        $('#vdTabel').datatable('destroy');
                    }
                    else {
                        $('#vdTabel').datatable({
                            pageSize: ps,
                            pagingNumberOfPages: 5
                        });
                    };
                });

                //7.tooltips
                $('.btndelete').tooltip();
                $('.btnedit').tooltip();

            }); //einde ready event

            $(function () {
                $("#sluitinfo").click(function () {
                    $("#rodebalk").hide();
                });
            });

            function verwijderDeal()
            {
                var btnid = $(this).attr("id"); //attribuut lezen in jQuery
                var id = btnid.substring(11);
                //dialog widget bij verwijderen record
                $("#warningDeletion").dialog(
                {
                buttons: [
                {
                    text: "Ja",
                    click: function () { window.location.href = '../control/vorige_deal.control.php?tpid=' + id; }
                },
                {
                    text: "Nee",
                    click: function () { $(this).dialog("close"); }
                }]
                });
            }//einde verwijderDeal

        </script>
    </head>
    <body>
        <div class="container">
        <div class="menuenwelkom">
        <?php include('../help/dashboard.php')?>
        <div class="pull-right">
             <div class="welcoming"><?php if ($lidStatus == 2) {echo "administrator";} elseif($lidStatus == 1) {echo $lid[0]['LidVoornaam']." ".$lid[0]['LidNaam'];}?></div>
        </div>
        </div>
            <div id="rodebalk" class="alert-info">
                <strong>&nbsp;Mijn vorige deals: <?php echo count($vorigeDealsLijst)?> rijen</strong>
                <button id="sluitinfo" type="button" class="close">&times;</button>    
            </div>
            <br />
            <?php
            if(count($vorigeDealsLijst) != 0)
            {
            ?>
            <div class="row-fluid">
                <label id="paginatie">
                    <select size="1" id="aantalPaginas">
                        <option></option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                    </select>&nbsp;rijen per pagina
                </label>
                <label id="filtering">
                Zoek:&nbsp;<input type="text" id="filter">
                </label>
            </div>
            <table id="vdTabel">
                    <thead>
                        <tr>
                        <th class="sorteer getal Id">DEAL NUMMER</th>
                        <th class="Datum">TRANSACTIE DATUM</th>
                        <th class="sorteer alfabet Koper">KOPER</th>
                        <th class="sorteer alfabet Verkoper">VERKOPER</th>
                        <th class="sorteer getal Bedrag">BEDRAG (€)</th>
                        <th class="sorteer alfabet Status">STATUS</th>
                        <th class="Actie">ACTIE</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    <?php
                    foreach ($vorigeDealsLijst as $vd)//$vd is ééndimensionale rij
                    {
                        $i=$vd['TransactieId'];
                    ?>
                    <tr id="<?php echo "vdRij".$i ?>">
                        <td id="<?php echo "vdId".$i ?>"><?php echo $vd['TransactieId'] ?></td>
                        <td id="<?php echo "vdDatum".$i ?>"><?php makeLocalTime($vd['TADatum']); ?></td>
                        <td id="<?php echo "vdKoper".$i ?>"><?php koperOphalen($vd['TransactieId']); ?></td>
                        <td id="<?php echo "vdVerkoper".$i ?>"><?php verkoperOphalen($vd['TransactieId']); ?></td>
                        <td id="<?php echo "vdBedrag".$i ?>"><?php echo $vd['OrderBedrag']; ?></td>
                        <td id="<?php echo "vdStatus".$i ?>"><?php echo statusOphalen($vd['DealStatusId']); ?></td>
                        <td id="<?php echo "vdActie".$i?>"><a id="<?php echo "vdLinkDelete".$i?>" title="wis"><button id="<?php echo "vdBtnDelete".$vd['TPId']?>" type="button" class="btndelete"></button></a><form id="<?php echo "vdEdit".$i?>" method="post" action="vorig_deal_view.php"><input id="vdid" name="vdid" value="<?php echo $i;?>"><button id="<?php echo "vdBtnEdit".$i?>" type="submit" class="btnedit" title="edit"></button></form></td>
                    </tr>
                    <?php
                    }
                    ?>
                    </tbody>
             </table>
             <?php
             }
             ?>
             <div class="paging"></div>
             <div id="warningDeletion">Bent u zeker om deze deal te verwijderen?</div>
             <div class="push"></div>   
        </div>
        <div id="footer" class="footer">vzw Onder Ons Lezen</div>    
    </body>
</html>
