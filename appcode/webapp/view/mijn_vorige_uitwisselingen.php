<?php
include('../../helpers/feedback.class.php');    
include('../../helpers/base.class.php');
include('../model/transactie.class.php');
include('../model/transactiepartner.class.php');
include('../help/time.php');
include('../model/lid.class.php');

session_start();

if(!isset($_SESSION['lidstatus']))
{
   header('Location: ../../../index.php');
}
else
{
    $lidStatus = $_SESSION['lidstatus']; 
    include('../help/sessie.class.php');
    Sessie::checkSessionId();
    Sessie::registerLastActivity();//heeft $_SESSION['lidid'] nodig
}

//tbv welcoming
if(isset($_SESSION['lidid']))
{
    $lidObject = new Lid();
    $lidObject->setLidId($_SESSION['lidid']);
    $lid = $lidObject->selectLidById();
}

function lenerOphalen($transactieId) {
    $lidObject = new Lid();
    $lener = $lidObject->selectLenerInExchange($transactieId);
    if($lener != NULL)
    {
        echo $lener['LidVoornaam']." ".$lener['LidNaam'];    
    }
    else
    {
        echo "Geen lid meer";
    }
}

function verlenerOphalen($transactieId) {
    $lidObject = new Lid();
    $verlener = $lidObject->selectVerlenerInExchange($transactieId);
    if($verlener != NULL)
    {
        echo $verlener['LidVoornaam']." ".$verlener['LidNaam'];
    }
    else
    {
        echo "Geen lid meer";
    }
}

$taObject = new Transactie();
$vorigeExchangesLijst = $taObject->selectMijnVorigeExchanges($_SESSION['lidid']);

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Mijn vorige uitwisselingen</title>
        <link rel="stylesheet" href="css/files.css" type="text/css">
        <link rel="stylesheet" href="css/vorigeexchanges.css" type="text/css">
         <?php include ('../help/jquery.php');?>
        <script type="text/javascript">
            $(document).ready(function () {
                //1. menu
                $("#jMenu").jMenu(
                {
                    ulWidth: '220px',
                    effects: {
                        effectSpeedOpen: 300,
                        effectTypeClose: 'slide'
                    },
                    animatedText: true
                });

                //2. snel sorteren dankzij de expando sortKey
                var tabel = $("#vexTabel");
                $('th', tabel).each(function (columnIndex) {
                    if ($(this).is('.sorteer')) {
                        var col = this;
                        $(this).click(function () {
                            var rijen = tabel.find('tbody > tr');
                            /*vooraf opslaan van keyA en keyB in sortKey*/
                            $.each(rijen, function (index, rij) {

                                if ($(col).is('.alfabet')) {
                                    rij.sortKey = $(rij).children('td').eq(columnIndex).text().toUpperCase();
                                }

                                if ($(col).is('.getal')) {
                                    var waarde = $(rij).children('td').eq(columnIndex).text();
                                    rij.sortKey = parseFloat(waarde);
                                }
                            });

                            rijen.sort(function (a, b) {
                                if (a.sortKey < b.sortKey) return -1;
                                if (a.sortKey > b.sortKey) return 1;
                                return 0;
                            });

                            $.each(rijen, function (index, rij) {
                                tabel.children('tbody').append(rij);
                                rij.sortKey = null;
                            });

                        }); //einde click event
                    } //einde if sorteer
                }); //einde each

                //3. filteren
                $("#filter").change(function () {
                    var tekst = $(this).val();
                    $("tbody tr").hide();
                    $("tbody tr td:contains('" + tekst + "')").parent().show();
                })

                //4. knoppen in actie kolom
                $(".btndelete").button(
                    {
                        icons: { primary: "ui-icon-trash" }
                    });

                $(".btnedit").button(
                    {
                        icons: { primary: "ui-icon-pencil" }
                    });

                //5. exchange verwijderen
                $("table").on("click", "button.btndelete", verwijderExchange);

                //6.paginatie
                $("#aantalPaginas").change(function () {
                    var ps = $("select option:selected").text();
                    if (ps == "") {
                        $('#vexTabel').datatable('destroy');
                    }
                    else {
                        $('#vexTabel').datatable({
                            pageSize: ps,
                            pagingNumberOfPages: 5
                        });
                    };
                });

                //7.tooltips
                $('.btndelete').tooltip();
                $('.btnedit').tooltip();

            }); //einde ready event

            $(function () {
                $("#sluitinfo").click(function () {
                    $("#rodebalk").hide();
                });
            });

            function verwijderExchange()
            {
                var btnid = $(this).attr("id"); //attribuut lezen in jQuery
                var id = btnid.substring(12);
                //dialog widget bij verwijderen record
                $("#warningDeletion").dialog(
                {
                buttons: [
                {
                    text: "Ja",
                    click: function () { window.location.href = '../control/vorige_exchange.control.php?tpid=' + id; }
                },
                {
                    text: "Nee",
                    click: function () { $(this).dialog("close"); }
                }]
                });
            }//einde verwijderExchange

        </script>
    </head>
    <body>
        <div class="container">
        <div class="menuenwelkom">
        <?php include('../help/dashboard.php')?>
        <div class="pull-right">
             <div class="welcoming"><?php if ($_SESSION['lidstatus'] == 2) {echo "administrator";} elseif($_SESSION['lidstatus'] == 1) {echo $lid[0]['LidVoornaam']." ".$lid[0]['LidNaam'];}?></div>
        </div>
        </div>
            <div id="rodebalk" class="alert-info">
                <strong>&nbsp;Mijn vorige uitwisselingen: <?php echo count($vorigeExchangesLijst)?> rijen</strong>
                <button id="sluitinfo" type="button" class="close">&times;</button>    
            </div>
            <br />
            <?php
            if(count($vorigeExchangesLijst) != 0)
            {
            ?>
            <div class="row-fluid">
                <label id="paginatie">
                    <select size="1" id="aantalPaginas">
                        <option></option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                    </select>&nbsp;rijen per pagina
                </label>
                <label id="filtering">
                Zoek:&nbsp;<input type="text" id="filter">
                </label>
            </div>
            <table id="vexTabel">
                    <thead>
                        <tr>
                        <th class="sorteer getal Id">UITWISSELING NR</th>
                        <th class="Datum">TRANSACTIE DATUM</th>
                        <th class="sorteer alfabet Lener">LENER</th>
                        <th class="sorteer alfabet Verlener">VERLENER</th>
                        <th class="DatumTerug">DATUM TERUG</th>
                        <th class="DueDate">DUE DATUM</th>
                        <th class="Actie">ACTIE</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    <?php
                    foreach ($vorigeExchangesLijst as $vex)//$vex is ééndimensionale rij
                    {
                        $i=$vex['TransactieId'];
                    ?>
                    <tr id="<?php echo "vexRij".$i ?>">
                        <td id="<?php echo "vexId".$i ?>"><?php echo $vex['TransactieId'] ?></td>
                        <td id="<?php echo "vexDatum".$i ?>"><?php makeLocalTime($vex['TADatum']); ?></td>
                        <td id="<?php echo "vexLener".$i ?>"><?php lenerOphalen($vex['TransactieId']); ?></td>
                        <td id="<?php echo "vexVerlener".$i ?>"><?php verlenerOphalen($vex['TransactieId']); ?></td>
                        <td id="<?php echo "vexDatumTerug".$i ?>"><?php makeLocalTime($vex['DatumTerug']); ?></td>
                        <td id="<?php echo "vexDueDate".$i ?>"><?php makeLocalTime($vex['DueDate']);?></td>
                        <td id="<?php echo "vexActie".$i?>"><a id="<?php echo "vexLinkDelete".$i?>" title="wis"><button id="<?php echo "vexBtnDelete".$vex['TPId']?>" type="button" class="btndelete"></button></a><form id="<?php echo "vexEdit".$i?>" method="post" action="vorig_exchange_view.php"><input id="vexid" name="vexid" value="<?php echo $i;?>"><button id="<?php echo "vexdBtnEdit".$i?>" type="submit" class="btnedit" title="edit"></button></form></td>
                    </tr>
                    <?php
                    }
                    ?>
                    </tbody>
             </table>
             <?php
             }
             ?>
             <div class="paging"></div>
             <div id="warningDeletion">Bent u zeker om deze uitwisseling te verwijderen?</div>
             <div class="push"></div>    
        </div>
        <div id="footer" class="footer">vzw Onder Ons Lezen</div>     
    </body>
</html> 
