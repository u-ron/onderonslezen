<?php
include('../../helpers/feedback.class.php');    
include('../../helpers/base.class.php');
include('../model/doc.class.php');
include('../model/registratie.class.php');
include('../model/lid.class.php');

session_start();

if(!isset($_SESSION['lidstatus']))
{
   header('Location: ../../../index.php');
}
else //enkel leden hebben toegang tot document formulier
{
    //ctln of er een documentid in de querystring is
    if(isset($_GET['documentid']))
    {
        //ctln of het de eigenaar is die inkijkt en geen ander lid
        $docId =  $_GET['documentid'];
        $lidId = eigenaarOphalen($docId);//ook bij onbestaande docid wordt er uitgelogd
        if($lidId != $_SESSION['lidid'])//geen eigenaar
        {   
            //sessionid wissen
            include('../help/sessie.class.php');
            $sessieObject1 = new Sessie();
            $sessieObject1->setId(1);
            $sessieObject1->setLidId($_SESSION['lidid']);
            $sessieObject1->setSessionId(NULL);
            $time = time();
            $sessieObject1->setLastActivity($time);
            $sessieObject1->setModifiedBy($_SESSION['username']);
            $sessieObject1->update();

            //gecachte bestanden wissen
            $files = glob('../view/cached/*');//array van bestanden in de cached folder
            foreach($files as $file)
            {
                if(is_file($file))
                    {
                        unlink($file);
                    }    
            }

            //alle sessie variabelen wissen
            session_destroy();
            header('Location: ../../../index.php');
            }
       else//wel eigenaar
       {
                //nagaan of doc niet verkocht is
                $docObject1 = new Doc();
                $docObject1->setDocId($docId);
                if($docObject1->isDocVerkocht() == FALSE)//doc niet verkocht
                {
                    $lidStatus = $_SESSION['lidstatus']; 
                    include('../help/sessie.class.php');
                    Sessie::checkSessionId();
                    Sessie::registerLastActivity();//heeft $_SESSION['lidid'] nodig
                }
                else//doc welverkocht
                {
                    //sessionid wissen
                    include('../help/sessie.class.php');
                    $sessieObject1 = new Sessie();
                    $sessieObject1->setId(1);
                    $sessieObject1->setLidId($_SESSION['lidid']);
                    $sessieObject1->setSessionId(NULL);
                    $time = time();
                    $sessieObject1->setLastActivity($time);
                    $sessieObject1->setModifiedBy($_SESSION['username']);
                    $sessieObject1->update();

                    //gecachte bestanden wissen
                    $files = glob('../view/cached/*');//array van bestanden in de cached folder
                    foreach($files as $file)
                    {
                        if(is_file($file))
                        {
                            unlink($file);
                        }    
                    }

                    //alle sessie variabelen wissen
                    session_destroy();
                    header('Location: ../../../index.php');
                }
            }
        }
        else //leeg formulier getoond
        {
                $lidStatus = $_SESSION['lidstatus']; 
                include('../help/sessie.class.php');
                Sessie::checkSessionId();
                Sessie::registerLastActivity();//heeft $_SESSION['lidid'] nodig
        }
}

//tbv de welcoming
if(isset($_SESSION['lidid']))
{
    $lidObject = new Lid();
    $lidObject->setLidId($_SESSION['lidid']);
    $lid = $lidObject->selectLidById();
}

//auteurs ophalen
include('../model/auteur.class.php');
$auteurObject = new Auteur(); 
$auteurs = $auteurObject->selectAll(); 

//uitgevers ophalen
include('../model/uitgever.class.php');
$uitgeverObject = new Uitgever(); 
$uitgevers = $uitgeverObject->selectAll(); 

//talen ophalen
include('../model/taal.class.php');
$taalObject = new Taal(); 
$talen = $taalObject->selectAll(); 

//doctypes ophalen
include('../model/doctype.class.php');
$docTypeObject = new DocType(); 
$docTypes = $docTypeObject->selectAll(); 

//boektoestanden ophalen
include('../model/toestand.class.php');
$toestandObject = new Toestand(); 
$toestanden = $toestandObject->selectAll(); 

//eigenaar van het getoonde doc ophalen
function eigenaarOphalen($docId)
{
    $registratieObject = new Registratie();
    $registratieObject->setDocId($docId);
    $registratieObject->selectRegistratieByDocId();
    $lidId=$registratieObject->getLidId();
    return $lidId;
}

//nagaan of document in transactie is
function transactieChecken($docId)
{
    $docObject = new Doc();
    $docObject->setDocId($docId);
    $jaOfNee = $docObject->isDocInTransaction();
    return $jaOfNee ;
}


if(isset($_GET['documentid']))//gevuld document formulier tonen
{
    $docObject = new Doc();
    $docId = $_GET['documentid'];
    $docObject->setDocId($docId);
    $docObject->selectDocById();

    $lidId = eigenaarOphalen($docId);
}
else //leeg document formulier tonen
{
    $docObject = new Doc();
    $docObject->setDocId("");
    $docObject->setTitel("");
    $docObject->setDocInfo("");
    $docObject->setJaar("");
    $docObject->setISBN("");
    $docObject->setPrijs("");
    $docObject->setTeLeen("");
    $docObject->setTeKoop("");
    $docObject->setDocTypeId("");
    $docObject->setTaalId("");
    $docObject->setAuteurId("");
    $docObject->setUitgeverId("");
    $docObject->setToestandId("");
    $docId = NULL;//presenteert querystring zonder meer ??
    $lidId = NULL;
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Document formulier</title>
        <link rel="stylesheet" href="css/files.css" type="text/css">
        <link rel="stylesheet" href="css/formulier.css" type="text/css">
        <link rel="stylesheet" href="css/registreer.css" type="text/css">
        <link rel="stylesheet" href="css/tabs.css" type="text/css">
        <?php include ('../help/jquery.php');?>
        <script type="text/javascript">
            function controleerJaar() {
                var q = $("#year").val();
                var patroon = /^(\d{4})$/;
                if (!patroon.test(q)) {
                    return false;
                }
                else {
                    return true;
                }
            }

            function foutBijJaar(zichtbaar) {
                if (zichtbaar && $("#year.foutveld").length == 0) {
                    //plaatst foutmelding en kleurt inputveld geel
                    $("#year").after("<br /><span class='foutmelding'>Het jaar is niet correct.</span><br />");
                    $("#year").addClass("foutveld");
                    $("#year").focus();
                }
                //verwijdert foutmelding en ontkleurt inputveld
                if (!zichtbaar && $("#year.foutveld").length != 0) {
                    $("#year").next().remove(); //verwijdert de eerste br tag
                    $("#year").next().remove();
                    $("#year").next().remove(); //verwijdert de laatste br tag
                    $("#year").removeClass("foutveld");
                }
            }

            function controleerDocInfo() {
                var q = $("#infoDoc").val();
                if (q.length > 255) {
                    return false;
                }
                else {
                    return true;
                }
            }

            function controleerDoel() {
                if (!$('#teKoop').prop('checked') && !$('#teLeen').prop('checked')) {
                    warningTonen();
                    return false;
                }
                else {
                    return true;
                }
            }

            function warningTonen() {
                $("#warningFrm").dialog({
                    autoOpen: false,
                    modal: true,
                    resizable: false,
                    buttons: {
                        "OK": function () {
                            $(this).dialog("close");
                        }
                    }
                });

                $("#warningFrm").dialog("open");
            }


            function foutBijDocInfo(zichtbaar) {
                if (zichtbaar && $("#infoDoc.foutveld").length == 0) {
                    //plaatst foutmelding en kleurt inputveld geel
                    $("#infoDoc").after("<br /><span class='foutmelding'>De info is te lang.</span><br />");
                    $("#infoDoc").addClass("foutveld");
                    $("#infoDoc").focus();
                }
                //verwijdert foutmelding en ontkleurt inputveld
                if (!zichtbaar && $("#infoDoc.foutveld").length != 0) {
                    $("#infoDoc").next().remove(); //verwijdert de eerste br tag
                    $("#infoDoc").next().remove();
                    $("#infoDoc").next().remove(); //verwijdert de laatste br tag
                    $("#infoDoc").removeClass("foutveld");
                }
            }

            $(document).ready(function () {
                //1. hoofdmenu
                $("#jMenu").jMenu(
                {
                    ulWidth: '220px',
                    effects: {
                        effectSpeedOpen: 300,
                        effectTypeClose: 'slide'
                    },
                    animatedText: true
                });

                //2. aanpassingen igv wijzigen
                if (window.location.href.indexOf('?') != '-1') {//nagaan of er een querystring is
                    var alertTekst = $("strong").text();
                    var nieuweString = alertTekst.replace("toevoegen", "wijzigen"); //retourneert een nieuwe string
                    $("strong").text(nieuweString);
                    $("#btnDocSave").text("Wijzigen");
                    $("#btnDocCancel").remove();
                    $("#btnDocSave").attr(
                {
                    id: "btnDocUpdate",
                    name: "btnDocUpdate"
                });
                }

                //3. opslaan en cancel button voorzien van stijl
                $("button[type=submit]").button(
                {
                    icons: { primary: " ui-icon-disk" }
                });
                $("button[type=reset]").button(
                {
                    icons: { primary: " ui-icon-cancel" }
                });

                //4. validatie jaar
                $("#year").change(function () {
                    var correctJaar = controleerJaar();
                    foutBijJaar(!correctJaar);
                });

                //validatie docinfo
                $("#infoDoc").change(function () {
                    var correctInfo = controleerDocInfo();
                    foutBijDocInfo(!correctInfo);
                });

                //validatie tekoop
                $("#teKoop").change(function () {
                    if ($('#teKoop').prop('checked')) {
                        $("#prijs").prop('required', true);
                    }
                    else {
                        $("#prijs").prop('required', false);
                    }
                });

                //validatie doel
                //ander event kan ook: zie start_aankoop.php
                $('#frmDocument').submit(function () {
                return controleerDoel();
                })
            }); //einde ready event

            $(function () {
                $("#sluitinfo").click(function () {
                    $("#rodebalk").hide();
                });
            });


        </script>
    </head>
    <body>
        <div class="container">
        <div class="menuenwelkom">
        <?php include('../help/dashboard.php')?>
        <div class="pull-right">
             <div class="welcoming"><?php if ($lidStatus == 2) {echo "administrator";} elseif($lidStatus == 1) {echo $lid[0]['LidVoornaam']." ".$lid[0]['LidNaam'];} ?></div>
        </div>
        </div>
        <?php include('../help/tabs.php')?>
        <?php 
        if($lidStatus != 0)
        {
        ?>
        <div id="rodebalk" class="alert-info">
            <strong>&nbsp;Document toevoegen</strong>
            <button id="sluitinfo" type="button" class="close">&times;</button>    
        </div>
        <?php
        }     
        ?>
        <p>
            <a href="<?php if($lidStatus != 0){echo "mijn_documenten.php";} else {echo "beschikbare_documenten.php"; };?>" class="buttonterug">&nbsp;Terug</a>
        </p>
        <form id="frmDocument" method="POST" action="../control/document.control.php" class="form-horizontal" enctype="multipart/form-data">
            <?php
                if(isset($_GET['documentid']))
                {
            ?>
            <div class="control-group">
                <label for="docid" class="control-label">doc nr:</label>
                <div class="controls"><input id="docid" name="docid" type="text" value="<?php echo $docObject->getDocId();?>" readonly="true"></div>
            </div>
            <?php
                }
            ?>
            
            <div class="control-group">
                <label for="titelDoc" class="control-label">TITEL:</label>
                <div class="controls"><input id="titelDoc" name="titel" type="text" autofocus="true" value="<?php echo $docObject->getTitel();?>" required style="width: 280px"></div>
            </div>

            <div class="control-group">
            <label for="auteur" class="control-label">AUTEUR:</label><div class="controls">
            <select id="auteur" name="auteurid" style="width: 280px;">
                <option></option>
                <?php
                    foreach($auteurs as $auteur)
                    {
                ?>
                <option value="<?php echo $auteur['AuteurId'];?>" <?php if($docObject->getAuteurId() == $auteur['AuteurId']) {echo "selected";}?>><?php echo $auteur['AuteurNaam']." ".$auteur['AuteurVoornaam'];?></option>
                <?php
                    }
                ?>
            </select>
            </div>
            </div>

            <div class="control-group">
            <label for="uitgever" class="control-label">UITGEVER:</label><div class="controls">
            <select id="uitgever" name="uitgeverid">
                <option></option>
                <?php
                    foreach($uitgevers as $uitgever)
                    {
                ?>
                <option value="<?php echo $uitgever['UitgeverId'];?>" <?php if($docObject->getUitgeverId() == $uitgever['UitgeverId']) {echo "selected";}?>><?php echo $uitgever['Uitgever'];?></option>
                <?php
                    }
                ?>
            </select>
            </div>
            </div>

            <div class="control-group">
            <label for="taal" class="control-label">TAAL:</label>
            <div class="controls">
            <select id="taal" name="taalid" required>
                <option></option>
                <?php
                    foreach($talen as $taal)
                    {
                ?>
                <option value="<?php echo $taal['TaalId'];?>" <?php if($docObject->getTaalId() == $taal['TaalId']) {echo "selected";}?>><?php echo $taal['Taal'];?></option>
                <?php
                    }
                ?>
            </select>
            </div>
            </div>

            <div class="control-group">
            <label for="doctype" class="control-label">DOCUMENT TYPE:</label>
            <div class="controls">
            <select id="doctype" name="doctypeid" required>
                <option></option>
                <?php
                    foreach($docTypes as $dtype)
                    {
                ?>
                <option value="<?php echo $dtype['DocTypeId'];?>" <?php if($docObject->getDocTypeId() == $dtype['DocTypeId']) {echo "selected";}?>><?php echo $dtype['DocType'];?></option>
                <?php
                    }
                ?>
            </select>
            </div>
            </div>

            <div class="control-group">
            <label for="toestand" class="control-label">TOESTAND:</label>
            <div class="controls">
            <select id="toestand" name="toestandid" required>
                <option></option>
                <?php
                    foreach($toestanden as $toestand)
                    {
                ?>
                <option value="<?php echo $toestand['ToestandId'];?>" <?php if($docObject->getToestandId() == $toestand['ToestandId']) {echo "selected";}?>><?php echo $toestand['Toestand'];?></option>
                <?php
                    }
                ?>
            </select>
            </div>
            </div>

            <div class="control-group">
                <label for="infoDoc" class="control-label">INFO:</label>
                <div class="controls"><textarea id="infoDoc" name="infoDoc" rows="4" cols="25" placeholder="max 255 karakters" style="resize: none"><?php echo $docObject->getDocInfo();?></textarea></div>
            </div>

            <div class="control-group">
                <label for="year" class="control-label">JAAR:</label>
                <div class="controls"><input id="year" name="jaar" type="text" value="<?php echo $docObject->getJaar();?>"></div>
            </div>

            <div class="control-group">
                <label for="ISBN" class="control-label">ISBN:</label>
                <div class="controls"><input id="ISBN" name="isbn" type="text" value="<?php echo $docObject->getISBN();?>"></div>
            </div>

            <div class="control-group">
                <label for="prijs" class="control-label">PRIJS (€):</label>
                <div class="controls"><input id="prijs" name="prijs" type="text" value="<?php echo $docObject->getPrijs();?>"></div>
            </div>
            
            <div class="control-group">
                <label for="teKoop" class="control-label">TE KOOP:</label>
                <div class="controls"><div><input id="teKoop" name="tekoop" type="checkbox" value="1" <?php echo ($docObject->getTeKoop() == '1')? 'checked' : '' ;?>></div></div>
            </div>

            <div class="control-group">
                <label for="teLeen" class="control-label">TE LEEN:</label>
                <div class="controls"><div><input id="teLeen" name="teleen" type="checkbox" value="1" <?php echo ($docObject->getTeLeen() == '1')? 'checked' : '' ;?>></div></div>
            </div>

            <!--door de checkbox in een div te plaatsen vervalt de stijlselectie .controls > input-->

            <div class="control-group">
                 <input id="idHidden" name="idhidden" value="<?php echo $docObject->getDocId(); ?>">
            </div>         
            <?php
            if($lidStatus != 0)
            {
            ?>
            <div class="control-group">
                <div class="controls">
                <button id="btnDocSave" name="btnDocSave" type="submit">&nbsp;Opslaan</button>
                <button id="btnDocCancel" type="reset">&nbsp;Annuleren</button>
                </div>
            </div>   
            <?php
            }
            ?>       
         </form>     
         <div id="warningFrm">Kies te koop, te leen of beiden</div>
         <div class="push"></div>   
        </div><!--einde container-->
        <div id="footer" class="footer">vzw Onder Ons Lezen</div>  
    </body>
</html>

