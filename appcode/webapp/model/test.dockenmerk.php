<?php
include('../../helpers/feedback.class.php');
include('../../helpers/base.class.php');
include('dockenmerk.class.php');

/*
$docKenmerk = new DocKenmerk();
$docKenmerk->setDocKenmerk('Dikte');
$docKenmerk->setDocKenmerkValue('2.1 cm');
$docKenmerk->setDocId(1);
$docKenmerk->setAddedBy('admin');
$docKenmerk->insert();
*/

/*
$objectU = new DocKenmerk();
$objectU->setDocKenmerkId(2);
$objectU->setDocKenmerk('Cover');
$objectU->setDocKenmerkValue('Zacht');
$objectU->setDocId(1);
$objectU->setModifiedBy('admin');
$objectU->update();
*/

/*
$objectS = new DocKenmerk();
$kenmerkLijst = $objectS->selectAll();
*/

/*
$objectSId = new DocKenmerk();
$objectSId->setDocKenmerkId(2);
$objectSId->selectDocKenmerkById();
*/

/*
$objectD = new DocKenmerk();
$objectD->setDocKenmerkId(4);
$objectD->delete();
*/

$objectS = new DocKenmerk();
$objectS->setDocId(1);
$docKenmerken = $objectS->selectDocKenmerkByDocId();


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        
        <p>Test selectDocKenmerkByDocId</p>
        <ul>
            <li>Message: <?php echo $objectS->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectS->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectS->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <tr>
                <th>Id</th>
                <th>Kenmerk</th>
                <th>Value</th>
                <th>DocId</th>
                <th>InsertedBy</th>
                <th>InsertedOn</th>
                <th>ModifiedBy</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
                foreach ($docKenmerken as $kenmerk)
                {
            ?>
            <tr>
                <td style="width:  30px"><?php echo $kenmerk['DocKenmerkId'] ?></td>
                <td style="width:  100px"><?php echo $kenmerk['DocKenmerk'] ?></td>
                <td style="width:  100px"><?php echo $kenmerk['DocKenmerkValue'] ?></td>
                <td style="width:  30px"><?php echo $kenmerk['DocId'] ?></td>
                <td style="width:  100px"><?php echo $kenmerk['AddedBy'] ?></td>
                <td style="width:  200px"><?php echo $kenmerk['InsertedOn'] ?></td>
                <td style="width:  100px"><?php echo $kenmerk['ModifiedBy'] ?></td>
                <td style="width:  200px"><?php echo $kenmerk['ModifiedOn'] ?></td>
            </tr>
            <?php
                }
            ?>
        </table>
        

        <!--
        <p>Test delete doc kenmerk</p>
        <ul>
            <li>Message: <?php echo $objectD->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectD->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectD->getErrorCode(); ?></li>
        </ul>
        -->

        <!--
        <p>Test selectDocKenmerkById</p>
        <ul>
            <li>Message: <?php echo $objectSId->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectSId->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectSId->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <tr>
                <th>Id</th>
                <th>Kenmerk</th>
                <th>Value</th>
                <th>DocId</th>
                <th>InsertedBy</th>
                <th>InsertedOn</th>
                <th>ModifiedBy</th>
                <th>ModifiedOn</th>
            </tr>
            <tr>
                <td style="width:  100px"><?php echo $objectSId->getDocKenmerkId(); ?></td>
                <td style="width:  100px"><?php echo $objectSId->getDocKenmerk(); ?></td>
                <td style="width:  100px"><?php echo $objectSId->getDocKenmerkValue(); ?></td>
                <td style="width:  100px"><?php echo $objectSId->getDocId(); ?></td>
                <td style="width:  100px"><?php echo $objectSId->getAddedBy(); ?></td>
                <td style="width:  200px"><?php echo $objectSId->getInsertedOn(); ?></td>
                <td style="width:  100px"><?php echo $objectSId->getModifiedBy(); ?></td>
                <td style="width:  200px"><?php echo $objectSId->getModifiedOn(); ?></td>
            </tr>
        </table>
        -->

        <!--
        <p>Test select all</p>
        <ul>
            <li>Message: <?php echo $objectS->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectS->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectS->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle kenmerken</caption>
            <tr>
                <th>Id</th>
                <th>Kenmerk</th>
                <th>Value</th>
                <th>DocId</th>
                <th>InsertedBy</th>
                <th>InsertedOn</th>
                <th>ModifiedBy</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
                foreach ($kenmerkLijst as $kenmerk)
                {
            ?>
            <tr>
                <td style="width:  30px"><?php echo $kenmerk['DocKenmerkId'] ?></td>
                <td style="width:  100px"><?php echo $kenmerk['DocKenmerk'] ?></td>
                <td style="width:  100px"><?php echo $kenmerk['DocKenmerkValue'] ?></td>
                <td style="width:  30px"><?php echo $kenmerk['DocId'] ?></td>
                <td style="width:  100px"><?php echo $kenmerk['AddedBy'] ?></td>
                <td style="width:  200px"><?php echo $kenmerk['InsertedOn'] ?></td>
                <td style="width:  100px"><?php echo $kenmerk['ModifiedBy'] ?></td>
                <td style="width:  200px"><?php echo $kenmerk['ModifiedOn'] ?></td>
            </tr>
            <?php
                }
            ?>
        </table>
        -->
        <!--
        <p>Test update dockenmerk</p>
        <ul>
            <li>Message: <?php echo $objectU->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectU->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectU->getErrorCode(); ?></li>
        </ul>
        -->
        <!--
        <p>Test insert dockenmerk</p>
        <ul>
            <li>Message: <?php echo $docKenmerk->getFeedback(); ?></li>
            <li>Error message: <?php echo $docKenmerk->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $docKenmerk->getErrorCode(); ?></li>
            <li>ID: <?php echo $docKenmerk->getDocKenmerkId(); ?></li>
        </ul>
        -->

       
        


    </body>
</html>
