<?php
    include('../../helpers/feedback.class.php');
    include('../../helpers/base.class.php');
    include('toestand.class.php');

    $toestandObject = new Toestand();
    $toestanden = $toestandObject->selectAll();

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        <?php 
             foreach($toestanden as $toestand)
             {
                 echo $toestand['ToestandId']." : ".$toestand['Toestand']."<br>";
             }
        ?>
       
    </body>
</html>
