<?php

class Doc extends \Base
{
    private $docId;
    private $titel;
    private $docInfo;
    private $jaar;
    private $iSBN;
    private $prijs;
    private $teLeen;
    private $teKoop;
    private $docTypeId;
    private $taalId;
    private $toestandId;
    private $auteurId;
    private $uitgeverId;

    private $addedBy;
    private $modifiedBy;
    private $insertedOn;
    private $modifiedOn; 

    /*constructor in basisklasse volstaat*/

    /*set $docId
    return true als nt leeg; return false als leeg
    */
    public function setDocId($value)
    {
        if (is_numeric($value))
        {
            $this->docId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /*set $titel
    return true als nt leeg; return false als leeg
    */
    public function setTitel($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->titel=$value;
            return TRUE;
        }
    }

    /*set $docInfo
     return true als nt leeg; return false als leeg
    */
    public function setDocInfo($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->docInfo=$value;
            return TRUE;
        }
    }

    public function setJaar($value)
    {
        if (is_numeric($value))
        {
            $this->jaar=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setISBN($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->iSBN=$value;
            return TRUE;
        }
    }

    public function setPrijs($value)
    {
        if (is_numeric($value))
        {
            $this->prijs=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setTeLeen($value)
    {
        if (is_bool($value))
        {
            $this->teLeen=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setTeKoop($value)
    {
        if (is_bool($value))
        {
            $this->teKoop=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setDocTypeId($value)
    {
        if (is_numeric($value))
        {
            $this->docTypeId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setTaalId($value)
    {
        if (is_numeric($value))
        {
            $this->taalId = $value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setToestandId($value)
    {
        if (is_numeric($value))
        {
            $this->toestandId = $value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setAuteurId($value)
    {
        if (is_numeric($value))
        {
            $this->auteurId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setUitgeverId($value)
    {
        if (is_numeric($value))
        {
            $this->uitgeverId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setAddedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->addedBy=$value;
            return TRUE;
        }
    }

    public function setModifiedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->modifiedBy=$value;
            return TRUE;
        }
    }

    /*setInsertedOn en setModifiedOn zitten vervat in de SQL-statements*/

    public function getDocId()
    {
        return $this->docId; 
    }

    public function getTitel()
    {
         return $this->titel;
    }

    public function getDocInfo()
    {
        return $this->docInfo;
    }

    public function getJaar()
    {
         return $this->jaar;
    }

    public function getISBN()
    {
         return $this->iSBN;
    }

    public function getPrijs()
    {
         return $this->prijs;
    }

    public function getTeLeen()
    {
         return $this->teLeen;
    }

    public function getTeKoop()
    {
         return $this->teKoop;
    }

    public function getDocTypeId()
    {
        return $this->docTypeId; 
    }

    public function getAuteurId()
    {
        return $this->auteurId;
    }

    public function getTaalId()
    {
         return $this->taalId;
    }

    public function getToestandId()
    {
         return $this->toestandId;
    }

    public function getUitgeverId()
    {
        return $this->uitgeverId;
    }

    public function getAddedBy()
    {
        return $this->addedBy;
    }

    public function getInsertedOn()
    {
        return $this->insertedOn;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function getModifiedOn()
    {
         return $this->modifiedOn;
    }


    /*noodzaakt het gebruik vd setmethodes*/
    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function insert()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try
            {
            $preparedStatement = $this->pdo->prepare('call docinsert(@pDocId, :pTitel, :pDocInfo, :pPrijs, :pJaar, :pISBN, :pTeLeen, :pTeKoop, :pDocTypeId, :pTaalId, :pAuteurId, :pUitgeverId, :pToestandId, :pAddedBy)');
            $preparedStatement->bindParam(':pTitel', $this->titel, \PDO::PARAM_STR, 255); 
            $preparedStatement->bindParam(':pDocInfo', $this->docInfo, \PDO::PARAM_STR, 255); 
            $preparedStatement->bindParam(':pPrijs', $this->prijs, \PDO::PARAM_STR, 255);
            $preparedStatement->bindParam(':pJaar', $this->jaar, \PDO::PARAM_INT, 11); 
            $preparedStatement->bindParam(':pISBN', $this->iSBN, \PDO::PARAM_STR, 255); 
            $preparedStatement->bindParam(':pTeLeen', $this->teLeen, \PDO::PARAM_BOOL);
            $preparedStatement->bindParam(':pTeKoop', $this->teKoop, \PDO::PARAM_BOOL);
            $preparedStatement->bindParam(':pDocTypeId', $this->docTypeId, \PDO::PARAM_INT, 11); 
            $preparedStatement->bindParam(':pTaalId', $this->taalId, \PDO::PARAM_INT, 11); 
            $preparedStatement->bindParam(':pAuteurId', $this->auteurId, \PDO::PARAM_INT, 11); 
            $preparedStatement->bindParam(':pUitgeverId', $this->uitgeverId, \PDO::PARAM_INT, 11); 
            $preparedStatement->bindParam(':pToestandId', $this->toestandId, \PDO::PARAM_INT, 11); 
            $preparedStatement->bindParam(':pAddedBy', $this->addedBy, \PDO::PARAM_STR, 255); 
            $success = $preparedStatement->execute(); 
            if ($success == 1)
            {
                $this->setDocId($this->pdo->query('select @pDocId')->fetchColumn()); 
                $this->feedback="Het document met als titel '{$this->titel}' en id <b> " . $this->getDocId() . "</b> is toegevoegd."; 
                $result = TRUE;
            }
            else
            {
                $this->feedback = "Het document is niet toegevoegd";
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
                $result = FALSE;
            }
            }
            catch (\PDOException $e)
            {
            $this->feedback="Het document met als titel '{$this->titel}' is niet toegevoegd."; 
            $this->errorMessage=$e->getMessage();
            $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*methode noodzaakt het gebruik vd andere methodes setDocId, setTitel enz.*/
    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function update()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {

        try
        {
        $preparedStatement=$this->pdo->prepare('call docupdate(:pId, :pTitel, :pDocInfo, :pJaar, :pISBN, :pPrijs, :pTeLeen, :pTeKoop, :pDocTypeId, :pTaalId, :pAuteurId, :pUitgeverId, :pToestandId, :pModifiedBy)');
        $preparedStatement->bindParam(':pId', $this->docId, \PDO::PARAM_INT, 11);
        $preparedStatement->bindParam(':pTitel', $this->titel, \PDO::PARAM_STR, 255); 
        $preparedStatement->bindParam(':pDocInfo', $this->docInfo, \PDO::PARAM_STR, 255); 
        $preparedStatement->bindParam(':pJaar', $this->jaar, \PDO::PARAM_INT, 11); 
        $preparedStatement->bindParam(':pISBN', $this->iSBN, \PDO::PARAM_STR, 255); 
        $preparedStatement->bindParam(':pPrijs', $this->prijs, \PDO::PARAM_STR, 255);
        $preparedStatement->bindParam(':pTeLeen', $this->teLeen, \PDO::PARAM_BOOL);
        $preparedStatement->bindParam(':pTeKoop', $this->teKoop, \PDO::PARAM_BOOL);
        $preparedStatement->bindParam(':pDocTypeId', $this->docTypeId, \PDO::PARAM_INT, 11); 
        $preparedStatement->bindParam(':pTaalId', $this->taalId, \PDO::PARAM_INT, 11); 
        $preparedStatement->bindParam(':pToestandId', $this->toestandId, \PDO::PARAM_INT, 11); 
        $preparedStatement->bindParam(':pAuteurId', $this->auteurId, \PDO::PARAM_INT, 11); 
        $preparedStatement->bindParam(':pUitgeverId', $this->uitgeverId, \PDO::PARAM_INT, 11); 
        $preparedStatement->bindParam(':pModifiedBy', $this->modifiedBy, \PDO::PARAM_STR, 255); 
        $preparedStatement->execute();

        $result = $preparedStatement->rowCount();
        if($result)
        {
            $this->feedback =  "Document {$this->docId} is gewijzigd.";
            $result = TRUE;
        }
        else
        {
            $this->feedback = "Document {$this->docId} is niet gevonden en dus niet gewijzigd.";
            $sQLErrorInfo = $preparedStatement->errorInfo();
            $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
            $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
             $this->feedback = "Document {$this->docId} is niet gewijzigd.";
             $this->errorMessage=$e->getMessage();
             $this->errorCode=$e->getCode();
        }
        $this->close();
        }
        return $result;
    }

    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function delete()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try{
                $preparedStatement = $this->pdo->prepare('call docdelete(:pId)');
                /*in stored procedure staat pId als parameter; hoeft hier niet idem te zijn*/
                $preparedStatement->bindParam(':pId', $this->docId, \PDO::PARAM_INT, 11);
                $preparedStatement->execute();
                $result = $preparedStatement->rowCount();
                if($result)
                {
                $this->feedback = "Document {$this->docId} is verwijderd.";
                $result = TRUE;
                }
                else
                {
                     $this->feedback = "Het document met id = {$this->docId} is niet gevonden en dus niet verwijderd.";
                     $sQLErrorInfo = $preparedStatement->errorInfo();
                     $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                     $this->errorMessage = $sQLErrorInfo[2];
                     $result = FALSE;
                }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "Het document {$this->docId} is niet verwijderd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    /*methode niet gebruikt*/
    public function selectAll()
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call docselectall');
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle documenten ingelezen.';
            }
            else
            {
                $this->feedback = 'De tabel Doc is leeg.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De stored procedure docselectall is niet uitgevoerd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    //retourneert FALSE bij mislukken en een 2dimens array bij slagen
    /*methode noodzaakt het gebruik vd de methode setDocId*/
    public function selectDocById()
    {
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call docselectbyid(:pId)');
        $preparedStatement -> bindParam(':pId', $this->docId, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();
        $this->rowCount = $preparedStatement->rowCount();
        //fetch the output
        if($result = $preparedStatement->fetchAll()) //Returns an array containing all of the result set rows 
        {
            $this->feedback = "{$preparedStatement->rowCount()} rij(en) met id = {$this->docId} in de tabel Doc gevonden.";
            $this->docId = $result[0]['DocId'];
            $this->titel = $result[0]['Titel'];
            $this->docInfo = $result[0]['DocInfo'];
            $this->jaar = $result[0]['Jaar'];
            $this->iSBN = $result[0]['ISBN'];
            $this->prijs = $result[0]['Prijs'];
            $this->teLeen = $result[0]['TeLeen'];
            $this->teKoop = $result[0]['TeKoop'];
            $this->docTypeId = $result[0]['DocTypeId'];
            $this->auteurId = $result[0]['AuteurId'];
            $this->uitgeverId = $result[0]['UitgeverId'];
            $this->taalId = $result[0]['TaalId'];
            $this->toestandId = $result[0]['ToestandId'];
            $this->addedBy = $result[0]['AddedBy'];
            $this->insertedOn = $result[0]['InsertedOn'];
            $this->modifiedBy = $result[0]['ModifiedBy'];
            $this->modifiedOn = $result[0]['ModifiedOn'];
        }
        else //retourneert lege array
        {
               $this->feedback = "Geen rijen met id = {$this->docId} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
                $this->feedback = "Fout => rij is niet gevonden.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
                $this->rowCount = -1;
        }
        $this->close();
        return $result;
        }
         
    }

    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    public function selectKoopwaarVanVerkoper($verkoperId)
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call DocSelectKoopwaarVanVerkoper(:pLidId)');
            $preparedStatement -> bindParam(':pLidId', $verkoperId, \PDO::PARAM_INT, 11);
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle koopwaar ingelezen.';
            }
            else
            {
                $this->feedback = 'Geen records.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De stored procedure docselectkoopwaarverkoper is niet uitgevoerd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    public function selectLeenwaarVanVerlener($verlenerId)
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call DocSelectLeenwaarVanVerLener(:pLidId)');
            $preparedStatement -> bindParam(':pLidId', $verlenerId, \PDO::PARAM_INT, 11);
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle leenwaar ingelezen.';
            }
            else
            {
                $this->feedback = 'Geen records.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De stored procedure docselectleenwaarverlener is niet uitgevoerd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    public function selectDocsInDeal($dealId)
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call DocSelectDocsInDeal(:pTAId)');
            $preparedStatement -> bindParam(':pTAId', $dealId, \PDO::PARAM_INT, 11);
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle docs van de deal ingelezen.';
            }
            else
            {
                $this->feedback = 'Geen records.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De stored procedure DocSelectDocsInDeal is niet uitgevoerd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }
    
    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    public function selectDocsInExchange($exchangeId)
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call DocSelectDocsInExchange(:pTAId)');
            $preparedStatement -> bindParam(':pTAId', $exchangeId, \PDO::PARAM_INT, 11);
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle docs van de exchange ingelezen.';
            }
            else
            {
                $this->feedback = 'Geen records.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De stored procedure DocSelectDocsInExchange is niet uitgevoerd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    public function selectAllAvailable()
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call docselectallavailable');
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle beschikbare documenten ingelezen.';
            }
            else
            {
                $this->feedback = 'Geen beschikbare docs in de tabel Doc.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $ex)
            {
                $this->feedback = "De stored procedure docselectallavailable is niet uitgevoerd.";
                $this->errorMessage=$ex->getMessage();
                $this->errorCode=$ex->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    public function filterAllAvailable($titel, $taalId, $docTypeId)
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call docfilterallavailable(:pTitel, :pTaalId, :pDocTypeId)');
            $preparedStatement->bindParam(':pTitel', $titel, \PDO::PARAM_STR, 255); 
            //$preparedStatement ->bindParam(':pJaar', $jaar, \PDO::PARAM_INT, 11);
            $preparedStatement ->bindParam(':pTaalId', $taalId, \PDO::PARAM_INT, 11);
            //$preparedStatement ->bindParam(':pToestandId', $toestandId, \PDO::PARAM_INT, 11);
            $preparedStatement ->bindParam(':pDocTypeId', $docTypeId, \PDO::PARAM_INT, 11);
            //$preparedStatement ->bindParam(':pLidId', $lidId, \PDO::PARAM_INT, 11);
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle gefilterde beschikbare  documenten ingelezen.';
            }
            else
            {
                $this->feedback = 'Geen docs in de tabel Doc.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $ex)
            {
                $this->feedback = "De stored procedure docfilterallavailable is niet uitgevoerd.";
                $this->errorMessage=$ex->getMessage();
                $this->errorCode=$ex->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*vergt het gebruik vd methode setDocId */
    public function isDocInTransaction()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try
            {
            $preparedStatement = $this->pdo->prepare('call docintransactie(:pDocId)');
            $preparedStatement->bindParam(':pDocId', $this->docId, \PDO::PARAM_INT, 11); 
            $preparedStatement->execute(); 
            $result = $preparedStatement->rowCount();
            if ($result)
            {
                $this->feedback="Het document met als id <b>" . $this->getDocId() . "</b> is in transactie."; 
                $result = TRUE;
            }
            else
            {
                $this->feedback = "Het document is niet in transactie";
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
                $result = FALSE;
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback="De stored procedure docintransactie is niet uitgevoerd."; 
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    public function selectDocumentenPerLid($lidId)
    {
        $result=FALSE;
        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call registratieselectdocumentenperlid(:pLidId)');
            $preparedStatement -> bindParam(':pLidId', $lidId, \PDO::PARAM_INT, 11);
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle documenten ingelezen.';
            }
            else
            {
                $this->feedback = 'De tabel registratie is leeg.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De stored procedure registratieselectdocumentenperlid is niet uitgevoerd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*vergt het gebruik vd methode setDocId */
    public function isDocVerkocht()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try
            {
            $preparedStatement = $this->pdo->prepare('call docverkocht(:pDocId)');
            $preparedStatement->bindParam(':pDocId', $this->docId, \PDO::PARAM_INT, 11); 
            $preparedStatement->execute(); 
            $result = $preparedStatement->rowCount();
            if ($result)
            {
                $this->feedback="Het document met als id <b>" . $this->getDocId() . "</b> is verkocht."; 
                $result = TRUE;
            }
            else
            {
                $this->feedback = "Het document is niet verkocht.";
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
                $result = FALSE;
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback="De stored procedure docverkocht is niet uitgevoerd."; 
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }
}
?>



