<?php

class Uitgever extends \Base
{
    private $uitgeverId;
    private $uitgever;
    private $uitgeverInfo;
    private $addedBy;
    private $modifiedBy;
    private $insertedOn;
    private $modifiedOn; 

    /*constructor in basisklasse volstaat*/

    /*set $uitgeverId
    return true als nt leeg; return false als leeg
    */
    public function setUitgeverId($value)
    {
        if (is_numeric($value))
        {
            $this->uitgeverId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /*set $uitgever
     return true als nt leeg; return false als leeg
    */
    public function setUitgever($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->uitgever=$value;
            return TRUE;
        }
    }

    /*set $uitgeverInfo
     return true als nt leeg; return false als leeg
    */
    public function setUitgeverInfo($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->uitgeverInfo=$value;
            return TRUE;
        }
    }

    public function setAddedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->addedBy=$value;
            return TRUE;
        }
    }

     public function setModifiedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->modifiedBy=$value;
            return TRUE;
        }
    }

    /*setInsertedOn en setModifiedOn zitten vervat in de SQL-statements*/

    public function getUitgeverId()
    {
        return $this->uitgeverId; 
    }

    public function getUitgever()
    {
         return $this->uitgever;
    }

    public function getUitgeverInfo()
    {
        return $this->uitgeverInfo;
    }

    public function getAddedBy()
    {
        return $this->addedBy;
    }

    public function getInsertedOn()
    {
        return $this->insertedOn;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function getModifiedOn()
    {
         return $this->modifiedOn;
    }


    /*noodzaakt het gebruik vd setmethodes*/
    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function insert()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try
            {
            $preparedStatement = $this->pdo->prepare('call uitgeverinsert(@pUitgeverId, :pUitgever, :pUitgeverInfo, :pAddedBy)');
            $preparedStatement->bindParam(':pUitgever', $this->uitgever, \PDO::PARAM_STR, 255); 
            $preparedStatement->bindParam(':pUitgeverInfo', $this->uitgeverInfo, \PDO::PARAM_STR, 255); 
            $preparedStatement->bindParam(':pAddedBy', $this->addedBy, \PDO::PARAM_STR, 255); 
            $success = $preparedStatement->execute(); 
            if ($success == 1)
            {
                $this->setUitgeverId($this->pdo->query('select @pUitgeverId')->fetchColumn()); 
                $this->feedback="De uitgever {$this->uitgever} met id <b> " . $this->getUitgeverId() . "</b> is toegevoegd."; 
                $result = TRUE;
            }
            else
            {
                $this->feedback = "De uitgever is niet toegevoegd";
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
                $result = FALSE;
            }
            }
            catch (\PDOException $e)
            {
            $this->feedback="De uitgever {$this->uitgever} is niet toegevoegd."; 
            $this->errorMessage=$e->getMessage();
            $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function delete()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try{
                $preparedStatement = $this->pdo->prepare('call uitgeverdelete(:pId)');
                /*in stored procedure staat pId als parameter; hoeft hierniet idem te zijn*/
                $preparedStatement->bindParam(':pId', $this->uitgeverId, \PDO::PARAM_INT, 11);
                $preparedStatement->execute();
                $result = $preparedStatement->rowCount();
                if($result)
                {
                $this->feedback = "Uitgever {$this->uitgeverId} is verwijderd.";
                $result = TRUE;
                }
                else
                {
                     $this->feedback = "De uitgever met id = {$this->uitgeverId} is niet gevonden en dus niet verwijderd.";
                     $sQLErrorInfo = $preparedStatement->errorInfo();
                     $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                     $this->errorMessage = $sQLErrorInfo[2];
                     $result = FALSE;
                }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De uitgever {$this->uitgeverId} is niet verwijderd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    public function selectAll()
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call uitgeverselectall');
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle uitgevers ingelezen.';
            }
            else
            {
                $this->feedback = 'De tabel Uitgever is leeg.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De stored procedure uitgeverselectall is niet uitgevoerd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*methode noodzaakt het gebruik vd andere methodes setUitgeverId, setUitgever enz.*/
    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function update()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {

        try
        {
        $preparedStatement=$this->pdo->prepare('call uitgeverupdate(:pId, :pUitgever, :pUitgeverInfo, :pModifiedBy)');
        $preparedStatement->bindParam(':pId', $this->uitgeverId, \PDO::PARAM_INT, 11);
        $preparedStatement->bindParam(':pUitgever', $this->uitgever, \PDO::PARAM_STR, 255);
        $preparedStatement->bindParam(':pUitgeverInfo', $this->uitgeverInfo, \PDO::PARAM_STR, 255); 
        $preparedStatement->bindParam(':pModifiedBy', $this->modifiedBy, \PDO::PARAM_STR, 255); 
        $preparedStatement->execute();

        $result = $preparedStatement->rowCount();
        if($result)
        {
            $this->feedback =  "Uitgever {$this->uitgeverId} is gewijzigd.";
            $result = TRUE;
        }
        else
        {
            $this->feedback = "Uitgever {$this->uitgeverId} is niet gevonden en dus niet gewijzigd.";
            $sQLErrorInfo = $preparedStatement->errorInfo();
            $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
            $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
             $this->feedback = "Uitgever {$this->uitgeverId} is niet gewijzigd.";
             $this->errorMessage=$e->getMessage();
             $this->errorCode=$e->getCode();
        }
        $this->close();
        }
        return $result;
    }

    //retourneert FALSE bij mislukken en een 2dimens array bij slagen
    /*methode noodzaakt het gebruik vd de methode setAuteurId*/
    public function selectUitgeverById()
    {
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call uitgeverselectbyid(:pId)');
        $preparedStatement -> bindParam(':pId', $this->uitgeverId, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();
        $this->rowCount = $preparedStatement->rowCount();
        //fetch the output
        if($result = $preparedStatement->fetchAll()) //Returns an array containing all of the result set rows 
        {
            $this->feedback = "{$preparedStatement->rowCount()} rij(en) met id = {$this->uitgeverId} in de tabel Uitgever gevonden.";
            $this->uitgeverId = $result[0]['UitgeverId'];
            $this->uitgever = $result[0]['Uitgever'];
            $this->uitgeverInfo = $result[0]['UitgeverInfo'];
            $this->addedBy = $result[0]['AddedBy'];
            $this->insertedOn = $result[0]['InsertedOn'];
            $this->modifiedBy = $result[0]['ModifiedBy'];
            $this->modifiedOn = $result[0]['ModifiedOn'];
        }
        else //retourneert lege array
        {
               $this->feedback = "Geen rijen met id = {$this->uitgeverId} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
                $this->feedback = "Fout => rij is niet gevonden.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
                $this->rowCount = -1;
        }
        $this->close();
        return $result;
        }
         
    }

    
    
}
?>



