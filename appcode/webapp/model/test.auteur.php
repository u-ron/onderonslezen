<?php
    include('../../helpers/feedback.class.php');
    include('../../helpers/base.class.php');
    include('auteur.class.php');

    //insert testen
    /*
    $auteur = new Auteur();
    $auteur->setAuteurNaam('Aspe');
    $auteur->setAuteurVoornaam('Pieter');
    $auteur->setAuteurInfo('Bruggeling');
    $auteur->setAddedBy('admin');
    $auteur->insert();
    */

    //insert testen met lange naam: 262 karakters
    /*
    $auteur = new Auteur();
    $auteur->setAuteurNaam('Aspeaaaaaa/aaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa//
    aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa//
    aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaa/aaaaaaaaaa');
    $auteur->setAuteurVoornaam('Pieter');
    $auteur->setAuteurInfo('Bruggeling');
    $auteur->setAddedBy('admin');
    $auteur->insert();
    /*
    Error message: Data too long for column 'pAuteurNaam' at row 1
    Error code: 22001/1406
    */
    

    //update testen
    /*
    $objectU = new Auteur();
    $objectU->setAuteurId(12);
    $objectU->setAuteurNaam('Vermeersch');
    $objectU->setAuteurVoornaam('E');
    $objectU->setAuteurInfo('Van Wetteren');
    //setModifiedBy() hoeft er niet bij; NULL voorzien in databank
    $objectU->update();
    */
    
    //selectAll testen
    /*
    $object = new Auteur();
    $auteursLijst=$object->selectAll();
    */

    //testen van auteurSelectById()
    /*
    $objectS = new Auteur();
    $objectS->setAuteurId(3);
    $result = $objectS->selectAuteurById();
    */

    //delete testen
    /*
    $objectD= new Auteur();
    $objectD->setAuteurId(3);
    $objectD->delete();
    */

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        
        <p>Test update auteur</p>
        <ul>
            <li>Message: <?php echo $objectU->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectU->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectU->getErrorCode(); ?></li>
        </ul>
        
        <!--
        <p>Test delete auteur</p>
        <ul>
            <li>Message: <?php echo $objectD->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectD->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectD->getErrorCode(); ?></li>
        </ul>
        -->

        <!--
        <p>Test selectAuteurById</p>
        <ul>
            <li>Message: <?php echo $objectS->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectS->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectS->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <tr>
                <th>AuteurId</th>
                <th>Naam</th>
                <th>Voornaam</th>
                <th>Info Auteur</th>
                <th>InsertedOn</th>
                <th>ModifiedOn</th>
            </tr>
            <tr>
                <td style="width:  100px"><?php echo $objectS->getAuteurId(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getAuteurNaam(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getAuteurVoornaam(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getAuteurInfo(); ?></td>                                                                    
                <td style="width:  200px"><?php echo $objectS->getInsertedOn(); ?></td>
                <td style="width:  200px"><?php echo $objectS->getModifiedOn(); ?></td>
            </tr>
        </table>
        -->

        <!--
        <p>Test select all</p>
        <ul>
            <li>Message: <?php echo $object->getFeedback(); ?></li>
            <li>Error message: <?php echo $object->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $object->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle auteurs</caption>
            <tr>
                <th>AuteurId</th>
                <th>Naam</th>
                <th>Voornaam</th>
                <th>Info Auteur</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
                foreach ($auteursLijst as $auteur)
                {
                    ?>
            <tr>
                <td style="width:  100px"><?php echo $auteur['AuteurId'] ?></td>
                <td style="width:  100px"><?php echo $auteur['AuteurNaam'] ?></td>
                <td style="width:  100px"><?php echo $auteur['AuteurVoornaam'] ?></td>
                <td style="width:  100px"><?php echo $auteur['AuteurInfo'] ?></td>
                <td style="width:  100px"><?php echo $auteur['AddedBy'] ?></td>
                <td style="width:  200px"><?php echo $auteur['InsertedOn'] ?></td>
                <td style="width:  100px"><?php echo $auteur['ModifiedBy'] ?></td>
                <td style="width:  200px"><?php echo $auteur['ModifiedOn'] ?></td>
            </tr>

            <?php
                }
            ?>
        </table>
        -->

        

        <!--
        <p>Test insert auteur</p>
        <ul>
            <li>Message: <?php echo $auteur->getFeedback(); ?></li>
            <li>Error message: <?php echo $auteur->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $auteur->getErrorCode(); ?></li>
            <li>ID: <?php echo $auteur->getAuteurId(); ?></li>
        </ul>
        -->

        <!--
        <p>Test filteren van auteurs</p>
        <ul>
            <li>Message: <?php echo $objectF->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectF->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectF->getErrorCode(); ?></li>
        </ul>
        <table id="regtabel" border="1">
            <caption>Gefilterde auteurs</caption>
            <tr>
                <th>Id</th>
                <th>Naam</th>
                <th>Voornaam</th>
                <th>Info</th>
                <th>InsertedOn</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
                foreach ($auteursLijst as $auteur)
                {
            ?>
            <tr>
                <td style="width:  100px"><?php echo $auteur['AuteurId'] ?></td>
                <td style="width:  200px"><?php echo $auteur['AuteurNaam'] ?></td>
                <td style="width:  200px"><?php echo $auteur['AuteurVoornaam'] ?></td>
                <td style="width:  100px"><?php echo $auteur['AuteurInfo'] ?></td>
                <td style="width:  200px"><?php echo $auteur['InsertedOn'] ?></td>
                <td style="width:  200px"><?php echo $auteur['ModifiedOn'] ?></td>
            </tr>
            <?php
                }
            ?>
        </table>
        -->


    </body>
</html>
