<?php
    include('../../helpers/feedback.class.php');
    include('../../helpers/base.class.php');
    include('foto.class.php');

    //insert testen
    /*
    $foto = new Foto();
    $foto->setFotoNaam('foto111.png');
    $foto->setURL('images/doc2/');
    $foto->setAddedBy('admin');
    $foto->setDocId(2);
    $foto->insert();
    */

    //update testen
    /*
    $objectU = new Foto();
    $objectU->setFotoId(3);
    $objectU->setFotoNaam('foto33.png');
    $objectU->setURL('images/doc1/');
    $objectU->setDocId(1);
    //setModifiedBy() hoeft er niet bij; NULL voorzien in databank
    $objectU->update();
    */
    
    //selectAll testen
    /*
    $object = new Foto();
    $fotosLijst=$object->selectAll();
    */

    //testen van fotoSelectById()
    /*
    $objectS = new Foto();
    $objectS->setFotoId(3);
    $objectS->selectFotoById();
    */

    //delete testen
    /*
    $objectD= new Foto();
    $objectD->setFotoId(6);
    $objectD->delete();
    */

    //testen van fotoSelectByDocId()
    
    $objectS = new Foto();
    $objectS->setDocId(1);
    $res = $objectS->selectFotoByDocId();
    


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        
        
        <p>Test select fotos by docid</p>
        <ul>
            <li>Message: <?php echo $objectS->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectS->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectS->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle foto's van doc</caption>
            <tr>
                <th>FotoId</th>
                <th>Naam</th>
                <th>URL</th>
                <th>DocId</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
                foreach ($res as $foto)
                {
            ?>
            <tr>
                <td style="width:  10px"><?php echo $foto['FotoId'] ?></td>
                <td style="width:  100px"><?php echo $foto['FotoNaam'] ?></td>
                <td style="width:  100px"><?php echo $foto['URL'] ?></td>
                <td style="width:  10px"><?php echo $foto['DocId'] ?></td>
                <td style="width:  100px"><?php echo $foto['AddedBy'] ?></td>
                <td style="width:  200px"><?php echo $foto['InsertedOn'] ?></td>
                <td style="width:  100px"><?php echo $foto['ModifiedBy'] ?></td>
                <td style="width:  200px"><?php echo $foto['ModifiedOn'] ?></td>
            </tr>

            <?php
            }
            ?>
        </table>
        

        <!--
        <p>Test delete foto</p>
        <ul>
            <li>Message: <?php echo $objectD->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectD->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectD->getErrorCode(); ?></li>
        </ul>
        -->

        <!--
        <p>Test selectFotoById</p>
        <ul>
            <li>Message: <?php echo $objectS->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectS->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectS->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <tr>
                <th>FotoId</th>
                <th>Naam</th>
                <th>URL</th>
                <th>DocId</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <tr>
                <td style="width:  100px"><?php echo $objectS->getFotoId(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getFotoNaam(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getURL(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getDocId(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getAddedBy(); ?></td>                                                                     
                <td style="width:  200px"><?php echo $objectS->getInsertedOn(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getModifiedBy(); ?></td>              
                <td style="width:  200px"><?php echo $objectS->getModifiedOn(); ?></td>
            </tr>
        </table>
        -->
        <!--
        <p>Test select all</p>
        <ul>
            <li>Message: <?php echo $object->getFeedback(); ?></li>
            <li>Error message: <?php echo $object->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $object->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle foto's</caption>
            <tr>
                <th>FotoId</th>
                <th>Naam</th>
                <th>URL</th>
                <th>DocId</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
                foreach ($fotosLijst as $foto)
                {
            ?>
            <tr>
                <td style="width:  10px"><?php echo $foto['FotoId'] ?></td>
                <td style="width:  100px"><?php echo $foto['FotoNaam'] ?></td>
                <td style="width:  100px"><?php echo $foto['URL'] ?></td>
                <td style="width:  10px"><?php echo $foto['DocId'] ?></td>
                <td style="width:  100px"><?php echo $foto['AddedBy'] ?></td>
                <td style="width:  200px"><?php echo $foto['InsertedOn'] ?></td>
                <td style="width:  100px"><?php echo $foto['ModifiedBy'] ?></td>
                <td style="width:  200px"><?php echo $foto['ModifiedOn'] ?></td>
            </tr>

            <?php
            }
            ?>
        </table>
        -->

        <!--
        <p>Test update foto</p>
        <ul>
            <li>Message: <?php echo $objectU->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectU->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectU->getErrorCode(); ?></li>
        </ul>
        -->

        <!--
        <p>Test insert foto</p>
        <ul>
            <li>Message: <?php echo $foto->getFeedback(); ?></li>
            <li>Error message: <?php echo $foto->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $foto->getErrorCode(); ?></li>
            <li>ID: <?php echo $foto->getFotoId(); ?></li>
        </ul>
        -->

        
        

        

       

        

       


    </body>
</html>
