<?php

class Lid extends \Base
{
    protected $lidId;
    protected $gebruikersNaam;
    protected $wachtwoord;
    protected $lidNaam;
    protected $lidVoornaam;
    protected $lidInfo;
    protected $adres;
    protected $telefoon;
    protected $email;
    protected $skypeNaam;
    protected $woonId;
    protected $gesloten;
    protected $lidStatus;
    protected $addedBy;
    protected $insertedOn;
    protected $modifiedBy;
    protected $modifiedOn; 

    /*constructor in basisklasse volstaat*/

    /*set $lidId
    return true als nt leeg; return false als leeg
    */
    public function setLidId($value)
    {
        if (is_numeric($value))
        {
            $this->lidId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /*set $gebruikersNaam
    * return true als nt leeg; return false als leeg
    */
    public function setGebruikersNaam($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->gebruikersNaam=$value;
            return TRUE;
        }
    }

    /*set $wachtwoord
    * return true als nt leeg; return false als leeg
    */
    public function setWachtwoord($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->wachtwoord=$value;
            return TRUE;
        }
    }

    /*set $lidNaam
    * return true als nt leeg; return false als leeg
    */
    public function setLidNaam($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->lidNaam=$value;
            return TRUE;
        }
    }

    /*set $lidVoornaam
    * return true als nt leeg; return false als leeg
    */
    public function setLidVoornaam($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->lidVoornaam=$value;
            return TRUE;
        }
    }

    /*set $lidInfo
    * return true als nt leeg; return false als leeg
    */
    public function setLidInfo($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->lidInfo=$value;
            return TRUE;
        }
    }

    /*set $adres
    * return true als nt leeg; return false als leeg
    */
    public function setAdres($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->adres=$value;
            return TRUE;
        }
    }

    /*set $woonId
    return true als nt leeg; return false als leeg
    */
    public function setWoonId($value)
    {
        if (is_numeric($value))
        {
            $this->woonId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /*set $telefoon
    * return true als nt leeg; return false als leeg
    */
    public function setTelefoon($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->telefoon=$value;
            return TRUE;
        }
    }

    /*set $email
    * return true als nt leeg; return false als leeg
    */
    public function setEmail($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->email=$value;
            return TRUE;
        }
    }

    /*set $skypeNaam
    * return true als nt leeg; return false als leeg
    */
    public function setSkypeNaam($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->skypeNaam=$value;
            return TRUE;
        }
    }

   

    /*set $gesloten
    return true als nt leeg; return false als leeg
    */
    public function setGesloten($value)
    {
        if (is_bool($value))
        {
            $this->gesloten=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /*set $lidStatus
      return true als nt leeg; return false als leeg
      methode wellicht niet nodig
    */
    public function setLidStatus($value)
    {
        if (is_numeric($value))
        {
            $this->lidStatus=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setAddedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->addedBy=$value;
            return TRUE;
        }
    }

     public function setModifiedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->modifiedBy=$value;
            return TRUE;
        }
    }

    /*setInsertedOn en setModifiedOn zitten vervat in de SQL-statements*/

    public function getLidId()
    {
        return $this->lidId; 
    }

    public function getGebruikersNaam()
    {
         return $this->gebruikersNaam;
    }

    public function getWachtwoord()
    {
        return $this->wachtwoord;
    }

    public function getLidNaam()
    {
         return $this->lidNaam;
    }

    public function getLidVoornaam()
    {
         return $this->lidVoornaam;
    }

    public function getLidInfo()
    {
        return $this->lidInfo;
    }

    public function getAdres()
    {
        return $this->adres;
    }

    public function getTelefoon()
    {
        return $this->telefoon;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getSkypeNaam()
    {
         return $this->skypeNaam;
    }

    public function getWoonId()
    {
        return $this->woonId; 
    }

    public function getGesloten()
    {
        return $this->gesloten;
    }

    public function getLidStatus()
    {
        return $this->lidStatus; 
    }

    public function getAddedBy()
    {
        return $this->addedBy;
    }

    public function getInsertedOn()
    {
        return $this->insertedOn;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function getModifiedOn()
    {
         return $this->modifiedOn;
    }


    /*noodzaakt het gebruik vd setmethodes*/
    /*de getmethodes worden aangesproken op het moment van uitvoeren (en dus niet op het moment van coderen) wanneer de ref variabelen hun waarde krijgen*/
    /*retourneert steeds boolean; $lidId ingevuld; ook feedback is voorzien*/
    public function insert()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try
            {
            $preparedStatement = $this->pdo->prepare('call lidinsert(@pLidId, :pGebruikersNaam, :pWachtwoord, :pLidNaam, :pLidVoornaam, :pLidInfo, :pAdres, :pTelefoon, :pEmail, :pSkypeNaam, :pWoonId, :pGesloten, :pLidStatus, :pAddedBy)');
            $preparedStatement->bindParam(':pGebruikersNaam', $this->gebruikersNaam, \PDO::PARAM_STR, 255); 
            $preparedStatement->bindParam(':pWachtwoord', $this->wachtwoord, \PDO::PARAM_STR, 255); 
            $preparedStatement->bindParam(':pLidNaam', $this->lidNaam, \PDO::PARAM_STR, 255);
            $preparedStatement->bindParam(':pLidVoornaam', $this->lidVoornaam, \PDO::PARAM_STR, 255);
            $preparedStatement->bindParam(':pLidInfo', $this->lidInfo, \PDO::PARAM_STR, 255); 
            $preparedStatement->bindParam(':pAdres', $this->adres, \PDO::PARAM_STR, 255); 
            $preparedStatement->bindParam(':pTelefoon', $this->telefoon, \PDO::PARAM_STR, 255); 
            $preparedStatement->bindParam(':pEmail', $this->email, \PDO::PARAM_STR, 255); 
            $preparedStatement->bindParam(':pSkypeNaam', $this->skypeNaam, \PDO::PARAM_STR, 255);
            $preparedStatement->bindParam(':pWoonId', $this->woonId, \PDO::PARAM_INT, 11);
            $preparedStatement->bindParam(':pGesloten', $this->gesloten, \PDO::PARAM_BOOL);
            $preparedStatement->bindValue(':pLidStatus', 1, \PDO::PARAM_INT);
            $preparedStatement->bindParam(':pAddedBy', $this->addedBy, \PDO::PARAM_STR, 255); 
            $success = $preparedStatement->execute(); 
            if ($success)
            {
                $this->setLidId($this->pdo->query('select @pLidId')->fetchColumn()); 
                $this->feedback="Het lid {$this->lidVoornaam} {$this->lidNaam} met id <b> " . $this->getLidId() . "</b> is toegevoegd."; 
                $result = TRUE;
            }
            else
            {
                $this->feedback = "De stored procedure lidinsert is niet geslaagd.";
                $sQLErrorInfo = $preparedStatement->errorInfo();//invulling van een array
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
                $result = FALSE;
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback="Het lid {$this->lidVoornaam} {$this->lidNaam} is niet toegevoegd."; 
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*methode noodzaakt het gebruik vd andere methodes setLidId enz.*/
    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function update()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {

        try
        {
        $preparedStatement=$this->pdo->prepare('call lidupdate(:pId, :pGebruikersNaam, :pWachtwoord, :pLidNaam, :pLidVoornaam, :pLidInfo, :pAdres, :pTelefoon, :pEmail, :pSkypeNaam, :pWoonId, :pGesloten, :pModifiedBy)');
        $preparedStatement->bindParam(':pId', $this->lidId, \PDO::PARAM_INT, 11);
        $preparedStatement->bindParam(':pGebruikersNaam', $this->gebruikersNaam, \PDO::PARAM_STR, 255); 
        $preparedStatement->bindParam(':pWachtwoord', $this->wachtwoord, \PDO::PARAM_STR, 255); 
        $preparedStatement->bindParam(':pLidNaam', $this->lidNaam, \PDO::PARAM_STR, 255);
        $preparedStatement->bindParam(':pLidVoornaam', $this->lidVoornaam, \PDO::PARAM_STR, 255);
        $preparedStatement->bindParam(':pLidInfo', $this->lidInfo, \PDO::PARAM_STR, 255); 
        $preparedStatement->bindParam(':pAdres', $this->adres, \PDO::PARAM_STR, 255); 
        $preparedStatement->bindParam(':pTelefoon', $this->telefoon, \PDO::PARAM_STR, 255); 
        $preparedStatement->bindParam(':pEmail', $this->email, \PDO::PARAM_STR, 255); 
        $preparedStatement->bindParam(':pSkypeNaam', $this->skypeNaam, \PDO::PARAM_STR, 255);
        $preparedStatement->bindParam(':pWoonId', $this->woonId, \PDO::PARAM_INT, 11);
        $preparedStatement->bindParam(':pGesloten', $this->gesloten, \PDO::PARAM_BOOL);
        $preparedStatement->bindParam(':pModifiedBy', $this->modifiedBy, \PDO::PARAM_STR, 255); 
        $preparedStatement->execute();

        $result = $preparedStatement->rowCount();
        if($result)
        {
            $this->feedback =  "Het lid {$this->lidId} is gewijzigd.";
            $result = TRUE;
        }
        else
        {
            $this->feedback = "Het lid {$this->lidId} is niet gevonden en dus niet gewijzigd.";
            $sQLErrorInfo = $preparedStatement->errorInfo();
            $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
            $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
             $this->feedback = "Lid {$this->lidId} is niet gewijzigd.";
             $this->errorMessage=$e->getMessage();
             $this->errorCode=$e->getCode();
        }
        $this->close();
        }
        return $result;
    }

    //retourneert FALSE bij mislukken en een 2dimens array bij slagen evenals invulling van alle variabelen
    /*methode noodzaakt het gebruik vd de methode setLidId*/
    public function selectLidById()
    {
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call lidselectbyid(:pId)');
        $preparedStatement -> bindParam(':pId', $this->lidId, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();
        $this->rowCount = $preparedStatement->rowCount();
        //fetch the output
        if($result = $preparedStatement->fetchAll()) //Returns an array containing all of the result set rows 
        {
            $this->feedback = "{$preparedStatement->rowCount()} rij(en) met id = {$this->lidId} in de tabel Lid gevonden.";
            $this->lidId = $result[0]['LidId'];
            $this->gebruikersNaam = $result[0]['GebruikersNaam'];
            $this->wachtwoord = $result[0]['Wachtwoord'];
            $this->lidNaam = $result[0]['LidNaam'];
            $this->lidVoornaam = $result[0]['LidVoornaam'];
            $this->lidInfo = $result[0]['LidInfo'];
            $this->adres = $result[0]['Adres'];
            $this->telefoon = $result[0]['Telefoon'];
            $this->email = $result[0]['Email'];
            $this->skypeNaam = $result[0]['SkypeNaam'];
            $this->woonId = $result[0]['WoonId'];
            $this->gesloten = $result[0]['Gesloten'];
            $this->lidStatus = $result[0]['LidStatus'];
            $this->addedBy = $result[0]['AddedBy'];
            $this->insertedOn = $result[0]['InsertedOn'];
            $this->modifiedBy = $result[0]['ModifiedBy'];
            $this->modifiedOn = $result[0]['ModifiedOn'];
        }
        else //retourneert lege array
        {
               $this->feedback = "Geen rijen met id = {$this->lidId} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
                $this->feedback = "Fout => rij is niet gevonden.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
                $this->rowCount = -1;
        }
        $this->close();
        return $result;
        }
    }



    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function delete()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try{
                $preparedStatement = $this->pdo->prepare('call liddelete(:pId)');
                /*in stored procedure staat pId als parameter; hoeft hierniet idem te zijn*/
                $preparedStatement->bindParam(':pId', $this->lidId, \PDO::PARAM_INT, 11);
                $preparedStatement->execute();
                $result = $preparedStatement->rowCount();
                if($result)
                {
                $this->feedback = "Lid {$this->lidId} is verwijderd.";
                $result = TRUE;
                }
                else
                {
                     $this->feedback = "Het lid met id = {$this->lidId} is niet gevonden en dus niet verwijderd.";
                     $sQLErrorInfo = $preparedStatement->errorInfo();
                     $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                     $this->errorMessage = $sQLErrorInfo[2];
                     $result = FALSE;
                }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "Het lid {$this->lidId} is niet verwijderd.";
                $this->errorMessage = $e->getMessage();
                $this->errorCode = $e->getCode();
            }
            $this->close();
        }
        return $result;
    }


    //retourneert FALSE bij mislukken en een 2dimens array bij slagen evenals invulling van alle variabelen
    /*methode noodzaakt het gebruik vd de methode setGebruikersNaam*/
    public function selectLidByGebruikersNaam()
    {
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call lidselectbygebruikersnaam(:pGebruikersNaam)');
        $preparedStatement -> bindParam(':pGebruikersNaam', $this->gebruikersNaam,  \PDO::PARAM_STR, 255);
        $preparedStatement->execute();
        $this->rowCount = $preparedStatement->rowCount();
        //fetch the output
        if($result = $preparedStatement->fetchAll()) //Returns an array containing all of the result set rows 
        {
            $this->feedback = "{$preparedStatement->rowCount()} rij(en) met gebruikersnaam = {$this->gebruikersNaam} in de tabel Lid gevonden.";
            $this->lidId = $result[0]['LidId'];
            $this->gebruikersNaam = $result[0]['GebruikersNaam'];
            $this->wachtwoord = $result[0]['Wachtwoord'];
            $this->lidNaam = $result[0]['LidNaam'];
            $this->lidVoornaam = $result[0]['LidVoornaam'];
            $this->lidInfo = $result[0]['LidInfo'];
            $this->adres = $result[0]['Adres'];
            $this->telefoon = $result[0]['Telefoon'];
            $this->email = $result[0]['Email'];
            $this->skypeNaam = $result[0]['SkypeNaam'];
            $this->woonId = $result[0]['WoonId'];
            $this->gesloten = $result[0]['Gesloten'];
            $this->lidStatus = $result[0]['LidStatus'];
            $this->addedBy = $result[0]['AddedBy'];
            $this->insertedOn = $result[0]['InsertedOn'];
            $this->modifiedBy = $result[0]['ModifiedBy'];
            $this->modifiedOn = $result[0]['ModifiedOn'];
        }
        else //retourneert lege array
        {
               $this->feedback = "Geen rijen met gebruikersnaam = {$this->gebruikersNaam} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
                $this->feedback = "Fout => rij is niet gevonden.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
                $this->rowCount = -1;
        }
        $this->close();
        return $result;
        }
    }

    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    public function selectAll()
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call lidselectall');
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle leden ingelezen.';
            }
            else
            {
                $this->feedback = 'De tabel lid is leeg.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $ex)
            {
                $this->feedback = "De stored procedure lidselectall is niet uitgevoerd.";
                $this->errorMessage=$ex->getMessage();
                $this->errorCode=$ex->getCode();
            }
            $this->close();
        }
        return $result;
    }
    
    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    public function selectVerkopers($lidId)
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call lidselectverkopers(:pLidId)');
            $preparedStatement->bindParam(':pLidId', $lidId, \PDO::PARAM_INT, 11);
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle verkopers ingelezen.';
            }
            else
            {
                $this->feedback = 'De tabel lid is leeg.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $ex)
            {
                $this->feedback = "De stored procedure lidselectverkopers is niet uitgevoerd.";
                $this->errorMessage=$ex->getMessage();
                $this->errorCode=$ex->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*retourneert false bij mislukken; bij slagen een 1dim array*/
    public function selectKoperInDeal($transactieId)
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
                $preparedStatement=$this->pdo->prepare('call lidselectkoper(:pTAId)');
                $preparedStatement->bindParam(':pTAId', $transactieId, \PDO::PARAM_INT, 11);
                $preparedStatement->execute();
                if ($result = $preparedStatement->fetch())
                {
                    $this->feedback = 'Koper ingelezen.';
                }
                else
                {
                    $this->feedback = 'Geen koper gevonden.';
                    $sQLErrorInfo = $preparedStatement->errorInfo();
                    $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                    $this->errorMessage = $sQLErrorInfo[2];
                }
            }
            catch (\PDOException $ex)
            {
                $this->feedback = "De stored procedure lidselectkoper is niet uitgevoerd.";
                $this->errorMessage=$ex->getMessage();
                $this->errorCode=$ex->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*retourneert false bij mislukken; bij slagen een 1dim array*/
    public function selectVerkoperInDeal($transactieId)
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
                $preparedStatement=$this->pdo->prepare('call lidselectverkoper(:pTAId)');
                $preparedStatement->bindParam(':pTAId', $transactieId, \PDO::PARAM_INT, 11);
                $preparedStatement->execute();
                if ($result = $preparedStatement->fetch())
                {
                    $this->feedback = 'Verkoper ingelezen.';
                }
                else
                {
                    $this->feedback = 'Geen verkoper gevonden.';
                    $sQLErrorInfo = $preparedStatement->errorInfo();
                    $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                    $this->errorMessage = $sQLErrorInfo[2];
                }
            }
            catch (\PDOException $ex)
            {
                $this->feedback = "De stored procedure lidselectverkoper is niet uitgevoerd.";
                $this->errorMessage=$ex->getMessage();
                $this->errorCode=$ex->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    public function selectVerleners($lidId)
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call lidselectverleners(:pLidId)');
            $preparedStatement->bindParam(':pLidId', $lidId, \PDO::PARAM_INT, 11);
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle verleners ingelezen.';
            }
            else
            {
                $this->feedback = 'De tabel lid is leeg.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $ex)
            {
                $this->feedback = "De stored procedure lidselectverleners is niet uitgevoerd.";
                $this->errorMessage=$ex->getMessage();
                $this->errorCode=$ex->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*retourneert false bij mislukken; bij slagen een 1dim array*/
    public function selectLenerInExchange($transactieId)
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
                $preparedStatement=$this->pdo->prepare('call lidselectlener(:pTAId)');
                $preparedStatement->bindParam(':pTAId', $transactieId, \PDO::PARAM_INT, 11);
                $preparedStatement->execute();
                if ($result = $preparedStatement->fetch())
                {
                    $this->feedback = 'Lener ingelezen.';
                }
                else
                {
                    $this->feedback = 'Geen lener gevonden.';
                    $sQLErrorInfo = $preparedStatement->errorInfo();
                    $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                    $this->errorMessage = $sQLErrorInfo[2];
                }
            }
            catch (\PDOException $ex)
            {
                $this->feedback = "De stored procedure lidselectlener is niet uitgevoerd.";
                $this->errorMessage=$ex->getMessage();
                $this->errorCode=$ex->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*retourneert false bij mislukken; bij slagen een 1dim array*/
    public function selectVerlenerInExchange($transactieId)
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
                $preparedStatement=$this->pdo->prepare('call lidselectverlener(:pTAId)');
                $preparedStatement->bindParam(':pTAId', $transactieId, \PDO::PARAM_INT, 11);
                $preparedStatement->execute();
                if ($result = $preparedStatement->fetch())
                {
                    $this->feedback = 'Verlener ingelezen.';
                }
                else
                {
                    $this->feedback = 'Geen verlener gevonden.';
                    $sQLErrorInfo = $preparedStatement->errorInfo();
                    $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                    $this->errorMessage = $sQLErrorInfo[2];
                }
            }
            catch (\PDOException $ex)
            {
                $this->feedback = "De stored procedure lidselectverlener is niet uitgevoerd.";
                $this->errorMessage=$ex->getMessage();
                $this->errorCode=$ex->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*methode noodzaakt het gebruik vd andere methodes setLidId enz.*/
    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function closeAccount()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {

        try
        {
        $preparedStatement=$this->pdo->prepare('call lidCloseAccount(:pId, :pGesloten, :pModifiedBy)');
        $preparedStatement->bindParam(':pId', $this->lidId, \PDO::PARAM_INT, 11);
        $preparedStatement->bindParam(':pGesloten', $this->gesloten, \PDO::PARAM_INT, 11);
        $preparedStatement->bindParam(':pModifiedBy', $this->modifiedBy, \PDO::PARAM_STR, 255); 
        $preparedStatement->execute();

        $result = $preparedStatement->rowCount();
        if($result)
        {
            $this->feedback =  "Het account {$this->lidId} is gesloten.";
            $result = TRUE;
        }
        else
        {
            $this->feedback = "Het account {$this->lidId} is niet gevonden en dus niet gewijzigd.";
            $sQLErrorInfo = $preparedStatement->errorInfo();
            $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
            $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
             $this->feedback = "Account {$this->lidId} is niet gewijzigd.";
             $this->errorMessage=$e->getMessage();
             $this->errorCode=$e->getCode();
        }
        $this->close();
        }
        return $result;
    }
    

}
?>



