<?php
    include('../../helpers/feedback.class.php');
    include('../../helpers/base.class.php');
    include('dealstatus.class.php');

    $dsObject = new DealStatus();
    $stati = $dsObject->selectAll();

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        <?php 
        foreach($stati as $status)
        {
           echo $status['DealStatusId']." : ".$status['DealStatus']."<br>";
        }
        ?>
       
    </body>
</html>
