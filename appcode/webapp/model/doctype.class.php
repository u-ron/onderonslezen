<?php

class DocType extends \Base
{
    private $docTypeId;
    private $docType;
    private $addedBy;
    private $insertedOn;
    private $modifiedBy;
    private $modifiedOn;

    /*constructor in basisklasse volstaat*/

    /*set $docTypeId
    return true als nt leeg; return false als leeg
    */
    public function setDocTypeId($value)
    {
        if (is_numeric($value))
        {
            $this->docTypeId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /*set $docType
    return true als nt leeg; return false als leeg
    */
    public function setDocType($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->docType=$value;
            return TRUE;
        }
    }

    public function setAddedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->addedBy=$value;
            return TRUE;
        }
    }

    public function setModifiedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->modifiedBy=$value;
            return TRUE;
        }
    }

    /*setInsertedOn en setModifiedOn zitten vervat in de SQL-statements*/

    public function getDocTypeId()
    {
        return $this->docTypeId; 
    }

    public function getDocType()
    {
         return $this->docType;
    }

    public function getAddedBy()
    {
        return $this->addedBy;
    }

    public function getInsertedOn()
    {
        return $this->insertedOn;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function getModifiedOn()
    {
         return $this->modifiedOn;
    }


    /*noodzaakt het gebruik vd setmethodes*/
    /*retourneert boolean*/
    public function insert()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try
            {
            $preparedStatement = $this->pdo->prepare('call doctypeinsert(@pDocTypeId, :pDocType, :pAddedBy)');
            $preparedStatement->bindParam(':pDocType', $this->docType, \PDO::PARAM_STR, 255); 
            $preparedStatement->bindParam(':pAddedBy', $this->addedBy, \PDO::PARAM_STR, 255); 
            $success = $preparedStatement->execute(); 
            if ($success == 1)
            {
                $this->setDocTypeId($this->pdo->query('select @pDocTypeId')->fetchColumn()); 
                $this->feedback="Het document type {$this->docType} met id <b> " . $this->getDocTypeId() . "</b> is toegevoegd."; 
                $result = TRUE;
            }
            else
            {
                $this->feedback = "Het document type is niet toegevoegd";
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
                $result = FALSE;
            }
            }
            catch (\PDOException $e)
            {
            $this->feedback="Het document type {$this->docType} is niet toegevoegd."; 
            $this->errorMessage=$e->getMessage();
            $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*noodzaakt het gebruik van setDocTypeId()*/
    public function delete()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try{
                $preparedStatement = $this->pdo->prepare('call doctypedelete(:pId)');
                $preparedStatement->bindParam(':pId', $this->docTypeId, \PDO::PARAM_INT, 11);
                $preparedStatement->execute();
                $result = $preparedStatement->rowCount();
                if($result)
                {
                $this->feedback = "Het document type {$this->docTypeId} is verwijderd.";
                $result = TRUE;
                }
                else
                {
                     $this->feedback = "Het document type met id = {$this->docTypeId} is niet gevonden en dus niet verwijderd.";
                     $sQLErrorInfo = $preparedStatement->errorInfo();
                     $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                     $this->errorMessage = $sQLErrorInfo[2];
                     $result = FALSE;
                }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "Het document type met id = {$this->docTypeId} is niet verwijderd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    public function selectAll()
    {
        $result=FALSE;
        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call doctypeselectall()');
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle document types ingelezen.';
            }
            else
            {
                $this->feedback = 'De tabel document type is leeg.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De stored procedure DocTypeSelectAll is niet uitgevoerd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*methode noodzaakt het gebruik vd andere set methodes*/
    /*retourneert boolean*/
    public function update()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {

        try
        {
        $preparedStatement=$this->pdo->prepare('call doctypeupdate(:pDocTypeId, :pDocType, :pModifiedBy)');
        $preparedStatement->bindParam(':pDocType', $this->docType, \PDO::PARAM_STR, 255);
        $preparedStatement->bindParam(':pModifiedBy', $this->modifiedBy, \PDO::PARAM_STR, 255);
        $preparedStatement->bindParam(':pDocTypeId', $this->docTypeId, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();

        $result = $preparedStatement->rowCount();
        if($result)
        {
            $this->feedback =  "Het document type {$this->docTypeId} is gewijzigd.";
            $result = TRUE;
        }
        else
        {
            $this->feedback = "Het document type {$this->docTypeId} is niet gevonden en dus niet gewijzigd.";
            $sQLErrorInfo = $preparedStatement->errorInfo();
            $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
            $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
             $this->feedback = "Het document type {$this->docTypeId} is niet gewijzigd.";
             $this->errorMessage=$e->getMessage();
             $this->errorCode=$e->getCode();
        }
        $this->close();
        }
        return $result;
    }

    //retourneert boolean
    /*methode noodzaakt het gebruik vd de andere methode setDocTypeId*/
    public function selectDocTypeById()
    {
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call DocTypeSelectById(:pId)');
        $preparedStatement -> bindParam(':pId', $this->docTypeId, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();
        $this->rowCount = $preparedStatement->rowCount();
        //fetch the output
        if($result = $preparedStatement->fetchAll())
        {
            $this->feedback = "{$preparedStatement->rowCount()} rij(en) met id = {$this->docTypeId} in de tabel DocType gevonden.";
            $this->docTypeId = $result[0]['DocTypeId'];
            $this->docType = $result[0]['DocType'];
            $this->addedBy = $result[0]['AddedBy'];
            $this->insertedOn = $result[0]['InsertedOn'];
            $this->modifiedBy = $result[0]['ModifiedBy'];
            $this->modifiedOn = $result[0]['ModifiedOn'];
        }
        else
        {
               $this->feedback = "Geen rijen met id = {$this->docTypeId} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
                $this->feedback = "Fout => rij is niet gevonden.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
                $this->rowCount = -1;
        }
        $this->close();
        return $result;
        }
         
    }
}
?>



