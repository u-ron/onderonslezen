<?php
    include('../../helpers/feedback.class.php');
    include('../../helpers/base.class.php');
    include('taal.class.php');

    $taalObject = new Taal();
    $talen = $taalObject->selectAll();

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        <?php 
             foreach($talen as $taal)
             {
                 echo $taal['TaalId']." : ".$taal['Taal']."<br>";
             }
        ?>
       
    </body>
</html>