<?php

class Foto extends \Base
{
	private $fotoId;
	private $fotoNaam;
	private $uRL;
	private $docId;
	private $addedBy;
	private $insertedOn;
	private $modifiedBy;
	private $modifiedOn;

	public function setFotoId($value)
	{
		if (is_numeric($value))
        {
            $this->fotoId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
	}

	public function setFotoNaam($value)
	{
		if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->fotoNaam=$value;
            return TRUE;
        }
	}

	public function setURL($value)
	{
		if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->uRL=$value;
            return TRUE;
        }
	}

	public function setDocId($value)
	{
		if (is_numeric($value))
        {
            $this->docId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
	}

	public function setAddedBy($value)
	{
		$this->addedBy = $value;
	}

	public function setModifiedBy($value)
	{
		$this->modifiedBy = $value;
	}

	 /*setInsertedOn en setModifiedOn zitten vervat in de SQL-statements*/

     public function getFotoId()
	{
		return $this->fotoId;
	}

	public function getFotoNaam()
	{
		return $this->fotoNaam;
	}

	public function getURL()
	{
		return $this->uRL;
	}

	public function getDocId()
	{
		return $this->docId;
	}

	public function getAddedBy()
	{
		return $this->addedBy;
	}

	public function getInsertedOn()
	{
		return $this->insertedOn;
	}

	public function getModifiedBy()
	{
		return $this->modifiedBy;
	}

	public function getModifiedOn()
	{
		return $this->modifiedOn;
	}

    public function insert()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try
            {
            $preparedStatement = $this->pdo->prepare('call fotoinsert(@pFotoId, :pFotoNaam, :pURL, :pDocId, :pAddedBy)');
            $preparedStatement->bindParam(':pFotoNaam', $this->fotoNaam, \PDO::PARAM_STR, 255); 
            $preparedStatement->bindParam(':pURL', $this->uRL, \PDO::PARAM_STR, 255);
            $preparedStatement->bindParam(':pDocId', $this->docId, \PDO::PARAM_INT, 11); 
            $preparedStatement->bindParam(':pAddedBy', $this->addedBy, \PDO::PARAM_STR, 255); 
            $success = $preparedStatement->execute(); 
            if ($success == 1)
            {
                $this->setFotoId($this->pdo->query('select @pFotoId')->fetchColumn()); 
                $this->feedback="De foto {$this->fotoNaam} met id <b> " . $this->getFotoId() . "</b> is toegevoegd."; //deel van string in " en deel in ' gaat niet.
                $result = TRUE;
            }
            else
            {
                $this->feedback = "Fout in stored procedure FotoInsert. De foto is niet toegevoegd";
                $sQLErrorInfo = $preparedStatement->errorInfo();//invulling van een array
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
                $result = FALSE;
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback="De foto {$this->fotoNaam} is niet toegevoegd."; 
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }
    
    /*methode noodzaakt het gebruik vd andere methodes setFotoId, setFotoNaam enz.*/
    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function update()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {

        try
        {
        $preparedStatement=$this->pdo->prepare('call fotoupdate(:pId, :pFotoNaam, :pURL, :pDocId, :pModifiedBy)');
        $preparedStatement->bindParam(':pId', $this->fotoId, \PDO::PARAM_INT, 11);
        $preparedStatement->bindParam(':pFotoNaam', $this->fotoNaam, \PDO::PARAM_STR, 255);
        $preparedStatement->bindParam(':pURL', $this->uRL, \PDO::PARAM_STR, 255);
        $preparedStatement->bindParam(':pDocId', $this->docId, \PDO::PARAM_INT, 11);
        $preparedStatement->bindParam(':pModifiedBy', $this->modifiedBy, \PDO::PARAM_STR, 255); 
        $preparedStatement->execute();

        $result = $preparedStatement->rowCount();
        if($result)
        {
            $this->feedback =  "Foto {$this->fotoId} is gewijzigd.";
            $result = TRUE;
        }
        else
        {
            $this->feedback = "Foto {$this->fotoId} is niet gevonden en dus niet gewijzigd.";
            $sQLErrorInfo = $preparedStatement->errorInfo();
            $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
            $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
             $this->feedback = "Foto {$this->fotoId} is niet gewijzigd.";
             $this->errorMessage=$e->getMessage();
             $this->errorCode=$e->getCode();
        }
        $this->close();
        }
        return $result;
    }

    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    /*niet gebruikt*/
    public function selectAll()
    {
        $result=FALSE;
        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call fotoselectall');
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle foto\'s ingelezen.';
            }
            else
            {
                $this->feedback = 'De tabel Foto is leeg.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De stored procedure fotoselectall is niet uitgevoerd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    //retourneert FALSE bij mislukken en een 2dimens array bij slagen
    /*methode noodzaakt het gebruik vd de methode setFotoId*/
    public function selectFotoById()
    {
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call fotoselectbyid(:pId)');
        $preparedStatement -> bindParam(':pId', $this->fotoId, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();
        $this->rowCount = $preparedStatement->rowCount();
        //fetch the output
        if($result = $preparedStatement->fetchAll()) //Returns an array containing all of the result set rows 
        {
            $this->feedback = "{$preparedStatement->rowCount()} rij(en) met id = {$this->fotoId} in de tabel Foto gevonden.";
            $this->fotoId = $result[0]['FotoId'];
            $this->fotoNaam = $result[0]['FotoNaam'];
            $this->uRL = $result[0]['URL'];
            $this->docId = $result[0]['DocId'];
            $this->addedBy = $result[0]['AddedBy'];
            $this->insertedOn = $result[0]['InsertedOn'];
            $this->modifiedBy = $result[0]['ModifiedBy'];
            $this->modifiedOn = $result[0]['ModifiedOn'];
        }
        else //retourneert lege array
        {
               $this->feedback = "Geen rijen met id = {$this->fotoId} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
                $this->feedback = "Fout => rij is niet gevonden.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
                $this->rowCount = -1;
        }
        $this->close();
        return $result;
        }
         
    }

    /*retourneert steeds boolean; ook feedback is voorzien*/
    /*vergt het gebruik vd methode setFotoId*/
    public function delete()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try
            {
                $preparedStatement = $this->pdo->prepare('call fotodelete(:pId)');
                $preparedStatement->bindParam(':pId', $this->fotoId, \PDO::PARAM_INT, 11);
                $preparedStatement->execute();
                $result = $preparedStatement->rowCount();
                if($result)
                {
                $this->feedback = "Foto {$this->fotoId} is verwijderd.";
                $result = TRUE;
                }
                else
                {
                     $this->feedback = "De foto met id = {$this->fotoId} is niet gevonden en dus niet verwijderd.";
                     $sQLErrorInfo = $preparedStatement->errorInfo();
                     $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                     $this->errorMessage = $sQLErrorInfo[2];
                     $result = FALSE;
                }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De foto {$this->fotoId} is niet verwijderd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    //retourneert FALSE bij mislukken en een 2dimens array bij slagen
    /*methode noodzaakt het gebruik vd de methode setDocId*/
    public function selectFotoByDocId()
    {
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call fotoselectbydocid(:pDocId)');
        $preparedStatement -> bindParam(':pDocId', $this->docId, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();
        $this->rowCount = $preparedStatement->rowCount();
        //fetch the output
        if($result = $preparedStatement->fetchAll()) //Returns an array containing all of the result set rows 
        {
            $this->feedback = "{$preparedStatement->rowCount()} rij(en) met id = {$this->docId} in de tabel Foto gevonden.";
            $this->fotoId = $result[0]['FotoId'];
            $this->fotoNaam = $result[0]['FotoNaam'];
            $this->uRL = $result[0]['URL'];
            $this->docId = $result[0]['DocId'];
            $this->addedBy = $result[0]['AddedBy'];
            $this->insertedOn = $result[0]['InsertedOn'];
            $this->modifiedBy = $result[0]['ModifiedBy'];
            $this->modifiedOn = $result[0]['ModifiedOn'];
        }
        else //retourneert lege array
        {
               $this->feedback = "Geen rijen met id = {$this->docId} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
                $this->feedback = "Fout => rij is niet gevonden.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
                $this->rowCount = -1;
        }
        $this->close();
        return $result;
        }
         
    }
}
?>

