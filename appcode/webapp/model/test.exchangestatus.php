<?php
    include('../../helpers/feedback.class.php');
    include('../../helpers/base.class.php');
    include('exchangestatus.class.php');

    $esObject = new ExchangeStatus();
    $stati = $esObject->selectAll();

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        <?php 
        foreach($stati as $status)
        {
           echo $status['ExchangeStatusId']." : ".$status['ExchangeStatus']."<br>";
        }
        ?>
       
    </body>
</html>
