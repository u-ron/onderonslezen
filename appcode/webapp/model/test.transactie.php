<?php
    include('../../helpers/feedback.class.php');
    include('../../helpers/base.class.php');
    include('transactie.class.php');

    //insert testen
    /*
    $ta = new Transactie();
    $ta->setTADatum('2014-10-11');//string in het MySQL formaat van een datum
    $ta->setOrderBedrag(99);//verwacht een numeric
    $ta->setTransportKost(100);//verwacht een numeric
    $ta->setDuedate('2014-10-30');//string in het MySQL formaat van een datum
    $ta->setDatumUit('2014-10-15');//string in het MySQL formaat van een datum
    $ta->setDatumTerug(NULL);//string in het MySQL formaat van een datum
    $ta->setTransactieTypeId(1);
    $ta->setDealStatusId(1);
    $ta->setExchangeStatusId(NULL);
    $ta->setAddedBy('admin');
    $ta->insert();
    */
    

    //update testen
    /*
    $objectU = new Transactie();
    $objectU->setTransactieId(1);
    $objectU->setTADatum('2014-11-11');//string in het MySQL formaat van een datum
    $objectU->setOrderBedrag(9.9);//verwacht een numeric
    $objectU->setTransportKost(10.1);//verwacht een numeric
    $objectU->setDuedate('2014-11-30');//string in het MySQL formaat van een datum
    $objectU->setDatumUit('2014-11-11');//string in het MySQL formaat van een datum
    $objectU->setDatumTerug(NULL);//string in het MySQL formaat van een datum
    $objectU->setTransactieTypeId(1);
    $objectU->setDealStatusId(1);
    $objectU->setExchangeStatusId(NULL);
    $objectU->setModifiedBy("admin");
    $objectU->update();
    */

    //selectall testen
    /*
    $objectS = new Transactie();
    $transactieLijst = $objectS->selectAll();
    */

    //selectById
    /*
    $objectSI = new Transactie();
    $objectSI->setTransactieId(1);
    $objectSI->selectTransactieById();
    */

    //delete testen
    /*
    $objectD = new Transactie();
    $objectD->setTransactieId(2);
    $objectD->delete();
    */

    //selecthangendedeals testen
    /*
    $objectS = new Transactie();
    $hdLijst = $objectS->selectMijnHangendeDeals(13);
    */

    //selectvorigedeals testen
    /*
    $objectS = new Transactie();
    $vdLijst = $objectS->selectMijnVorigeDeals(4);
    */
    
    //selecthangendeexchanges testen
    /*
    $objectS = new Transactie();
    $heLijst = $objectS->selectMijnHangendeExchanges(1);//voorl lid 1
    */

    //selectvorigeverkopen testen
    /*
    $objectS = new Transactie();
    $vvLijst = $objectS->selectMijnVorigeVerkopen(4);
    */

    //countTransactiesBezig testen
    /*
    $objectS = new Transactie();
    $aantal = $objectS->countTransactiesBezig();
    echo $aantal[0]['aantal'];
    */

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        <!--
        <p>Test select mijn vv</p>
        <ul>
            <li>Message: <?php echo $objectS->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectS->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectS->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle vorige verkopen van lid</caption>
            <tr>
                <th>TAId</th>
            </tr>
            <?php
            foreach ($vvLijst as $vv)
            {
            ?>
            <tr>
                <td style="width:  100px"><?php echo $vv['transactieid'] ?></td>
            </tr>
            <?php
            }
            ?>
        </table>
        -->

        <!--
        <p>Test select mijn hex</p>
        <ul>
            <li>Message: <?php echo $objectS->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectS->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectS->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle transacties</caption>
            <tr>
                <th>TAId</th>
                <th>TADatum</th>
                <th>Order bedrag</th>
                <th>Transport kost</th>
                <th>Due date</th>
                <th>Datum uit</ISBN>
                <th>Datum terug</th>
                <th>TAType</th>
                <th>DealStatusId</th>
                <th>ExchangeStatusId</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
            foreach ($heLijst as $ta)
            {
            ?>
            <tr>
                <td style="width:  100px"><?php echo $ta['TransactieId'] ?></td>
                <td style="width:  100px"><?php echo $ta['TADatum'] ?></td>
                <td style="width:  100px"><?php echo $ta['OrderBedrag'] ?></td>
                <td style="width:  100px"><?php echo $ta['TransportKost'] ?></td>
                <td style="width:  100px"><?php echo $ta['DueDate'] ?></td>
                <td style="width:  100px"><?php echo $ta['DatumUit'] ?></td>
                <td style="width:  100px"><?php echo $ta['DatumTerug'] ?></td>
                <td style="width:  100px"><?php echo $ta['TransactieTypeId'] ?></td>
                <td style="width:  100px"><?php echo $ta['DealStatusId'] ?></td>
                <td style="width:  100px"><?php echo $ta['ExchangeStatusId'] ?></td>
                <td style="width:  100px"><?php echo $ta['AddedBy'] ?></td>
                <td style="width:  100px"><?php echo $ta['InsertedOn'] ?></td>
                <td style="width:  100px"><?php echo $ta['ModifiedBy'] ?></td>
                <td style="width:  100px"><?php echo $ta['ModifiedOn'] ?></td>
            </tr>

            <?php
            }
            ?>
        </table>
        -->

        <!--
        <p>Test select mijn vd</p>
        <ul>
            <li>Message: <?php echo $objectS->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectS->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectS->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle transacties</caption>
            <tr>
                <th>TAId</th>
                <th>TADatum</th>
                <th>Order bedrag</th>
                <th>Transport kost</th>
                <th>Due date</th>
                <th>Datum uit</ISBN>
                <th>Datum terug</th>
                <th>TAType</th>
                <th>DealStatusId</th>
                <th>ExchangeStatusId</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
            foreach ($vdLijst as $ta)
            {
            ?>
            <tr>
                <td style="width:  100px"><?php echo $ta['TransactieId'] ?></td>
                <td style="width:  100px"><?php echo $ta['TADatum'] ?></td>
                <td style="width:  100px"><?php echo $ta['OrderBedrag'] ?></td>
                <td style="width:  100px"><?php echo $ta['TransportKost'] ?></td>
                <td style="width:  100px"><?php echo $ta['DueDate'] ?></td>
                <td style="width:  100px"><?php echo $ta['DatumUit'] ?></td>
                <td style="width:  100px"><?php echo $ta['DatumTerug'] ?></td>
                <td style="width:  100px"><?php echo $ta['TransactieTypeId'] ?></td>
                <td style="width:  100px"><?php echo $ta['DealStatusId'] ?></td>
                <td style="width:  100px"><?php echo $ta['ExchangeStatusId'] ?></td>
                <td style="width:  100px"><?php echo $ta['AddedBy'] ?></td>
                <td style="width:  100px"><?php echo $ta['InsertedOn'] ?></td>
                <td style="width:  100px"><?php echo $ta['ModifiedBy'] ?></td>
                <td style="width:  100px"><?php echo $ta['ModifiedOn'] ?></td>
            </tr>

            <?php
            }
            ?>
        </table>
        -->

        <!--
        <p>Test select mijn hd</p>
        <ul>
            <li>Message: <?php echo $objectS->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectS->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectS->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle transacties</caption>
            <tr>
                <th>TAId</th>
                <th>TADatum</th>
                <th>Order bedrag</th>
                <th>Transport kost</th>
                <th>Due date</th>
                <th>Datum uit</ISBN>
                <th>Datum terug</th>
                <th>TAType</th>
                <th>DealStatusId</th>
                <th>ExchangeStatusId</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
            foreach ($hdLijst as $ta)
            {
            ?>
            <tr>
                <td style="width:  100px"><?php echo $ta['TransactieId'] ?></td>
                <td style="width:  100px"><?php echo $ta['TADatum'] ?></td>
                <td style="width:  100px"><?php echo $ta['OrderBedrag'] ?></td>
                <td style="width:  100px"><?php echo $ta['TransportKost'] ?></td>
                <td style="width:  100px"><?php echo $ta['DueDate'] ?></td>
                <td style="width:  100px"><?php echo $ta['DatumUit'] ?></td>
                <td style="width:  100px"><?php echo $ta['DatumTerug'] ?></td>
                <td style="width:  100px"><?php echo $ta['TransactieTypeId'] ?></td>
                <td style="width:  100px"><?php echo $ta['DealStatusId'] ?></td>
                <td style="width:  100px"><?php echo $ta['ExchangeStatusId'] ?></td>
                <td style="width:  100px"><?php echo $ta['AddedBy'] ?></td>
                <td style="width:  100px"><?php echo $ta['InsertedOn'] ?></td>
                <td style="width:  100px"><?php echo $ta['ModifiedBy'] ?></td>
                <td style="width:  100px"><?php echo $ta['ModifiedOn'] ?></td>
            </tr>

            <?php
            }
            ?>
        </table>
        -->

        <!--
        <p>Test delete doc</p>
        <ul>
            <li>Message: <?php echo $objectD->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectD->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectD->getErrorCode(); ?></li>
        </ul>
        -->
        <!--
        <p>Test selectTransactieById</p>
        <ul>
            <li>Message: <?php echo $objectSI->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectSI->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectSI->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <tr>
                <th>TransactieId</th>
                <th>TADatum</th>
                <th>Orderbedrag</th>
                <th>Transport kost</th>
                <th>Due date</th>
                <th>Datum uit</ISBN>
                <th>Datum terug</th>
                <th>TAType</th>
                <th>DealStatusId</th>
                <th>ExchangeStatusId</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <tr>
                <td style="width:  100px"><?php echo $objectSI->getTransactieId(); ?></td>
                <td style="width:  100px"><?php echo $objectSI->getTADatum(); ?></td>
                <td style="width:  100px"><?php echo $objectSI->getOrderBedrag(); ?></td>
                <td style="width:  100px"><?php echo $objectSI->getTransportKost(); ?></td>  
                <td style="width:  100px"><?php echo $objectSI->getDueDate(); ?></td> 
                <td style="width:  100px"><?php echo $objectSI->getDatumUit(); ?></td> 
                <td style="width:  100px"><?php echo $objectSI->getDatumTerug(); ?></td>
                <td style="width:  100px"><?php echo $objectSI->getTransactieTypeId(); ?></td>
                <td style="width:  100px"><?php echo $objectSI->getDealStatusId(); ?></td>  
                <td style="width:  100px"><?php echo $objectSI->getExchangeStatusId(); ?></td> 
                <td style="width:  200px"><?php echo $objectSI->getAddedBy(); ?></td>                                                                
                <td style="width:  200px"><?php echo $objectSI->getInsertedOn(); ?></td>
                <td style="width:  200px"><?php echo $objectSI->getModifiedBy(); ?></td>     
                <td style="width:  200px"><?php echo $objectSI->getModifiedOn(); ?></td>
            </tr>
        </table>
        -->

        <!--
        <p>Test select all</p>
        <ul>
            <li>Message: <?php echo $objectS->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectS->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectS->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle transacties</caption>
            <tr>
                <th>TAId</th>
                <th>TADatum</th>
                <th>Order bedrag</th>
                <th>Transport kost</th>
                <th>Due date</th>
                <th>Datum uit</ISBN>
                <th>Datum terug</th>
                <th>TAType</th>
                <th>DealStatusId</th>
                <th>ExchangeStatusId</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
            foreach ($transactieLijst as $ta)
            {
            ?>
            <tr>
                <td style="width:  100px"><?php echo $ta['TransactieId'] ?></td>
                <td style="width:  100px"><?php echo $ta['TADatum'] ?></td>
                <td style="width:  100px"><?php echo $ta['OrderBedrag'] ?></td>
                <td style="width:  100px"><?php echo $ta['TransportKost'] ?></td>
                <td style="width:  100px"><?php echo $ta['DueDate'] ?></td>
                <td style="width:  100px"><?php echo $ta['DatumUit'] ?></td>
                <td style="width:  100px"><?php echo $ta['DatumTerug'] ?></td>
                <td style="width:  100px"><?php echo $ta['TransactieTypeId'] ?></td>
                <td style="width:  100px"><?php echo $ta['DealStatusId'] ?></td>
                <td style="width:  100px"><?php echo $ta['ExchangeStatusId'] ?></td>
                <td style="width:  100px"><?php echo $ta['AddedBy'] ?></td>
                <td style="width:  100px"><?php echo $ta['InsertedOn'] ?></td>
                <td style="width:  100px"><?php echo $ta['ModifiedBy'] ?></td>
                <td style="width:  100px"><?php echo $ta['ModifiedOn'] ?></td>
            </tr>

            <?php
            }
            ?>
        </table>
        -->
        <!--
        <p>Test update doc</p>
        <ul>
            <li>Message: <?php echo $objectU->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectU->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectU->getErrorCode(); ?></li>
        </ul>
        -->
        <!--
        <p>Test insert ta</p>
        <ul>
            <li>Message: <?php echo $ta->getFeedback(); ?></li>
            <li>Error message: <?php echo $ta->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $ta->getErrorCode(); ?></li>
            <li>ID: <?php echo $ta->getTransactieId(); ?></li>
        </ul>
        --> 
    </body>
</html>
