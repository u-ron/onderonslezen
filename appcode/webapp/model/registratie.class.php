<?php

class Registratie extends \Base
{
	private $regId;
	private $lidId;
	private $docId;
	private $addedBy;
	private $insertedOn;
	private $modifiedBy;
	private $modifiedOn;

	public function setRegId($value)
	{
		if (is_numeric($value))
        {
            $this->regId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
	}

	public function setLidId($value)
	{
		if (is_numeric($value))
        {
            $this->lidId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
	}

	public function setDocId($value)
	{
		if (is_numeric($value))
        {
            $this->docId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
	}

	public function setAddedBy($value)
	{
		$this->addedBy = $value;
	}

	public function setModifiedBy($value)
	{
		$this->modifiedBy = $value;
	}

	 /*setInsertedOn en setModifiedOn zitten vervat in de SQL-statements*/

     public function getRegId()
	{
		return $this->regId;
	}

	public function getLidId()
	{
		return $this->lidId;
	}

	public function getDocId()
	{
		return $this->docId;
	}

	public function getAddedBy()
	{
		return $this->addedBy;
	}

	public function getInsertedOn()
	{
		return $this->insertedOn;
	}

	public function getModifiedBy()
	{
		return $this->modifiedBy;
	}

	public function getModifiedOn()
	{
		return $this->modifiedOn;
	}

    public function insert()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try
            {
            $preparedStatement = $this->pdo->prepare('call registratieinsert(@pRegId, :pLidId, :pDocId, :pAddedBy)');
            $preparedStatement->bindParam(':pLidId', $this->lidId, \PDO::PARAM_INT, 11); 
            $preparedStatement->bindParam(':pDocId', $this->docId, \PDO::PARAM_INT, 11); 
            $preparedStatement->bindParam(':pAddedBy', $this->addedBy, \PDO::PARAM_STR, 255); 
            $success = $preparedStatement->execute(); 
            if ($success)
            {
                $this->setRegId($this->pdo->query('select @pRegId')->fetchColumn()); 
                $this->feedback="De registratie met id <b> " . $this->getRegId() . "</b> is toegevoegd."; 
                $result = TRUE;
            }
            else
            {
                $this->feedback = "Fout in stored procedure RegistratieInsert.";
                $sQLErrorInfo = $preparedStatement->errorInfo();//invulling van een array
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
                $result = FALSE;
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback="De registratie {$this->regId} is niet toegevoegd."; 
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }
    
    /*methode noodzaakt het gebruik vd andere methodes setRegId enz.*/
    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function update()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {

        try
        {
        $preparedStatement=$this->pdo->prepare('call registratieupdate(:pId, :pLidId, :pDocId, :pModifiedBy)');
        $preparedStatement->bindParam(':pId', $this->regId, \PDO::PARAM_INT, 11);
        $preparedStatement->bindParam(':pLidId', $this->lidId, \PDO::PARAM_INT, 11);
        $preparedStatement->bindParam(':pDocId', $this->docId, \PDO::PARAM_INT, 11);
        $preparedStatement->bindParam(':pModifiedBy', $this->modifiedBy, \PDO::PARAM_STR, 255); 
        $preparedStatement->execute();

        $result = $preparedStatement->rowCount();
        if($result)
        {
            $this->feedback =  "Registratie {$this->regId} is gewijzigd.";
            $result = TRUE;
        }
        else
        {
            $this->feedback = "Registratie {$this->regId} is niet gevonden en dus niet gewijzigd.";
            $sQLErrorInfo = $preparedStatement->errorInfo();
            $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
            $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
             $this->feedback = "Registratie {$this->regId} is niet gewijzigd.";
             $this->errorMessage = $e->getMessage();
             $this->errorCode = $e->getCode();
        }
        $this->close();
        }
        return $result;
    }

    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    /*niet gebruikt*/
    public function selectAll()
    {
        $result=FALSE;
        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call registratieselectall');
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle registraties ingelezen.';
            }
            else
            {
                $this->feedback = 'De tabel registratie is leeg.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De stored procedure registratieselectall is niet uitgevoerd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    //retourneert FALSE bij mislukken en een 2dimens array bij slagen
    /*methode noodzaakt het gebruik vd de methode setLidId*/
    public function selectRegistratieByLidId()
    {
        $this->errorCode = 'none';
        $this->errorMessage = 'none';
        $this->feedback = 'none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call registratieselectbylidid(:pLidId)');
        $preparedStatement -> bindParam(':pLidId', $this->lidId, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();
        $this->rowCount = $preparedStatement->rowCount();
        //fetch the output
        if($result = $preparedStatement->fetchAll()) //Returns an array containing all of the result set rows 
        {
            $this->feedback = "{$preparedStatement->rowCount()} rij(en) met id = {$this->lidId} in de tabel Registratie gevonden.";
            $this->regId = $result[0]['RegId'];
            $this->lidId = $result[0]['LidId'];
            $this->docId = $result[0]['DocId'];
            $this->addedBy = $result[0]['AddedBy'];
            $this->insertedOn = $result[0]['InsertedOn'];
            $this->modifiedBy = $result[0]['ModifiedBy'];
            $this->modifiedOn = $result[0]['ModifiedOn'];
        }
        else //retourneert lege array
        {
               $this->feedback = "Geen rijen met id = {$this->lidId} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
                $this->feedback = "Fout => rij is niet gevonden.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
                $this->rowCount = -1;
        }
        $this->close();
        return $result;
        }
    }

    //retourneert FALSE bij mislukken en een 2dimens array bij slagen
    /*methode noodzaakt het gebruik vd de methode setDocId*/
    public function selectRegistratieByDocId()
    {
        $this->errorCode = 'none';
        $this->errorMessage = 'none';
        $this->feedback = 'none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call registratieselectbydocid(:pDocId)');
        $preparedStatement -> bindParam(':pDocId', $this->docId, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();
        $this->rowCount = $preparedStatement->rowCount();
        //fetch the output
        if($result = $preparedStatement->fetchAll()) //Returns an array containing all of the result set rows 
        {
            $this->feedback = "{$preparedStatement->rowCount()} rij(en) met id = {$this->docId} in de tabel Registratie gevonden.";
            $this->regId = $result[0]['RegId'];
            $this->lidId = $result[0]['LidId'];
            $this->docId = $result[0]['DocId'];
            $this->addedBy = $result[0]['AddedBy'];
            $this->insertedOn = $result[0]['InsertedOn'];
            $this->modifiedBy = $result[0]['ModifiedBy'];
            $this->modifiedOn = $result[0]['ModifiedOn'];
        }
        else //retourneert lege array
        {
               $this->feedback = "Geen rijen met id = {$this->docId} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
                $this->feedback = "Fout => rij is niet gevonden.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
                $this->rowCount = -1;
        }
        $this->close();
        return $result;
        }
    }

    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function delete()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try
            {
                $preparedStatement = $this->pdo->prepare('call registratiedelete(:pId)');
                $preparedStatement->bindParam(':pId', $this->regId, \PDO::PARAM_INT, 11);
                $preparedStatement->execute();
                $result = $preparedStatement->rowCount();
                if($result)
                {
                $this->feedback = "Registratie {$this->regId} is verwijderd.";
                $result = TRUE;
                }
                else
                {
                     $this->feedback = "De reg met id = {$this->regId} is niet gevonden en dus niet verwijderd.";
                     $sQLErrorInfo = $preparedStatement->errorInfo();
                     $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                     $this->errorMessage = $sQLErrorInfo[2];
                     $result = FALSE;
                }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De reg {$this->regId} is niet verwijderd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    
}
?>

