<?php
    include('../../helpers/feedback.class.php');
    include('../../helpers/base.class.php');
    include('lid.class.php');

    //insert testen
    /*
    $lid = new Lid();
    $lid->setGebruikersNaam('admin');
    $lid->setWachtwoord('1234');
    $lid->setLidNaam('Eelbode');
    $lid->setLidVoornaam('Ronald');
    $lid->setLidInfo('enige administrator');
    $lid->setAdres('Stationsplein 29');
    $lid->setTelefoon('0477/909831');
    $lid->setEmail('ronald.eelbode@telenet.be');
    $lid->setSkypeNaam('ronald.eelbode');
    $lid->setWoonId(1);
    $lid->setGesloten(false);
    //lidStatus wordt voorlopig op 1 gezet;//1 is gewoon lid; 2 is administrator
    $lid->setAddedBy('admin');
    $lid->insert();
    */

    //update testen
    /*
    $objectU = new Lid();
    $objectU->setLidId('1');
    $objectU->setGebruikersNaam('Pol');
    $objectU->setWachtwoord('Pol123');
    $objectU->setLidNaam('Polens');
    $objectU->setLidVoornaam('Pol');
    $objectU->setLidInfo('Pool');
    $objectU->setAdres('Polenplein 1');
    $objectU->setTelefoon('051784512');
    $objectU->setEmail('pp@gmail.com');
    $objectU->setSkypeNaam('pol.polens');
    $objectU->setWoonId('1');
    $objectU->setGesloten(false);
    $objectU->setModifiedBy('admin');
    $objectU->update();
    */
    
    //selectAll testen
    /*
    $object = new Lid();
    $ledenLijst=$object->selectAll();
    */

    //testen van LidSelectById()
    /*
    $objectS = new Lid();
    $objectS->setLidId('2');
    $result = $objectS->selectLidById();
    */

    //delete testen
    /*
    $objectD= new Lid();
    $objectD->setLidId(2);
    $objectD->delete();
    */

    //testen van selectLidByGebruikersNaam()
    /*
    $objectS = new Lid();
    $objectS->setGebruikersNaam('admin');
    $result = $objectS->selectLidByGebruikersNaam();
    */

    //selectVerkopers testen
    /*
    $object = new Lid();
    $verkopersLijst=$object->selectVerkopers();
    */

    //test selectKoperInDeal
    
    $object = new Lid();
    $transactieId = 16;
    $result = $object->selectKoperInDeal($transactieId);
    echo $result[0];
    

    //test selectVerkoperInDeal
    
    $object = new Lid();
    $transactieId = 16;
    $result = $object->selectVerkoperInDeal($transactieId);
     echo $result[0];
    
    //selectVerleners testen
    /*
    $object = new Lid();
    $verlenersLijst=$object->selectVerleners(9);
    */

    //test selectLenerInExchange
    /*
    $object = new Lid();
    $transactieId = 10;
    $result = $object->selectLenerInExchange($transactieId);
    */

    //test selectVerlenerInExchange
    /*
    $object = new Lid();
    $transactieId = 10;
    $result = $object->selectVerlenerInExchange($transactieId);
    */
    /*
    $objectU = new Lid();
    $objectU->setLidId(14);
    $objectU->setGesloten(TRUE);
    $objectU->setModifiedBy('admin');
    $objectU->closeAccount();
    */
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        <!--
        <p>Test close account</p>
        <ul>
            <li>Message: <?php echo $objectU->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectU->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectU->getErrorCode(); ?></li>
        </ul>
        -->

        <!--
        <p>Test select(Ver)lenerInEx</p>
        <ul>
            <li>Message: <?php echo $object->getFeedback(); ?></li>
            <li>Error message: <?php echo $object->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $object->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <tr>
                <th>Naam</th>
                <th>Voornaam</th>
            </tr>
            <tr>
                <td style="width:  100px"><?php echo $result['LidNaam']; ?></td>
                <td style="width:  100px"><?php echo $result['LidVoornaam']; ?></td>
            </tr>
        </table>
        -->

        <!--
        <p>Test select verleners</p>
        <ul>
            <li>Message: <?php echo $object->getFeedback(); ?></li>
            <li>Error message: <?php echo $object->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $object->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle verleners</caption>
            <tr>
                <th>LidId</th>
                <th>Naam</th>
                <th>Voornaam</th>
            </tr>
            <?php
            foreach ($verlenersLijst as $verlener)
            {
            ?>
            <tr>
                <td style="width:  10px"><?php echo $verlener['LidId'] ?></td>
                <td style="width:  100px"><?php echo $verlener['LidNaam'] ?></td>
                <td style="width:  100px"><?php echo $verlener['LidVoornaam'] ?></td>
            </tr>
            <?php
            }
            ?>
        </table>
        -->

        <!--
        <p>Test select verkopers</p>
        <ul>
            <li>Message: <?php echo $object->getFeedback(); ?></li>
            <li>Error message: <?php echo $object->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $object->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle verkopers</caption>
            <tr>
                <th>LidId</th>
                <th>Naam</th>
                <th>Voornaam</th>
            </tr>
            <?php
            foreach ($verkopersLijst as $verkoper)
            {
            ?>
            <tr>
                <td style="width:  10px"><?php echo $verkoper['LidId'] ?></td>
                <td style="width:  100px"><?php echo $verkoper['LidNaam'] ?></td>
                <td style="width:  100px"><?php echo $verkoper['LidVoornaam'] ?></td>
            </tr>
            <?php
            }
            ?>
        </table>
        -->

        <!--
        <p>Test select all</p>
        <ul>
            <li>Message: <?php echo $object->getFeedback(); ?></li>
            <li>Error message: <?php echo $object->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $object->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle Leden</caption>
            <tr>
                <th>LidId</th>
                <th>Gebruikersnaam</th>
                <th>Wachtwoord</th>
                <th>Naam</th>
                <th>Voornaam</th>
                <th>Info Lid</th>
                <th>Adres</th>
                <th>Telefoon</th>
                <th>Email</th>
                <th>Skype</th>
                <th>WoonId</th>
                <th>Gesloten</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
            foreach ($ledenLijst as $lid)
            {
            ?>
            <tr>
                <td style="width:  10px"><?php echo $lid['LidId'] ?></td>
                <td style="width:  100px"><?php echo $lid['GebruikersNaam'] ?></td>
                <td style="width:  100px"><?php echo $lid['Wachtwoord'] ?></td>
                <td style="width:  100px"><?php echo $lid['LidNaam'] ?></td>
                <td style="width:  100px"><?php echo $lid['LidVoornaam'] ?></td>
                <td style="width:  100px"><?php echo $lid['LidInfo'] ?></td>
                <td style="width:  100px"><?php echo $lid['Adres'] ?></td>
                <td style="width:  100px"><?php echo $lid['Telefoon'] ?></td>
                <td style="width:  100px"><?php echo $lid['Email'] ?></td>
                <td style="width:  100px"><?php echo $lid['SkypeNaam'] ?></td>
                <td style="width:  100px"><?php echo $lid['WoonId'] ?></td>
                <td style="width:  100px"><?php echo $lid['Gesloten'] ?></td>
                <td style="width:  100px"><?php echo $lid['AddedBy'] ?></td>
                <td style="width:  200px"><?php echo $lid['InsertedOn'] ?></td>
                <td style="width:  100px"><?php echo $lid['ModifiedBy'] ?></td>
                <td style="width:  200px"><?php echo $lid['ModifiedOn'] ?></td>
            </tr>

            <?php
            }
            ?>
        </table>
        -->

        <!--
        <p>Test selectLidByGebruikersNaam</p>
        <ul>
            <li>Message: <?php echo $objectS->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectS->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectS->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <tr>
                <th>LidId</th>
                <th>Gebruikersnaam</th>
                <th>Wachtwoord</th>
                <th>Naam</th>
                <th>Voornaam</th>
                <th>Info Lid</th>
                <th>Adres</th>
                <th>Telefoon</th>
                <th>Email</th>
                <th>Skype</th>
                <th>WoonId</th>
                <th>Gesloten</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <tr>
                <td style="width:  100px"><?php echo $objectS->getLidId(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getGebruikersNaam(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getWachtwoord(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getLidNaam(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getLidVoornaam(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getLidInfo(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getAdres(); ?></td>     
                <td style="width:  100px"><?php echo $objectS->getTelefoon(); ?></td>  
                <td style="width:  100px"><?php echo $objectS->getEmail(); ?></td>   
                <td style="width:  100px"><?php echo $objectS->getSkypeNaam(); ?></td> 
                <td style="width:  100px"><?php echo $objectS->getWoonId(); ?></td>     
                <td style="width:  100px"><?php echo $objectS->getGesloten(); ?></td> 
                <td style="width:  100px"><?php echo $objectS->getAddedBy(); ?></td>                                                                                
                <td style="width:  200px"><?php echo $objectS->getInsertedOn(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getModifiedBy(); ?></td>       
                <td style="width:  200px"><?php echo $objectS->getModifiedOn(); ?></td>
            </tr>
        </table>
        -->

        <!--
        <p>Test update Lid</p>
        <ul>
            <li>Message: <?php echo $objectU->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectU->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectU->getErrorCode(); ?></li>
        </ul>
        -->

        <!--
        <p>Test insert Lid</p>
        <ul>
            <li>Message: <?php echo $lid->getFeedback(); ?></li>
            <li>Error message: <?php echo $lid->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $lid->getErrorCode(); ?></li>
            <li>ID: <?php echo $lid->getLidId(); ?></li>
        </ul>
        -->

        <!--
        <p>Test delete Lid</p>
        <ul>
            <li>Message: <?php echo $objectD->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectD->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectD->getErrorCode(); ?></li>
        </ul>
        -->

        <!--
        <p>Test selectLidById</p>
        <ul>
            <li>Message: <?php echo $objectS->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectS->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectS->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <tr>
                <th>LidId</th>
                <th>Gebruikersnaam</th>
                <th>Wachtwoord</th>
                <th>Naam</th>
                <th>Voornaam</th>
                <th>Info Lid</th>
                <th>Adres</th>
                <th>Telefoon</th>
                <th>Email</th>
                <th>Skype</th>
                <th>WoonId</th>
                <th>Gesloten</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <tr>
                <td style="width:  100px"><?php echo $objectS->getLidId(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getGebruikersNaam(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getWachtwoord(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getLidNaam(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getLidVoornaam(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getLidInfo(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getAdres(); ?></td>     
                <td style="width:  100px"><?php echo $objectS->getTelefoon(); ?></td>  
                <td style="width:  100px"><?php echo $objectS->getEmail(); ?></td>   
                <td style="width:  100px"><?php echo $objectS->getSkypeNaam(); ?></td> 
                <td style="width:  100px"><?php echo $objectS->getWoonId(); ?></td>     
                <td style="width:  100px"><?php echo $objectS->getGesloten(); ?></td> 
                <td style="width:  100px"><?php echo $objectS->getAddedBy(); ?></td>                                                                                
                <td style="width:  200px"><?php echo $objectS->getInsertedOn(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getModifiedBy(); ?></td>       
                <td style="width:  200px"><?php echo $objectS->getModifiedOn(); ?></td>
            </tr>
        </table>
        -->
    </body>
</html>

