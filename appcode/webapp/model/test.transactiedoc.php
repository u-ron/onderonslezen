<?php
    include('../../helpers/feedback.class.php');
    include('../../helpers/base.class.php');
    include('transactiedoc.class.php');

    //insert testen
    /*
    $tad = new TransactieDoc();
    $tad->setTransactieId(1);
    $tad->setDocId(3);
    $tad->setAddedBy('admin');
    $tad->insert();
    */
    

    //update testen
    /*
    $objectU = new TransactieDoc();
    $objectU->setTDId(4);
    $objectU->setTransactieId(1);
    $objectU->setDocId(3);
    $objectU->setModifiedBy("admin");
    $objectU->update();
    */

    //selectall testen
    /*
    $objectS = new TransactieDoc();
    $tdLijst = $objectS->selectAll();
    */

    //selectById
    /*
    $objectSI = new TransactieDoc();
    $objectSI->setTDId(4);
    $objectSI->selectTransactieDocById();
    */

    //delete testen
    /*
    $objectD = new TransactieDoc();
    $objectD->setTDId(3);
    $objectD->delete();
    */

    //deleteTransactieDocByTransactieId testen
    /*
    $objectD = new TransactieDoc();
    $objectD->setTransactieId(8);
    $objectD->deleteTransactieDocByTransactieId();
    */

    //testen van selectdocidsintransactie
    /*
    $object = new TransactieDoc();
    $result = $object->selectDocIdsInTransactie(4);
    print_r($result);
    foreach($result as $docId)
    {
        if($docId['DocId'] == 27) {echo "OK";exit;} 
    }
    */

    //testen van selectdocidsfromtransactie
    $object = new TransactieDoc();
    $object->setTransactieId(10);
    $result = $object->selectTransactieDocFromTransactie();

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        
        <p>Test selectTransactieDocFromTransactie</p>
        <ul>
            <li>Message: <?php echo $object->getFeedback(); ?></li>
            <li>Error message: <?php echo $object->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $object->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle doc ids</caption>
            <tr>
                <th>TDId</th>
                <th>DocId</th>
                <th>TransactieId</th>
                <th>AddedBy</th>
                 <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
            foreach ($result as $td)
            {
            ?>
            <tr>
                <td style="width:  100px"><?php echo $td['TDId']; ?></td>
                <td style="width:  100px"><?php echo $td['DocId']; ?></td>
                <td style="width:  100px"><?php echo $td['TransactieId']; ?></td>
                <td style="width:  100px"><?php echo $td['AddedBy'] ?></td>
                <td style="width:  100px"><?php echo $td['InsertedOn'] ?></td>
                <td style="width:  100px"><?php echo $td['ModifiedBy'] ?></td>
                <td style="width:  100px"><?php echo $td['ModifiedOn'] ?></td>
            </tr>

            <?php
            }
            ?>
        </table>
        
        
        <!--
        <p>Test select docids in transactie</p>
        <ul>
            <li>Message: <?php echo $object->getFeedback(); ?></li>
            <li>Error message: <?php echo $object->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $object->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle doc ids</caption>
            <tr>
                <th>DocId</th>
            </tr>
            <?php
            foreach ($result as $td)
            {
            ?>
            <tr>
                <td style="width:  100px"><?php echo $td['DocId']; ?></td>
            </tr>

            <?php
            }
            ?>
        </table>
        -->

        <!--
        <p>Test deleteTransactieDocByTransactieId</p>
        <ul>
            <li>Message: <?php echo $objectD->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectD->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectD->getErrorCode(); ?></li>
        </ul>
        -->

        <!--
        <p>Test delete td</p>
        <ul>
            <li>Message: <?php echo $objectD->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectD->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectD->getErrorCode(); ?></li>
        </ul>
        -->
        <!--
        <p>Test selectTransactieById</p>
        <ul>
            <li>Message: <?php echo $objectSI->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectSI->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectSI->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <tr>
                <th>TDId</th>
                <th>TAId</th>
                <th>DocId</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <tr>
                <td style="width:  100px"><?php echo $objectSI->getTDId(); ?></td>
                <td style="width:  100px"><?php echo $objectSI->getTransactieId(); ?></td>
                <td style="width:  100px"><?php echo $objectSI->getDocId(); ?></td>
                <td style="width:  200px"><?php echo $objectSI->getAddedBy(); ?></td>                                                                
                <td style="width:  200px"><?php echo $objectSI->getInsertedOn(); ?></td>
                <td style="width:  200px"><?php echo $objectSI->getModifiedBy(); ?></td>     
                <td style="width:  200px"><?php echo $objectSI->getModifiedOn(); ?></td>
            </tr>
        </table>
        -->

        
        <!--
        <p>Test select all</p>
        <ul>
            <li>Message: <?php echo $objectS->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectS->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectS->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle transactiedocs</caption>
            <tr>
                <th>TDId</th>
                <th>TAId</th>
                <th>DocId</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
            foreach ($tdLijst as $td)
            {
            ?>
            <tr>
                <td style="width:  100px"><?php echo $td['TDId'] ?></td>
                <td style="width:  100px"><?php echo $td['TransactieId'] ?></td>
                <td style="width:  100px"><?php echo $td['DocId'] ?></td>
                <td style="width:  100px"><?php echo $td['AddedBy'] ?></td>
                <td style="width:  100px"><?php echo $td['InsertedOn'] ?></td>
                <td style="width:  100px"><?php echo $td['ModifiedBy'] ?></td>
                <td style="width:  100px"><?php echo $td['ModifiedOn'] ?></td>
            </tr>

            <?php
            }
            ?>
        </table>
        -->
        
        <!--
        <p>Test update doc</p>
        <ul>
            <li>Message: <?php echo $objectU->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectU->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectU->getErrorCode(); ?></li>
        </ul>
        -->

       
        <!--
        <p>Test insert tad</p>
        <ul>
            <li>Message: <?php echo $tad->getFeedback(); ?></li>
            <li>Error message: <?php echo $tad->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $tad->getErrorCode(); ?></li>
            <li>ID: <?php echo $tad->getTDId(); ?></li>
        </ul>
        --> 
       
        
    </body>
</html>


