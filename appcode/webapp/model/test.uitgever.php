<?php
    include('../../helpers/feedback.class.php');
    include('../../helpers/base.class.php');
    include('uitgever.class.php');

    //insert testen
    /*
    $uitgever = new Uitgever();
    $uitgever->setUitgever('Roularta');
    $uitgever->setUitgeverInfo('tijdschriften');
    $uitgever->setAddedBy('admin');
    $uitgever->insert();
    */

    //insert testen met lange naam: 262 karakters
    /*
    $uitgever = new Uitgever();
    $uitgever->setUitgeverNaam('Aspeaaaaaa/aaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa//
    aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaaa//
    aaaaaaaaaa/aaaaaaaaaa/aaaaaaaaa/aaaaaaaaaa');
    $uitgever->setUitgeverVoornaam('Pieter');
    $uitgever->setUitgeverInfo('Bruggeling');
    $uitgever->setAddedBy('admin');
    $uitgever->insert();
    /*
    Error message: Data too long for column 'pUitgeverNaam' at row 1
    Error code: 22001/1406
    */
    

    //update testen
    /*
    $objectU = new Uitgever();
    $objectU->setUitgeverId(3);
    $objectU->setUitgever('Roularta');
    $objectU->setUitgeverInfo('Tijdschriften; in Roeselare');
    //setModifiedBy() hoeft er niet bij; NULL voorzien in databank
    $objectU->update();
    */
    
    //selectAll testen
    /*
    $object = new Uitgever();
    $uitgeversLijst=$object->selectAll();
    */

    //testen van uitgeverSelectById()
    /*
    $objectS = new Uitgever();
    $objectS->setUitgeverId(3);
    $result = $objectS->selectUitgeverById();
    */

    //delete testen
    
    $objectD= new Uitgever();
    $objectD->setUitgeverId(2);
    $objectD->delete();
    

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        
        <p>Test delete uitgever</p>
        <ul>
            <li>Message: <?php echo $objectD->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectD->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectD->getErrorCode(); ?></li>
        </ul>
        
        <!--
        <p>Test selectUitgeverById</p>
        <ul>
            <li>Message: <?php echo $objectS->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectS->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectS->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <tr>
                <th>UitgeverId</th>
                <th>Naam</th>
                <th>Info Uitgever</th>
                <th>InsertedOn</th>
                <th>ModifiedOn</th>
            </tr>
            <tr>
                <td style="width:  100px"><?php echo $objectS->getUitgeverId(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getUitgever(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getUitgeverInfo(); ?></td>                                                                    
                <td style="width:  200px"><?php echo $objectS->getInsertedOn(); ?></td>
                <td style="width:  200px"><?php echo $objectS->getModifiedOn(); ?></td>
            </tr>
        </table>
        -->
        <!--
        <p>Test select all</p>
        <ul>
            <li>Message: <?php echo $object->getFeedback(); ?></li>
            <li>Error message: <?php echo $object->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $object->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle uitgevers</caption>
            <tr>
                <th>UitgeverId</th>
                <th>Naam</th>
                <th>Info Uitgever</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
                foreach ($uitgeversLijst as $uitgever)
                {
                    ?>
            <tr>
                <td style="width:  100px"><?php echo $uitgever['UitgeverId'] ?></td>
                <td style="width:  100px"><?php echo $uitgever['Uitgever'] ?></td>
                <td style="width:  100px"><?php echo $uitgever['UitgeverInfo'] ?></td>
                <td style="width:  100px"><?php echo $uitgever['AddedBy'] ?></td>
                <td style="width:  200px"><?php echo $uitgever['InsertedOn'] ?></td>
                <td style="width:  100px"><?php echo $uitgever['ModifiedBy'] ?></td>
                <td style="width:  200px"><?php echo $uitgever['ModifiedOn'] ?></td>
            </tr>

            <?php
                }
            ?>
        </table>
        -->

        <!--
        <p>Test update uitgever</p>
        <ul>
            <li>Message: <?php echo $objectU->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectU->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectU->getErrorCode(); ?></li>
        </ul>
        -->

        <!--
        <p>Test insert uitgever</p>
        <ul>
            <li>Message: <?php echo $uitgever->getFeedback(); ?></li>
            <li>Error message: <?php echo $uitgever->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $uitgever->getErrorCode(); ?></li>
            <li>ID: <?php echo $uitgever->getUitgeverId(); ?></li>
        </ul>
        -->

        

    </body>
</html>
