<?php

class TransactieDoc extends \Base
{
    private $tDId;
    private $transactieId;
    private $docId;
    private $addedBy;
    private $modifiedBy;
    private $insertedOn;
    private $modifiedOn; 

    /*constructor in basisklasse volstaat*/

    /*set $transactieId
    return true als nt leeg; return false als leeg
    */
    public function setTDId($value)
    {
        if (is_numeric($value))
        {
            $this->tDId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setTransactieId($value)
    {
        if (is_numeric($value))
        {
            $this->transactieId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setDocId($value)
    {
        if (is_numeric($value))
        {
            $this->docId = $value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setAddedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->addedBy=$value;
            return TRUE;
        }
    }

    public function setModifiedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->modifiedBy=$value;
            return TRUE;
        }
    }

    /*setInsertedOn en setModifiedOn zitten vervat in de SQL-statements*/

    public function getTDId()
    {
        return $this->tDId; 
    }

    public function getTransactieId()
    {
        return $this->transactieId; 
    }

    public function getDocId()
    {
         return $this->docId;
    }

    public function getAddedBy()
    {
        return $this->addedBy;
    }

    public function getInsertedOn()
    {
        return $this->insertedOn;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function getModifiedOn()
    {
         return $this->modifiedOn;
    }


    /*noodzaakt het gebruik vd setmethodes*/
    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function insert()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try
            {
            $preparedStatement = $this->pdo->prepare('call transactiedocinsert(@pTDId, :pTransactieId, :pDocId, :pAddedBy)');
            $preparedStatement->bindParam(':pTransactieId', $this->transactieId, \PDO::PARAM_INT, 11); 
            $preparedStatement->bindParam(':pDocId', $this->docId, \PDO::PARAM_INT, 11); 
            $preparedStatement->bindParam(':pAddedBy', $this->addedBy, \PDO::PARAM_STR, 255); 
            $success = $preparedStatement->execute(); 
            if ($success)
            {
                $this->setTDId($this->pdo->query('select @pTDId')->fetchColumn()); 
                $this->feedback = "De transactiedoc met als id <b> " . $this->getTDId() . "</b> is toegevoegd."; 
                $result = TRUE;
            }
            else
            {
                $this->feedback = "De stored procedure transactiedocinsert is niet uitgevoerd.";
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
                $result = FALSE;
            }
            }
            catch (\PDOException $e)
            {
            $this->feedback="De transactiedoc is niet toegevoegd."; 
            $this->errorMessage=$e->getMessage();
            $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*methode noodzaakt het gebruik vd andere methodes setTPId enz.*/
    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function update()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {

        try
        {
        $preparedStatement=$this->pdo->prepare('call transactiedocupdate(:pTDId, :pTransactieId, :pDocId, :pModifiedBy)');
        $preparedStatement->bindParam(':pTDId', $this->tDId, \PDO::PARAM_INT, 11);
        $preparedStatement->bindParam(':pTransactieId', $this->transactieId, \PDO::PARAM_INT, 11); 
        $preparedStatement->bindParam(':pDocId', $this->docId, \PDO::PARAM_INT, 11); 
        $preparedStatement->bindParam(':pModifiedBy', $this->modifiedBy, \PDO::PARAM_STR, 255); 
        $preparedStatement->execute();

        $result = $preparedStatement->rowCount();
        if($result)
        {
            $this->feedback =  "Transactiedoc {$this->tDId} is gewijzigd.";
            $result = TRUE;
        }
        else
        {
            $this->feedback = "Transactiedoc {$this->tDId} is niet gevonden en dus niet gewijzigd.";
            $sQLErrorInfo = $preparedStatement->errorInfo();
            $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
            $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $ex)
        {
             $this->feedback = "De stored procedure transactiedocupdate is niet uitgevoerd.";
             $this->errorMessage = $ex->getMessage();
             $this->errorCode = $ex->getCode();
        }
        $this->close();
        }
        return $result;
    }

    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    /*niet gebruikt*/
    public function selectAll()
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call transactiedocselectall');
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle transactiedocs ingelezen.';
            }
            else
            {
                $this->feedback = 'De tabel Transactiedoc is leeg.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $ex)
            {
                $this->feedback = "De stored procedure transactiedocselectall is niet uitgevoerd.";
                $this->errorMessage = $ex->getMessage();
                $this->errorCode = $ex->getCode();
            }
            $this->close();
        }
        return $result;
    }

    //retourneert FALSE bij mislukken en een 2dimens array bij slagen
    /*methode noodzaakt het gebruik vd de methode setTPId*/
    public function selectTransactieDocById()
    {
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call transactiedocselectbyid(:pId)');
        $preparedStatement -> bindParam(':pId', $this->transactieId, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();
        $this->rowCount = $preparedStatement->rowCount();
        //fetch the output
        if($result = $preparedStatement->fetchAll()) //Returns an array containing all of the result set rows 
        {
            $this->feedback = "{$preparedStatement->rowCount()} rij(en) met transactieid = {$this->transactieId} in de tabel Transactiedoc gevonden.";
            $this->tDId = $result[0]['TDId'];
            $this->transactieId = $result[0]['TransactieId'];
            $this->docId = $result[0]['DocId'];
            $this->addedBy = $result[0]['AddedBy'];
            $this->insertedOn = $result[0]['InsertedOn'];
            $this->modifiedBy = $result[0]['ModifiedBy'];
            $this->modifiedOn = $result[0]['ModifiedOn'];
        }
        else //retourneert lege array
        {
               $this->feedback = "Geen rijen met transactieid = {$this->transactieId} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
                $this->feedback = "De stored procedure transactiedocselectbyid is niet uitgevoerd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
                $this->rowCount = -1;
        }
        $this->close();
        return $result;
        }
         
    }


    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function delete()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try
            {
                $preparedStatement = $this->pdo->prepare('call transactiedocdelete(:pId)');
                $preparedStatement->bindParam(':pId', $this->tDId, \PDO::PARAM_INT, 11);
                $preparedStatement->execute();
                $result = $preparedStatement->rowCount();
                if($result)
                {
                    $this->feedback = "Transactiedoc {$this->tDId} is verwijderd.";
                    $result = TRUE;
                }
                else
                {
                     $this->feedback = "De transactiedoc met id = {$this->tDId} is niet gevonden en dus niet verwijderd.";
                     $sQLErrorInfo = $preparedStatement->errorInfo();
                     $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                     $this->errorMessage = $sQLErrorInfo[2];
                     $result = FALSE;
                }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De sp transactiedocdelete is niet uitgevoerd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*methode noodzaakt het gebruik vd de methode setTransactieId*/
    public function deleteTransactieDocByTransactieId()
    {
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call TransactieDocDeleteByTransactieId(:pTAId)');
        $preparedStatement -> bindParam(':pTAId', $this->transactieId, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();
        $result = $preparedStatement->rowCount();
        if($result) 
        {
             $this->feedback = "Alle Transactiedoc records met transactieid {$this->transactieId} zijn verwijderd.";
             $result = TRUE;
        }
        else 
        {
               $this->feedback = "Geen rijen met transactieId = {$this->transactieId} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $ex)
        {
                $this->feedback = "De sp TransactieDocDeleteByTransactieId is niet gevonden.";
                $this->errorMessage=$ex->getMessage();
                $this->errorCode=$ex->getCode();
        }
        $this->close();
        return $result;
        }
    }
    
    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    /*Wordt de methode gebruikt?*//*Wellicht docs te koop en te leen*/
    public function selectDocIdsInTransactie()
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call TransactieDocSelectDocIDInTransactie');
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle docids in transactie ingelezen.';
            }
            else
            {
                $this->feedback = 'Geen docids in de tabel Transactiedoc.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $ex)
            {
                $this->feedback = "De stored procedure TransactieDocSelectDocIDInTransactie is niet uitgevoerd.";
                $this->errorMessage = $ex->getMessage();
                $this->errorCode = $ex->getCode();
            }
            $this->close();
        }
        return $result;
    }
    
    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    /*vergt het gebruik vd methode setTransactieId*/
    public function selectTransactieDocFromTransactie()
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call TransactieDocSelectDocIDFromTransactie(:pTAId)');
            $preparedStatement -> bindParam(':pTAId', $this->transactieId, \PDO::PARAM_INT, 11);
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle records van deze transactieid ingelezen.';
            }
            else
            {
                $this->feedback = 'Geen records van deze transactieid in de tabel Transactiedoc.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $ex)
            {
                $this->feedback = "De stored procedure TransactieDocSelectDocIDFromTransactie is niet uitgevoerd.";
                $this->errorMessage = $ex->getMessage();
                $this->errorCode = $ex->getCode();
            }
            $this->close();
        }
        return $result;
    }
    
}
?>



