<?php
include('../../helpers/feedback.class.php');
include('../../helpers/base.class.php');
include('doctype.class.php');

/*
$docTypeObject = new DocType();
$typeLijst = $docTypeObject->selectAll();
*/

/*
$docTypeObject->setDocType('Strip');
$docTypeObject->setInsertedBy('admin');
$docTypeObject->insert();
*/

/*
$objectU = new DocType();
$objectU->setDocTypeId(5);
$objectU->setDocType('Stripalbum');
$objectU->setModifiedBy('admin');
$objectU->update();
*/

/*
$objectSId = new DocType();
$objectSId->setDocTypeId(5);
$objectSId->selectDocTypeById();
*/

$objectD = new DocType();
$objectD->setDocTypeId(5);
$objectD->delete();

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
         
        <p>Test delete doc type</p>
        <ul>
            <li>Message: <?php echo $objectD->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectD->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectD->getErrorCode(); ?></li>
        </ul>
        
        <!--
        <p>Test selectDocTypeById</p>
        <ul>
            <li>Message: <?php echo $objectSId->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectSId->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectSId->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <tr>
                <th>Id</th>
                <th>DocType</th>
                <th>Added By</th>
                <th>InsertedOn</th>
                <th>Modified By</th>
                <th>ModifiedOn</th>
            </tr>
            <tr>
                <td style="width:  100px"><?php echo $objectSId->getDocTypeId(); ?></td>
                <td style="width:  100px"><?php echo $objectSId->getDocType(); ?></td>
                <td style="width:  100px"><?php echo $objectSId->getAddedBy(); ?></td>
                <td style="width:  200px"><?php echo $objectSId->getInsertedOn(); ?></td>
                <td style="width:  100px"><?php echo $objectSId->getModifiedBy(); ?></td>
                <td style="width:  200px"><?php echo $objectSId->getModifiedOn(); ?></td>
            </tr>
        </table>
        -->

        <!--
        <p>Test update doctype</p>
        <ul>
            <li>Message: <?php echo $objectU->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectU->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectU->getErrorCode(); ?></li>
        </ul>
        -->

        <!--
        <p>Test insert lezer</p>
        <ul>
            <li>Message: <?php echo $docTypeObject->getFeedback(); ?></li>
            <li>Error message: <?php echo $docTypeObject->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $docTypeObject->getErrorCode(); ?></li>
            <li>ID: <?php echo $docTypeObject->getDocTypeId(); ?></li>
        </ul>
        -->

        <!--
        <p>Test select all</p>
        <ul>
            <li>Message: <?php echo $docTypeObject->getFeedback(); ?></li>
            <li>Error message: <?php echo $docTypeObject->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $docTypeObject->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle doctypes</caption>
            <tr>
                <th>Id</th>
                <th>Type</th>
                <th>InsertedOn</th>
                <th>InsertedBy</th>
            </tr>
            <?php
                foreach ($typeLijst as $type)
                {
            ?>
            <tr>
                <td style="width:  100px"><?php echo $type['DocTypeId'] ?></td>
                <td style="width:  100px"><?php echo $type['DocType'] ?></td>
                <td style="width:  200px"><?php echo $type['InsertedOn'] ?></td>
                <td style="width:  200px"><?php echo $type['ModifiedOn'] ?></td>
            </tr>
            <?php
                }
            ?>
        </table>
        --> 


    </body>
</html>
