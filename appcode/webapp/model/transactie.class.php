<?php

class Transactie extends \Base
{
    private $transactieId;
    private $taDatum;
    private $orderBedrag;
    private $transportKost;
    private $dueDate;
    private $datumUit;
    private $datumTerug;
    private $transactieTypeId;
    private $dealStatusId;
    private $exchangeStatusId;
    private $addedBy;
    private $modifiedBy;
    private $insertedOn;
    private $modifiedOn; 

    /*constructor in basisklasse volstaat*/

    /*set $transactieId
    return true als nt leeg; return false als leeg
    */
    public function setTransactieId($value)
    {
        if (is_numeric($value))
        {
            $this->transactieId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /*set $taDatum
    return true als nt leeg; return false als leeg
    */
    public function setTADatum($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->taDatum=$value;
            return TRUE;
        }
    }

    /*set $orderBedrag
     return true als nt leeg; return false als leeg
    */
    public function setOrderBedrag($value)
    {
        if (!empty($value))
        {
            $this->orderBedrag=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /*set $orderBedrag
     return true als nt leeg; return false als leeg
    */
    public function setTransportKost($value)
    {
        if (!empty($value))
        {
            $this->transportKost=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setDueDate($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->dueDate=$value;
            return TRUE;
        }
    }

    public function setDatumUit($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->datumUit=$value;
            return TRUE;
        }
    }

    public function setDatumTerug($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->datumTerug=$value;
            return TRUE;
        }
    }

    public function setTransactieTypeId($value)
    {
        if (is_numeric($value))
        {
            $this->transactieTypeId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setDealStatusId($value)
    {
        if (is_numeric($value))
        {
            $this->dealStatusId = $value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setExchangeStatusId($value)
    {
        if (is_numeric($value))
        {
            $this->exchangeStatusId = $value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setAddedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->addedBy=$value;
            return TRUE;
        }
    }

    public function setModifiedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->modifiedBy=$value;
            return TRUE;
        }
    }

    /*setInsertedOn en setModifiedOn zitten vervat in de SQL-statements*/

    public function getTransactieId()
    {
        return $this->transactieId; 
    }

    public function getTADatum()
    {
         return $this->taDatum;
    }

    public function getOrderBedrag()
    {
        return $this->orderBedrag;
    }

    public function getTransportKost()
    {
         return $this->transportKost;
    }

    public function getDueDate()
    {
         return $this->dueDate;
    }

    public function getDatumUit()
    {
         return $this->datumUit;
    }

    public function getDatumTerug()
    {
         return $this->datumTerug;
    }

    public function getTransactieTypeId()
    {
        return $this->transactieTypeId; 
    }

    public function getDealStatusId()
    {
         return $this->dealStatusId;
    }

    public function getExchangeStatusId()
    {
         return $this->exchangeStatusId;
    }

    public function getAddedBy()
    {
        return $this->addedBy;
    }

    public function getInsertedOn()
    {
        return $this->insertedOn;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function getModifiedOn()
    {
         return $this->modifiedOn;
    }


    /*noodzaakt het gebruik vd setmethodes*/
    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function insert()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try
            {
            $preparedStatement = $this->pdo->prepare('call transactieinsert(@pTransactieId, :pTADatum, :pOrderBedrag, :pTransportKost, :pDueDate, :pDatumUit, :pDatumTerug, :pTransactieTypeId, :pDealStatusId, :pExchangeStatusId, :pAddedBy)');
            $preparedStatement->bindParam(':pTADatum', $this->taDatum, \PDO::PARAM_STR, 255); 
            $preparedStatement->bindParam(':pOrderBedrag', $this->orderBedrag, \PDO::PARAM_STR, 255);
            $preparedStatement->bindParam(':pTransportKost', $this->transportKost, \PDO::PARAM_STR, 255);
            $preparedStatement->bindParam(':pDueDate', $this->dueDate, \PDO::PARAM_STR, 255); 
            $preparedStatement->bindParam(':pDatumUit', $this->datumUit, \PDO::PARAM_STR, 255);
            $preparedStatement->bindParam(':pDatumTerug', $this->datumTerug, \PDO::PARAM_STR, 255);
            $preparedStatement->bindParam(':pTransactieTypeId', $this->transactieTypeId, \PDO::PARAM_INT, 11); 
            $preparedStatement->bindParam(':pDealStatusId', $this->dealStatusId, \PDO::PARAM_INT, 11); 
            $preparedStatement->bindParam(':pExchangeStatusId', $this->exchangeStatusId, \PDO::PARAM_INT, 11); 
            $preparedStatement->bindParam(':pAddedBy', $this->addedBy, \PDO::PARAM_STR, 255); 
            $success = $preparedStatement->execute(); 
            if ($success)
            {
                $this->setTransactieId($this->pdo->query('select @pTransactieId')->fetchColumn()); 
                $this->feedback="De transactie met als datum '{$this->taDatum}' en id <b> " . $this->getTransactieId() . "</b> is toegevoegd."; 
                $result = TRUE;
            }
            else
            {
                $this->feedback = "De stored procedure transactieinsert is niet uitgevoerd.";
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
                $result = FALSE;
            }
            }
            catch (\PDOException $e)
            {
            $this->feedback="De transactie met als datum '{$this->taDatum}' is niet toegevoegd."; 
            $this->errorMessage=$e->getMessage();
            $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*methode noodzaakt het gebruik vd andere methodes setDocId, setTitel enz.*/
    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function update()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {

        try
        {
        $preparedStatement=$this->pdo->prepare('call transactieupdate(:pTransactieId, :pTADatum, :pOrderBedrag, :pTransportKost, :pDueDate, :pDatumUit, :pDatumTerug, :pTransactieTypeId, :pDealStatusId, :pExchangeStatusId, :pModifiedBy)');
        $preparedStatement->bindParam(':pTransactieId', $this->transactieId, \PDO::PARAM_INT, 11);
        $preparedStatement->bindParam(':pTADatum', $this->taDatum, \PDO::PARAM_STR, 255); 
        $preparedStatement->bindParam(':pOrderBedrag', $this->orderBedrag, \PDO::PARAM_STR, 255);
        $preparedStatement->bindParam(':pTransportKost', $this->transportKost, \PDO::PARAM_STR, 255);
        $preparedStatement->bindParam(':pDueDate', $this->dueDate, \PDO::PARAM_STR, 255); 
        $preparedStatement->bindParam(':pDatumUit', $this->datumUit, \PDO::PARAM_STR, 255);
        $preparedStatement->bindParam(':pDatumTerug', $this->datumTerug, \PDO::PARAM_STR, 255);
        $preparedStatement->bindParam(':pTransactieTypeId', $this->transactieTypeId, \PDO::PARAM_INT, 11); 
        $preparedStatement->bindParam(':pDealStatusId', $this->dealStatusId, \PDO::PARAM_INT, 11); 
        $preparedStatement->bindParam(':pExchangeStatusId', $this->exchangeStatusId, \PDO::PARAM_INT, 11); 
        $preparedStatement->bindParam(':pModifiedBy', $this->modifiedBy, \PDO::PARAM_STR, 255); 
        $preparedStatement->execute();

        $result = $preparedStatement->rowCount();
        if($result)
        {
            $this->feedback =  "Transactie {$this->transactieId} is gewijzigd.";
            $result = TRUE;
        }
        else
        {
            $this->feedback = "Transactie {$this->transactieId} is niet gevonden en dus niet gewijzigd.";
            $sQLErrorInfo = $preparedStatement->errorInfo();
            $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
            $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $ex)
        {
             $this->feedback = "Transactie {$this->transactieId} is niet gewijzigd.";
             $this->errorMessage = $ex->getMessage();
             $this->errorCode = $ex->getCode();
        }
        $this->close();
        }
        return $result;
    }

    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    /*niet gebruikt*/
    public function selectAll()
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call transactieselectall');
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle transacties ingelezen.';
            }
            else
            {
                $this->feedback = 'De tabel Transactie is leeg.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $ex)
            {
                $this->feedback = "De stored procedure transactieselectall is niet uitgevoerd.";
                $this->errorMessage = $ex->getMessage();
                $this->errorCode = $ex->getCode();
            }
            $this->close();
        }
        return $result;
    }

    //retourneert FALSE bij mislukken en een 2dimens array bij slagen
    /*methode noodzaakt het gebruik vd de methode setTransactieId*/
    public function selectTransactieById()
    {
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call transactieselectbyid(:pId)');
        $preparedStatement -> bindParam(':pId', $this->transactieId, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();
        $this->rowCount = $preparedStatement->rowCount();
        //fetch the output
        if($result = $preparedStatement->fetchAll()) //Returns an array containing all of the result set rows 
        {
            $this->feedback = "{$preparedStatement->rowCount()} rij(en) met id = {$this->transactieId} in de tabel Transactie gevonden.";
            $this->transactieId = $result[0]['TransactieId'];
            $this->taDatum = $result[0]['TADatum'];
            $this->orderBedrag = $result[0]['OrderBedrag'];
            $this->transportKost = $result[0]['TransportKost'];
            $this->dueDate = $result[0]['DueDate'];
            $this->datumUit = $result[0]['DatumUit'];
            $this->datumTerug = $result[0]['DatumTerug'];
            $this->transactieTypeId = $result[0]['TransactieTypeId'];
            $this->dealStatusId = $result[0]['DealStatusId'];
            $this->exchangeStatusId = $result[0]['ExchangeStatusId'];
            $this->addedBy = $result[0]['AddedBy'];
            $this->insertedOn = $result[0]['InsertedOn'];
            $this->modifiedBy = $result[0]['ModifiedBy'];
            $this->modifiedOn = $result[0]['ModifiedOn'];
        }
        else //retourneert lege array
        {
               $this->feedback = "Geen rijen met id = {$this->transactieId} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
                $this->feedback = "Fout => rij is niet gevonden.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
                $this->rowCount = -1;
        }
        $this->close();
        return $result;
        }
         
    }


    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function delete()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try{
                $preparedStatement = $this->pdo->prepare('call transactiedelete(:pId)');
                /*in stored procedure staat pId als parameter; hoeft hier niet idem te zijn*/
                $preparedStatement->bindParam(':pId', $this->transactieId, \PDO::PARAM_INT, 11);
                $preparedStatement->execute();
                $result = $preparedStatement->rowCount();
                if($result)
                {
                $this->feedback = "Transactie {$this->transactieId} is verwijderd.";
                $result = TRUE;
                }
                else
                {
                     $this->feedback = "De transactie met id = {$this->transactieId} is niet gevonden en dus niet verwijderd.";
                     $sQLErrorInfo = $preparedStatement->errorInfo();
                     $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                     $this->errorMessage = $sQLErrorInfo[2];
                     $result = FALSE;
                }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De transactie {$this->transactieId} is niet verwijderd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    
    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    public function selectMijnHangendeDeals($lidId)
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call TransactieSelectMijnHangendeDeals(:pLidId)');
            $preparedStatement->bindParam(':pLidId', $lidId, \PDO::PARAM_INT, 11);
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle hangende deals ingelezen.';
            }
            else
            {
                $this->feedback = 'Geen hangende deals in de tabel Transactie.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $ex)
            {
                $this->feedback = "De stored procedure transactiemijnselecthangendedeals is niet uitgevoerd.";
                $this->errorMessage = $ex->getMessage();
                $this->errorCode = $ex->getCode();
            }
            $this->close();
        }
        return $result;
    }
    
    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    public function selectMijnVorigeDeals($lidId)
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call TransactieSelectMijnVorigeDeals(:pLidId)');
            $preparedStatement->bindParam(':pLidId', $lidId, \PDO::PARAM_INT, 11);
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle vorige deals ingelezen.';
            }
            else
            {
                $this->feedback = 'Geen vorige deals in de tabel Transactie.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $ex)
            {
                $this->feedback = "De stored procedure transactiemijnselectvorigedeals is niet uitgevoerd.";
                $this->errorMessage = $ex->getMessage();
                $this->errorCode = $ex->getCode();
            }
            $this->close();
        }
        return $result;
    }
    
     /*retourneert false bij mislukken; bij slagen een 2dim array*/
    public function selectMijnHangendeExchanges($lidId)
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call TransactieSelectMijnHangendeExchanges(:pLidId)');
            $preparedStatement->bindParam(':pLidId', $lidId, \PDO::PARAM_INT, 11);
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle hangende exchanges ingelezen.';
            }
            else
            {
                $this->feedback = 'Geen hangende exchanges in de tabel Transactie.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $ex)
            {
                $this->feedback = "De stored procedure transactiemijnselecthangendeexchanges is niet uitgevoerd.";
                $this->errorMessage = $ex->getMessage();
                $this->errorCode = $ex->getCode();
            }
            $this->close();
        }
        return $result;
    }
    
    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    public function selectMijnVorigeExchanges($lidId)
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call TransactieSelectMijnVorigeExchanges(:pLidId)');
            $preparedStatement->bindParam(':pLidId', $lidId, \PDO::PARAM_INT, 11);
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle vorige exchanges ingelezen.';
            }
            else
            {
                $this->feedback = 'Geen vorige exchanges in de tabel Transactie.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $ex)
            {
                $this->feedback = "De stored procedure transactiemijnselectvorigeexchanges is niet uitgevoerd.";
                $this->errorMessage = $ex->getMessage();
                $this->errorCode = $ex->getCode();
            }
            $this->close();
        }
        return $result;
    }
    
    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    public function selectMijnVorigeVerkopen($lidId)
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call TransactieSelectMijnVorigeVerkopen(:pLidId)');
            $preparedStatement->bindParam(':pLidId', $lidId, \PDO::PARAM_INT, 11);
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle vorige verkopen ingelezen.';
            }
            else
            {
                $this->feedback = 'Geen vorige verkopen in de tabel Transactie.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $ex)
            {
                $this->feedback = "De stored procedure transactiemijnselectvorigeverkopen is niet uitgevoerd.";
                $this->errorMessage = $ex->getMessage();
                $this->errorCode = $ex->getCode();
            }
            $this->close();
        }
        return $result;
    }

    public function countTransactiesBezig()
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call TransactiesBezigCount');
            $result = $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle transacties geteld.';
            }
            else
            {
                $this->feedback = 'De tabel Transactie is leeg.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $ex)
            {
                $this->feedback = "De stored procedure TransactiesBezigCount is niet uitgevoerd.";
                $this->errorMessage = $ex->getMessage();
                $this->errorCode = $ex->getCode();
            }
            $this->close();
        }
        return $result;
    }
}
?>



