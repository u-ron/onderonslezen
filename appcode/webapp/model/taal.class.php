<?php
    class Taal extends \Base
     {
        private $taalId;
        private $taal;

        
        public function setTaalId($value)
        {
            if (is_numeric($value))
        {
            $this->taalId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        }

        public function getTaal()
        {
            return $this->taal;
        }

        public function selectAll()
        {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call taalselectall()');
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle talen ingelezen.';
            }
            else
            {
                $this->feedback = 'De tabel Taal is leeg.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De stored procedure taalSelectAll is niet uitgevoerd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

        //retourneert boolean
        /*methode noodzaakt het gebruik vd de andere methodes setId*/
      
        public function selectTaalById()
        {
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call TaalSelectById(:pId)');
        $preparedStatement -> bindParam(':pId', $this->taalId, \PDO::PARAM_INT, 10);
        $preparedStatement->execute();
        $this->rowCount = $preparedStatement->rowCount();
        //fetch the output
        if($result = $preparedStatement->fetchAll())
          {
            $this->feedback = "{$preparedStatement->rowCount()} rij(en) met id = {$this->taalId} in de tabel Taal gevonden.";
            $this->id = $result[0]['TaalId'];
            $this->taal = $result[0]['Taal'];
          }
          else
          {
               $this->feedback = "Geen rijen met id = {$this->taalId} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
          }
        }
        catch (\PDOException $e)
        {
                $this->feedback = "Fout => rij is niet gevonden.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
                $this->rowCount = -1;
        }
        $this->close();
         return $result;
         }
         
    }
    
    }
?>


