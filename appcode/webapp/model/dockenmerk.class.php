<?php

class DocKenmerk extends \Base
{
    private $docKenmerkId;
    private $docKenmerk;
    private $docKenmerkValue;
    private $docId;//FK
    private $addedBy;
    private $insertedOn;
    private $modifiedBy;
    private $modifiedOn;

    /*constructor in basisklasse volstaat*/

    /*set $docKenmerkId
    return true als nt leeg; return false als leeg
    */
    public function setDocKenmerkId($value)
    {
        if (is_numeric($value))
        {
            $this->docKenmerkId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /*set $docKenmerk
    return true als nt leeg; return false als leeg
    */
    public function setDocKenmerk($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->docKenmerk=$value;
            return TRUE;
        }
    }

    /*set $docKenmerkValue
    return true als nt leeg; return false als leeg
    */
    public function setDocKenmerkValue($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->docKenmerkValue=$value;
            return TRUE;
        }
    }

    public function setDocId($value)
    {
        if (is_numeric($value))
        {
            $this->docId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setAddedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->addedBy=$value;
            return TRUE;
        }
    }

    public function setModifiedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->modifiedBy=$value;
            return TRUE;
        }
    }

    /*setInsertedOn en setModifiedOn zitten vervat in de SQL-statements*/

    public function getDocKenmerkId()
    {
        return $this->docKenmerkId; 
    }

    public function getDocKenmerk()
    {
         return $this->docKenmerk;
    }

    public function getDocKenmerkValue()
    {
         return $this->docKenmerkValue;
    }

    public function getDocId()
    {
        return $this->docId; 
    }

    public function getAddedBy()
    {
        return $this->addedBy;
    }

    public function getInsertedOn()
    {
        return $this->insertedOn;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function getModifiedOn()
    {
         return $this->modifiedOn;
    }


    /*noodzaakt het gebruik vd setmethodes*/
    /*retourneert boolean*/
    public function insert()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try
            {
            $preparedStatement = $this->pdo->prepare('call dockenmerkinsert(@pDocKenmerkId, :pDocKenmerk, :pDocKenmerkValue, :pDocId, :pAddedBy)');
            $preparedStatement->bindParam(':pDocKenmerk', $this->docKenmerk, \PDO::PARAM_STR, 255);
            $preparedStatement->bindParam(':pDocKenmerkValue', $this->docKenmerkValue, \PDO::PARAM_STR, 255);
            $preparedStatement->bindParam(':pDocId', $this->docId, \PDO::PARAM_INT, 11);
            $preparedStatement->bindParam(':pAddedBy', $this->addedBy, \PDO::PARAM_STR, 255); 
            $success = $preparedStatement->execute(); 
            if ($success == 1)
            {
                $this->setDocKenmerkId($this->pdo->query('select @pDocKenmerkId')->fetchColumn()); 
                $this->feedback="Het document kenmerk {$this->docKenmerk} met id <b>" . $this->getDocKenmerkId() . "</b> is toegevoegd."; 
                $result = TRUE;
            }
            else
            {
                $this->feedback = "Het document kenmerk is niet toegevoegd";
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
                $result = FALSE;
            }
            }
            catch (\PDOException $e)
            {
            $this->feedback="Het document kenmerk '{$this->docKenmerk}' is niet toegevoegd."; 
            $this->errorMessage=$e->getMessage();
            $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

     /*methode noodzaakt het gebruik vd andere set methodes*/
    /*retourneert boolean*/
    public function update()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {

        try
        {
        $preparedStatement=$this->pdo->prepare('call dockenmerkupdate(:pDocKenmerkId, :pDocKenmerk, :pDocKenmerkValue, :pDocId, :pModifiedBy)');
        $preparedStatement->bindParam(':pDocKenmerk', $this->docKenmerk, \PDO::PARAM_STR, 255);
        $preparedStatement->bindParam(':pDocKenmerkValue', $this->docKenmerkValue, \PDO::PARAM_STR, 255);
        $preparedStatement->bindParam(':pDocId', $this->docId, \PDO::PARAM_INT, 11);
        $preparedStatement->bindParam(':pModifiedBy', $this->modifiedBy, \PDO::PARAM_STR, 255);
        $preparedStatement->bindParam(':pDocKenmerkId', $this->docKenmerkId, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();

        $result = $preparedStatement->rowCount();
        if($result)
        {
            $this->feedback =  "Het documentkenmerk {$this->docKenmerkId} is gewijzigd.";
            $result = TRUE;
        }
        else
        {
            $this->feedback = "Het documentkenmerk {$this->docKenmerkId} is niet gevonden en dus niet gewijzigd.";
            $sQLErrorInfo = $preparedStatement->errorInfo();
            $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
            $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
             $this->feedback = "Het documentkenmerk {$this->docKenmerkId} is niet gewijzigd.";
             $this->errorMessage=$e->getMessage();
             $this->errorCode=$e->getCode();
        }
        $this->close();
        }
        return $result;
    }

    /*noodzaakt het gebruik van setDocKenmerkId()*/
    public function delete()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try{
                $preparedStatement = $this->pdo->prepare('call dockenmerkdelete(:pId)');
                $preparedStatement->bindParam(':pId', $this->docKenmerkId, \PDO::PARAM_INT, 11);
                $preparedStatement->execute();
                $result = $preparedStatement->rowCount();
                if($result)
                {
                $this->feedback = "Het document kenmerk {$this->docKenmerkId} is verwijderd.";
                $result = TRUE;
                }
                else
                {
                     $this->feedback = "Het document kenmerk met id = {$this->docKenmerkId} is niet gevonden en dus niet verwijderd.";
                     $sQLErrorInfo = $preparedStatement->errorInfo();
                     $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                     $this->errorMessage = $sQLErrorInfo[2];
                     $result = FALSE;
                }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "Het document kenmerk met id = {$this->docKenmerkId} is niet verwijderd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*niet gebruikt*/
    public function selectAll()
    {
        $result=FALSE;
        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call dockenmerkselectall()');
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle document kenmerken ingelezen.';
            }
            else
            {
                $this->feedback = 'De tabel document kenmerk is leeg.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De stored procedure DocKenmerkSelectAll is niet uitgevoerd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    //retourneert boolean
    /*methode noodzaakt het gebruik vd de methode setDocKenmerkId*/
    public function selectDocKenmerkById()
    {
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call DocKenmerkSelectById(:pId)');
        $preparedStatement -> bindParam(':pId', $this->docKenmerkId, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();
        $this->rowCount = $preparedStatement->rowCount();
        //fetch the output
        if($result = $preparedStatement->fetchAll())
        {
            $this->feedback = "{$preparedStatement->rowCount()} rij(en) met id = {$this->docKenmerkId} in de tabel DocKenmerk gevonden.";
            $this->docKenmerkId = $result[0]['DocKenmerkId'];
            $this->docKenmerk = $result[0]['DocKenmerk'];
            $this->docKenmerkValue = $result[0]['DocKenmerkValue'];
            $this->docId = $result[0]['DocId'];
            $this->addedBy = $result[0]['AddedBy'];
            $this->insertedOn = $result[0]['InsertedOn'];
            $this->modifiedBy = $result[0]['ModifiedBy'];
            $this->modifiedOn = $result[0]['ModifiedOn'];
        }
        else
        {
               $this->feedback = "Geen rijen met id = {$this->docKenmerkId} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
                $this->feedback = "Fout => rij is niet gevonden.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
                $this->rowCount = -1;
        }
        $this->close();
        return $result;
        }
         
    }

    //retourneert boolean
    /*methode noodzaakt het gebruik vd de methode setDocId*/
    public function selectDocKenmerkByDocId()
    {
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call DocKenmerkSelectByDocId(:pDocId)');
        $preparedStatement -> bindParam(':pDocId', $this->docId, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();
        $this->rowCount = $preparedStatement->rowCount();
        //fetch the output
        if($result = $preparedStatement->fetchAll())
        {
            $this->feedback = "{$preparedStatement->rowCount()} rij(en) met id = {$this->docId} in de tabel DocKenmerk gevonden.";
            $this->docKenmerkId = $result[0]['DocKenmerkId'];
            $this->docKenmerk = $result[0]['DocKenmerk'];
            $this->docKenmerkValue = $result[0]['DocKenmerkValue'];
            $this->docId = $result[0]['DocId'];
            $this->addedBy = $result[0]['AddedBy'];
            $this->insertedOn = $result[0]['InsertedOn'];
            $this->modifiedBy = $result[0]['ModifiedBy'];
            $this->modifiedOn = $result[0]['ModifiedOn'];
        }
        else
        {
               $this->feedback = "Geen rijen met id = {$this->docId} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
                $this->feedback = "Fout => rij is niet gevonden.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
                $this->rowCount = -1;
        }
        $this->close();
        return $result;
        }
         
    }

}
?>



