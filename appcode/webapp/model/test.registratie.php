<?php
    include('../../helpers/feedback.class.php');
    include('../../helpers/base.class.php');
    include('registratie.class.php');

    //insert testen
    /*
    $registratie = new Registratie();
    $registratie->setLidId(2);
    $registratie->setDocId(2);
    $registratie->setAddedBy('admin');
    $registratie->insert();
    */

    //update testen
    /*
    $objectU = new Registratie();
    $objectU->setRegId(6);
    $objectU->setLidId(6);
    $objectU->setDocId(2);
    $objectU->setModifiedBy('Admin');
    $objectU->update();
    */
    
    //selectAll testen
    /*
    $object = new Registratie();
    $registratiesLijst=$object->selectAll();
    */

    //testen van registratieSelectByLidId()
    /*
    $objectS = new Registratie();
    $objectS->setLidId(2);
    $result = $objectS->selectRegistratieByLidId();
    */

    //delete testen
    /*
    $objectD= new Registratie();
    $objectD->setRegId(2);
    $objectD->delete();
    */

    //testen van selectRegistratieByDocId
    /*
    $objectS = new Registratie();
    $objectS->setDocId(2);
    $result = $objectS->selectRegistratieByDocId();
    */

    

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        
        
        <!--
        <p>Test selectRegistratieByDocId</p>
        <ul>
            <li>Message: <?php echo $objectS->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectS->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectS->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <tr>
                <th>RegId</th>
                <th>LidId</th>
                <th>DocId</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
            foreach ($result as $registratie)
            {
            ?>
            <tr>
                <td style="width:  100px"><?php echo $registratie['RegId'] ?></td>
                <td style="width:  100px"><?php echo $registratie['LidId'] ?></td>
                <td style="width:  100px"><?php echo $registratie['DocId'] ?></td>
                <td style="width:  100px"><?php echo $registratie['AddedBy'] ?></td>
                <td style="width:  200px"><?php echo $registratie['InsertedOn'] ?></td>
                <td style="width:  100px"><?php echo $registratie['ModifiedBy'] ?></td>
                <td style="width:  200px"><?php echo $registratie['ModifiedOn'] ?></td>
            </tr>

            <?php
            }
            ?>
        </table>
        -->

        <!--
        <p>Test selectRegistratieByLidId</p>
        <ul>
            <li>Message: <?php echo $objectS->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectS->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectS->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <tr>
                <th>RegId</th>
                <th>LidId</th>
                <th>DocId</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
            foreach ($result as $registratie)
            {
            ?>
            <tr>
                <td style="width:  100px"><?php echo $registratie['RegId'] ?></td>
                <td style="width:  100px"><?php echo $registratie['LidId'] ?></td>
                <td style="width:  100px"><?php echo $registratie['DocId'] ?></td>
                <td style="width:  100px"><?php echo $registratie['AddedBy'] ?></td>
                <td style="width:  200px"><?php echo $registratie['InsertedOn'] ?></td>
                <td style="width:  100px"><?php echo $registratie['ModifiedBy'] ?></td>
                <td style="width:  200px"><?php echo $registratie['ModifiedOn'] ?></td>
            </tr>

            <?php
            }
            ?>
        </table>
        -->

        <!--
        <p>Test select all</p>
        <ul>
            <li>Message: <?php echo $object->getFeedback(); ?></li>
            <li>Error message: <?php echo $object->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $object->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle registraties</caption>
            <tr>
                <th>RegId</th>
                <th>LidId</th>
                <th>DocId</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
                foreach ($registratiesLijst as $registratie)
                {
            ?>
            <tr>
                <td style="width:  100px"><?php echo $registratie['RegId'] ?></td>
                <td style="width:  100px"><?php echo $registratie['LidId'] ?></td>
                <td style="width:  100px"><?php echo $registratie['DocId'] ?></td>
                <td style="width:  100px"><?php echo $registratie['AddedBy'] ?></td>
                <td style="width:  200px"><?php echo $registratie['InsertedOn'] ?></td>
                <td style="width:  100px"><?php echo $registratie['ModifiedBy'] ?></td>
                <td style="width:  200px"><?php echo $registratie['ModifiedOn'] ?></td>
            </tr>
            <?php
            }
            ?>
        </table>
        -->
        <!--
        <p>Test update reg</p>
        <ul>
            <li>Message: <?php echo $objectU->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectU->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectU->getErrorCode(); ?></li>
        </ul>
        -->

        <!--
        <p>Test insert reg</p>
        <ul>
            <li>Message: <?php echo $registratie->getFeedback(); ?></li>
            <li>Error message: <?php echo $registratie->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $registratie->getErrorCode(); ?></li>
            <li>ID: <?php echo $registratie->getRegId(); ?></li>
        </ul>
        -->

        <!--
        <p>Test delete reg</p>
        <ul>
            <li>Message: <?php echo $objectD->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectD->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectD->getErrorCode(); ?></li>
        </ul>
        -->

        
        

        
        

        
    </body>
</html>
