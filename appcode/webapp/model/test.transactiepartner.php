<?php
    include('../../helpers/feedback.class.php');
    include('../../helpers/base.class.php');
    include('transactiepartner.class.php');

    //insert testen
    /*
    $tap = new TransactiePartner();
    $tap->setTransactieId(1);
    $tap->setLidId(1);
    $tap->setRolId(1);
    $tap->setAddedBy('admin');
    $tap->insert();
    */
    

    //update testen
    /*
    $objectU = new TransactiePartner();
    $objectU->setTPId(2);
    $objectU->setTransactieId(1);
    $objectU->setLidId(1);
    $objectU->setRolId(4);
    $objectU->setModifiedBy("admin");
    $objectU->update();
    */

    //selectall testen
    /*
    $objectS = new TransactiePartner();
    $tpLijst = $objectS->selectAll();
    */

    //selectById
    /*
    $objectSI = new TransactiePartner();
    $objectSI->setTPId(2);
    $objectSI->selectTransactiepartnerById();
    */

    //delete testen
    /*
    $objectD = new TransactiePartner();
    $objectD->setTPId(2);
    $objectD->setModifiedBy('admin');
    $objectD->delete();
    */

    //testen van getKoper
    $objectK = new TransactiePartner();
    $objectK->setTransactieId(11);
    $result = $objectK->selectKoperVanTransactie();
    echo "lidid koper: ".$result[0];

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        
        <p>Test selectKoperVanTransactie</p>
        <ul>
            <li>Message: <?php echo $objectK->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectK->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectK->getErrorCode(); ?></li>
        </ul>
        

        <!--
        <p>Test select all</p>
        <ul>
            <li>Message: <?php echo $objectS->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectS->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectS->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle transactiepartners</caption>
            <tr>
                <th>TPId</th>
                <th>TAId</th>
                <th>RolId</th>
                <th>LidId</th>
                <th>DelStatus</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
            foreach ($tpLijst as $tp)
            {
            ?>
            <tr>
                <td style="width:  100px"><?php echo $tp['TPId'] ?></td>
                <td style="width:  100px"><?php echo $tp['TransactieId'] ?></td>
                <td style="width:  100px"><?php echo $tp['RolId'] ?></td>
                <td style="width:  100px"><?php echo $tp['LidId'] ?></td>
                <td style="width:  100px"><?php echo $tp['DelStatus'] ?></td>
                <td style="width:  100px"><?php echo $tp['AddedBy'] ?></td>
                <td style="width:  100px"><?php echo $tp['InsertedOn'] ?></td>
                <td style="width:  100px"><?php echo $tp['ModifiedBy'] ?></td>
                <td style="width:  100px"><?php echo $tp['ModifiedOn'] ?></td>
            </tr>

            <?php
            }
            ?>
        </table>
        -->

        <!--
        <p>Test delete tp</p>
        <ul>
            <li>Message: <?php echo $objectD->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectD->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectD->getErrorCode(); ?></li>
        </ul>
        -->
        <!--
        <p>Test selectTransactieById</p>
        <ul>
            <li>Message: <?php echo $objectSI->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectSI->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectSI->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <tr>
                <th>TPId</th>
                <th>TAId</th>
                <th>RolId</th>
                <th>LidId</th>
                <th>DelStatus</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <tr>
                <td style="width:  100px"><?php echo $objectSI->getTPId(); ?></td>
                <td style="width:  100px"><?php echo $objectSI->getTransactieId(); ?></td>
                <td style="width:  100px"><?php echo $objectSI->getRolId(); ?></td>
                <td style="width:  100px"><?php echo $objectSI->getLidId(); ?></td>  
                <td style="width:  100px"><?php echo $objectSI->getDelStatus(); ?></td>  
                <td style="width:  200px"><?php echo $objectSI->getAddedBy(); ?></td>                                                                
                <td style="width:  200px"><?php echo $objectSI->getInsertedOn(); ?></td>
                <td style="width:  200px"><?php echo $objectSI->getModifiedBy(); ?></td>     
                <td style="width:  200px"><?php echo $objectSI->getModifiedOn(); ?></td>
            </tr>
        </table>
        -->

        
        
        <!--
        <p>Test update doc</p>
        <ul>
            <li>Message: <?php echo $objectU->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectU->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectU->getErrorCode(); ?></li>
        </ul>
        -->

        <!--
        <p>Test insert tap</p>
        <ul>
            <li>Message: <?php echo $tap->getFeedback(); ?></li>
            <li>Error message: <?php echo $tap->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $tap->getErrorCode(); ?></li>
            <li>ID: <?php echo $tap->getTPId(); ?></li>
        </ul>
        -->
        
        
    </body>
</html>

