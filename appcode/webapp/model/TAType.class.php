<?php

class TAType extends \Base
{
    private $transactieTypeId;
    private $transactieType;
    private $addedBy;
    private $insertedOn;
    private $modifiedBy;
    private $modifiedOn;

    /*constructor in basisklasse volstaat*/

    /*set $transactieTypeId
    return true als nt leeg; return false als leeg
    */
    public function setTransactieTypeId($value)
    {
        if (is_numeric($value))
        {
            $this->transactieTypeId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /*set $transactieType
    return true als nt leeg; return false als leeg
    */
    public function setTransactieType($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->transactieType=$value;
            return TRUE;
        }
    }

    public function setAddedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->addedBy=$value;
            return TRUE;
        }
    }

    public function setModifiedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->modifiedBy=$value;
            return TRUE;
        }
    }

    /*setInsertedOn en setModifiedOn zitten vervat in de SQL-statements*/

    public function getTransactieTypeId()
    {
        return $this->docTypeId; 
    }

    public function getTransactieType()
    {
         return $this->docType;
    }

    public function getAddedBy()
    {
        return $this->addedBy;
    }

    public function getInsertedOn()
    {
        return $this->insertedOn;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function getModifiedOn()
    {
         return $this->modifiedOn;
    }

    public function selectAll()
    {
        $result=FALSE;
        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call taselectall()');
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle ta types ingelezen.';
            }
            else
            {
                $this->feedback = 'De tabel ta type is leeg.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De stored procedure taSelectAll is niet uitgevoerd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    

    //retourneert boolean
    /*methode noodzaakt het gebruik vd de andere methode setDocTypeId*/
    public function selectDocTypeById()
    {
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call DocTypeSelectById(:pId)');
        $preparedStatement -> bindParam(':pId', $this->docTypeId, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();
        $this->rowCount = $preparedStatement->rowCount();
        //fetch the output
        if($result = $preparedStatement->fetchAll())
        {
            $this->feedback = "{$preparedStatement->rowCount()} rij(en) met id = {$this->docTypeId} in de tabel DocType gevonden.";
            $this->docTypeId = $result[0]['DocTypeId'];
            $this->docType = $result[0]['DocType'];
            $this->addedBy = $result[0]['AddedBy'];
            $this->insertedOn = $result[0]['InsertedOn'];
            $this->modifiedBy = $result[0]['ModifiedBy'];
            $this->modifiedOn = $result[0]['ModifiedOn'];
        }
        else
        {
               $this->feedback = "Geen rijen met id = {$this->docTypeId} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
                $this->feedback = "Fout => rij is niet gevonden.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
                $this->rowCount = -1;
        }
        $this->close();
        return $result;
        }
         
    }
}
?>



