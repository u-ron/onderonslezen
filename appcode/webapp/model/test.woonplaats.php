<?php
    include('../../helpers/feedback.class.php');
    include('../../helpers/base.class.php');
    include('woonplaats.class.php');

    //insert testen
    /*
    $woonplaats = new Woonplaats();
    $woonplaats->setGemeente('Willebroek');
    $woonplaats->setPostcode('2830');
    $woonplaats->setAddedBy('admin');
    $woonplaats->insert();
    */

    //update testen
    /*
    $objectU = new Woonplaats();
    $objectU->setWoonplaatsId(5);
    $objectU->setGemeente('Willebroek');
    $objectU->setPostcode('2830');
    $objectU->setModifiedBy('admin');
    $objectU->update();
    */
    
    //selectAll testen
    /*
    $object = new Woonplaats();
    $woonplaatsenLijst=$object->selectAll();
    */

    //testen van woonplaatsSelectById()
    /*
    $objectS = new Woonplaats();
    $objectS->setWoonplaatsId(3);
    $objectS->selectWoonplaatsById();
    */

    //delete testen
    
    $objectD= new Woonplaats();
    $objectD->setWoonplaatsId(5);
    $objectD->delete();
    

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>

        <p>Test delete woonplaats</p>
        <ul>
            <li>Message: <?php echo $objectD->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectD->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectD->getErrorCode(); ?></li>
        </ul>
        
        
        
        <!--
        <p>Test selectWoonplaatsById</p>
        <ul>
            <li>Message: <?php echo $objectS->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectS->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectS->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <tr>
                <th>WoonplaatsId</th>
                <th>Gemeente</th>
                <th>Postcode</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <tr>
                <td style="width:  100px"><?php echo $objectS->getWoonplaatsId(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getGemeente(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getPostcode(); ?></td>  
                <td style="width:  100px"><?php echo $objectS->getAddedBy(); ?></td>                                                                    
                <td style="width:  200px"><?php echo $objectS->getInsertedOn(); ?></td>
                <td style="width:  100px"><?php echo $objectS->getModifiedBy(); ?></td>         
                <td style="width:  200px"><?php echo $objectS->getModifiedOn(); ?></td>
            </tr>
        </table>
        -->
        <!--
        <p>Test select all</p>
        <ul>
            <li>Message: <?php echo $object->getFeedback(); ?></li>
            <li>Error message: <?php echo $object->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $object->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle woonplaatsen</caption>
            <tr>
                <th>WoonplaatsId</th>
                <th>Gemeente</th>
                <th>Postcode</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
            foreach ($woonplaatsenLijst as $woonplaats)
            {
            ?>
            <tr>
                <td style="width:  100px"><?php echo $woonplaats['WoonplaatsId'] ?></td>
                <td style="width:  100px"><?php echo $woonplaats['Gemeente'] ?></td>
                <td style="width:  100px"><?php echo $woonplaats['Postcode'] ?></td>
                <td style="width:  100px"><?php echo $woonplaats['AddedBy'] ?></td>
                <td style="width:  200px"><?php echo $woonplaats['InsertedOn'] ?></td>
                <td style="width:  100px"><?php echo $woonplaats['ModifiedBy'] ?></td>
                <td style="width:  200px"><?php echo $woonplaats['ModifiedOn'] ?></td>
            </tr>
            <?php
            }
            ?>
        </table>
        -->
        <!--
        <p>Test update woonplaats</p>
        <ul>
            <li>Message: <?php echo $objectU->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectU->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectU->getErrorCode(); ?></li>
        </ul>
        -->
        <!--
        <p>Test insert woonplaats</p>
        <ul>
            <li>Message: <?php echo $woonplaats->getFeedback(); ?></li>
            <li>Error message: <?php echo $woonplaats->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $woonplaats->getErrorCode(); ?></li>
            <li>ID: <?php echo $woonplaats->getWoonplaatsId(); ?></li>
        </ul>
        -->
    </body>
</html>
