<?php
include('../../helpers/feedback.class.php');
include('../../helpers/base.class.php');
include('tatype.class.php');


$taTypeObject = new TAType();
$lijst = $taTypeObject->selectAll();


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        
        <p>Test select all</p>
        <ul>
            <li>Message: <?php echo $docTypeObject->getFeedback(); ?></li>
            <li>Error message: <?php echo $docTypeObject->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $docTypeObject->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle tatypes</caption>
            <tr>
                <th>Id</th>
                <th>TAType</th>
                <th>InsertedOn</th>
                <th>InsertedBy</th>
            </tr>
            <?php
                foreach ($lijst as $type)
                {
            ?>
            <tr>
                <td style="width:  100px"><?php echo $type['TATypeId'] ?></td>
                <td style="width:  100px"><?php echo $type['TAType'] ?></td>
                <td style="width:  200px"><?php echo $type['InsertedOn'] ?></td>
                <td style="width:  200px"><?php echo $type['ModifiedOn'] ?></td>
            </tr>
            <?php
                }
            ?>
        </table>
       


    </body>
</html>
