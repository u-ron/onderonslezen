<?php

class Auteur extends \Base
{
    private $auteurId;
    private $auteurNaam;
    private $auteurVoornaam;
    private $auteurInfo;
    private $addedBy;
    private $modifiedBy;
    private $insertedOn;
    private $modifiedOn; 

    /*constructor in basisklasse volstaat*/

    /*set $auteurId
    return true als nt leeg; return false als leeg
    */
    public function setAuteurId($value)
    {
        if (is_numeric($value))
        {
            $this->auteurId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /*set $auteurNaam
    * return true als nt leeg; return false als leeg
    */
    public function setAuteurNaam($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->auteurNaam=$value;
            return TRUE;
        }
    }

    /*set $auteurVoornaam
    * return true als nt leeg; return false als leeg
    */
    public function setAuteurVoornaam($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->auteurVoornaam=$value;
            return TRUE;
        }
    }

    /*set $auteurInfo
    * return true als nt leeg; return false als leeg
    */
    public function setAuteurInfo($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->auteurInfo=$value;
            return TRUE;
        }
    }

    public function setAddedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->addedBy=$value;
            return TRUE;
        }
    }

     public function setModifiedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->modifiedBy=$value;
            return TRUE;
        }
    }

    /*setInsertedOn en setModifiedOn zitten vervat in de SQL-statements*/

    public function getAuteurId()
    {
        return $this->auteurId; 
    }

    public function getAuteurNaam()
    {
         return $this->auteurNaam;
    }

    public function getAuteurVoornaam()
    {
         return $this->auteurVoornaam;
    }

    public function getAuteurInfo()
    {
        return $this->auteurInfo;
    }

    public function getAddedBy()
    {
        return $this->addedBy;
    }

    public function getInsertedOn()
    {
        return $this->insertedOn;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function getModifiedOn()
    {
         return $this->modifiedOn;
    }


    /*noodzaakt het gebruik vd setmethodes*/
    /*de getmethodes worden aangesproken op het moment van uitvoeren (en dus niet op het moment van coderen) wanneer de ref variabelen hun waarde krijgen*/
    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function insert()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try
            {
            $preparedStatement = $this->pdo->prepare('call auteurinsert(@pAuteurId, :pAuteurNaam, :pAuteurVoornaam, :pAuteurInfo, :pAddedBy)');
            $preparedStatement->bindParam(':pAuteurNaam', $this->auteurNaam, \PDO::PARAM_STR, 255); 
            $preparedStatement->bindParam(':pAuteurVoornaam', $this->auteurVoornaam, \PDO::PARAM_STR, 255);
            $preparedStatement->bindParam(':pAuteurInfo', $this->auteurInfo, \PDO::PARAM_STR, 255); 
            $preparedStatement->bindParam(':pAddedBy', $this->addedBy, \PDO::PARAM_STR, 255); 
            $success = $preparedStatement->execute(); 
            if ($success == 1)
            {
                $this->setAuteurId($this->pdo->query('select @pAuteurId')->fetchColumn()); 
                $this->feedback="De auteur {$this->auteurVoornaam} {$this->auteurNaam} met id <b> " . $this->getAuteurId() . "</b> is toegevoegd."; 
                $result = TRUE;
            }
            else
            {
                $this->feedback = "De auteur is niet toegevoegd";
                $sQLErrorInfo = $preparedStatement->errorInfo();//invulling van een array
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
                $result = FALSE;
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback="De auteur {$this->auteurVoornaam} {$this->auteurNaam} is niet toegevoegd."; 
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function delete()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try{
                $preparedStatement = $this->pdo->prepare('call auteurdelete(:pId)');
                /*in stored procedure staat pId als parameter; hoeft hierniet idem te zijn*/
                $preparedStatement->bindParam(':pId', $this->auteurId, \PDO::PARAM_INT, 11);
                $preparedStatement->execute();
                $result = $preparedStatement->rowCount();
                if($result)
                {
                $this->feedback = "Auteur {$this->auteurId} is verwijderd.";
                $result = TRUE;
                }
                else
                {
                     $this->feedback = "De auteur met id = {$this->auteurId} is niet gevonden en dus niet verwijderd.";
                     $sQLErrorInfo = $preparedStatement->errorInfo();
                     $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                     $this->errorMessage = $sQLErrorInfo[2];
                     $result = FALSE;
                }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De auteur {$this->auteurId} is niet verwijderd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    public function selectAll()
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call auteurselectall');
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle auteurs ingelezen.';
            }
            else
            {
                $this->feedback = 'De tabel auteur is leeg.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De stored procedure auteurselectall is niet uitgevoerd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*methode noodzaakt het gebruik vd andere methodes setAuteurId, setNaam, setVoornaam enz.*/
    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function update()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {

        try
        {
        $preparedStatement=$this->pdo->prepare('call auteurupdate(:pId, :pAuteurNaam, :pAuteurVoornaam, :pAuteurInfo, :pModifiedBy)');
        $preparedStatement->bindParam(':pId', $this->auteurId, \PDO::PARAM_INT, 11);
        $preparedStatement->bindParam(':pAuteurNaam', $this->auteurNaam, \PDO::PARAM_STR, 255);
        $preparedStatement->bindParam(':pAuteurVoornaam', $this->auteurVoornaam, \PDO::PARAM_STR, 255);
        $preparedStatement->bindParam(':pAuteurInfo', $this->auteurInfo, \PDO::PARAM_STR, 255); 
        $preparedStatement->bindParam(':pModifiedBy', $this->modifiedBy, \PDO::PARAM_STR, 255); 
        $preparedStatement->execute();

        $result = $preparedStatement->rowCount();
        if($result)
        {
            $this->feedback =  "Auteur {$this->auteurId} is gewijzigd.";
            $result = TRUE;
        }
        else
        {
            $this->feedback = "Auteur {$this->auteurId} is niet gevonden en dus niet gewijzigd.";
            $sQLErrorInfo = $preparedStatement->errorInfo();
            $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
            $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
             $this->feedback = "Auteur {$this->auteurId} is niet gewijzigd.";
             $this->errorMessage=$e->getMessage();
             $this->errorCode=$e->getCode();
        }
        $this->close();
        }
        return $result;
    }

    //retourneert FALSE bij mislukken en een 2dimens array bij slagen evenals een set van alle variabelen
    /*methode noodzaakt het gebruik vd de methode setAuteurId*/
    public function selectAuteurById()
    {
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call auteurselectbyid(:pId)');
        $preparedStatement -> bindParam(':pId', $this->auteurId, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();
        $this->rowCount = $preparedStatement->rowCount();
        //fetch the output
        if($result = $preparedStatement->fetchAll()) //Returns an array containing all of the result set rows 
        {
            $this->feedback = "{$preparedStatement->rowCount()} rij(en) met id = {$this->auteurId} in de tabel Auteur gevonden.";
            $this->id = $result[0]['AuteurId'];
            $this->auteurNaam = $result[0]['AuteurNaam'];
            $this->auteurVoornaam = $result[0]['AuteurVoornaam'];
            $this->auteurInfo = $result[0]['AuteurInfo'];
            $this->addedBy = $result[0]['AddedBy'];
            $this->insertedOn = $result[0]['InsertedOn'];
            $this->modifiedBy = $result[0]['ModifiedBy'];
            $this->modifiedOn = $result[0]['ModifiedOn'];
        }
        else //retourneert lege array
        {
               $this->feedback = "Geen rijen met id = {$this->auteurId} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
                $this->feedback = "Fout => rij is niet gevonden.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
                $this->rowCount = -1;
        }
        $this->close();
        return $result;
        }
         
    }

}
?>



