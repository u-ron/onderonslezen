<?php
    include('../../helpers/feedback.class.php');
    include('../../helpers/base.class.php');
    include('doc.class.php');

    //insert testen
    /*
    $doc = new Doc();
    $doc->setTitel('Zwarte tranen');
    $doc->setDocInfo('Bruggeling');
    $doc->setJaar(2000);
    $doc->setISBN('978-90-533-3683-0');
    $doc->setPrijs(5);//verwacht een numeric
    $doc->setTeLeen(TRUE);
    $doc->setTeKoop(FALSE);
    $doc->setDocTypeId(1);
    $doc->setTaalId(1);
    $doc->setToestandId(2);
    $doc->setAuteurId(2);
    $doc->setUitgeverId(4);
    $doc->setAddedBy('admin');
    $doc->insert();
    */

    //insert testen met NULL als optionele FK
    /*
    $doc = new Doc();
    $doc->setTitel('Trekking');
    $doc->setDocInfo('');
    $doc->setJaar(2000);
    $doc->setISBN('978-1-74059-768-5');
    $doc->setPrijs(5);//verwacht een numeric
    $doc->setTeLeen(TRUE);
    $doc->setTeKoop(FALSE);
    $doc->setDocTypeId(2);
    $doc->setTaalId(3);
    $doc->setToestandId(2);
    $doc->setAuteurId(NULL);
    $doc->setUitgeverId(NULL);
    $doc->setAddedBy('admin');
    $doc->insert();
    */

    //update testen
    /*
    $doc = new Doc();
    $doc->setTitel('Zwarte tranen');
    $doc->setDocInfo('Roman');
    $doc->setJaar(2000);
    $doc->setISBN('978-90-533-3683-0');
    $doc->setPrijs(5);//verwacht een numeric
    $doc->setTeLeen(TRUE);
    $doc->setTeKoop(FALSE);
    $doc->setDocTypeId(1);
    $doc->setTaalId(1);
    $doc->setToestandId(2);
    $doc->setAuteurId(2);
    $doc->setUitgeverId(4);
    $doc->setDocId(3);
    //setModifiedBy() hoeft er niet bij; NULL voorzien in databank
    $doc->update();
    */
    
    //selectAll testen
    /*
    $doc = new Doc();
    $docsLijst=$doc->selectAll();
    */

    //testen van docSelectById()
    /*
    $docS = new Doc();
    $docS->setDocId(3);
    $result = $docS->selectDocById();
    */

    //delete testen
    /*
    $docD= new Doc();
    $docD->setDocId(2);
    $docD->delete();
    */

    //selectKoopwaarVerkoper testen
    /*
    $doc = new Doc();
    $verkoperId = 4;
    $koopwaarLijst=$doc->selectKoopwaarVanVerkoper($verkoperId);
    print_r($koopwaarLijst);
    */

    //testen van selectdocsindeal
    /*
    $doc = new Doc();
    $docsInDeal = $doc->selectDocsInDeal(4);
    print_r($docsInDeal);
    */

    //selectLeenwaarVerlener testen
    /*
    $doc = new Doc();
    $verlenerId = 3;
    $leenwaarLijst=$doc->selectLeenwaarVanVerlener($verlenerId);
    print_r($leenwaarLijst);
    */
   
    //testen van selectdocsindeal
    /*
    $doc = new Doc();
    $docsInExchange = $doc->selectDocsInExchange(10);
    print_r($docsInExchange);
    */

    //testen van filterAllAvailable
    //Le Courrier moet verschijnen in titel =e en tijdschrijft keuze
    /*
    $doc = new Doc();
    $titel = "e";
    //$taalId= "";//lege string gaat niet
    $taalId = NULL;
    $docTypeId = 5;
    $result = $doc->filterAllAvailable($titel, $taalId, $docTypeId);
    print_r($result);
    */

    //testen van isDocInTransactie
    /*
     $doc = new Doc();
     $doc->setDocId(43);
     $result = $doc->isDocInTransaction();
     echo "resultaat: ".$result;
     */

     //testen van isDocVerkocht
     $doc = new Doc();
     $doc->setDocId(43);
     $result = $doc->isDocVerkocht();
     echo "resultaat: ".$result;


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        <!--
        <p>Test select documenten per lid</p>
        <ul>
            <li>Message: <?php echo $objectS->getFeedback(); ?></li>
            <li>Error message: <?php echo $objectS->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $objectS->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Docs per lid</caption>
            <tr>
                <th>RegId</th>
                <th>LidId</th>
                <th>DocId</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
                foreach ($result as $doc)
                {
            ?>
            <tr>
                <td style="width:  100px"><?php echo $doc['RegId'] ?></td>
                <td style="width:  100px"><?php echo $doc['LidId'] ?></td>
                <td style="width:  100px"><?php echo $doc['DocId'] ?></td>
                <td style="width:  100px"><?php echo $doc['AddedBy'] ?></td>
                <td style="width:  200px"><?php echo $doc['InsertedOn'] ?></td>
                <td style="width:  100px"><?php echo $doc['ModifiedBy'] ?></td>
                <td style="width:  200px"><?php echo $doc['ModifiedOn'] ?></td>
            </tr>
            <?php
            }
            ?>
        </table>
        -->

        <!--
        <p>Test isDocInTransaction</p>
        <ul>
            <li>Message: <?php echo $doc->getFeedback(); ?></li>
            <li>Error message: <?php echo $doc->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $doc->getErrorCode(); ?></li>
        </ul>
        -->

        <!--
        <p>Test select docs in exchange</p>
        <ul>
            <li>Message: <?php echo $doc->getFeedback(); ?></li>
            <li>Error message: <?php echo $doc->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $doc->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle documenten in exchange</caption>
            <tr>
                <th>DocId</th>
                <th>Titel</th>
                <th>AuteurId</th>
                <th>UitgeverId</th>
            </tr>
            <?php
            foreach ($docsInExchange as $doc)
            {
            ?>
            <tr>
                <td style="width:  10px"><?php echo $doc['docid'] ?></td>
                <td style="width:  100px"><?php echo $doc['titel'] ?></td>
                <td style="width:  20px"><?php echo $doc['auteurid'] ?></td>
                <td style="width:  20px"><?php echo $doc['uitgeverid'] ?></td>
            </tr>

            <?php
            }
            ?>
        </table>
        -->

        <!--
        <p>Test select leenwaar verlener</p>
        <ul>
            <li>Message: <?php echo $doc->getFeedback(); ?></li>
            <li>Error message: <?php echo $doc->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $doc->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle leenwaar</caption>
            <tr>
               
                <th>Titel</th>
                <th>DocType</th>
                <th>Auteur</th>
            </tr>
            <?php
            foreach ($leenwaarLijst as $doc)
            {
            ?>
            <tr>
             
                <td style="width:  100px"><?php echo $doc['titel'] ?></td>
                <td style="width:  100px"><?php echo $doc['doctype'] ?></td>
                <td style="width:  20px"><?php echo $doc['auteurnaam'] ?></td>
            </tr>

            <?php
            }
            ?>
        </table>
        -->

        <!--
        <p>Test select docs in deal</p>
        <ul>
            <li>Message: <?php echo $doc->getFeedback(); ?></li>
            <li>Error message: <?php echo $doc->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $doc->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle documenten in deal</caption>
            <tr>
                <th>DocId</th>
                <th>Titel</th>
                <th>Prijs</th>
                <th>AuteurId</th>
                <th>UitgeverId</th>
            </tr>
            <?php
            foreach ($docsInDeal as $doc)
            {
            ?>
            <tr>
                <td style="width:  10px"><?php echo $doc['docid'] ?></td>
                <td style="width:  100px"><?php echo $doc['titel'] ?></td>
                <td style="width:  100px"><?php echo $doc['prijs'] ?></td>
                <td style="width:  20px"><?php echo $doc['auteurid'] ?></td>
                <td style="width:  20px"><?php echo $doc['uitgeverid'] ?></td>
            </tr>

            <?php
            }
            ?>
        </table>
        -->

        <!--
        <p>Test select koopwaar verkoper</p>
        <ul>
            <li>Message: <?php echo $doc->getFeedback(); ?></li>
            <li>Error message: <?php echo $doc->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $doc->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle koopwaar</caption>
            <tr>
               
                <th>Titel</th>
                <th>DocType</th>
                <th>Prijs</th>
                <th>AuteurId</th>
            </tr>
            <?php
            foreach ($koopwaarLijst as $doc)
            {
            ?>
            <tr>
             
                <td style="width:  100px"><?php echo $doc['titel'] ?></td>
                <td style="width:  100px"><?php echo $doc['doctype'] ?></td>
                <td style="width:  100px"><?php echo $doc['prijs'] ?></td>
                <td style="width:  20px"><?php echo $doc['auteurid'] ?></td>
            </tr>

            <?php
            }
            ?>
        </table>
        -->


        <!--
        <p>Test insert doc</p>
        <ul>
            <li>Message: <?php echo $doc->getFeedback(); ?></li>
            <li>Error message: <?php echo $doc->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $doc->getErrorCode(); ?></li>
            <li>ID: <?php echo $doc->getDocId(); ?></li>
        </ul>
        -->

        <!--
        <p>Test select all</p>
        <ul>
            <li>Message: <?php echo $doc->getFeedback(); ?></li>
            <li>Error message: <?php echo $doc->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $doc->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <caption>Alle documenten</caption>
            <tr>
                <th>DocId</th>
                <th>Titel</th>
                <th>Info doc</th>
                <th>Jaar</th>
                <th>Prijs</th>
                <th>ISBN</ISBN>
                <th>Te koop</th>
                <th>Te leen</th>
                <th>AuteurId</th>
                <th>UitgeverId</th>
                <th>Added by</th>
                <th>InsertedOn</th>
                <th>Modified by</th>
                <th>ModifiedOn</th>
            </tr>
            <?php
            foreach ($docsLijst as $doc)
            {
            ?>
            <tr>
                <td style="width:  10px"><?php echo $doc['DocId'] ?></td>
                <td style="width:  100px"><?php echo $doc['Titel'] ?></td>
                <td style="width:  100px"><?php echo $doc['DocInfo'] ?></td>
                <td style="width:  100px"><?php echo $doc['Jaar'] ?></td>
                <td style="width:  100px"><?php echo $doc['Prijs'] ?></td>
                <td style="width:  100px"><?php echo $doc['ISBN'] ?></td>
                <td style="width:  40px"><?php echo $doc['TeKoop'] ?></td>
                <td style="width:  40px"><?php echo $doc['TeLeen'] ?></td>
                <td style="width:  20px"><?php echo $doc['AuteurId'] ?></td>
                <td style="width:  20px"><?php echo $doc['UitgeverId'] ?></td>
                <td style="width:  100px"><?php echo $doc['AddedBy'] ?></td>
                <td style="width:  100px"><?php echo $doc['InsertedOn'] ?></td>
                <td style="width:  100px"><?php echo $doc['ModifiedBy'] ?></td>
                <td style="width:  100px"><?php echo $doc['ModifiedOn'] ?></td>
            </tr>

            <?php
            }
            ?>
        </table>
        -->

        <!--
        <p>Test update doc</p>
        <ul>
            <li>Message: <?php echo $doc->getFeedback(); ?></li>
            <li>Error message: <?php echo $doc->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $doc->getErrorCode(); ?></li>
        </ul>
        -->
        <!--
        <p>Test delete doc</p>
        <ul>
            <li>Message: <?php echo $docD->getFeedback(); ?></li>
            <li>Error message: <?php echo $docD->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $docD->getErrorCode(); ?></li>
        </ul>
        -->
        <!--
        <p>Test selectDocById</p>
        <ul>
            <li>Message: <?php echo $docS->getFeedback(); ?></li>
            <li>Error message: <?php echo $docS->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $docS->getErrorCode(); ?></li>
        </ul>
        <table border="1">
            <tr>
                <th>DocId</th>
                <th>Titel</th>
                <th>Info Auteur</th>
                <th>Jaar</th>
                <th>Prijs</th>
                <th>AuteurId</th>
                <th>Toestand</th>
                <th>AddedBy</th>
                <th>InsertedOn</th>
                <th>ModifiedBy</th>
                <th>ModifiedOn</th>
            </tr>
            <tr>
                <td style="width:  10px"><?php echo $docS->getDocId(); ?></td>
                <td style="width:  100px"><?php echo $docS->getTitel(); ?></td>
                <td style="width:  100px"><?php echo $docS->getDocInfo(); ?></td>  
                <td style="width:  100px"><?php echo $docS->getJaar(); ?></td> 
                <td style="width:  100px"><?php echo $docS->getPrijs(); ?></td> 
                <td style="width:  20px"><?php echo $docS->getAuteurId(); ?></td>  
                <td style="width:  20px"><?php echo $docS->getToestandId(); ?></td> 
                <td style="width:  200px"><?php echo $docS->getAddedBy(); ?></td>                                                                
                <td style="width:  200px"><?php echo $docS->getInsertedOn(); ?></td>
                <td style="width:  200px"><?php echo $docS->getModifiedBy(); ?></td>     
                <td style="width:  200px"><?php echo $docS->getModifiedOn(); ?></td>
            </tr>
        </table>
        -->

        

       
        

        

    </body>
</html>
