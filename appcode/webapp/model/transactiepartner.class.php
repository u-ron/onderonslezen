<?php

class TransactiePartner extends \Base
{
    private $tPId;
    private $transactieId;
    private $rolId;
    private $lidId;
    private $delStatus;
    private $addedBy;
    private $modifiedBy;
    private $insertedOn;
    private $modifiedOn; 

    /*constructor in basisklasse volstaat*/

    /*set $transactieId
    return true als nt leeg; return false als leeg
    */
    public function setTPId($value)
    {
        if (is_numeric($value))
        {
            $this->tPId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setTransactieId($value)
    {
        if (is_numeric($value))
        {
            $this->transactieId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setRolId($value)
    {
        if (is_numeric($value))
        {
            $this->rolId = $value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function setLidId($value)
    {
        if (is_numeric($value))
        {
            $this->lidId = $value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    /*niet nodig*/
    public function setDelStatus($value)
    {
        if(is_bool($value))
        {
            $this->delStatus = $value;
            return TRUE;
        }
        else
        {
            return FALSE;    
        }
    }

    public function setAddedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->addedBy=$value;
            return TRUE;
        }
    }

    public function setModifiedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->modifiedBy=$value;
            return TRUE;
        }
    }

    /*setInsertedOn en setModifiedOn zitten vervat in de SQL-statements*/

    public function getTPId()
    {
        return $this->tPId; 
    }

    public function getTransactieId()
    {
        return $this->transactieId; 
    }

    public function getLidId()
    {
         return $this->lidId;
    }

    public function getRolId()
    {
         return $this->rolId;
    }

    /*niet nodig*/
    public function getDelStatus()
    {
         return $this->delStatus;
    }

    public function getAddedBy()
    {
        return $this->addedBy;
    }

    public function getInsertedOn()
    {
        return $this->insertedOn;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function getModifiedOn()
    {
         return $this->modifiedOn;
    }


    /*noodzaakt het gebruik vd setmethodes*/
    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function insert()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try
            {
            $preparedStatement = $this->pdo->prepare('call transactiepartnerinsert(@pTPId, :pTransactieId, :pLidId, :pRolId, :pDelStatus, :pAddedBy)');
            $preparedStatement->bindParam(':pTransactieId', $this->transactieId, \PDO::PARAM_INT, 11); 
            $preparedStatement->bindParam(':pLidId', $this->lidId, \PDO::PARAM_INT, 11); 
            $preparedStatement->bindParam(':pRolId', $this->rolId, \PDO::PARAM_INT, 11); 
            $preparedStatement->bindValue(':pDelStatus', 0); 
            $preparedStatement->bindParam(':pAddedBy', $this->addedBy, \PDO::PARAM_STR, 255); 
            $success = $preparedStatement->execute(); 
            if ($success)
            {
                $this->setTPId($this->pdo->query('select @pTPId')->fetchColumn()); 
                $this->feedback="De transactiepartner met als id <b> " . $this->getTPId() . "</b> is toegevoegd."; 
                $result = TRUE;
            }
            else
            {
                $this->feedback = "De stored procedure transactiepartnerinsert is niet uitgevoerd.";
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
                $result = FALSE;
            }
            }
            catch (\PDOException $e)
            {
            $this->feedback="De transactiepartner is niet toegevoegd."; 
            $this->errorMessage=$e->getMessage();
            $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*methode noodzaakt het gebruik vd andere methodes setTPId enz.*/
    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function update()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {

        try
        {
        $preparedStatement=$this->pdo->prepare('call transactiepartnerupdate(:pTPId, :pTransactieId, :pLidId, :pRolId, :pModifiedBy)');
        $preparedStatement->bindParam(':pTPId', $this->tPId, \PDO::PARAM_INT, 11);
        $preparedStatement->bindParam(':pTransactieId', $this->transactieId, \PDO::PARAM_INT, 11); 
        $preparedStatement->bindParam(':pLidId', $this->lidId, \PDO::PARAM_INT, 11); 
        $preparedStatement->bindParam(':pRolId', $this->rolId, \PDO::PARAM_INT, 11); 
        $preparedStatement->bindParam(':pModifiedBy', $this->modifiedBy, \PDO::PARAM_STR, 255); 
        $preparedStatement->execute();

        $result = $preparedStatement->rowCount();
        if($result)
        {
            $this->feedback =  "Transactiepartner {$this->tPId} is gewijzigd.";
            $result = TRUE;
        }
        else
        {
            $this->feedback = "Transactiepartner {$this->tPId} is niet gevonden en dus niet gewijzigd.";
            $sQLErrorInfo = $preparedStatement->errorInfo();
            $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
            $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $ex)
        {
             $this->feedback = "De stored procedure transactiepartnerupdate is niet uitgevoerd.";
             $this->errorMessage = $ex->getMessage();
             $this->errorCode = $ex->getCode();
        }
        $this->close();
        }
        return $result;
    }

    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    /*niet gebruikt*/
    public function selectAll()
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call transactiepartnerselectall');
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle transactiepartners ingelezen.';
            }
            else
            {
                $this->feedback = 'De tabel Transactiepartner is leeg.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $ex)
            {
                $this->feedback = "De stored procedure transactiepartnerselectall is niet uitgevoerd.";
                $this->errorMessage = $ex->getMessage();
                $this->errorCode = $ex->getCode();
            }
            $this->close();
        }
        return $result;
    }

    //retourneert FALSE bij mislukken en een 2dimens array bij slagen
    /*methode noodzaakt het gebruik vd de methode setTPId*/
    public function selectTransactiePartnerById()
    {
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call transactiepartnerselectbyid(:pId)');
        $preparedStatement -> bindParam(':pId', $this->tPId, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();
        $this->rowCount = $preparedStatement->rowCount();
        //fetch the output
        if($result = $preparedStatement->fetchAll()) //Returns an array containing all of the result set rows 
        {
            $this->feedback = "{$preparedStatement->rowCount()} rij(en) met id = {$this->tPId} in de tabel Transactiepartner gevonden.";
            $this->tPId = $result[0]['TPId'];
            $this->transactieId = $result[0]['TransactieId'];
            $this->rolId = $result[0]['RolId'];
            $this->lidId = $result[0]['LidId'];
            $this->delStatus = $result[0]['DelStatus'];
            $this->addedBy = $result[0]['AddedBy'];
            $this->insertedOn = $result[0]['InsertedOn'];
            $this->modifiedBy = $result[0]['ModifiedBy'];
            $this->modifiedOn = $result[0]['ModifiedOn'];
        }
        else //retourneert lege array
        {
               $this->feedback = "Geen rijen met id = {$this->tPId} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
                $this->feedback = "Fout => rij is niet gevonden.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
                $this->rowCount = -1;
        }
        $this->close();
        return $result;
        }
         
    }

    /*vergt het gebruik van de methodes setTPId en setModifiedBy*/
    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function delete()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try
            {
                $preparedStatement = $this->pdo->prepare('call transactiepartnerdelete(:pId, :pModifiedBy)');
                $preparedStatement->bindParam(':pId', $this->tPId, \PDO::PARAM_INT, 11);
                $preparedStatement->bindParam(':pModifiedBy', $this->modifiedBy, \PDO::PARAM_STR, 255); 
                $preparedStatement->execute();
                $result = $preparedStatement->rowCount();
                if($result)
                {
                    $this->feedback = "Transactiepartner {$this->tPId} heedt de delstatus = 1.";
                    $result = TRUE;
                }
                else
                {
                     $this->feedback = "De transactiepartner met id = {$this->tPId} is niet gevonden en dus niet verwijderd.";
                     $sQLErrorInfo = $preparedStatement->errorInfo();
                     $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                     $this->errorMessage = $sQLErrorInfo[2];
                     $result = FALSE;
                }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De sp transactiepartnerdelete is niet uitgevoerd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    
    //retourneert FALSE bij mislukken en een 1dim array van 1 element bij slagen
    /*methode noodzaakt het gebruik vd de methode setTransactieId*/
    /*niet gebruikt*/
    public function selectKoperVanTransactie()
    {
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call TransactiePartnerGetKoper(:pTransactieId)');
        $preparedStatement -> bindParam(':pTransactieId', $this->transactieId, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();
        if($result = $preparedStatement->fetch())  
        {
            $this->lidId = $result[0];
            $this->feedback = "OK";
        }
        else 
        {
               $this->feedback = "Geen rijen met id = {$this->transactieId} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $ex)
        {
                $this->feedback = "Fout => lidid is niet gevonden.";
                $this->errorMessage=$ex->getMessage();
                $this->errorCode=$ex->getCode();
        }
        $this->close();
        return $result;
        }
         
    }
    

    
    
    
}
?>



