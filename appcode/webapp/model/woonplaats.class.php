<?php

class Woonplaats extends \Base
{
    private $woonplaatsId;
    private $gemeente;
    private $postcode;
    private $addedBy;
    private $modifiedBy;
    private $insertedOn;
    private $modifiedOn; 

    /*constructor in basisklasse volstaat*/

    /*set $woonplaatsId
    return true als nt leeg; return false als leeg
    */
    public function setWoonplaatsId($value)
    {
        if (is_numeric($value))
        {
            $this->woonplaatsId=$value;
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /*set $uitgever
     return true als nt leeg; return false als leeg
    */
    public function setGemeente($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->gemeente=$value;
            return TRUE;
        }
    }

    /*set $uitgeverInfo
     return true als nt leeg; return false als leeg
    */
    public function setPostcode($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->postcode=$value;
            return TRUE;
        }
    }

    public function setAddedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->addedBy=$value;
            return TRUE;
        }
    }

     public function setModifiedBy($value)
    {
        if (empty($value))
        {
            return FALSE;
        }
        else
        {
            $this->modifiedBy=$value;
            return TRUE;
        }
    }

    /*setInsertedOn en setModifiedOn zitten vervat in de SQL-statements*/

    public function getWoonplaatsId()
    {
        return $this->woonplaatsId; 
    }

    public function getGemeente()
    {
         return $this->gemeente;
    }

    public function getPostcode()
    {
        return $this->postcode;
    }

    public function getAddedBy()
    {
        return $this->addedBy;
    }

    public function getInsertedOn()
    {
        return $this->insertedOn;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function getModifiedOn()
    {
         return $this->modifiedOn;
    }


    /*noodzaakt het gebruik vd setmethodes*/
    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function insert()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try
            {
            $preparedStatement = $this->pdo->prepare('call woonplaatsinsert(@pWoonplaatsId, :pGemeente, :pPostcode, :pAddedBy)');
            $preparedStatement->bindParam(':pGemeente', $this->gemeente, \PDO::PARAM_STR, 255); 
            $preparedStatement->bindParam(':pPostcode', $this->postcode, \PDO::PARAM_STR, 255); 
            $preparedStatement->bindParam(':pAddedBy', $this->addedBy, \PDO::PARAM_STR, 255); 
            $success = $preparedStatement->execute(); 
            if ($success)
            {
                $this->setWoonplaatsId($this->pdo->query('select @pWoonplaatsId')->fetchColumn()); 
                $this->feedback="De woonplaats {$this->gemeente} {$this->postcode} met id <b> " . $this->getWoonplaatsId() . "</b> is toegevoegd."; 
                $result = TRUE;
            }
            else
            {
                $this->feedback = "De woonplaats is niet toegevoegd";
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
                $result = FALSE;
            }
            }
            catch (\PDOException $e)
            {
            $this->feedback="De woonplaats {$this->gemeente} is niet toegevoegd."; 
            $this->errorMessage=$e->getMessage();
            $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*methode noodzaakt het gebruik vd andere methodes setWoonplaatsId, setUitgever enz.*/
    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function update()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {

        try
        {
        $preparedStatement=$this->pdo->prepare('call woonplaatsupdate(:pId, :pGemeente, :pPostcode, :pModifiedBy)');
        $preparedStatement->bindParam(':pId', $this->woonplaatsId, \PDO::PARAM_INT, 11);
        $preparedStatement->bindParam(':pGemeente', $this->gemeente, \PDO::PARAM_STR, 255);
        $preparedStatement->bindParam(':pPostcode', $this->postcode, \PDO::PARAM_STR, 255); 
        $preparedStatement->bindParam(':pModifiedBy', $this->modifiedBy, \PDO::PARAM_STR, 255); 
        $preparedStatement->execute();

        $result = $preparedStatement->rowCount();
        if($result)
        {
            $this->feedback =  "Woonplaats {$this->woonplaatsId} is gewijzigd.";
            $result = TRUE;
        }
        else
        {
            $this->feedback = "Woonplaats {$this->woonplaatsId} is niet gevonden en dus niet gewijzigd.";
            $sQLErrorInfo = $preparedStatement->errorInfo();
            $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
            $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
             $this->feedback = "Woonplaats {$this->woonplaatsId} is niet gewijzigd.";
             $this->errorMessage=$e->getMessage();
             $this->errorCode=$e->getCode();
        }
        $this->close();
        }
        return $result;
    }

    /*retourneert steeds boolean; ook feedback is voorzien*/
    public function delete()
    {
        $result=FALSE;
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';

        if($this->connect())
        {
            try{
                $preparedStatement = $this->pdo->prepare('call woonplaatsdelete(:pId)');
                /*in stored procedure staat pId als parameter; hoeft hierniet idem te zijn*/
                $preparedStatement->bindParam(':pId', $this->woonplaatsId, \PDO::PARAM_INT, 11);
                $preparedStatement->execute();
                $result = $preparedStatement->rowCount();
                if($result)
                {
                $this->feedback = "Woonplaats {$this->woonplaatsId} is verwijderd.";
                $result = TRUE;
                }
                else
                {
                     $this->feedback = "De woonplaats met id = {$this->woonplaatsId} is niet gevonden en dus niet verwijderd.";
                     $sQLErrorInfo = $preparedStatement->errorInfo();
                     $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                     $this->errorMessage = $sQLErrorInfo[2];
                     $result = FALSE;
                }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De woonplaats {$this->woonplaatsId} is niet verwijderd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    /*retourneert false bij mislukken; bij slagen een 2dim array*/
    public function selectAll()
    {
        $result=FALSE;

        if($this->connect())
        {
            try
            {
            $preparedStatement=$this->pdo->prepare('call woonplaatsselectall');
            $preparedStatement->execute();
            if ($result = $preparedStatement->fetchAll())
            {
                $this->feedback = 'Alle woonplaatsen ingelezen.';
            }
            else
            {
                $this->feedback = 'De tabel Woonplaats is leeg.';
                $sQLErrorInfo = $preparedStatement->errorInfo();
                $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
                $this->errorMessage = $sQLErrorInfo[2];
            }
            }
            catch (\PDOException $e)
            {
                $this->feedback = "De stored procedure woonplaatsselectall is niet uitgevoerd.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
            }
            $this->close();
        }
        return $result;
    }

    

    //retourneert FALSE bij mislukken en een 2dimens array bij slagen
    /*methode noodzaakt het gebruik vd de methode setAuteurId*/
    public function selectWoonplaatsById()
    {
        $this->errorCode='none';
        $this->errorMessage='none';
        $this->feedback='none';
        $result=FALSE;

        if($this -> connect())
        {
        try 
        {
       
        $preparedStatement = $this->pdo->prepare('call woonplaatsselectbyid(:pId)');
        $preparedStatement -> bindParam(':pId', $this->woonplaatsId, \PDO::PARAM_INT, 11);
        $preparedStatement->execute();
        $this->rowCount = $preparedStatement->rowCount();
        //fetch the output
        if($result = $preparedStatement->fetchAll()) //Returns an array containing all of the result set rows 
        {
            $this->feedback = "{$preparedStatement->rowCount()} rij(en) met id = {$this->woonplaatsId} in de tabel Woonplaats gevonden.";
            $this->woonplaatsId = $result[0]['WoonplaatsId'];
            $this->gemeente = $result[0]['Gemeente'];
            $this->postcode = $result[0]['Postcode'];
            $this->addedBy = $result[0]['AddedBy'];
            $this->insertedOn = $result[0]['InsertedOn'];
            $this->modifiedBy = $result[0]['ModifiedBy'];
            $this->modifiedOn = $result[0]['ModifiedOn'];
        }
        else //retourneert lege array
        {
               $this->feedback = "Geen rijen met id = {$this->woonplaatsId} gevonden.";
               $sQLErrorInfo = $preparedStatement->errorInfo();
               $this->errorCode = $sQLErrorInfo[0].'/'.$sQLErrorInfo[1];
               $this->errorMessage = $sQLErrorInfo[2];
        }
        }
        catch (\PDOException $e)
        {
                $this->feedback = "Fout => rij is niet gevonden.";
                $this->errorMessage=$e->getMessage();
                $this->errorCode=$e->getCode();
                $this->rowCount = -1;
        }
        $this->close();
        return $result;
        }
         
    }

    
    
}
?>



