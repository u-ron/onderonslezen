<?php
include('../../helpers/feedback.class.php');    
include('../../helpers/base.class.php');
include('../model/uitgever.class.php');
include('../model/lid.class.php'); 
include('../help/cleaninput.php');
session_start();


//verwijderen
if(isset($_GET['uitgeverid']))
{
    $uitgeverObject = new Uitgever();
    $uitgeverId = $_GET['uitgeverid'];
    $uitgeverObject->setUitgeverId($uitgeverId);
    $result = $uitgeverObject->delete();
    if($result)
    {
        header('Location: ../../../appcode/webapp/view/uitgevers.php');
    }
    else
    {
        $message = "Geen verwijdering mogelijk.";
        $_SESSION['message'] = $message;
        header('Location: ../../../appcode/webapp/view/uitgevers.php');
    }
}

//toevoegen
//$_POST is always set, but its content might be empty.
if(isset($_POST['btnUitgeverSave']))
{
        $_POST = opschonenInput($_POST);
        $uitgeverObject = new Uitgever();
        $uitgeverObject->setUitgever($_POST['uitgever']);
        $uitgeverObject->setUitgeverInfo($_POST['infoUitgever']);
        $uitgeverObject->setAddedBy('admin');
        $result = $uitgeverObject->insert();
        if($result)
        {
            header('Location: ../../../appcode/webapp/view/uitgevers.php');
        }
        else
        {
            $message = $uitgeverObject->getFeedback();
            $_SESSION['message'] = $message;
            header('Location: ../../../appcode/webapp/view/uitgevers.php');
        }
}
//wijzigen
elseif(isset($_POST['btnUitgeverUpdate']))
{
        $_POST = opschonenInput($_POST);
        $uitgeverObject = new Uitgever();
        $uitgeverObject->setUitgeverId($_POST['idHidden']);
        $uitgeverObject->setUitgever($_POST['uitgever']);
        $uitgeverObject->setUitgeverInfo($_POST['infoUitgever']);
        $uitgeverObject->setModifiedBy('admin');
        $result = $uitgeverObject->update();
        if($result)
        {
            header('Location: ../../../appcode/webapp/view/uitgevers.php');
        }
        else
        {
            $message = $uitgeverObject->getFeedback();
            $_SESSION['message'] = $message;
            header('Location: ../../../appcode/webapp/view/uitgevers.php');
        }
}
?>




