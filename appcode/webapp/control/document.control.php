<?php
include('../../../appcode/helpers/feedback.class.php');    
include('../../helpers/base.class.php');
include('../model/doc.class.php');
include('../model/registratie.class.php');
include('../model/lid.class.php'); 
include('../help/cleaninput.php');

session_start();



//verwijderen van een document
//een doc kan pas verwijderd worden als er geen dockenmerken en foto's meer zijn. De fk constraint zorgt hiervoor.
// Evenals het document niet in transactie is, wat dient gecontroleerd te worden.

if(isset($_GET['documentid']))
{
    //nagaan of een doc in transactie is
    $docObject1 = new Doc();
    $docObject1->setDocId($_GET['documentid']);
    $result1 = $docObject1->isDocIntransaction();

    if(!$result1)//niet meer in TA
    {
        //1. verwijdering uit tabel TransactieDoc
        //niet nodig in code; gebeurt op SQL niveau met set NULL


        //2. verwijdering uit tabel TransactieDoc
        //niet nodig in code; gebeurt op SQL niveau met cascade

        //3. verwijdering uit tabel doc
        $docObject2 = new Doc();
        $docObject2->setDocId($_GET['documentid']);
        $result2 = $docObject2->delete();
        if($result2)
        {
            header('Location: ../../../appcode/webapp/view/mijn_documenten.php');
        }
        else
        {
            $message = "Document verwijdering pas mogelijk als het geen foto's en specifieke kenmerken meer heeft. Evenmin mag
            het document nog in transactie zijn.";
            $_SESSION['message'] = $message;
            header('Location: ../../../appcode/webapp/view/mijn_documenten.php');
        }
    }
    else//wel in TA
    {
        $message = "Document verwijdering niet mogelijk als het document nog in transactie is.";
        $_SESSION['message'] = $message;
        header('Location: ../../../appcode/webapp/view/mijn_documenten.php');
    }
}

//toevoegen van een document
//$_POST is always set, but its content might be empty.
if(isset($_POST['btnDocSave']))
{
        $_POST = opschonenInput($_POST);
        //tabel Doc
        $docObject = new Doc();
        $docObject->setTitel($_POST['titel']);
        $docObject->setAuteurId($_POST['auteurid']);
        $docObject->setUitgeverId($_POST['uitgeverid']);
        $docObject->setTaalId($_POST['taalid']);
        $docObject->setDocTypeId($_POST['doctypeid']);
        $docObject->setToestandId($_POST['toestandid']);
        $docObject->setDocInfo($_POST['infoDoc']);
        $docObject->setJaar($_POST['jaar']);
        $docObject->setISBN($_POST['isbn']);
        $docObject->setPrijs($_POST['prijs']);
        if(isset($_POST['teleen']))
        {
              $docObject->setTeLeen(TRUE);
        }
        else{
            $docObject->setTeLeen(FALSE);
        }

        if(isset($_POST['tekoop']))
        {
              $docObject->setTeKoop(TRUE);
        }
        else
        {
             $docObject->setTeKoop(FALSE);
        }
        //zonder isset is er de foutmelding undefined index      

        $docObject->setAddedBy($_SESSION['username']);
        $result = $docObject->insert();
        $nieuwDocId = $docObject->getDocId();

        //tabel Registratie
        $regObject = new Registratie();
        $regObject->setLidId($_SESSION['lidid']);
        $regObject->setDocId($nieuwDocId);
        $regObject->setAddedBy($_SESSION['username']);
        $regObject->insert();
        if($result)
        {
            header('Location: ../../../appcode/webapp/view/mijn_documenten.php');    
        }
        else
        {
            $message = $docObject->getFeedback();
            $_SESSION['message'] = $message;
            header('Location: ../../../appcode/webapp/view/mijn_documenten.php');    
        }
        
}
//wijzigen van een document
elseif(isset($_POST['btnDocUpdate']))
{
        $_POST = opschonenInput($_POST);
        $docObject = new Doc();
        $docObject->setDocId($_POST['idhidden']);
        $docObject->setTitel($_POST['titel']);
        $docObject->setAuteurId($_POST['auteurid']);
        $docObject->setUitgeverId($_POST['uitgeverid']);
        $docObject->setTaalId($_POST['taalid']);
        $docObject->setDocTypeId($_POST['doctypeid']);
        $docObject->setToestandId($_POST['toestandid']);
        $docObject->setDocInfo($_POST['infoDoc']);
        $docObject->setJaar($_POST['jaar']);
        $docObject->setISBN($_POST['isbn']);
        $docObject->setPrijs($_POST['prijs']);
        if(isset($_POST['teleen']))
        {
            $docObject->setTeLeen(TRUE);
        }
        else{
            $docObject->setTeLeen(FALSE);
        }
        if(isset($_POST['tekoop']))
        {
            $docObject->setTeKoop(TRUE);
        }
        else{
             $docObject->setTeKoop(FALSE);
        }
        //zonder isset gaat wellicht een lege string naar de tabel; enkel 1 of 0 kan in de tabel      
        $docObject->setModifiedBy($_SESSION['username']);
        $result = $docObject->update();
        if($result)
        {
            header('Location: ../../../appcode/webapp/view/mijn_documenten.php');    
        }
        else
        {
            $message = $docObject->getFeedback();
            $_SESSION['message'] = $message;
            header('Location: ../../../appcode/webapp/view/mijn_documenten.php');    
        }

        //update en delete in de tabel registratie zijn blijkbaar nt nodig
}

//gecachte bestanden wissen
$files = glob('../view/cached/*');//array van bestanden in de cached folder
foreach($files as $file)
{
    if(is_file($file))
    {
        unlink($file);
    }    
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        
        <p>Test delete doc</p>
        <ul>
            <li>Message: <?php echo $docObject->getFeedback(); ?></li>
            <li>Error message: <?php echo $docObject->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $docObject->getErrorCode(); ?></li>
        </ul>
        

        <!--
        <p>Test update doc</p>
        <ul>
            <li>Message: <?php echo $docObject->getFeedback(); ?></li>
            <li>Error message: <?php echo $docObject->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $docObject->getErrorCode(); ?></li>
            <li>$_POST['teleen']: <?php echo $_POST['teleen']?></li>
            <li>$_POST['tekoop']: <?php echo $_POST['tekoop']?></li>
        </ul>
        -->

        <!--
        <p>Test insert doc</p>
        <ul>
            <li>Message: <?php echo $docObject->getFeedback(); ?></li>
            <li>Error message: <?php echo $docObject->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $docObject->getErrorCode(); ?></li>
            <li>ID: <?php echo $docObject->getDocId(); ?></li>
            <li>$_POST['auteurid']: <?php echo $_POST['auteurid']?></li>
            <li>$_POST['uitgeverid']: <?php echo $_POST['uitgeverid']?></li>
            <li>$_POST['teleen']: <?php echo $_POST['teleen']?></li>
        </ul>
        <p>Test insert reg</p>
        <ul>
            <li>Message: <?php echo $regObject->getFeedback(); ?></li>
            <li>Error message: <?php echo $regObject->getErrorMessage(); ?></li>
            <li>Error code: <?php echo $regObject->getErrorCode(); ?></li>
            <li>ID: <?php echo $regObject->getRegId(); ?></li>
        </ul>
        -->

        

        

       
    </body>
</html>



