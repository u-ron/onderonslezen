<?php
include('../../helpers/feedback.class.php');    
include('../../helpers/base.class.php');
include('../model/transactiepartner.class.php');
include('../model/lid.class.php'); 

session_start();

//verwijderen
if(isset($_GET['tpid']))
{
    $tpObject = new TransactiePartner();
    $tpId = $_GET['tpid'];
    $tpObject->setTPId($tpId);
    $tpObject->setModifiedBy($_SESSION['username']);//bepaald in session.php
    $result = $tpObject->delete();
    if($result)
    {
        header('Location: ../../../appcode/webapp/view/mijn_vorige_deals.php');
    }

}
?>
