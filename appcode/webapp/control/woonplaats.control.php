<?php
include('../../helpers/feedback.class.php');    
include('../../helpers/base.class.php');
include('../model/woonplaats.class.php');
include('../model/lid.class.php'); 
include('../help/cleaninput.php');
session_start();


//verwijderen
if(isset($_GET['woonplaatsid']))
{
    $woonplaatsObject = new Woonplaats();
    $woonplaatsId = $_GET['woonplaatsid'];
    $woonplaatsObject->setWoonplaatsId($woonplaatsId);
    $result = $woonplaatsObject->delete();
    if($result)
    {
        header('Location: ../../../appcode/webapp/view/woonplaatsen.php');
    }
    else
    {
        $message = "Geen verwijdering mogelijk.";
        $_SESSION['message'] = $message;
        header('Location: ../../../appcode/webapp/view/woonplaatsen.php');
    }
}

//toevoegen
//$_POST is always set, but its content might be empty.
if(isset($_POST['btnWoonplaatsSave']))
{
        $_POST = opschonenInput($_POST);
        $woonplaatsObject = new Woonplaats();
        $woonplaatsObject->setGemeente($_POST['gemeente']);
        $woonplaatsObject->setPostcode($_POST['postcode']);
        $woonplaatsObject->setAddedBy('admin');
        $result = $woonplaatsObject->insert();
        if($result)
        {
            header('Location: ../../../appcode/webapp/view/woonplaatsen.php');
        }
        else
        {
            $message = $woonplaatsObject->getFeedback();
            $_SESSION['message'] = $message;
            header('Location: ../../../appcode/webapp/view/woonplaatsen.php');
        }
}
//wijzigen
elseif(isset($_POST['btnWoonplaatsUpdate']))
{
        $_POST = opschonenInput($_POST);
        $woonplaatsObject = new Woonplaats();
        $woonplaatsObject->setWoonplaatsId($_POST['idHidden']);
        $woonplaatsObject->setGemeente($_POST['gemeente']);
        $woonplaatsObject->setPostcode($_POST['postcode']);
        $woonplaatsObject->setModifiedBy('admin');
        $result = $woonplaatsObject->update();
        if($result)
        {
            header('Location: ../../../appcode/webapp/view/woonplaatsen.php');
        }
        else
        {
            $message = $woonplaatsObject->getFeedback();
            $_SESSION['message'] = $message;
            header('Location: ../../../appcode/webapp/view/woonplaatsen.php');
        }
}


?>




