<?php
class Base extends \Feedback
{

protected $pdo;
protected $rowCount;


public function __construct()
{
    \Feedback::__construct();
}


public function connect()
{
    $result=FALSE;
    try
    {
        //1.locaal
        $connectionString = 'mysql:host=127.0.0.1;dbname=dbool';
        $password='root';
        $userName="root";
        $this->pdo = new \PDO($connectionString, $userName, $password);
        $this->feedback = 'Met databank verbonden.';
        $result=TRUE;
        //2. op server met $connectionString
        //$connectionString = 'mysql:host=mysql51-133.perso;dbname=rfewebsiikbiblio';
        //$password='d5qp86fk';
        //$userName="rfewebsiikbiblio";
        //$this->pdo = new \PDO($connectionString, $userName, $password);
        //$this->feedback = 'Met databank verbonden.';
        //$result=TRUE;


        //3.
        //mysql_connect('www.rfewebsites.be', 'rfewebsiikbiblio', 'd5lp86fk');
    }
    catch(\PDOException $e)
    {
        $this->feedback='Niet met databank verbonden.';
        $this->errorMessage=$e->getMessage();
        $this->errorCode=$e->getCode();
    }
    return $result;
}


//close methode sluit de databankverbinding
public function close()
{
    $this->pdo=NULL;
}

}

?>


