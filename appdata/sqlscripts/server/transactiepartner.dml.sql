﻿-- Created : Wednesday 12th of November 2014 05:27:00 AM
USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TransactiePartnerInsert`;
DELIMITER //
CREATE PROCEDURE `TransactiePartnerInsert`
(
	OUT pTPId INT ,
	IN pTransactieId INT ,
	IN pLidId INT ,
	IN pRolId INT ,
    IN pDelStatus TINYINT,
	IN pAddedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `transactiepartner`
	(
		`transactiepartner`.`TransactieId`,
		`transactiepartner`.`LidId`,
		`transactiepartner`.`RolId`,
        `transactiepartner`.`DelStatus`,
		`transactiepartner`.`AddedBy`,
		`transactiepartner`.`InsertedOn`
	)
	VALUES
	(
		pTransactieId,
		pLidId,
		pRolId,
        pDelStatus,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pTPId;
END //
DELIMITER ;

call transactiepartnerinsert(@pTPId, 1, 1, 1, 0, 'admin');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TransactiePartnerUpdate`;
DELIMITER //
CREATE PROCEDURE `TransactiePartnerUpdate`
(
	IN pTPId INT ,
	IN pTransactieId INT ,
	IN pLidId INT ,
	IN pRolId INT ,
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `transactiepartner`
SET
	`TransactieId` = pTransactieId,
	`LidId` = pLidId,
	`RolId` = pRolId,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `transactiepartner`.TPId = pTPId;
END //
DELIMITER ;

call transactiepartnerupdate(1, 1, 1, 2, 'admin');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TransactiePartnerSelectAll`;
DELIMITER //
CREATE PROCEDURE `TransactiePartnerSelectAll`()
BEGIN
	SELECT * FROM `transactiepartner` where delstatus <> 1;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TransactiePartnerSelectById`;
DELIMITER //
CREATE PROCEDURE `TransactiePartnerSelectById`
(
	IN pTPId INT 
)
BEGIN
	SELECT * FROM `transactiepartner` WHERE `transactiepartner`.`TPId` = pTPId;
END //
DELIMITER ;


USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TransactiePartnerDelete`;
DELIMITER //
CREATE PROCEDURE `TransactiePartnerDelete`
(
	IN pTPId INT,
    IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `transactiepartner`
SET
	`DelStatus` = 1,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `transactiepartner`.TPId = pTPId;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TransactiePartnerGetKoper`;
DELIMITER //
CREATE PROCEDURE `TransactiePartnerGetKoper`
(
	IN pTransactieId INT 
)
BEGIN
	SELECT LidId FROM `transactiepartner` WHERE `transactiepartner`.`TransactieId` = pTransactieId and transactiepartner.RolId = 3;
END //
DELIMITER ;

call TransactiePartnerGetKoper(11)






