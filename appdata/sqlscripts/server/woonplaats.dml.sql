﻿USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `WoonplaatsInsert`;
DELIMITER //
CREATE PROCEDURE `WoonplaatsInsert`
(
	OUT pWoonplaatsId INT ,
	IN pGemeente VARCHAR (255) CHARACTER SET UTF8 ,
	IN pPostcode VARCHAR (255) CHARACTER SET UTF8 ,
	IN pAddedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `woonplaats`
	(
		`woonplaats`.`Gemeente`,
		`woonplaats`.`PostCode`,
		`woonplaats`.`AddedBy`,
		`woonplaats`.`InsertedOn`
	)
	VALUES
	(
		pGemeente,
		pPostcode,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pWoonplaatsId;
END //
DELIMITER ;

call woonplaatsinsert(@pWoonPlaatsId, 'Tielt', '8700', 'admin');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `WoonplaatsUpdate`;
DELIMITER //
CREATE PROCEDURE `WoonplaatsUpdate`
(
	IN pWoonplaatsId INT ,
	IN pGemeente VARCHAR (255) CHARACTER SET UTF8 ,
	IN pPostcode VARCHAR (255) CHARACTER SET UTF8 ,
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `woonplaats`
SET
	`Gemeente` = pGemeente,
	`Postcode` = pPostcode,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `woonplaats`.WoonplaatsId = pWoonplaatsId;
END //
DELIMITER ;

call woonplaatsupdate('1', 'Tielt', '8700', 'Admin');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `WoonplaatsSelectAll`;
DELIMITER //
CREATE PROCEDURE `WoonplaatsSelectAll` ()
BEGIN
	SELECT * FROM `woonplaats` order by postcode;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `WoonplaatsSelectById`;
DELIMITER //
CREATE PROCEDURE `WoonplaatsSelectById`
(
	IN pWoonplaatsId INT 
)
BEGIN
	SELECT * FROM `woonplaats` WHERE `woonplaats`.`WoonplaatsId` = pWoonplaatsId;
END //
DELIMITER ;

call WoonplaatsSelectById(2); //gaat ook: call WoonplaatsSelectById('2');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `WoonplaatsDelete`;
DELIMITER //
CREATE PROCEDURE `WoonplaatsDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `woonplaats`
WHERE `woonplaats`.WoonplaatsId = pId;
END //
DELIMITER ;





