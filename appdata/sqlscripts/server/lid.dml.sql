﻿-- Created : Saturday 8th of November 2014 11:01:05 AM
USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `LidInsert`;
DELIMITER //
CREATE PROCEDURE `LidInsert`
(
	OUT pLidId INT ,
	IN pGebruikersNaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pWachtwoord VARCHAR (255) CHARACTER SET UTF8 ,
	IN pLidNaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pLidVoornaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pLidInfo VARCHAR (255) CHARACTER SET UTF8 ,
	IN pAdres VARCHAR (255) CHARACTER SET UTF8 ,
	IN pTelefoon VARCHAR (255) CHARACTER SET UTF8 ,
	IN pEmail VARCHAR (255) CHARACTER SET UTF8 ,
	IN pSkypeNaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pWoonId INT ,
	IN pGesloten BOOL ,
    IN pLidStatus INT,
	IN pAddedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `lid`
	(
		`lid`.`GebruikersNaam`,
		`lid`.`Wachtwoord`,
		`lid`.`LidNaam`,
		`lid`.`LidVoornaam`,
		`lid`.`LidInfo`,
		`lid`.`Adres`,
		`lid`.`Telefoon`,
		`lid`.`Email`,
		`lid`.`SkypeNaam`,
		`lid`.`WoonId`,
		`lid`.`Gesloten`,
        `lid`.`LidStatus`,
		`lid`.`AddedBy`,
		`lid`.`InsertedOn`
	)
	VALUES
	(
		pGebruikersNaam,
		pWachtwoord,
		pLidNaam,
		pLidVoornaam,
		pLidInfo,
		pAdres,
		pTelefoon,
		pEmail,
		pSkypeNaam,
		pWoonId,
		pGesloten,
        pLidStatus,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pLidId;
END //
DELIMITER ;

call LidInsert(@pLidId, 'JosK', 'joss','Jolens', 'Jan', NULL, 'Vredestraat 2', '0477/455612', 'josj@skynet.be', 'josjskype', '1', '0', '1', 'admin');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `LidUpdate`;
DELIMITER //
CREATE PROCEDURE `LidUpdate`
(
	IN pLidId INT ,
	IN pGebruikersNaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pWachtwoord VARCHAR (255) CHARACTER SET UTF8 ,
	IN pLidNaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pLidVoornaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pLidInfo VARCHAR (255) CHARACTER SET UTF8 ,
	IN pAdres VARCHAR (255) CHARACTER SET UTF8 ,
	IN pTelefoon VARCHAR (255) CHARACTER SET UTF8 ,
	IN pEmail VARCHAR (255) CHARACTER SET UTF8 ,
	IN pSkypeNaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pWoonId INT ,
	IN pGesloten BOOL ,
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `lid`
SET
	`GebruikersNaam` = pGebruikersNaam,
	`Wachtwoord` = pWachtwoord,
	`LidNaam` = pLidNaam,
	`LidVoornaam` = pLidVoornaam,
	`LidInfo` = pLidInfo,
	`Adres` = pAdres,
	`Telefoon` = pTelefoon,
	`Email` = pEmail,
	`SkypeNaam` = pSkypeNaam,
	`WoonId` = pWoonId,
	`Gesloten` = pGesloten,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `lid`.LidId = pLidId;
END //
DELIMITER ;

call lidupdate('1','JanJ', 'jan63','Janssens', 'Jan', NULL, 'Vredestraat 2', '0477/455612', 'jj@skynet.be', 'janskype', '2', '0', 'admin'); 

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `LidDelete`;
DELIMITER //
CREATE PROCEDURE `LidDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `lid` WHERE `lid`.LidId = pId;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `LidSelectAll`;
DELIMITER //
CREATE PROCEDURE `LidSelectAll`
(
)
BEGIN
	SELECT * FROM `lid` order by lid.lidnaam;
END //
DELIMITER ;

call lidselectall();

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `LidSelectById`;
DELIMITER //
CREATE PROCEDURE `LidSelectById`
(
	IN pLidId INT 
)
BEGIN
	SELECT * FROM `lid` WHERE `lid`.`LidId` = pLidId;
END //
DELIMITER ;

call lidselectbyid('1');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `LidSelectByGebruikersNaam`;
DELIMITER //
CREATE PROCEDURE `LidSelectByGebruikersNaam`
(
	IN pGebruikersNaam VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	SELECT * FROM `lid` WHERE `lid`.`GebruikersNaam` = pGebruikersNaam;
END //
DELIMITER ;

call lidselectbygebruikersnaam('admin');


USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `LidSelectVerkopers`;
DELIMITER //
CREATE PROCEDURE `LidSelectVerkopers`
(
    IN pLidId INT
)
BEGIN
	SELECT distinct lid.LidNaam, lid.LidVoornaam, lid.LidId FROM lid, registratie, doc where lid.LidId = registratie.LidId and registratie.DocId = doc.DocId and doc.TeKoop = true and lid.LidId != pLidId order by lid.LidNaam;
END //
DELIMITER ;

call lidselectverkopers(3);

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `LidSelectKoper`;
DELIMITER //
CREATE PROCEDURE `LidSelectKoper`
(
	IN pTAId INT
)
BEGIN
select * from lid inner join transactiepartner on lid.lidid = transactiepartner.lidid 
    where transactiepartner.transactieid = pTAId and transactiepartner.rolid = 3;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `LidSelectVerkoper`;
DELIMITER //
CREATE PROCEDURE `LidSelectVerkoper`
( IN pTAId INT)
BEGIN
select * from lid inner join transactiepartner on lid.lidid = transactiepartner.lidid 
    where transactiepartner.transactieid = pTAId and transactiepartner.rolid = 4;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `LidSelectVerleners`;
DELIMITER //
CREATE PROCEDURE `LidSelectVerleners`
(
    IN pLidId INT
)
BEGIN
	SELECT distinct lid.LidNaam, lid.LidVoornaam, lid.LidId FROM lid, registratie, doc where lid.LidId = registratie.LidId and registratie.DocId = doc.DocId and doc.TeLeen = true and lid.LidId != pLidId order by lid.LidNaam;
END //
DELIMITER ;

call lidselectverleners();

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `LidSelectLener`;
DELIMITER //
CREATE PROCEDURE `LidSelectLener`
(
	IN pTAId INT
)
BEGIN
select * from lid inner join transactiepartner on lid.lidid = transactiepartner.lidid 
    where transactiepartner.transactieid = pTAId and transactiepartner.rolid = 1;
END //
DELIMITER ;

call lidselectlener(10);

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `LidSelectVerlener`;
DELIMITER //
CREATE PROCEDURE `LidSelectVerlener`
(IN pTAId INT)
BEGIN
select * from lid inner join transactiepartner on lid.lidid = transactiepartner.lidid 
    where transactiepartner.transactieid = pTAId and transactiepartner.rolid = 2;
END //
DELIMITER ;

call lidselectverlener(10);