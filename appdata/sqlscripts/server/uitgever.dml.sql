﻿-- Created : Saturday 1st of November 2014 05:43:17 AM
USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `UitgeverInsert`;
DELIMITER //
CREATE PROCEDURE `UitgeverInsert`
(
	OUT pUitgeverId INT ,
	IN pUitgever VARCHAR (255) CHARACTER SET UTF8 ,
	IN pUitgeverInfo VARCHAR (255) CHARACTER SET UTF8 ,
	IN pAddedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `uitgever`
	(
		`uitgever`.`Uitgever`,
		`uitgever`.`UitgeverInfo`,
		`uitgever`.`AddedBy`,
		`uitgever`.`InsertedOn`
	)
	VALUES
	(
		pUitgever,
		pUitgeverInfo,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pUitgeverId;
END //
DELIMITER ;

call UitgeverInsert(@pUitgeverId, 'Lannoo', NULL, 'Admin');
call UitgeverInsert(@pUitgeverId, 'Standaard Uitgeverij', NULL, 'Admin');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `UitgeverUpdate`;
DELIMITER //
CREATE PROCEDURE `UitgeverUpdate`
(
	IN pUitgeverId INT ,
	IN pUitgever VARCHAR (255) CHARACTER SET UTF8 ,
	IN pUitgeverInfo VARCHAR (255) CHARACTER SET UTF8 ,
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `uitgever`
SET
	`Uitgever` = pUitgever,
	`UitgeverInfo` = pUitgeverInfo,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `uitgever`.UitgeverId = pUitgeverId;
END //
DELIMITER ;

call UitgeverUpdate('1', 'Lannoo', 'in Tielt', 'Admin');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `UitgeverDelete`;
DELIMITER //
CREATE PROCEDURE `UitgeverDelete`
(
	IN pUitgeverId INT
)
BEGIN
DELETE FROM `uitgever`
WHERE `uitgever`.UitgeverId = pUitgeverId;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `UitgeverSelectAll`;
DELIMITER //
CREATE PROCEDURE `UitgeverSelectAll`
(
)
BEGIN
	SELECT * FROM `uitgever` order by uitgever.Uitgever;
END //
DELIMITER ;

call UitgeverSelectAll();



USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `UitgeverSelectById`;
DELIMITER //
CREATE PROCEDURE `UitgeverSelectById`
(
	IN pUitgeverId INT 
)
BEGIN
	SELECT * FROM `uitgever` WHERE `uitgever`.`UitgeverId` = pUitgeverId;
END //
DELIMITER ;

call UitgeverSelectById(1);