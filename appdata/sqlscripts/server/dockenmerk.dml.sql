﻿USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `DocKenmerkInsert`;
DELIMITER //
CREATE PROCEDURE `DocKenmerkInsert`
(
	OUT pDocKenmerkId INT ,
	IN pDocKenmerk VARCHAR (255) CHARACTER SET UTF8 ,
	IN pDocKenmerkValue VARCHAR (255) CHARACTER SET UTF8 ,
	IN pDocId INT ,
	IN pAddedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `dockenmerk`
	(
		`dockenmerk`.`DocKenmerk`,
		`dockenmerk`.`DocKenmerkValue`,
		`dockenmerk`.`DocId`,
		`dockenmerk`.`AddedBy`,
		`dockenmerk`.`InsertedOn`
	)
	VALUES
	(
		pDocKenmerk,
		pDocKenmerkValue,
		pDocId,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pDocKenmerkId;
END //
DELIMITER ;

call DocKenmerkInsert(@pDocKenmerkId, 'Formaat', 'Paperback', '1', 'Admin');
call DocKenmerkInsert(@pDocKenmerkId, 'Aantal bladzijden', '233', '1', 'Admin');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `DocKenmerkUpdate`;
DELIMITER //
CREATE PROCEDURE `DocKenmerkUpdate`
(
	IN pDocKenmerkId INT ,
	IN pDocKenmerk VARCHAR (255) CHARACTER SET UTF8 ,
	IN pDocKenmerkValue VARCHAR (255) CHARACTER SET UTF8 ,
	IN pDocId INT ,
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `dockenmerk`
SET
	`DocKenmerk` = pDocKenmerk,
	`DocKenmerkValue` = pDocKenmerkValue,
	`DocId` = pDocId,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `dockenmerk`.DocKenmerkId = pDocKenmerkId;
END //
DELIMITER ;

call DocKenmerkUpdate('1', 'Aantal pagina''s', '230', '1', 'Admin');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `DocKenmerkDelete`;
DELIMITER //
CREATE PROCEDURE `DocKenmerkDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `dockenmerk` WHERE `dockenmerk`.DocKenmerkId = pId;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `DocKenmerkSelectAll`;
DELIMITER //
CREATE PROCEDURE `DocKenmerkSelectAll`
(
)
BEGIN
	SELECT * FROM `dockenmerk`
;
END //
DELIMITER ;

call DocKenmerkSelectAll();


USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `DocKenmerkSelectById`;
DELIMITER //
CREATE PROCEDURE `DocKenmerkSelectById`
(
	IN pDocKenmerkId INT 
)
BEGIN
	SELECT * FROM `dockenmerk` WHERE `dockenmerk`.`DocKenmerkId` = pDocKenmerkId;
END //
DELIMITER ;

call DocKenmerkSelectById('2');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `DocKenmerkSelectByDocId`;
DELIMITER //
CREATE PROCEDURE `DocKenmerkSelectByDocId`
(
	IN pDocId INT 
)
BEGIN
	SELECT * FROM `dockenmerk` WHERE `dockenmerk`.`DocId` = pDocId;
END //
DELIMITER ;

call DocKenmerkSelectByDocId('1');