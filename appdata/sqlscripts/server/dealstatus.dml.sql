﻿insert into dealstatus (dealstatusid, dealstatus) values('1', 'Voorstel');
insert into dealstatus (dealstatusid, dealstatus) values('2', 'Aanvaard');
insert into dealstatus (dealstatusid, dealstatus) values('3', 'Afgekeurd');
insert into dealstatus (dealstatusid, dealstatus) values('4', 'Betaald');
insert into dealstatus (dealstatusid, dealstatus) values('5', 'Verzonden');
insert into dealstatus (dealstatusid, dealstatus) values('6', 'Afgeleverd');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS DealStatusSelectById;
DELIMITER //
CREATE PROCEDURE DealStatusSelectById
(
	IN pId INT
)
BEGIN
	SELECT * FROM dealstatus WHERE DealStatusId = pId;
END //
DELIMITER ;

call DealStatusSelectById(2);

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS DealStatusSelectAll;
DELIMITER //
CREATE PROCEDURE DealStatusSelectAll()
BEGIN
	SELECT * FROM dealstatus;
END //
DELIMITER ;

call DealStatusSelectAll();