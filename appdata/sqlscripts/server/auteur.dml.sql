﻿-- Created : Saturday 1st of November 2014 03:07:10 AM

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `AuteurInsert`;
DELIMITER //
CREATE PROCEDURE `AuteurInsert`
(
	OUT pAuteurId INT ,
	IN pAuteurNaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pAuteurVoornaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pAuteurInfo VARCHAR (255) CHARACTER SET UTF8 ,
	IN pAddedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `auteur`
	(
		`AuteurNaam`,
		`AuteurVoornaam`,
		`AuteurInfo`,
		`AddedBy`,
		`InsertedOn`
	)
	VALUES
	(
		pAuteurNaam,
		pAuteurVoornaam,
		pAuteurInfo,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pAuteurId;
END //
DELIMITER ;

call AuteurInsert(@pAuteurId, 'Brusselmans', 'Herman', 'Vlaams schrijver', NULL);
call AuteurInsert(@pAuteurId, 'Lanoy', 'Tom', 'Vlaams schrijver', 'RE');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `AuteurUpdate`;
DELIMITER //
CREATE PROCEDURE `AuteurUpdate`
(
	IN pAuteurId INT ,
	IN pAuteurNaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pAuteurVoornaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pAuteurInfo VARCHAR (255) CHARACTER SET UTF8 ,
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `auteur`
SET
	`AuteurNaam` = pAuteurNaam,
	`AuteurVoornaam` = pAuteurVoornaam,
	`AuteurInfo` = pAuteurInfo,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `auteur`.AuteurId = pAuteurId;
END //
DELIMITER ;

call AuteurUpdate('1', 'Brusselmans', 'H', 'Vlaams schrijver', 'RE');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `AuteurDelete`;
DELIMITER //
CREATE PROCEDURE `AuteurDelete`
(
	IN pAuteurId INT
)
BEGIN
DELETE FROM `auteur`
WHERE `auteur`.AuteurId = pAuteurId;
END //
DELIMITER ;


USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `AuteurSelectAll`;
DELIMITER //
CREATE PROCEDURE `AuteurSelectAll`
(
)
BEGIN
	SELECT * FROM `auteur` order by auteur.Auteurnaam;
END //
DELIMITER ;

call auteurselectall();



USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `AuteurSelectById`;
DELIMITER //
CREATE PROCEDURE `AuteurSelectById`
(
	IN pAuteurId INT 
)
BEGIN
	SELECT * FROM `auteur` WHERE `auteur`.`AuteurId` = pAuteurId;
END //
DELIMITER ;


call auteurselectbyid('2');
