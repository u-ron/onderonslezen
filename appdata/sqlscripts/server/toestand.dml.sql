﻿insert into toestand (toestandid, toestand) values('1', 'Nieuw');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS ToestandSelectById;
DELIMITER //
CREATE PROCEDURE ToestandSelectById
(
	IN pId INT
)
BEGIN
	SELECT * FROM toestand
	WHERE ToestandId = pId;
END //
DELIMITER ;

call ToestandSelectById(2);

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS ToestandSelectAll;
DELIMITER //
CREATE PROCEDURE ToestandSelectAll()
BEGIN
	SELECT * FROM toestand;
END //
DELIMITER ;

call ToestandSelectAll();