﻿insert into taal (taalid, taal) values('1', 'Nederlands');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TaalSelectById`;
DELIMITER //
CREATE PROCEDURE `TaalSelectById`
(
	IN pId INT
)
BEGIN
	SELECT * FROM taal
	WHERE TaalId = pId;
END //
DELIMITER ;

call TaalSelectById(2);

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS TaalSelectAll;
DELIMITER //
CREATE PROCEDURE TaalSelectAll()
BEGIN
	SELECT * FROM taal;
END //
DELIMITER ;

call TaalSelectAll();

