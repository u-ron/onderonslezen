﻿USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `LoginAttemptInsert`;
DELIMITER //
CREATE PROCEDURE `LoginAttemptInsert`
(
	OUT pId INT ,
	IN pIdMember INT ,
	IN pTime VARCHAR (30) ,
	IN pInsertedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO loginattempt
	(
		IdMember,
		Time,
		InsertedBy,
		InsertedOn
	)
	VALUES
	(
		pIdMember,
		pTime,
		pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `LoginAttemptCountIdMemberByTime`;
DELIMITER //
CREATE PROCEDURE `LoginAttemptCountIdMemberByTime`
(
        IN pIdMember INT,
		IN pTime VARCHAR (30) 
)
BEGIN
	SELECT COUNT(`IdMember`) AS Counted FROM loginattempt
		WHERE IdMember = pIdMember and Time >= pTime;
END //
DELIMITER ;

call LoginAttemptCountIdMemberByTime(14, 1424018071);