﻿insert into exchangestatus (exchangestatusid, exchangestatus) values('1', 'Voorstel');
insert into exchangestatus (exchangestatusid, exchangestatus) values('2', 'Aanvaard');
insert into exchangestatus (exchangestatusid, exchangestatus) values('3', 'Afgekeurd');
insert into exchangestatus (exchangestatusid, exchangestatus) values('4', 'In uitwisseling');
insert into exchangestatus (exchangestatusid, exchangestatus) values('5', 'Teruggekeerd');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS ExchangeStatusSelectById;
DELIMITER //
CREATE PROCEDURE ExchangeStatusSelectById
(
	IN pId INT
)
BEGIN
	SELECT * FROM exchangestatus WHERE ExchangeStatusId = pId;
END //
DELIMITER ;

call ExchangeStatusSelectById(2);

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS ExchangeStatusSelectAll;
DELIMITER //
CREATE PROCEDURE ExchangeStatusSelectAll()
BEGIN
	SELECT * FROM exchangestatus;
END //
DELIMITER ;

call ExchangeStatusSelectAll();