﻿USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `RegistratieInsert`;
DELIMITER //
CREATE PROCEDURE `RegistratieInsert`
(
	OUT pRegId INT ,
	IN pLidId INT ,
	IN pDocId INT ,
	IN pAddedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `registratie`
	(
		`registratie`.`LidId`,
		`registratie`.`DocId`,
		`registratie`.`AddedBy`,
		`registratie`.`InsertedOn`
	)
	VALUES
	(
		pLidId,
		pDocId,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pRegId;
END //
DELIMITER ;

call registratieinsert(@pRegId, '1','2','admin');
call registratieinsert(@pRegId, '6','3','admin');
call registratieinsert(@pRegId, '1','1','admin');
call registratieinsert(@pRegId, '7','4','admin');
call registratieinsert(@pRegId, '3','5','admin');


USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `RegistratieUpdate`;
DELIMITER //
CREATE PROCEDURE `RegistratieUpdate`
(
	IN pRegId INT ,
	IN pLidId INT ,
	IN pDocId INT ,
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `registratie`
SET
	`LidId` = pLidId,
	`DocId` = pDocId,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `registratie`.RegId = pRegId;
END //
DELIMITER ;

call registratieupdate('1','2','1','admin');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `RegistratieSelectAll`;
DELIMITER //
CREATE PROCEDURE `RegistratieSelectAll`()
BEGIN
	SELECT * FROM `registratie`
;
END //
DELIMITER ;


USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `RegistratieDelete`;
DELIMITER //
CREATE PROCEDURE `RegistratieDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `registratie`
WHERE `registratie`.RegId = pId;
END //
DELIMITER ;

 
USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `RegistratieSelectByLidId`;
DELIMITER //
CREATE PROCEDURE `RegistratieSelectByLidId`
(
	IN pLidId INT 
)
BEGIN
	SELECT * FROM `registratie` WHERE `registratie`.`LidId` = pLidId;
END //
DELIMITER ;



USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `RegistratieSelectByDocId`;
DELIMITER //
CREATE PROCEDURE `RegistratieSelectByDocId`(IN pDocId INT)
BEGIN
	SELECT * FROM registratie WHERE registratie.DocId = pDocId;
END //
DELIMITER ;

call RegistratieSelectByDocId(50);