﻿-- Created : Saturday 1st of November 2014 03:57:39 PM
USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `DocInsert`;
DELIMITER //
CREATE PROCEDURE `DocInsert`
(
	OUT pDocId INT ,
	IN pTitel VARCHAR (255) CHARACTER SET UTF8 ,
	IN pDocInfo VARCHAR (255) CHARACTER SET UTF8 ,
    IN pPrijs VARCHAR (255),
	IN pJaar INT ,
    IN pISBN VARCHAR(255),
	IN pTeLeen BOOL ,
	IN pTeKoop BOOL ,
	IN pDocTypeId INT ,
	IN pTaalId INT ,
	IN pAuteurId INT ,
	IN pUitgeverId INT ,
	IN pToestandId INT ,
	IN pAddedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `doc`
	(
		`doc`.`Titel`,
		`doc`.`DocInfo`,
        `doc`.`Prijs`,
		`doc`.`Jaar`,
        `doc`.`ISBN`,
		`doc`.`TeLeen`,
		`doc`.`TeKoop`,
		`doc`.`DocTypeId`,
		`doc`.`TaalId`,
		`doc`.`AuteurId`,
		`doc`.`UitgeverId`,
		`doc`.`ToestandId`,
		`doc`.`AddedBy`,
		`doc`.`InsertedOn`
	)
	VALUES
	(
		pTitel,
		pDocInfo,
        pPrijs,
		pJaar,
        pISBN,
		pTeLeen,
		pTeKoop,
		pDocTypeId,
		pTaalId,
		pAuteurId,
		pUitgeverId,
		pToestandId,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pDocId;
END //
DELIMITER ;

call DocInsert(@pDocId, 'Zwarte tranen', NULL, NULL, '2002', NULL, '5', '1', '1', '1', '1', '2', NULL, '3', 'Admin'); 
call DocInsert(@pDocId, 'jQuery De Basis', NULL, NULL, '2012', '978-90-430-2525-6', '1','0','1','1','7','4', '1', 'Admin');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `DocUpdate`;
DELIMITER //
CREATE PROCEDURE `DocUpdate`
(
	IN pDocId INT ,
	IN pTitel VARCHAR (255) CHARACTER SET UTF8 ,
	IN pDocInfo VARCHAR (255) CHARACTER SET UTF8 ,
	IN pJaar INT ,
    IN pISBN VARCHAR (255) CHARACTER SET UTF8 ,
    IN pPrijs VARCHAR (255),
	IN pTeLeen BOOL ,
	IN pTeKoop BOOL ,
	IN pDocTypeId INT ,
	IN pTaalId INT ,
	IN pAuteurId INT ,
	IN pUitgeverId INT ,
	IN pToestandId INT ,
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `doc`
SET
	`Titel` = pTitel,
	`DocInfo` = pDocInfo,
	`Jaar` = pJaar,
	`ISBN` = pISBN,
    `Prijs` = pPrijs,
	`TeLeen` = pTeLeen,
	`TeKoop` = pTeKoop,
	`DocTypeId` = pDocTypeId,
	`TaalId` = pTaalId,
	`AuteurId` = pAuteurId,
	`UitgeverId` = pUitgeverId,
	`ToestandId` = pToestandId,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `doc`.DocId = pDocId;
END //
DELIMITER ;

call DocUpdate('2', 'Zwarte tranen', 'Roman', '2002', NULL, '2', '1', '1', '1', '1', '2', NULL, '2', 'Admin');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `DocDelete`;
DELIMITER //
CREATE PROCEDURE `DocDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `doc`
WHERE `doc`.DocId = pId;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `DocSelectAll`;
DELIMITER //
CREATE PROCEDURE `DocSelectAll`
(
)
BEGIN
	SELECT * FROM `doc`;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `DocSelectById`;
DELIMITER //
CREATE PROCEDURE `DocSelectById`
(
	IN pDocId INT 
)
BEGIN
	SELECT * FROM `doc` WHERE `doc`.`DocId` = pDocId;
END //
DELIMITER ;

call DocSelectById('2');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `DocSelectKoopwaarVanVerkoper`;
DELIMITER //
CREATE PROCEDURE `DocSelectKoopwaarVanVerkoper`
(
    pLidId INT
)
BEGIN
SELECT doc.docid as docid, doc.titel as titel, doctype.doctype as doctype, doc.prijs as prijs, auteur.auteurnaam as auteurnaam
FROM auteur right outer join (doctype inner join (registratie inner join doc on registratie.docid = doc.docid) on doctype.doctypeid = doc.doctypeid) on
auteur.auteurid = doc.auteurid where doc.tekoop = true and 
registratie.lidid = pLidId and doc.docid not in (select doc.docid from doc inner join (transactiedoc inner join transactie on transactie.transactieid = transactiedoc.transactieid) on doc.docid = transactiedoc.docid where transactie.dealstatusid in (1,2,3,5,6,7) or transactie.exchangestatusid in (1,2,3,5)) order by doc.docid;
END //
DELIMITER ;
/*de subquery duidt de documenten aan die in transactie zijn of een nieuwe eigenaar gekregen hebben*/
call docselectkoopwaarvanverkoper(3);


USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS DocSelectDocsInDeal;
DELIMITER //
CREATE PROCEDURE DocSelectDocsInDeal
(
    IN pTAId INT
)
BEGIN
SELECT doc.docid as docid, doc.titel, doc.doctypeid, doc.prijs, doc.auteurid 
FROM doc inner join transactiedoc on doc.docid = transactiedoc.docid where transactiedoc.transactieid = pTAId order by transactiedoc.docid;
END //
DELIMITER ;

call DocSelectDocsInDeal(4);

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `DocSelectLeenwaarVanVerlener`;
DELIMITER //
CREATE PROCEDURE `DocSelectLeenwaarVanVerlener`
(
    pLidId INT
)
BEGIN
SELECT doc.docid as docid, doc.titel as titel, doctype.doctype as doctype, doc.prijs as prijs, auteur.auteurnaam as auteurnaam
FROM auteur right outer join (doctype inner join (registratie inner join doc on registratie.docid = doc.docid) on doctype.doctypeid = doc.doctypeid) on
auteur.auteurid = doc.auteurid where doc.teleen = true and 
registratie.lidid = pLidId and doc.docid not in
(select doc.docid from doc inner join (transactiedoc inner join transactie on transactie.transactieid = transactiedoc.transactieid) on doc.docid = transactiedoc.docid where transactie.dealstatusid in (1,2,3,5,6,7) or transactie.exchangestatusid in (1,2,3,5))
order by doc.docid;
END //
DELIMITER ;
/*de subquery duidt de documenten aan die in transactie zijn of een nieuwe eigenaar gekregen hebben*/
call docselectleenwaarvanverlener(3);

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `DocSelectAllAvailable`;
DELIMITER //
CREATE PROCEDURE `DocSelectAllAvailable`
(
)
BEGIN
	SELECT doc.DocId, doc.Titel, doc.DocInfo, doc.Jaar, doc.ISBN, doc.Prijs, doc.TeKoop, doc.TeLeen, doc.DocTypeId,
    doc.TaalId, doc.AuteurId, doc.UitgeverId, doc.ToestandId FROM doc where doc.DocId not in
    (select doc.docid from doc inner join (transactiedoc inner join transactie on transactie.transactieid = transactiedoc.transactieid) on doc.docid = transactiedoc.docid where transactie.dealstatusid in (1,2,3,5,6,7) or transactie.exchangestatusid in (1,2,3,5));
END //
DELIMITER ;
/*de subquery duidt de documenten aan die in transactie zijn of een nieuwe eigenaar gekregen hebben*/


USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS DocSelectDocsInExchange;
DELIMITER //
CREATE PROCEDURE DocSelectDocsInExchange
(
    IN pTAId INT
)
BEGIN
SELECT doc.docid, doc.titel, doc.doctypeid, doc.auteurid, doc.uitgeverid 
FROM doc inner join transactiedoc on doc.docid = transactiedoc.docid where transactiedoc.transactieid = pTAId order by transactiedoc.docid;
END //
DELIMITER ;

call DocSelectDocsInExchange(10);

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `DocFilterAllAvailable`;
DELIMITER //
CREATE PROCEDURE `DocFilterAllAvailable`
(
   IN pTitel VARCHAR (255) CHARACTER SET UTF8 ,
   IN pTaalId INT,
   IN pDocTypeId INT
)
BEGIN
if ((pTaalId is NULL) AND (pDocTypeId is not NULL)) then 
SELECT doc.docid as docid, doc.titel as titel, doc.tekoop, doc.teleen, doc.doctypeid as doctypeid, doc.auteurid as auteurid
FROM auteur right outer join (doc inner join doctype on doc.doctypeid = doctype.doctypeid) on auteur.auteurid = doc.auteurid
where doc.titel like CONCAT('%', pTitel,'%') and doc.taalid in (1,2,3,4,5) and doc.doctypeid = pDocTypeId and doc.docid not in  (select doc.docid from doc inner join (transactiedoc inner join transactie on transactie.transactieid = transactiedoc.transactieid) on doc.docid = transactiedoc.docid where transactie.dealstatusid in (1,2,3,5,6,7) or transactie.exchangestatusid in (1,2,3,5)) order by doc.docid;

elseif ((pDocTypeId is NULL) and (pTaalId is not NULL)) then 
SELECT doc.docid as docid, doc.titel as titel, doc.tekoop, doc.teleen, doctype.doctypeid as doctypeid, doc.auteurid as auteurid
FROM auteur right outer join (doc inner join doctype on doc.doctypeid = doctype.doctypeid) on auteur.auteurid = doc.auteurid
where doc.titel like CONCAT('%', pTitel,'%') and doc.taalid = pTaalId and doc.doctypeid in (select doctypeid from doctype) and doc.docid not in  (select doc.docid from doc inner join (transactiedoc inner join transactie on transactie.transactieid = transactiedoc.transactieid) on doc.docid = transactiedoc.docid where transactie.dealstatusid in (1,2,3,5,6,7) or transactie.exchangestatusid in (1,2,3,5)) order by doc.docid;

elseif ((pTaalId is NULL) AND (pDocTypeId is NULL)) then
SELECT doc.docid as docid, doc.titel as titel, doc.tekoop, doc.teleen, doctype.doctypeid as doctypeid, doc.auteurid as auteurid
FROM auteur right outer join (doc inner join doctype on doc.doctypeid = doctype.doctypeid) on auteur.auteurid = doc.auteurid
where doc.titel like CONCAT('%', pTitel,'%') and doc.taalid in (1,2,3,4,5) and doc.doctypeid in (select doctypeid from doctype) and doc.docid not in  (select doc.docid from doc inner join (transactiedoc inner join transactie on transactie.transactieid = transactiedoc.transactieid) on doc.docid = transactiedoc.docid where transactie.dealstatusid in (1,2,3,5,6,7) or transactie.exchangestatusid in (1,2,3,5)) order by doc.docid;

else
SELECT doc.docid as docid, doc.titel as titel, doc.tekoop, doc.teleen, doctype.doctypeid as doctypeid, doc.auteurid as auteurid
FROM auteur right outer join (doc inner join doctype on doc.doctypeid = doctype.doctypeid) on auteur.auteurid = doc.auteurid
where doc.titel like CONCAT('%', pTitel,'%') and doc.taalid = pTaalId and doc.doctypeid = pDocTypeId and doc.docid not in  (select doc.docid from doc inner join (transactiedoc inner join transactie on transactie.transactieid = transactiedoc.transactieid) on doc.docid = transactiedoc.docid where transactie.dealstatusid in (1,2,3,5,6,7) or transactie.exchangestatusid in (1,2,3,5)) order by doc.docid;
end if;
END //
DELIMITER ;
/*de subquery duidt de documenten aan die in transactie zijn of een nieuwe eigenaar gekregen hebben*/

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `DocInTransactie`;
DELIMITER //
CREATE PROCEDURE `DocInTransactie`
(
	IN pDocId INT 
)
BEGIN
	 (select doc.docid from doc inner join (transactiedoc inner join transactie on transactie.transactieid = transactiedoc.transactieid) on doc.docid = transactiedoc.docid where doc.docid = pDocId and (transactie.dealstatusid in (1,2,3,5) or transactie.exchangestatusid in (1,2,3,5)));
END //
DELIMITER ;
/*de subquery duidt de documenten aan die in transactie zijn*/

call docintransactie(40);

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `RegistratieSelectDocumentenPerLid`;
DELIMITER //
CREATE PROCEDURE `RegistratieSelectDocumentenPerLid`(IN pLidId INT)
BEGIN
	SELECT * FROM registratie inner join doc on doc.DocId = registratie.DocId where registratie.LidId = pLidId and doc.DocId not in
    (select doc.docid from doc inner join (transactiedoc inner join transactie on transactie.transactieid = transactiedoc.transactieid) on doc.docid = transactiedoc.docid where transactie.dealstatusid in (6,7)) order by doc.docid;
END //
DELIMITER ;
/*de subquery duidt de verkochte documenten aan*/

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `DocVerkocht`;
DELIMITER //
CREATE PROCEDURE `DocVerkocht`
(
	IN pDocId INT 
)
BEGIN
	 (select doc.docid from doc inner join (transactiedoc inner join transactie on transactie.transactieid = transactiedoc.transactieid) on doc.docid = transactiedoc.docid where doc.docid = pDocId and transactie.dealstatusid in (6,7));
END //
DELIMITER ;
