﻿USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TransactieInsert`;
DELIMITER //
CREATE PROCEDURE `TransactieInsert`
(
	OUT pTransactieId INT ,
	IN pTADatum DATE ,
	IN pOrderBedrag VARCHAR (255) CHARACTER SET UTF8 ,
	IN pTransportKost VARCHAR (255) CHARACTER SET UTF8 ,
    IN pDueDate DATE,
    IN pDatumUit DATE,
	IN pDatumTerug DATE ,
	IN pTransactieTypeId INT ,
	IN pDealStatusId INT ,
	IN pExchangeStatusId INT ,
	IN pAddedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `transactie`
	(
		`transactie`.`TADatum`,
		`transactie`.`OrderBedrag`,
		`transactie`.`TransportKost`,
        `transactie`.`DueDate`,
		`transactie`.`DatumUit`,
		`transactie`.`DatumTerug`,
		`transactie`.`TransactieTypeId`,
		`transactie`.`DealStatusId`,
		`transactie`.`ExchangeStatusId`,
		`transactie`.`AddedBy`,
		`transactie`.`InsertedOn`
	)
	VALUES
	(
		pTADatum,
		pOrderBedrag,
		pTransportKost,
		pDueDate,
        pDatumUit,
		pDatumTerug,
		pTransactieTypeId,
		pDealStatusId,
		pExchangeStatusId,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pTransactieId;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TransactieUpdate`;
DELIMITER //
CREATE PROCEDURE `TransactieUpdate`
(
	IN pTransactieId INT ,
	IN pTADatum DATE ,
	IN pOrderBedrag VARCHAR (255) CHARACTER SET UTF8 ,
	IN pTransportKost VARCHAR (255) CHARACTER SET UTF8 ,
    IN pDueDate DATE,
    IN pDatumUit DATE,
	IN pDatumTerug DATE ,
	IN pTransactieTypeId INT ,
	IN pDealStatusId INT ,
	IN pExchangeStatusId INT ,
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `transactie`
SET
	`TADatum` = pTADatum,
	`OrderBedrag` = pOrderBedrag,
	`TransportKost` = pTransportKost,
	`DueDate` = pDueDate,
    `DatumUit` = pDatumUit,
	`DatumTerug` = pDatumTerug,
	`TransactieTypeId` = pTransactieTypeId,
	`DealStatusId` = pDealStatusId,
	`ExchangeStatusId` = pExchangeStatusId,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `transactie`.TransactieId = pTransactieId;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TransactieSelectAll`;
DELIMITER //
CREATE PROCEDURE `TransactieSelectAll`
(
)
BEGIN
	SELECT * FROM `transactie`;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TransactieDelete`;
DELIMITER //
CREATE PROCEDURE `TransactieDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `transactie`
WHERE `transactie`.TransactieId = pId;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TransactieSelectById`;
DELIMITER //
CREATE PROCEDURE `TransactieSelectById`
(
	IN pTransactieId INT 
)
BEGIN
	SELECT * FROM `transactie` WHERE `transactie`.`TransactieId` = pTransactieId;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TransactieSelectMijnHangendeDeals`;
DELIMITER //
CREATE PROCEDURE `TransactieSelectMijnHangendeDeals`
( IN pLidId INT)
BEGIN
select * from transactie, transactiepartner, lid
where transactie.transactieid = transactiepartner.transactieid and transactiepartner.lidid = lid.lidid and
lid.lidid = pLidId and transactie.dealstatusid in (1,2,3,5);
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS TransactieSelectMijnVorigeDeals;
DELIMITER //
CREATE PROCEDURE `TransactieSelectMijnVorigeDeals`
( IN pLidId INT)
BEGIN
select * from transactie, transactiepartner, lid
where transactie.transactieid = transactiepartner.transactieid and transactiepartner.lidid = lid.lidid and
lid.lidid = pLidId and transactie.dealstatusid in (6,7) and transactiepartner.delstatus = 0;
END //
DELIMITER ;

call TransactieSelectMijnVorigeDeals(3);

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TransactieSelectMijnHangendeExchanges`;
DELIMITER //
CREATE PROCEDURE `TransactieSelectMijnHangendeExchanges`
( IN pLidId INT)
BEGIN
select * from transactie, transactiepartner, lid
where transactie.transactieid = transactiepartner.transactieid and transactiepartner.lidid = lid.lidid and
lid.lidid = pLidId and transactie.exchangestatusid in (1,2,3,5);
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS TransactieSelectMijnVorigeExchanges;
DELIMITER //
CREATE PROCEDURE `TransactieSelectMijnVorigeExchanges`
( IN pLidId INT)
BEGIN
select * from transactie, transactiepartner, lid
where transactie.transactieid = transactiepartner.transactieid and transactiepartner.lidid = lid.lidid and
lid.lidid = pLidId and transactie.exchangestatusid = 6 and transactiepartner.delstatus = 0;
END //
DELIMITER ;

call TransactieSelectMijnVorigeExchanges(3);

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS TransactieSelectMijnVorigeVerkopen;
DELIMITER //
CREATE PROCEDURE `TransactieSelectMijnVorigeVerkopen`
( IN pLidId INT)
BEGIN
select transactie.transactieid from transactie, transactiepartner, lid
where transactie.transactieid = transactiepartner.transactieid and transactiepartner.lidid = lid.lidid and
lid.lidid = pLidId and transactie.dealstatusid in (6,7) and transactiepartner.rolid = 4;
END //
DELIMITER ;

call TransactieSelectMijnVorigeVerkopen(9);

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TransactiesBezigCount`;
DELIMITER //
CREATE PROCEDURE `TransactiesBezigCount`
(
)
BEGIN
	SELECT count(*) as aantal FROM transactie where dealstatusid in (1,2,3,5) or exchangestatusid in (1,2,3,5);
END //
DELIMITER ;

call TransactiesBezigCount();

