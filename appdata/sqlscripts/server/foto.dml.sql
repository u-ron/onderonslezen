﻿USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `FotoInsert`;
DELIMITER //
CREATE PROCEDURE `FotoInsert`
(
	OUT pFotoId INT ,
	IN pFotoNaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pURL VARCHAR (255) CHARACTER SET UTF8 ,
	IN pDocId INT ,
	IN pAddedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `foto`
	(
		`foto`.`FotoNaam`,
		`foto`.`URL`,
		`foto`.`DocId`,
		`foto`.`AddedBy`,
		`foto`.`InsertedOn`
	)
	VALUES
	(
		pFotoNaam,
		pURL,
		pDocId,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pFotoId;
END //
DELIMITER ;

call fotoinsert(@pFotoId, 'foto3.gif', 'images/doc2/', '2', 'admin');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `FotoUpdate`;
DELIMITER //
CREATE PROCEDURE `FotoUpdate`
(
	IN pFotoId INT ,
	IN pFotoNaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pURL VARCHAR (255) CHARACTER SET UTF8 ,
	IN pDocId INT ,
	IN pModifiedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
UPDATE `foto`
SET
	`FotoNaam` = pFotoNaam,
	`URL` = pURL,
	`DocId` = pDocId,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `foto`.FotoId = pFotoId;
END //
DELIMITER ;

call fotoupdate('5', 'foto3.gif', 'images/doc1/', '1', 'admin');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `FotoDelete`;
DELIMITER //
CREATE PROCEDURE `FotoDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `foto`
WHERE `foto`.FotoId = pId;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `FotoSelectAll`;
DELIMITER //
CREATE PROCEDURE `FotoSelectAll`
(
)
BEGIN
	SELECT * FROM `foto`
;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `FotoSelectById`;
DELIMITER //
CREATE PROCEDURE `FotoSelectById`
(
	IN pFotoId INT 
)
BEGIN
	SELECT * FROM `foto` WHERE `foto`.`FotoId` = pFotoId;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `FotoSelectByDocId`;
DELIMITER //
CREATE PROCEDURE `FotoSelectByDocId`
(
	IN pDocId INT 
)
BEGIN
	SELECT * FROM `foto` WHERE `foto`.`DocId` = pDocId;
END //
DELIMITER ;