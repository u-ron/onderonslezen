﻿-- Created : Wednesday 12th of November 2014 03:40:58 PM
USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TransactieDocInsert`;
DELIMITER //
CREATE PROCEDURE `TransactieDocInsert`
(
	OUT pTDId INT ,
	IN pTransactieId INT ,
	IN pDocId INT ,
	IN pAddedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `transactiedoc`
	(
		`transactiedoc`.`TransactieId`,
		`transactiedoc`.`DocId`,
		`transactiedoc`.`AddedBy`,
		`transactiedoc`.`InsertedOn`
	)
	VALUES
	(
		pTransactieId,
		pDocId,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pTDId;
END //
DELIMITER ;

call transactiedocinsert(@pTDId, 1, 1, 'admin');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TransactieDocUpdate`;
DELIMITER //
CREATE PROCEDURE `TransactieDocUpdate`
(
	IN pTDId INT ,
	IN pTransactieId INT ,
	IN pDocId INT ,
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `transactiedoc`
SET
	`TransactieId` = pTransactieId,
	`DocId` = pDocId,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `transactiedoc`.TDId = pTDId;
END //
DELIMITER ;

call transactiedocupdate(1, 1, 2, 'admin');

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TransactieDocSelectAll`;
DELIMITER //
CREATE PROCEDURE `TransactieDocSelectAll`()
BEGIN
	SELECT * FROM `transactiedoc`
;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TransactieDocDelete`;
DELIMITER //
CREATE PROCEDURE `TransactieDocDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `transactiedoc`
WHERE `transactiedoc`.TDId = pId;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TransactieDocSelectById`;
DELIMITER //
CREATE PROCEDURE `TransactieDocSelectById`
(
	IN pTDId INT 
)
BEGIN
	SELECT * FROM `transactiedoc` WHERE `transactiedoc`.`TDId` = pTDId;
END //
DELIMITER ;

call transactiedocselectbyid(1);



USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS TransactieDocDeleteByTransactieId;
DELIMITER //
CREATE PROCEDURE `TransactieDocDeleteByTransactieId`
(
	IN pTAId INT 
)
BEGIN
	delete FROM `transactiedoc` WHERE `transactiedoc`.`TransactieId` = pTAId;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `TransactieDocSelectDocIDFromTransactie`;
DELIMITER //
CREATE PROCEDURE `TransactieDocSelectDocIDFromTransactie`
(
    IN pTAId INT
)
BEGIN
	SELECT * FROM `transactiedoc` where transactiedoc.TransactieId = pTAId;
END //
DELIMITER ;

call TransactieDocSelectDocIDFromTransactie(4);


