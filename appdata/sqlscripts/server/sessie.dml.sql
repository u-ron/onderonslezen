﻿USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `SessieUpdate`;
DELIMITER //
CREATE PROCEDURE `SessieUpdate`
(
	IN pId INT ,
	IN pSessionId VARCHAR (255) CHARACTER SET UTF8 ,
	IN pLidId INT,
	IN pLastActivity VARCHAR(30),
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE sessie
SET
	 SessionId = pSessionId,
	 LidId = pLidId,
	 LastActivity = pLastActivity,
	 ModifiedBy = pModifiedBy,
	 ModifiedOn = NOW()
WHERE Id = pId;
END //
DELIMITER ;

USE rfewebsiikbiblio;
DROP PROCEDURE IF EXISTS `SessieSelectById`;
DELIMITER //
CREATE PROCEDURE `SessieSelectById`
(
	IN pId INT 
)
BEGIN
	SELECT * FROM sessie WHERE Id = pId;
END //
DELIMITER ;


