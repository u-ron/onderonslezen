﻿-- Created : Saturday 1st of November 2014 03:07:10 AM

USE dbool;
DROP PROCEDURE IF EXISTS `AuteurInsert`;
DELIMITER //
CREATE PROCEDURE `AuteurInsert`
(
	OUT pAuteurId INT ,
	IN pAuteurNaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pAuteurVoornaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pAuteurInfo VARCHAR (255) CHARACTER SET UTF8 ,
	IN pAddedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `Auteur`
	(
		`Auteur`.`AuteurNaam`,
		`Auteur`.`AuteurVoornaam`,
		`Auteur`.`AuteurInfo`,
		`Auteur`.`AddedBy`,
		`Auteur`.`InsertedOn`
	)
	VALUES
	(
		pAuteurNaam,
		pAuteurVoornaam,
		pAuteurInfo,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pAuteurId;
END //
DELIMITER ;

call AuteurInsert(@pAuteurId, 'Brusselmans', 'Herman', 'Vlaams schrijver', NULL);
call AuteurInsert(@pAuteurId, 'Lanoy', 'Tom', 'Vlaams schrijver', 'RE');

USE dbool;
DROP PROCEDURE IF EXISTS `AuteurUpdate`;
DELIMITER //
CREATE PROCEDURE `AuteurUpdate`
(
	IN pAuteurId INT ,
	IN pAuteurNaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pAuteurVoornaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pAuteurInfo VARCHAR (255) CHARACTER SET UTF8 ,
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `Auteur`
SET
	`AuteurNaam` = pAuteurNaam,
	`AuteurVoornaam` = pAuteurVoornaam,
	`AuteurInfo` = pAuteurInfo,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `Auteur`.AuteurId = pAuteurId;
END //
DELIMITER ;

call AuteurUpdate('1', 'Brusselmans', 'H', 'Vlaams schrijver', 'RE');

USE dbool;
DROP PROCEDURE IF EXISTS `AuteurDelete`;
DELIMITER //
CREATE PROCEDURE `AuteurDelete`
(
	IN pAuteurId INT
)
BEGIN
DELETE FROM `Auteur`
WHERE `Auteur`.AuteurId = pAuteurId;
END //
DELIMITER ;


USE dbool;
DROP PROCEDURE IF EXISTS `AuteurSelectAll`;
DELIMITER //
CREATE PROCEDURE `AuteurSelectAll`
(
)
BEGIN
	SELECT * FROM `Auteur` order by Auteur.Auteurnaam;
;
END //
DELIMITER ;

call auteurselectall();



USE dbool;
DROP PROCEDURE IF EXISTS `AuteurSelectById`;
DELIMITER //
CREATE PROCEDURE `AuteurSelectById`
(
	IN pAuteurId INT 
)
BEGIN
	SELECT * FROM `Auteur` WHERE `Auteur`.`AuteurId` = pAuteurId;
END //
DELIMITER ;


call auteurselectbyid('2');
