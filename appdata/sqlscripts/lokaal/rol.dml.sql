﻿insert into rol (rolid, rol) values('1', 'Ontlener');
insert into rol (rolid, rol) values('2', 'Verlener');
insert into rol (rolid, rol) values('3', 'Koper');
insert into rol (rolid, rol) values('4', 'Verkoper');

USE dbool;
DROP PROCEDURE IF EXISTS RolSelectById;
DELIMITER //
CREATE PROCEDURE RolSelectById
(
	IN pId INT
)
BEGIN
	SELECT * FROM rol WHERE rolId = pId;
END //
DELIMITER ;

call RolSelectById(2);

USE dbool;
DROP PROCEDURE IF EXISTS RolSelectAll;
DELIMITER //
CREATE PROCEDURE RolSelectAll()
BEGIN
	SELECT * FROM rol;
END //
DELIMITER ;

call RolSelectAll();