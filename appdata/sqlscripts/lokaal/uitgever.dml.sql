﻿-- Created : Saturday 1st of November 2014 05:43:17 AM
USE dbool;
DROP PROCEDURE IF EXISTS `UitgeverInsert`;
DELIMITER //
CREATE PROCEDURE `UitgeverInsert`
(
	OUT pUitgeverId INT ,
	IN pUitgever VARCHAR (255) CHARACTER SET UTF8 ,
	IN pUitgeverInfo VARCHAR (255) CHARACTER SET UTF8 ,
	IN pAddedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `Uitgever`
	(
		`Uitgever`.`Uitgever`,
		`Uitgever`.`UitgeverInfo`,
		`Uitgever`.`AddedBy`,
		`Uitgever`.`InsertedOn`
	)
	VALUES
	(
		pUitgever,
		pUitgeverInfo,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pUitgeverId;
END //
DELIMITER ;

call UitgeverInsert(@pUitgeverId, 'Lannoo', NULL, 'Admin');
call UitgeverInsert(@pUitgeverId, 'Standaard Uitgeverij', NULL, 'Admin');

USE dbool;
DROP PROCEDURE IF EXISTS `UitgeverUpdate`;
DELIMITER //
CREATE PROCEDURE `UitgeverUpdate`
(
	IN pUitgeverId INT ,
	IN pUitgever VARCHAR (255) CHARACTER SET UTF8 ,
	IN pUitgeverInfo VARCHAR (255) CHARACTER SET UTF8 ,
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `Uitgever`
SET
	`Uitgever` = pUitgever,
	`UitgeverInfo` = pUitgeverInfo,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `Uitgever`.UitgeverId = pUitgeverId;
END //
DELIMITER ;

call UitgeverUpdate('1', 'Lannoo', 'in Tielt', 'Admin');

USE dbool;
DROP PROCEDURE IF EXISTS `UitgeverDelete`;
DELIMITER //
CREATE PROCEDURE `UitgeverDelete`
(
	IN pUitgeverId INT
)
BEGIN
DELETE FROM `Uitgever`
WHERE `Uitgever`.UitgeverId = pUitgeverId;
END //
DELIMITER ;

USE dbool;
DROP PROCEDURE IF EXISTS `UitgeverSelectAll`;
DELIMITER //
CREATE PROCEDURE `UitgeverSelectAll`
(
)
BEGIN
	SELECT * FROM `Uitgever` order by Uitgever.Uitgever;
END //
DELIMITER ;

call UitgeverSelectAll();

USE dbool;
DROP PROCEDURE IF EXISTS `UitgeverCount`;
DELIMITER //
CREATE PROCEDURE `UitgeverCount`
(
)
BEGIN
	SELECT count(*) FROM `Uitgever`;
END //
DELIMITER ;

USE dbool;
DROP PROCEDURE IF EXISTS `UitgeverSelectById`;
DELIMITER //
CREATE PROCEDURE `UitgeverSelectById`
(
	IN pUitgeverId INT 
)
BEGIN
	SELECT * FROM `Uitgever` WHERE `Uitgever`.`UitgeverId` = pUitgeverId;
END //
DELIMITER ;

call UitgeverSelectById(1);