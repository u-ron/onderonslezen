﻿USE dbool;
DROP PROCEDURE IF EXISTS `FotoInsert`;
DELIMITER //
CREATE PROCEDURE `FotoInsert`
(
	OUT pFotoId INT ,
	IN pFotoNaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pURL VARCHAR (255) CHARACTER SET UTF8 ,
	IN pDocId INT ,
	IN pAddedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `Foto`
	(
		`Foto`.`FotoNaam`,
		`Foto`.`URL`,
		`Foto`.`DocId`,
		`Foto`.`AddedBy`,
		`Foto`.`InsertedOn`
	)
	VALUES
	(
		pFotoNaam,
		pURL,
		pDocId,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pFotoId;
END //
DELIMITER ;

call fotoinsert(@pFotoId, 'foto3.gif', 'images/doc2/', '2', 'admin');

USE dbool;
DROP PROCEDURE IF EXISTS `FotoUpdate`;
DELIMITER //
CREATE PROCEDURE `FotoUpdate`
(
	IN pFotoId INT ,
	IN pFotoNaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pURL VARCHAR (255) CHARACTER SET UTF8 ,
	IN pDocId INT ,
	IN pModifiedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
UPDATE `Foto`
SET
	`FotoNaam` = pFotoNaam,
	`URL` = pURL,
	`DocId` = pDocId,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `Foto`.FotoId = pFotoId;
END //
DELIMITER ;

call fotoupdate('5', 'foto3.gif', 'images/doc1/', '1', 'admin');

USE dbool;
DROP PROCEDURE IF EXISTS `FotoDelete`;
DELIMITER //
CREATE PROCEDURE `FotoDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `Foto`
WHERE `Foto`.FotoId = pId;
END //
DELIMITER ;

USE dbool;
DROP PROCEDURE IF EXISTS `FotoSelectAll`;
DELIMITER //
CREATE PROCEDURE `FotoSelectAll`
(
)
BEGIN
	SELECT * FROM `Foto`
;
END //
DELIMITER ;

USE dbool;
DROP PROCEDURE IF EXISTS `FotoSelectById`;
DELIMITER //
CREATE PROCEDURE `FotoSelectById`
(
	IN pFotoId INT 
)
BEGIN
	SELECT * FROM `Foto` WHERE `Foto`.`FotoId` = pFotoId;
END //
DELIMITER ;

USE dbool;
DROP PROCEDURE IF EXISTS `FotoSelectByDocId`;
DELIMITER //
CREATE PROCEDURE `FotoSelectByDocId`
(
	IN pDocId INT 
)
BEGIN
	SELECT * FROM `Foto` WHERE `Foto`.`DocId` = pDocId;
END //
DELIMITER ;

call FotoSelectByDocId(1);
