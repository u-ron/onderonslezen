﻿-- An Orm Apart -- Wednesday 12th of November 2014 05:18:40 AM
USE `dbool`;
DROP TABLE IF EXISTS `TransactiePartner`;
CREATE TABLE `TransactiePartner` (
	`TPId` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(TPId),
	`TransactieId` INT NOT NULL,
	`LidId` INT NOT NULL,
	`RolId` INT NOT NULL,
    `DelStatus` TINYINT(1) NOT NULL,
	`AddedBy` VARCHAR (255) CHARACTER SET UTF8 NULL,
	`InsertedOn` TIMESTAMP NULL,
	`ModifiedBy` VARCHAR (255) CHARACTER SET UTF8 NULL,
	`ModifiedOn` TIMESTAMP NULL);



