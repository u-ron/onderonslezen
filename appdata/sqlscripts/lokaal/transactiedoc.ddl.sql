﻿USE `dbool`;
CREATE TABLE `transactiedoc` (
  `TDId` int(11) NOT NULL AUTO_INCREMENT,
  `TransactieId` int(11) DEFAULT NULL,
  `DocId` int(11) DEFAULT NULL,
  `AddedBy` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `InsertedOn` timestamp NULL DEFAULT NULL,
  `ModifiedBy` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ModifiedOn` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`TDId`),
  KEY `transactiedoc_ibfk_1` (`DocId`),
  CONSTRAINT `transactiedoc_ibfk_1` FOREIGN KEY (`DocId`) REFERENCES `doc` (`DocId`) ON DELETE SET NULL
);

    