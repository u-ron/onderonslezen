﻿insert into toestand (toestandid, toestand) values('1', 'Nieuw');

USE dbool;
DROP PROCEDURE IF EXISTS ToestandSelectById;
DELIMITER //
CREATE PROCEDURE ToestandSelectById
(
	IN pId INT
)
BEGIN
	SELECT * FROM Toestand
	WHERE ToestandId = pId;
END //
DELIMITER ;

call ToestandSelectById(2);

USE dbool;
DROP PROCEDURE IF EXISTS ToestandSelectAll;
DELIMITER //
CREATE PROCEDURE ToestandSelectAll()
BEGIN
	SELECT * FROM Toestand;
END //
DELIMITER ;

call ToestandSelectAll();