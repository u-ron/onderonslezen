﻿-- Created : Saturday 8th of November 2014 11:01:05 AM
USE dbool;
DROP PROCEDURE IF EXISTS `LidInsert`;
DELIMITER //
CREATE PROCEDURE `LidInsert`
(
	OUT pLidId INT ,
	IN pGebruikersNaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pWachtwoord VARCHAR (255) CHARACTER SET UTF8 ,
	IN pLidNaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pLidVoornaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pLidInfo VARCHAR (255) CHARACTER SET UTF8 ,
	IN pAdres VARCHAR (255) CHARACTER SET UTF8 ,
	IN pTelefoon VARCHAR (255) CHARACTER SET UTF8 ,
	IN pEmail VARCHAR (255) CHARACTER SET UTF8 ,
	IN pSkypeNaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pWoonId INT ,
	IN pGesloten BOOL ,
    IN pLidStatus INT,
	IN pAddedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `Lid`
	(
		`Lid`.`GebruikersNaam`,
		`Lid`.`Wachtwoord`,
		`Lid`.`LidNaam`,
		`Lid`.`LidVoornaam`,
		`Lid`.`LidInfo`,
		`Lid`.`Adres`,
		`Lid`.`Telefoon`,
		`Lid`.`Email`,
		`Lid`.`SkypeNaam`,
		`Lid`.`WoonId`,
		`Lid`.`Gesloten`,
        `Lid`.`LidStatus`,
		`Lid`.`AddedBy`,
		`Lid`.`InsertedOn`
	)
	VALUES
	(
		pGebruikersNaam,
		pWachtwoord,
		pLidNaam,
		pLidVoornaam,
		pLidInfo,
		pAdres,
		pTelefoon,
		pEmail,
		pSkypeNaam,
		pWoonId,
		pGesloten,
        pLidStatus,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pLidId;
END //
DELIMITER ;

call LidInsert(@pLidId, 'JosK', 'joss','Jolens', 'Jan', NULL, 'Vredestraat 2', '0477/455612', 'josj@skynet.be', 'josjskype', '1', '0', '1', 'admin');

USE dbool;
DROP PROCEDURE IF EXISTS `LidUpdate`;
DELIMITER //
CREATE PROCEDURE `LidUpdate`
(
	IN pLidId INT ,
	IN pGebruikersNaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pWachtwoord VARCHAR (255) CHARACTER SET UTF8 ,
	IN pLidNaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pLidVoornaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pLidInfo VARCHAR (255) CHARACTER SET UTF8 ,
	IN pAdres VARCHAR (255) CHARACTER SET UTF8 ,
	IN pTelefoon VARCHAR (255) CHARACTER SET UTF8 ,
	IN pEmail VARCHAR (255) CHARACTER SET UTF8 ,
	IN pSkypeNaam VARCHAR (255) CHARACTER SET UTF8 ,
	IN pWoonId INT ,
	IN pGesloten BOOL ,
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `Lid`
SET
	`GebruikersNaam` = pGebruikersNaam,
	`Wachtwoord` = pWachtwoord,
	`LidNaam` = pLidNaam,
	`LidVoornaam` = pLidVoornaam,
	`LidInfo` = pLidInfo,
	`Adres` = pAdres,
	`Telefoon` = pTelefoon,
	`Email` = pEmail,
	`SkypeNaam` = pSkypeNaam,
	`WoonId` = pWoonId,
	`Gesloten` = pGesloten,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `Lid`.LidId = pLidId;
END //
DELIMITER ;

call lidupdate('1','JanJ', 'jan63','Janssens', 'Jan', NULL, 'Vredestraat 2', '0477/455612', 'jj@skynet.be', 'janskype', '2', '0', 'admin'); 

USE dbool;
DROP PROCEDURE IF EXISTS `LidDelete`;
DELIMITER //
CREATE PROCEDURE `LidDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `Lid` WHERE `Lid`.LidId = pId;
END //
DELIMITER ;

USE dbool;
DROP PROCEDURE IF EXISTS `LidSelectAll`;
DELIMITER //
CREATE PROCEDURE `LidSelectAll`
(
)
BEGIN
	SELECT * FROM `Lid` order by lid.lidnaam;
END //
DELIMITER ;

call lidselectall();

USE dbool;
DROP PROCEDURE IF EXISTS `LidSelectById`;
DELIMITER //
CREATE PROCEDURE `LidSelectById`
(
	IN pLidId INT 
)
BEGIN
	SELECT * FROM `Lid` WHERE `Lid`.`LidId` = pLidId;
END //
DELIMITER ;

call lidselectbyid('1');

USE dbool;
DROP PROCEDURE IF EXISTS `LidSelectByGebruikersNaam`;
DELIMITER //
CREATE PROCEDURE `LidSelectByGebruikersNaam`
(
	IN pGebruikersNaam VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	SELECT * FROM `Lid` WHERE `Lid`.`GebruikersNaam` = pGebruikersNaam;
END //
DELIMITER ;

call lidselectbygebruikersnaam('admin');


USE dbool;
DROP PROCEDURE IF EXISTS `LidSelectVerkopers`;
DELIMITER //
CREATE PROCEDURE `LidSelectVerkopers`
(
    IN pLidId INT
)
BEGIN
	SELECT distinct Lid.LidNaam, Lid.LidVoornaam, Lid.LidId FROM Lid, Registratie, Doc where Lid.LidId = Registratie.LidId and Registratie.DocId = Doc.DocId and Doc.TeKoop = true and Lid.LidId != pLidId order by Lid.LidNaam;
END //
DELIMITER ;

call lidselectverkopers(3);

USE dbool;
DROP PROCEDURE IF EXISTS `LidSelectKoper`;
DELIMITER //
CREATE PROCEDURE `LidSelectKoper`
(
	IN pTAId INT
)
BEGIN
select * from lid inner join transactiepartner on lid.lidid = transactiepartner.lidid 
    where transactiepartner.transactieid = pTAId and transactiepartner.rolid = 3;
END //
DELIMITER ;

USE dbool;
DROP PROCEDURE IF EXISTS `LidSelectVerkoper`;
DELIMITER //
CREATE PROCEDURE `LidSelectVerkoper`
( IN pTAId INT)
BEGIN
select * from lid inner join transactiepartner on lid.lidid = transactiepartner.lidid 
    where transactiepartner.transactieid = pTAId and transactiepartner.rolid = 4;
END //
DELIMITER ;

USE dbool;
DROP PROCEDURE IF EXISTS `LidSelectVerleners`;
DELIMITER //
CREATE PROCEDURE `LidSelectVerleners`
(
    IN pLidId INT
)
BEGIN
	SELECT distinct Lid.LidNaam, Lid.LidVoornaam, Lid.LidId FROM Lid, Registratie, Doc where Lid.LidId = Registratie.LidId and Registratie.DocId = Doc.DocId and Doc.TeLeen = true and Lid.LidId != pLidId order by Lid.LidNaam;
END //
DELIMITER ;

call lidselectverleners();

USE dbool;
DROP PROCEDURE IF EXISTS `LidSelectLener`;
DELIMITER //
CREATE PROCEDURE `LidSelectLener`
(
	IN pTAId INT
)
BEGIN
select * from lid inner join transactiepartner on lid.lidid = transactiepartner.lidid 
    where transactiepartner.transactieid = pTAId and transactiepartner.rolid = 1;
END //
DELIMITER ;

call lidselectlener(10);

USE dbool;
DROP PROCEDURE IF EXISTS `LidSelectVerlener`;
DELIMITER //
CREATE PROCEDURE `LidSelectVerlener`
(IN pTAId INT)
BEGIN
select * from lid inner join transactiepartner on lid.lidid = transactiepartner.lidid 
    where transactiepartner.transactieid = pTAId and transactiepartner.rolid = 2;
END //
DELIMITER ;

call lidselectverlener(10);

USE dbool;
DROP PROCEDURE IF EXISTS `LidCloseAccount`;
DELIMITER //
CREATE PROCEDURE `LidCloseAccount`
(
	IN pLidId INT ,
	IN pGesloten TINYINT (1) ,
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `Lid`
SET
    Gesloten = pGesloten,
	ModifiedBy = pModifiedBy,
	ModifiedOn = NOW()
WHERE `Lid`.LidId = pLidId;
END //
DELIMITER ;

call LidCloseAccount(14, 1, 'admin');