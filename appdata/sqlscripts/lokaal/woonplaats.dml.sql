﻿USE dbool;
DROP PROCEDURE IF EXISTS `WoonplaatsInsert`;
DELIMITER //
CREATE PROCEDURE `WoonplaatsInsert`
(
	OUT pWoonplaatsId INT ,
	IN pGemeente VARCHAR (255) CHARACTER SET UTF8 ,
	IN pPostcode VARCHAR (255) CHARACTER SET UTF8 ,
	IN pAddedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `Woonplaats`
	(
		`Woonplaats`.`Gemeente`,
		`Woonplaats`.`PostCode`,
		`Woonplaats`.`AddedBy`,
		`Woonplaats`.`InsertedOn`
	)
	VALUES
	(
		pGemeente,
		pPostcode,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pWoonplaatsId;
END //
DELIMITER ;

call woonplaatsinsert(@pWoonPlaatsId, 'Tielt', '8700', 'admin');

USE dbool;
DROP PROCEDURE IF EXISTS `WoonplaatsUpdate`;
DELIMITER //
CREATE PROCEDURE `WoonplaatsUpdate`
(
	IN pWoonplaatsId INT ,
	IN pGemeente VARCHAR (255) CHARACTER SET UTF8 ,
	IN pPostcode VARCHAR (255) CHARACTER SET UTF8 ,
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `Woonplaats`
SET
	`Gemeente` = pGemeente,
	`Postcode` = pPostcode,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `Woonplaats`.WoonplaatsId = pWoonplaatsId;
END //
DELIMITER ;

call woonplaatsupdate('1', 'Tielt', '8700', 'Admin');

USE dbool;
DROP PROCEDURE IF EXISTS `WoonplaatsSelectAll`;
DELIMITER //
CREATE PROCEDURE `WoonplaatsSelectAll` ()
BEGIN
	SELECT * FROM `Woonplaats` order by postcode;
END //
DELIMITER ;

USE dbool;
DROP PROCEDURE IF EXISTS `WoonplaatsSelectById`;
DELIMITER //
CREATE PROCEDURE `WoonplaatsSelectById`
(
	IN pWoonplaatsId INT 
)
BEGIN
	SELECT * FROM `Woonplaats` WHERE `Woonplaats`.`WoonplaatsId` = pWoonplaatsId;
END //
DELIMITER ;

call WoonplaatsSelectById(2); //gaat ook: call WoonplaatsSelectById('2');

USE dbool;
DROP PROCEDURE IF EXISTS `WoonplaatsDelete`;
DELIMITER //
CREATE PROCEDURE `WoonplaatsDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `Woonplaats`
WHERE `Woonplaats`.WoonplaatsId = pId;
END //
DELIMITER ;





