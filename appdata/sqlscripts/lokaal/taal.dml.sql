﻿insert into taal (taalid, taal) values('1', 'Nederlands');

USE dbool;
DROP PROCEDURE IF EXISTS `TaalSelectById`;
DELIMITER //
CREATE PROCEDURE `TaalSelectById`
(
	IN pId INT
)
BEGIN
	SELECT * FROM Taal
	WHERE TaalId = pId;
END //
DELIMITER ;

call TaalSelectById(2);

USE dbool;
DROP PROCEDURE IF EXISTS TaalSelectAll;
DELIMITER //
CREATE PROCEDURE TaalSelectAll()
BEGIN
	SELECT * FROM Taal;
END //
DELIMITER ;

call TaalSelectAll();

