﻿-- Created on Saturday 1st of November 2014 05:39:06 AM
 
USE `dbool`;
DROP TABLE IF EXISTS `Uitgever`;
CREATE TABLE `Uitgever` (
	`UitgeverId` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(UitgeverId),
	`Uitgever` VARCHAR (255) CHARACTER SET UTF8 NOT NULL,
	`UitgeverInfo` VARCHAR (255) CHARACTER SET UTF8 NULL,
	`AddedBy` VARCHAR (255) CHARACTER SET UTF8 NULL,
	`InsertedOn` TIMESTAMP NULL,
	`ModifiedBy` VARCHAR (255) CHARACTER SET UTF8 NULL,
	`ModifiedOn` TIMESTAMP NULL);

