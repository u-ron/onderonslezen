﻿-- Created : Saturday 1st of November 2014 03:41:20 AM

USE dbool;
DROP PROCEDURE IF EXISTS `DocTypeInsert`;
DELIMITER //
CREATE PROCEDURE `DocTypeInsert`
(
	OUT pDocTypeId INT ,
	IN pDocType VARCHAR (255) CHARACTER SET UTF8 ,
	IN pAddedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `DocType`
	(
		`DocType`.`DocType`,
		`DocType`.`AddedBy`,
		`DocType`.`InsertedOn`
	)
	VALUES
	(
		pDocType,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pDocTypeId;
END //
DELIMITER ;

call DocTypeInsert(@pDocTypeId, 'Atlas', 'Admin');
call DocTypeInsert(@pDocTypeId, 'Boek', 'Admin');
call DocTypeInsert(@pDocTypeId, 'Cursus', 'Admin');
call DocTypeInsert(@pDocTypeId, 'Encyclopedie', 'Admin');


USE dbool ;
DROP PROCEDURE IF EXISTS `DocTypeUpdate`;
DELIMITER //
CREATE PROCEDURE `DocTypeUpdate`
(
	IN pDocTypeId INT ,
	IN pDocType VARCHAR (255) CHARACTER SET UTF8 ,
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `DocType`
SET
	`DocType` = pDocType,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `DocType`.DocTypeId = pDocTypeId;
END //
DELIMITER ;

call DocTypeUpdate('2','boek','Admin');

USE dbool;
DROP PROCEDURE IF EXISTS `DocTypeDelete`;
DELIMITER //
CREATE PROCEDURE `DocTypeDelete`
(
	IN pDocTypeId INT
)
BEGIN
DELETE FROM `DocType`
WHERE `DocType`.DocTypeId = pDocTypeId;
END //
DELIMITER ;


USE dbool;
DROP PROCEDURE IF EXISTS `DocTypeSelectAll`;
DELIMITER //
CREATE PROCEDURE `DocTypeSelectAll`
(
)
BEGIN
	SELECT * FROM `DocType`
;
END //
DELIMITER ;

call doctypeselectall();

USE dbool;
DROP PROCEDURE IF EXISTS `DocTypeSelectById`;
DELIMITER //
CREATE PROCEDURE `DocTypeSelectById`
(
	IN pDocTypeId INT 
)
BEGIN
	SELECT * FROM `DocType` WHERE `DocType`.`DocTypeId` = pDocTypeId;
END //
DELIMITER ;

call doctypeselectbyid('3');
