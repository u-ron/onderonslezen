﻿USE dbool;
DROP PROCEDURE IF EXISTS `RegistratieInsert`;
DELIMITER //
CREATE PROCEDURE `RegistratieInsert`
(
	OUT pRegId INT ,
	IN pLidId INT ,
	IN pDocId INT ,
	IN pAddedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `Registratie`
	(
		`Registratie`.`LidId`,
		`Registratie`.`DocId`,
		`Registratie`.`AddedBy`,
		`Registratie`.`InsertedOn`
	)
	VALUES
	(
		pLidId,
		pDocId,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pRegId;
END //
DELIMITER ;

call registratieinsert(@pRegId, '1','2','admin');
call registratieinsert(@pRegId, '6','3','admin');
call registratieinsert(@pRegId, '1','1','admin');
call registratieinsert(@pRegId, '7','4','admin');
call registratieinsert(@pRegId, '3','5','admin');


USE dbool;
DROP PROCEDURE IF EXISTS `RegistratieUpdate`;
DELIMITER //
CREATE PROCEDURE `RegistratieUpdate`
(
	IN pRegId INT ,
	IN pLidId INT ,
	IN pDocId INT ,
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `Registratie`
SET
	`LidId` = pLidId,
	`DocId` = pDocId,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `Registratie`.RegId = pRegId;
END //
DELIMITER ;

call registratieupdate('1','2','1','admin');

USE dbool;
DROP PROCEDURE IF EXISTS `RegistratieSelectAll`;
DELIMITER //
CREATE PROCEDURE `RegistratieSelectAll`()
BEGIN
	SELECT * FROM `Registratie`
;
END //
DELIMITER ;


USE dbool;
DROP PROCEDURE IF EXISTS `RegistratieDelete`;
DELIMITER //
CREATE PROCEDURE `RegistratieDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `Registratie`
WHERE `Registratie`.RegId = pId;
END //
DELIMITER ;

 
USE dbool;
DROP PROCEDURE IF EXISTS `RegistratieSelectByLidId`;
DELIMITER //
CREATE PROCEDURE `RegistratieSelectByLidId`
(
	IN pLidId INT 
)
BEGIN
	SELECT * FROM `Registratie` WHERE `Registratie`.`LidId` = pLidId;
END //
DELIMITER ;




USE dbool;
DROP PROCEDURE IF EXISTS `RegistratieSelectByDocId`;
delimiter //
CREATE PROCEDURE `RegistratieSelectByDocId`(
	IN pDocId INT 
)
BEGIN
	SELECT * FROM `Registratie` WHERE `Registratie`.`DocId` = pDocId;
END //
DELIMITER ;