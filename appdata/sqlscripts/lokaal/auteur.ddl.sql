﻿-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Auteur
-- Created on Saturday 1st of November 2014 03:01:44 AM
-- 
USE `dbool`;
DROP TABLE IF EXISTS `Auteur`;
CREATE TABLE `Auteur` (
	`AuteurId` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(AuteurId),
	`AuteurNaam` VARCHAR (255) CHARACTER SET UTF8 NOT NULL,
	`AuteurVoornaam` VARCHAR (255) CHARACTER SET UTF8,
	`AuteurInfo` VARCHAR (255) CHARACTER SET UTF8,
	`AddedBy` VARCHAR (255) CHARACTER SET UTF8 NULL,
	`InsertedOn` TIMESTAMP NULL,
	`ModifiedBy` VARCHAR (255) CHARACTER SET UTF8 NULL,
	`ModifiedOn` TIMESTAMP NULL);




