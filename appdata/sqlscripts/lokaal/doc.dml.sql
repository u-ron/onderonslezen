﻿-- Created : Saturday 1st of November 2014 03:57:39 PM
USE dbool;
DROP PROCEDURE IF EXISTS `DocInsert`;
DELIMITER //
CREATE PROCEDURE `DocInsert`
(
	OUT pDocId INT ,
	IN pTitel VARCHAR (255) CHARACTER SET UTF8 ,
	IN pDocInfo VARCHAR (255) CHARACTER SET UTF8 ,
    IN pPrijs VARCHAR (255),
	IN pJaar INT ,
    IN pISBN VARCHAR(255),
	IN pTeLeen BOOL ,
	IN pTeKoop BOOL ,
	IN pDocTypeId INT ,
	IN pTaalId INT ,
	IN pAuteurId INT ,
	IN pUitgeverId INT ,
	IN pToestandId INT ,
	IN pAddedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `Doc`
	(
		`Doc`.`Titel`,
		`Doc`.`DocInfo`,
        `Doc`.`Prijs`,
		`Doc`.`Jaar`,
        `Doc`.`ISBN`,
		`Doc`.`TeLeen`,
		`Doc`.`TeKoop`,
		`Doc`.`DocTypeId`,
		`Doc`.`TaalId`,
		`Doc`.`AuteurId`,
		`Doc`.`UitgeverId`,
		`Doc`.`ToestandId`,
		`Doc`.`AddedBy`,
		`Doc`.`InsertedOn`
	)
	VALUES
	(
		pTitel,
		pDocInfo,
        pPrijs,
		pJaar,
        pISBN,
		pTeLeen,
		pTeKoop,
		pDocTypeId,
		pTaalId,
		pAuteurId,
		pUitgeverId,
		pToestandId,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pDocId;
END //
DELIMITER ;

call DocInsert(@pDocId, 'Zwarte tranen', NULL, NULL, '2002', NULL, '5', '1', '1', '1', '1', '2', NULL, '3', 'Admin'); 
call DocInsert(@pDocId, 'jQuery De Basis', NULL, NULL, '2012', '978-90-430-2525-6', '1','0','1','1','7','4', '1', 'Admin');

USE dbool;
DROP PROCEDURE IF EXISTS `DocUpdate`;
DELIMITER //
CREATE PROCEDURE `DocUpdate`
(
	IN pDocId INT ,
	IN pTitel VARCHAR (255) CHARACTER SET UTF8 ,
	IN pDocInfo VARCHAR (255) CHARACTER SET UTF8 ,
	IN pJaar INT ,
    IN pISBN VARCHAR (255) CHARACTER SET UTF8 ,
    IN pPrijs VARCHAR (255),
	IN pTeLeen BOOL ,
	IN pTeKoop BOOL ,
	IN pDocTypeId INT ,
	IN pTaalId INT ,
	IN pAuteurId INT ,
	IN pUitgeverId INT ,
	IN pToestandId INT ,
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `Doc`
SET
	`Titel` = pTitel,
	`DocInfo` = pDocInfo,
	`Jaar` = pJaar,
	`ISBN` = pISBN,
    `Prijs` = pPrijs,
	`TeLeen` = pTeLeen,
	`TeKoop` = pTeKoop,
	`DocTypeId` = pDocTypeId,
	`TaalId` = pTaalId,
	`AuteurId` = pAuteurId,
	`UitgeverId` = pUitgeverId,
	`ToestandId` = pToestandId,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `Doc`.DocId = pDocId;
END //
DELIMITER ;

call DocUpdate('2', 'Zwarte tranen', 'Roman', '2002', NULL, '2', '1', '1', '1', '1', '2', NULL, '2', 'Admin');

USE dbool;
DROP PROCEDURE IF EXISTS `DocDelete`;
DELIMITER //
CREATE PROCEDURE `DocDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `Doc`
WHERE `Doc`.DocId = pId;
END //
DELIMITER ;

USE dbool;
DROP PROCEDURE IF EXISTS `DocSelectAll`;
DELIMITER //
CREATE PROCEDURE `DocSelectAll`
(
)
BEGIN
	SELECT * FROM `Doc`;
END //
DELIMITER ;

USE dbool;
DROP PROCEDURE IF EXISTS `DocSelectById`;
DELIMITER //
CREATE PROCEDURE `DocSelectById`
(
	IN pDocId INT 
)
BEGIN
	SELECT * FROM `Doc` WHERE `Doc`.`DocId` = pDocId;
END //
DELIMITER ;

call DocSelectById('2');

USE dbool;
DROP PROCEDURE IF EXISTS `DocSelectKoopwaarVanVerkoper`;
DELIMITER //
CREATE PROCEDURE `DocSelectKoopwaarVanVerkoper`
(
    pLidId INT
)
BEGIN
SELECT doc.docid as docid, doc.titel as titel, doctype.doctype as doctype, doc.prijs as prijs, auteur.auteurnaam as auteurnaam
FROM auteur right outer join (doctype inner join (registratie inner join doc on registratie.docid = doc.docid) on doctype.doctypeid = doc.doctypeid) on
auteur.auteurid = doc.auteurid where doc.tekoop = true and 
registratie.lidid = pLidId and doc.docid not in (select doc.docid from doc inner join (transactiedoc inner join transactie on transactie.transactieid = transactiedoc.transactieid) on doc.docid = transactiedoc.docid where transactie.dealstatusid in (1,2,3,5,6,7) or transactie.exchangestatusid in (1,2,3,5))
order by doc.docid;
END //
DELIMITER ;
/*de subquery duidt de documenten aan die in transactie zijn of een nieuwe eigenaar gekregen hebben*/

call docselectkoopwaarvanverkoper(3);


USE dbool;
DROP PROCEDURE IF EXISTS DocSelectDocsInDeal;
DELIMITER //
CREATE PROCEDURE DocSelectDocsInDeal
(
    IN pTAId INT
)
BEGIN
SELECT doc.docid as docid, doc.titel, doc.doctypeid, doc.prijs, doc.auteurid 
FROM doc inner join transactiedoc on doc.docid = transactiedoc.docid where transactiedoc.transactieid = pTAId order by transactiedoc.docid;
END //
DELIMITER ;

call DocSelectDocsInDeal(4);

USE dbool;
DROP PROCEDURE IF EXISTS `DocSelectLeenwaarVanVerlener`;
DELIMITER //
CREATE PROCEDURE `DocSelectLeenwaarVanVerlener`
(
    pLidId INT
)
BEGIN
SELECT doc.docid as docid, doc.titel as titel, doctype.doctype as doctype, doc.prijs as prijs, auteur.auteurnaam as auteurnaam
FROM auteur right outer join (doctype inner join (registratie inner join doc on registratie.docid = doc.docid) on doctype.doctypeid = doc.doctypeid) on
auteur.auteurid = doc.auteurid where doc.teleen = true and 
registratie.lidid = pLidId and doc.docid not in
(select doc.docid from doc inner join (transactiedoc inner join transactie on transactie.transactieid = transactiedoc.transactieid) on doc.docid = transactiedoc.docid where transactie.dealstatusid in (1,2,3,5,6,7) or transactie.exchangestatusid in (1,2,3,5))
order by doc.docid;
END //
DELIMITER ;
/*de subquery duidt de documenten aan die in transactie zijn of een nieuwe eigenaar gekregen hebben*/

call docselectleenwaarvanverlener(3);

USE dbool;
DROP PROCEDURE IF EXISTS `DocSelectAllAvailable`;
DELIMITER //
CREATE PROCEDURE `DocSelectAllAvailable`
(
)
BEGIN
	SELECT Doc.DocId, Doc.Titel, Doc.DocInfo, Doc.Jaar, Doc.ISBN, Doc.Prijs, Doc.TeKoop, Doc.TeLeen, Doc.DocTypeId,
    Doc.TaalId, Doc.AuteurId, Doc.UitgeverId, Doc.ToestandId FROM Doc where Doc.DocId not in
    (select doc.docid from doc inner join (transactiedoc inner join transactie on transactie.transactieid = transactiedoc.transactieid) on doc.docid = transactiedoc.docid where transactie.dealstatusid in (1,2,3,5,6,7) or transactie.exchangestatusid in (1,2,3,5));
END //
DELIMITER ;
/*de subquery duidt de documenten aan die in transactie zijn of een nieuwe eigenaar gekregen hebben*/

USE dbool;
DROP PROCEDURE IF EXISTS DocSelectDocsInExchange;
DELIMITER //
CREATE PROCEDURE DocSelectDocsInExchange
(
    IN pTAId INT
)
BEGIN
SELECT doc.docid, doc.titel, doc.doctypeid, doc.auteurid, doc.uitgeverid 
FROM doc inner join transactiedoc on doc.docid = transactiedoc.docid where transactiedoc.transactieid = pTAId order by transactiedoc.docid;
END //
DELIMITER ;

call DocSelectDocsInExchange(10);

USE dbool;
DROP PROCEDURE IF EXISTS `DocFilterAllAvailable`;
DELIMITER //
CREATE PROCEDURE `DocFilterAllAvailable`
(
   IN pTitel VARCHAR (255) CHARACTER SET UTF8 ,
   IN pTaalId INT,
   IN pDocTypeId INT
)
BEGIN
if ((pTaalId is NULL) AND (pDocTypeId is not NULL)) then 
SELECT doc.docid as docid, doc.titel as titel, doc.tekoop, doc.teleen, doc.doctypeid as doctypeid, doc.auteurid as auteurid
FROM auteur right outer join (doc inner join doctype on doc.doctypeid = doctype.doctypeid) on auteur.auteurid = doc.auteurid
where doc.titel like CONCAT('%', pTitel,'%') and doc.taalid in (1,2,3,4,5) and doc.doctypeid = pDocTypeId and doc.docid not in  (select doc.docid from doc inner join (transactiedoc inner join transactie on transactie.transactieid = transactiedoc.transactieid) on doc.docid = transactiedoc.docid where transactie.dealstatusid in (1,2,3,5,6,7) or transactie.exchangestatusid in (1,2,3,5)) order by doc.docid;

elseif ((pDocTypeId is NULL) and (pTaalId is not NULL)) then 
SELECT doc.docid as docid, doc.titel as titel, doc.tekoop, doc.teleen, doctype.doctypeid as doctypeid, doc.auteurid as auteurid
FROM auteur right outer join (doc inner join doctype on doc.doctypeid = doctype.doctypeid) on auteur.auteurid = doc.auteurid
where doc.titel like CONCAT('%', pTitel,'%') and doc.taalid = pTaalId and doc.doctypeid in (select doctypeid from doctype) and doc.docid not in  (select doc.docid from doc inner join (transactiedoc inner join transactie on transactie.transactieid = transactiedoc.transactieid) on doc.docid = transactiedoc.docid where transactie.dealstatusid in (1,2,3,5,6,7) or transactie.exchangestatusid in (1,2,3,5)) order by doc.docid;

elseif ((pTaalId is NULL) AND (pDocTypeId is NULL)) then
SELECT doc.docid as docid, doc.titel as titel, doc.tekoop, doc.teleen, doctype.doctypeid as doctypeid, doc.auteurid as auteurid
FROM auteur right outer join (doc inner join doctype on doc.doctypeid = doctype.doctypeid) on auteur.auteurid = doc.auteurid
where doc.titel like CONCAT('%', pTitel,'%') and doc.taalid in (1,2,3,4,5) and doc.doctypeid in (select doctypeid from doctype) and doc.docid not in  (select doc.docid from doc inner join (transactiedoc inner join transactie on transactie.transactieid = transactiedoc.transactieid) on doc.docid = transactiedoc.docid where transactie.dealstatusid in (1,2,3,5,6,7) or transactie.exchangestatusid in (1,2,3,5)) order by doc.docid;

else
SELECT doc.docid as docid, doc.titel as titel, doc.tekoop, doc.teleen, doctype.doctypeid as doctypeid, doc.auteurid as auteurid
FROM auteur right outer join (doc inner join doctype on doc.doctypeid = doctype.doctypeid) on auteur.auteurid = doc.auteurid
where doc.titel like CONCAT('%', pTitel,'%') and doc.taalid = pTaalId and doc.doctypeid = pDocTypeId and doc.docid not in  (select doc.docid from doc inner join (transactiedoc inner join transactie on transactie.transactieid = transactiedoc.transactieid) on doc.docid = transactiedoc.docid where transactie.dealstatusid in (1,2,3,5,6,7) or transactie.exchangestatusid in (1,2,3,5)) order by doc.docid;

end if;
END //
DELIMITER ;
/*de subquery duidt de documenten aan die in transactie zijn of een nieuwe eigenaar gekregen hebben*/
/*elseif in één woord is de juiste mySQL syntax alhoewel niet blauw opkleurend in Webmatrix*/


USE dbool;
DROP PROCEDURE IF EXISTS `DocInTransactie`;
DELIMITER //
CREATE PROCEDURE `DocInTransactie`
(
	IN pDocId INT 
)
BEGIN
	 (select doc.docid from doc inner join (transactiedoc inner join transactie on transactie.transactieid = transactiedoc.transactieid) on doc.docid = transactiedoc.docid where doc.docid = pDocId and (transactie.dealstatusid in (1,2,3,5) or transactie.exchangestatusid in (1,2,3,5)));
END //
DELIMITER ;

call docintransactie(40);


USE dbool;
DROP PROCEDURE IF EXISTS `RegistratieSelectDocumentenPerLid`;
DELIMITER //
CREATE PROCEDURE `RegistratieSelectDocumentenPerLid`(IN pLidId INT)
BEGIN
	SELECT * FROM Registratie inner join Doc on Doc.DocId = Registratie.DocId where Registratie.LidId = pLidId and Doc.DocId not in
    (select doc.docid from doc inner join (transactiedoc inner join transactie on transactie.transactieid = transactiedoc.transactieid) on doc.docid = transactiedoc.docid where transactie.dealstatusid in (6,7));
END //
DELIMITER ;
/*de subquery duidt de verkochte documenten aan*/
call RegistratieSelectDocumentenPerLid(2);

USE dbool;
DROP PROCEDURE IF EXISTS `DocVerkocht`;
DELIMITER //
CREATE PROCEDURE `DocVerkocht`
(
	IN pDocId INT 
)
BEGIN
	 (select doc.docid from doc inner join (transactiedoc inner join transactie on transactie.transactieid = transactiedoc.transactieid) on doc.docid = transactiedoc.docid where doc.docid = pDocId and transactie.dealstatusid in (6,7));
END //
DELIMITER ;

call docverkocht(40);