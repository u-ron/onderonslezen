﻿-- An Orm Apart -- Saturday 8th of November 2014 11:00:37 AM
USE `dbool`;
DROP TABLE IF EXISTS `Lid`;
CREATE TABLE `lid` (
  `LidId` int(11) NOT NULL AUTO_INCREMENT,
  `GebruikersNaam` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Wachtwoord` varchar(255) CHARACTER SET utf8 NOT NULL,
  `LidNaam` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LidVoornaam` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LidInfo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Adres` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Telefoon` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `SkypeNaam` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `WoonId` int(11) DEFAULT NULL,
  `Gesloten` tinyint(1) NOT NULL,
  `AddedBy` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `InsertedOn` timestamp NULL DEFAULT NULL,
  `ModifiedBy` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ModifiedOn` timestamp NULL DEFAULT NULL,
  `LidStatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`LidId`),
  UNIQUE KEY `uc_Gebruikersnaam` (`GebruikersNaam`),
  UNIQUE KEY `Wachtwoord_UNIQUE` (`Wachtwoord`),
  KEY `WoonId` (`WoonId`),
  CONSTRAINT `lid_ibfk_1` FOREIGN KEY (`WoonId`) REFERENCES `woonplaats` (`WoonplaatsId`)
) 

/*BOOL type wordt in mySQL omgezet in TINYINT(1)*/