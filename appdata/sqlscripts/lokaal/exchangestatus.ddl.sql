﻿USE dbool; 
DROP TABLE IF EXISTS exchangestatus; 

CREATE TABLE exchangestatus ( 
	ExchangeStatusId INT NOT NULL AUTO_INCREMENT, 
	CONSTRAINT PRIMARY KEY(ExchangeStatusId), 
	ExchangeStatus VARCHAR (255) CHARACTER SET UTF8 NULL
    );  