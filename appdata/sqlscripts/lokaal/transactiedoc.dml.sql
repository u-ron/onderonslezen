﻿-- Created : Wednesday 12th of November 2014 03:40:58 PM
USE dbool;
DROP PROCEDURE IF EXISTS `TransactieDocInsert`;
DELIMITER //
CREATE PROCEDURE `TransactieDocInsert`
(
	OUT pTDId INT ,
	IN pTransactieId INT ,
	IN pDocId INT ,
	IN pAddedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `TransactieDoc`
	(
		`TransactieDoc`.`TransactieId`,
		`TransactieDoc`.`DocId`,
		`TransactieDoc`.`AddedBy`,
		`TransactieDoc`.`InsertedOn`
	)
	VALUES
	(
		pTransactieId,
		pDocId,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pTDId;
END //
DELIMITER ;

call transactiedocinsert(@pTDId, 1, 1, 'admin');

USE dbool;
DROP PROCEDURE IF EXISTS `TransactieDocUpdate`;
DELIMITER //
CREATE PROCEDURE `TransactieDocUpdate`
(
	IN pTDId INT ,
	IN pTransactieId INT ,
	IN pDocId INT ,
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `TransactieDoc`
SET
	`TransactieId` = pTransactieId,
	`DocId` = pDocId,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `TransactieDoc`.TDId = pTDId;
END //
DELIMITER ;

call transactiedocupdate(1, 1, 2, 'admin');

USE dbool;
DROP PROCEDURE IF EXISTS `TransactieDocSelectAll`;
DELIMITER //
CREATE PROCEDURE `TransactieDocSelectAll`()
BEGIN
	SELECT * FROM `TransactieDoc`
;
END //
DELIMITER ;

USE dbool;
DROP PROCEDURE IF EXISTS `TransactieDocDelete`;
DELIMITER //
CREATE PROCEDURE `TransactieDocDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `TransactieDoc`
WHERE `TransactieDoc`.TDId = pId;
END //
DELIMITER ;

USE dbool;
DROP PROCEDURE IF EXISTS `TransactieDocSelectById`;
DELIMITER //
CREATE PROCEDURE `TransactieDocSelectById`
(
	IN pTDId INT 
)
BEGIN
	SELECT * FROM `TransactieDoc` WHERE `TransactieDoc`.`TDId` = pTDId;
END //
DELIMITER ;

call transactiedocselectbyid(1);



USE dbool;
DROP PROCEDURE IF EXISTS TransactieDocDeleteByTransactieId;
DELIMITER //
CREATE PROCEDURE `TransactieDocDeleteByTransactieId`
(
	IN pTAId INT 
)
BEGIN
	delete FROM `TransactieDoc` WHERE `TransactieDoc`.`TransactieId` = pTAId;
END //
DELIMITER ;

/*Gebruikt?*/
/*
USE dbool;
DROP PROCEDURE IF EXISTS `TransactieDocSelectDocIDInTransactie`;
DELIMITER //
CREATE PROCEDURE `TransactieDocSelectDocIDInTransactie`()
BEGIN
	SELECT DocId FROM `TransactieDoc`
;
END //
DELIMITER ;
*/

USE dbool;
DROP PROCEDURE IF EXISTS `TransactieDocSelectDocIDFromTransactie`;
DELIMITER //
CREATE PROCEDURE `TransactieDocSelectDocIDFromTransactie`
(
    IN pTAId INT
)
BEGIN
	SELECT * FROM `TransactieDoc` where TransactieDoc.TransactieId = pTAId;
END //
DELIMITER ;

call TransactieDocSelectDocIDFromTransactie(4);


