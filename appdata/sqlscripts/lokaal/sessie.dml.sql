﻿insert into sessie values(1, 'abcd', 3, '1444014390', NULL, NULL);


USE dbool;
DROP PROCEDURE IF EXISTS `SessieUpdate`;
DELIMITER //
CREATE PROCEDURE `SessieUpdate`
(
	IN pId INT ,
	IN pSessionId VARCHAR (255) CHARACTER SET UTF8 ,
	IN pLidId INT,
	IN pLastActivity VARCHAR(30),
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `Sessie`
SET
	 SessionId = pSessionId,
	 LidId = pLidId,
	 LastActivity = pLastActivity,
	 ModifiedBy = pModifiedBy,
	 ModifiedOn = NOW()
WHERE Id = pId;
END //
DELIMITER ;

call SessieUpdate('1', 'poiu', 3, NOW(), 'admin');

USE dbool;
DROP PROCEDURE IF EXISTS `SessieSelectById`;
DELIMITER //
CREATE PROCEDURE `SessieSelectById`
(
	IN pId INT 
)
BEGIN
	SELECT * FROM Sessie WHERE Id = pId;
END //
DELIMITER ;


call sessieselectbyid(1);
