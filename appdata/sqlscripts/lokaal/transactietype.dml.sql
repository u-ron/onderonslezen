﻿USE dbool;
DROP PROCEDURE IF EXISTS `transactietypeSelectAll`;
DELIMITER //
CREATE PROCEDURE `transactietypeSelectAll`()
BEGIN
	SELECT * FROM `transactietype`
;
END //
DELIMITER ;

insert into transactietype(transactietypeid, transactietype) values('1', 'Deal');
insert into transactietype(transactietypeid, transactietype) values('2', 'Exchange');
