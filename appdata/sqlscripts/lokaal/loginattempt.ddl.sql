﻿USE `dbool`;
DROP TABLE IF EXISTS `LoginAttempt`;
CREATE TABLE `LoginAttempt` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`IdMember` INT NOT NULL,
	`Time` VARCHAR (30) NOT NULL,
	`InsertedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`InsertedOn` TIMESTAMP NULL,
	`ModifiedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`ModifiedOn` TIMESTAMP NULL,
	CONSTRAINT `loginattempt_ibfk_1` FOREIGN KEY (`IdMember`) REFERENCES `lid` (`LidId`) ON DELETE CASCADE);