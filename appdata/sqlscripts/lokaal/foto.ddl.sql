﻿-- An Orm Apart -- Friday 7th of November 2014 06:54:29 AM
USE `dbool`;
DROP TABLE IF EXISTS `Foto`;
CREATE TABLE `foto` (
  `FotoId` int(11) NOT NULL AUTO_INCREMENT,
  `FotoNaam` varchar(255) CHARACTER SET utf8 NOT NULL,
  `URL` varchar(255) CHARACTER SET utf8 NOT NULL,
  `DocId` int(11) NOT NULL,
  `AddedBy` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `InsertedOn` timestamp NULL DEFAULT NULL,
  `ModifiedBy` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ModifiedOn` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`FotoId`),
  KEY `DocId` (`DocId`),
  CONSTRAINT `foto_ibfk_1` FOREIGN KEY (`DocId`) REFERENCES `doc` (`DocId`)
);

