﻿-- An Orm Apart -- Tuesday 11th of November 2014 03:23:13 AM
-- 
USE `dbool`;
DROP TABLE IF EXISTS `Transactie`;
CREATE TABLE `Transactie` (
	`TransactieId` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(TransactieId),
	`TADatum` DATE NOT NULL,
	`OrderBedrag`  VARCHAR (255) CHARACTER SET UTF8 NULL,
	`TransportKost`  VARCHAR (255) CHARACTER SET UTF8 NULL,
	`DueDate` DATE NULL,
	`DatumUit` DATE NULL,
	`DatumTerug` DATE NULL,
	`TransactieTypeId` INT NOT NULL,
	`DealStatusId` INT NULL,
	`ExchangeStatusId` INT NULL,
	`AddedBy` VARCHAR (255) CHARACTER SET UTF8 NULL,
	`InsertedOn` TIMESTAMP NULL,
	`ModifiedBy` VARCHAR (255) CHARACTER SET UTF8 NULL,
	`ModifiedOn` TIMESTAMP NULL,
	CONSTRAINT FOREIGN KEY (`TransactieTypeId`) REFERENCES `TransactieType` (`TransactieTypeId`),
	CONSTRAINT FOREIGN KEY (`DealStatusId`) REFERENCES `DealStatus` (`DealStatusId`),
	CONSTRAINT FOREIGN KEY (`ExchangeStatusId`) REFERENCES `ExchangeStatus` (`ExchangeStatusId`));

