﻿-- Created : Wednesday 12th of November 2014 05:27:00 AM
USE dbool;
DROP PROCEDURE IF EXISTS `TransactiePartnerInsert`;
DELIMITER //
CREATE PROCEDURE `TransactiePartnerInsert`
(
	OUT pTPId INT ,
	IN pTransactieId INT ,
	IN pLidId INT ,
	IN pRolId INT ,
    IN pDelStatus TINYINT,
	IN pAddedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `TransactiePartner`
	(
		`TransactiePartner`.`TransactieId`,
		`TransactiePartner`.`LidId`,
		`TransactiePartner`.`RolId`,
        `TransactiePartner`.`DelStatus`,
		`TransactiePartner`.`AddedBy`,
		`TransactiePartner`.`InsertedOn`
	)
	VALUES
	(
		pTransactieId,
		pLidId,
		pRolId,
        pDelStatus,
		pAddedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pTPId;
END //
DELIMITER ;

call transactiepartnerinsert(@pTPId, 1, 1, 1, 0, 'admin');

USE dbool;
DROP PROCEDURE IF EXISTS `TransactiePartnerUpdate`;
DELIMITER //
CREATE PROCEDURE `TransactiePartnerUpdate`
(
	IN pTPId INT ,
	IN pTransactieId INT ,
	IN pLidId INT ,
	IN pRolId INT ,
	IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `TransactiePartner`
SET
	`TransactieId` = pTransactieId,
	`LidId` = pLidId,
	`RolId` = pRolId,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `TransactiePartner`.TPId = pTPId;
END //
DELIMITER ;

call transactiepartnerupdate(1, 1, 1, 2, 'admin');

USE dbool;
DROP PROCEDURE IF EXISTS `TransactiePartnerSelectAll`;
DELIMITER //
CREATE PROCEDURE `TransactiePartnerSelectAll`()
BEGIN
	SELECT * FROM `TransactiePartner` where delstatus <> 1;
END //
DELIMITER ;

USE dbool;
DROP PROCEDURE IF EXISTS `TransactiePartnerSelectById`;
DELIMITER //
CREATE PROCEDURE `TransactiePartnerSelectById`
(
	IN pTPId INT 
)
BEGIN
	SELECT * FROM `TransactiePartner` WHERE `TransactiePartner`.`TPId` = pTPId;
END //
DELIMITER ;


USE dbool;
DROP PROCEDURE IF EXISTS `TransactiePartnerDelete`;
DELIMITER //
CREATE PROCEDURE `TransactiePartnerDelete`
(
	IN pTPId INT,
    IN pModifiedBy VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `TransactiePartner`
SET
	`DelStatus` = 1,
	`ModifiedBy` = pModifiedBy,
	`ModifiedOn` = NOW()
WHERE `TransactiePartner`.TPId = pTPId;
END //
DELIMITER ;

USE dbool;
DROP PROCEDURE IF EXISTS `TransactiePartnerGetKoper`;
DELIMITER //
CREATE PROCEDURE `TransactiePartnerGetKoper`
(
	IN pTransactieId INT 
)
BEGIN
	SELECT LidId FROM `TransactiePartner` WHERE `TransactiePartner`.`TransactieId` = pTransactieId and TransactiePartner.RolId = 3;
END //
DELIMITER ;

call TransactiePartnerGetKoper(11)






