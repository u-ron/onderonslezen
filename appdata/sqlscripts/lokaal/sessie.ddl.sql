﻿USE `dbool`;
DROP TABLE IF EXISTS `Sessie`;
CREATE TABLE `sessie` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `SessionId` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LidId` int(11) DEFAULT NULL,
  `LastActivity` varchar(30) DEFAULT NULL,
  `ModifiedBy` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ModifiedOn` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`)
) 
