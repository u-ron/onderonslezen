﻿USE `dbool`;
DROP TABLE IF EXISTS `Registratie`;
CREATE TABLE `registratie` (
  `RegId` int(11) NOT NULL AUTO_INCREMENT,
  `LidId` int(11) NOT NULL,
  `DocId` int(11) NOT NULL,
  `AddedBy` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `InsertedOn` timestamp NULL DEFAULT NULL,
  `ModifiedBy` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ModifiedOn` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`RegId`),
  KEY `registratie_ibfk_1` (`DocId`),
  KEY `LidId` (`LidId`),
  CONSTRAINT `registratie_ibfk_2` FOREIGN KEY (`LidId`) REFERENCES `lid` (`LidId`) ON DELETE CASCADE,
  CONSTRAINT `registratie_ibfk_1` FOREIGN KEY (`DocId`) REFERENCES `doc` (`DocId`) ON DELETE CASCADE
);

