<?php
include('appcode/helpers/feedback.class.php');    
include('appcode/helpers/base.class.php');
include('appcode/webapp/model/lid.class.php');
include('appcode/webapp/model/doc.class.php');
include('appcode/webapp/model/transactie.class.php');
include('appcode/webapp/help/time.php');
include_once('appcode/webapp/help/analyticstracking.php');

session_start();
//terug op 0 zetten van de lidstatus sessie om een lid na uitloggen als bezoeker toe te laten
//$_SESSION['lidstatus'] = 0;//levert probleem bij wildvreemde toegang

$gebruikersNaam = isset($_SESSION['gebruikersnaam'])? $_SESSION['gebruikersnaam'] : "";
$wachtwoord = isset($_SESSION['wachtwoord'])? $_SESSION['wachtwoord'] : "";
$foutMeldingGebruikersNaam = isset($_SESSION['foutmeldinggebruikersnaam'])? $_SESSION['foutmeldinggebruikersnaam'] : "";
$foutMeldingWachtwoord = isset($_SESSION['foutmeldingwachtwoord'])? $_SESSION['foutmeldingwachtwoord'] : "";
$melding3 = "";

if(isset($_SESSION['inschrijvinggelukt']))
{
    $melding3 = $_SESSION['inschrijvinggelukt'];
}

if(isset($_SESSION['melding']))
{
    $melding3 = $_SESSION['melding'];
}

unset($_SESSION['gebruikersnaam']);
unset($_SESSION['wachtwoord']);
unset($_SESSION['foutmeldinggebruikersnaam']);
unset($_SESSION['foutmeldingwachtwoord']);
unset($_SESSION['inschrijvinggelukt']);

if (isset($_POST['submit']))
{
	$gebruikersNaam = $_POST['gebruikersnaam'];
	$wachtWoord = $_POST['wachtwoord'];
    $lidObject = new Lid();
}

$lidObject = new Lid();
$alleLeden = $lidObject->selectAll();

$docObject = new Doc();
$availableDocs = $docObject->selectAllAvailable();

$transactieObject = new Transactie();
$result = $transactieObject->countTransactiesBezig();
$aantal = $result[0]['aantal'];
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <title>Eerste pagina</title>
        <link rel="stylesheet" href="appcode/webapp/view/css/files.css">
        <link rel="stylesheet" href="appcode/webapp/view/css/index.css">
        <script type="text/javascript" src="appcode/webapp/view/jquery/jquery-1.11.2.js"></script>
        <link href="appcode/webapp/view/jquery/plugins/ui/jquery-ui-1.11.2.custom/jquery-ui.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="appcode/webapp/view/jquery/plugins/ui/jquery-ui-1.11.2.custom/jquery-ui.js"></script>
        <script>
            $(document).ready(function () {
                $("button").button(
                {
                    icons: { primary: " ui-icon-arrowthickstop-1-e"}
                });
            });
        </script>
    </head>
    <body>
        <div class="containerIndex">
        
        <div class="tile _1x2x2"><img src="appcode/images/logo_ool.png" alt="logo"></div>
        
        <div class="tile _1x2x2">
		<form id="frmLogin" method="POST" action="appcode/webapp/control/login.control.php" class="form-horizontal">
            <div class="control-group">
                <label for="gebruikersnaam" class="control-label">Gebruikersnaam</label>
                <div class="controls">
                     <input type="text" name="gebruikersnaam" id="gebruikersnaam" placeholder="Gebruikersnaam" value="<?php echo $gebruikersNaam; ?>" required>
                </div>
                <p id="message1" class="message"><?php echo $foutMeldingGebruikersNaam; ?></p>
            </div>
           
			<div class="control-group">
                <label for="wachtwoord" class="control-label">Wachtwoord</label>
				<div class="controls">
						<input type="password" name="wachtwoord" id="wachtwoord" placeholder="Wachtwoord" value="<?php echo $wachtwoord; ?>" required>
				</div>
                <p id="message2" class="message"><?php echo $foutMeldingWachtwoord; ?></p>
            </div>		
            
			<div class="control-group">
                <div class="controls">
					    <button id="login" name="submit" type="submit">&nbsp;Aanmelden</button>
                </div>
            </div>
             <p id="message3" class="message"><?php echo $melding3; ?></p>	
		</form>
        </div>
        
        <div id="tekst" class="tile _1x2x2">
        <p>Welkom op de website van de community Onder Ons Lezen.</p>
        <p>Onder Ons Lezen is een vereniging om tweedehandsdocumenten uit te wisselen en /of te verkopen.<br>
         Boeken, tijdschriften, strips, cursussen enz. kunnen aangeprezen worden met een veelheid aan informatie en foto's.</p>
        <p>Het lidmaatschap is gratis.</p>
		<p>Ik wens de website als bezoeker te bekijken.&nbsp;&nbsp;<a href="appcode/webapp/view/welkom.php">Bezoek</a></p>
        <p>Ik ben geïnteresseerd om lid te worden van de community Onder Ons Lezen.&nbsp;&nbsp;<a href="appcode/webapp/view/inschrijving.php">Inschrijving</a></p>
        </div>

        <div id="statistiek" class="tile _1x2x2">
        <p>Vandaag, <?php echo makelocalTime(date("Y-m-d H:i:s"));?>, zijn er</p>
        <p><b><?php echo count($alleLeden);?></b> leden, </p>
        <p><b><?php echo count($availableDocs);?></b> documenten beschikbaar</p>
        <p>en <b><?php echo $aantal." "?></b> transacties aan de gang.</p>
        </div>   
             </div>		
    </body>
</html>
